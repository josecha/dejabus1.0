<?
class modificarDatos{
	//PROPIEDADES
	public $datos = array();
	public $tipoTabla = "";
	public $tabla = "";
	public $id = 0;
	public $campoID = "id";
	public $datosMod = array();
			
	
	//CONSTRUCTORES
	public function __construct ($tabla, $id, $mod=0){
		$this->tipoTabla = $tabla;
		$this->id = trim($id);
		
		//asignamos nombre de tabla correcto
		switch ($this->tipoTabla){
			case 'admin': $this->tabla = 'users'; break;
			case 'gps': $this->tabla = 'GPs'; break;
			case 'userLow': $this->tabla = 'users'; break;
			case 'viajes': $this->tabla = 'viajes'; break;
			case 'ruta': $this->tabla = 'RutaControl'; break; 
			case 'areasControl': $this->tabla = 'geoControlT3'; break; 
		}
		
		//la tabla viajes tiene un id llamado idviaje, por eso esto
		if ($tabla == "viajes"){
			$this->campoID = "idviaje";
		}
		
		//si no esta modificando datos
		if ($mod==0){
			$this->getDatos();
		}
	}
	
	
	//M�TODOS	
	//saca los datos del registro especificado en la tabla especificada
	public function getDatos (){
		global $rs, $conn;
		
		if ($this->tabla == "geoControlT3"){
			$rs->Open("
				SELECT 
					CONVERT(CHAR(19),$this->tabla.fechaInicio,120) AS fechaInicioDT,
					CONVERT(CHAR(19),$this->tabla.fechafin,120) AS fechafinDT,
					$this->tabla.gps,  
					GPs.descripcion as gpsDescripcion,  
					$this->tabla.geocontrol,  
					$this->tabla.tipo, 
					$this->tabla.id
				FROM $this->tabla INNER JOIN GPs ON (GPs.id = $this->tabla.gps) 
				WHERE $this->tabla.$this->campoID = $this->id", $conn); 
		}else{
			$rs->Open("SELECT * FROM $this->tabla WHERE $this->campoID = $this->id", $conn); 
		}
		
		$this->datos = fetch_assoc($rs);
		$rs->Close();
	}
	
		
	//imprime los datos
	function printDatos(){
		switch($this->tipoTabla){
			case "admin":
				echo trim(trim($this->datos[0]["nombre"]).'@dt@'.trim($this->datos[0]["login"]).'@dt@'.trim($this->datos[0]["pwd"]).'@dt@'.trim($this->datos[0]["datos"]).'@dt@'.trim($this->datos[0]["telefono"]).'@dt@'.trim($this->datos[0]["mail1"]).'@dt@'.trim($this->datos[0]["mail2"]).'@dt@'.trim($this->datos[0]["mail3"]).'@dt@'.trim($this->datos[0]["cel1"]).'@dt@'.trim($this->datos[0]["cel2"]).'@dt@'.trim($this->datos[0]["id"]));
				break;
			case "gps":
				global $rs, $conn;
				
				//creamos opciones para eleguir ususario
				$rs->Open("SELECT id, nombre FROM users WHERE tipo = 1 ORDER BY nombre ASC", $conn); 
				$datosUser = fetch_assoc($rs);
				$rs->Close();
				
				$optionsUsr = '';
				for ($a=0; $a<count($datosUser); $a++){
					$optionsUsr .= '<option value="'.$datosUser[$a]['id'].'" '.(($datosUser[$a]['id']==$this->datos[0]["usr"])?'selected="selected"':'').' >'.$datosUser[$a]['nombre'].'</option>'; 
				}
								
				echo trim(trim($optionsUsr).'@dt@'.trim($this->datos[0]["id"]).'@dt@'.trim(str_replace("'", "", $this->datos[0]["descripcion"])));				
				break;
			case "userLow":
				echo trim(trim($this->datos[0]["nombre"]).'@dt@'.trim($this->datos[0]["login"]).'@dt@'.trim($this->datos[0]["pwd"]).'@dt@'.trim($this->datos[0]["datos"]).'@dt@'.trim($this->datos[0]["telefono"]).'@dt@'.trim($this->datos[0]["mail1"]).'@dt@'.trim($this->datos[0]["mail2"]).'@dt@'.trim($this->datos[0]["mail3"]).'@dt@'.trim($this->datos[0]["cel1"]).'@dt@'.trim($this->datos[0]["cel2"]).'@dt@'.trim($this->datos[0]["id"]));
				break;
			case "viajes":
				global $u;
				
				echo trim(trim($this->datos[0]["origen"]).'@dt@'.trim($this->datos[0]["destino"]).'@dt@'.trim($this->datos[0]["mercancia"]).'@dt@'.trim($this->datos[0]["vel"]).'@dt@'.trim($this->datos[0]["tiempomuerto"]).'@dt@'.trim($this->datos[0]["idviaje"]).'@dt@'.trim(str_replace('value="'.$this->datos[0]["ruta"].'"','value="'.$this->datos[0]["ruta"].'" selected="selected" ',str_replace('selected="selected"','',$u->rutaSelectForViajeForm()))));				
				break;
			case "ruta":
				echo trim(trim($this->datos[0]["nombre"]).'@dt@'.trim($this->datos[0]["descripcion"]).'@dt@'.trim($this->datos[0]["id"]));
				break;
			case "areasControl":
				global $u;
				
				//fecha Inicial
				$fechaI = date_parse(trim($this->datos[0]["fechaInicioDT"]));
				$HI = twoDigit($fechaI['hour']);
				$MI = twoDigit($fechaI['minute']);
				$fechaI = twoDigit($fechaI['day']).'/'.twoDigit($fechaI['month']).'/'.$fechaI['year'];
				
				//fecha Final
				$fechaF = date_parse(trim($this->datos[0]["fechafinDT"]));
				$HF = twoDigit($fechaF['hour']);
				$MF = twoDigit($fechaF['minute']);
				$fechaF = twoDigit($fechaF['day']).'/'.twoDigit($fechaF['month']).'/'.$fechaF['year'];
				
				//obtenemos el gpsList
				$gpsList = $u->gpsLibresAreasControl().'<div><input id="gps_x" name="gps_x" type="checkbox" checked="checked" value="'.trim($this->datos[0]["gps"]).'" class="gpsDivList"/><label>'.$this->datos[0]["gpsDescripcion"].'</label></div>';
				
				//obtenemos el goecontrolList
				$geocontrolList = str_replace('value="'.$this->datos[0]["geocontrol"].'"','value="'.$this->datos[0]["geocontrol"].'" selected="selected"',$u->geoareaSelect());
				
				echo trim($fechaI.'@dt@'.$HI.'@dt@'.$MI.'@dt@'.$fechaF.'@dt@'.$HF.'@dt@'.$MF.'@dt@'.trim($geocontrolList).'@dt@'.trim($gpsList).'@dt@'.((trim($this->datos[0]["tipo"]))?'1':'0').'@dt@'.trim($this->datos[0]["id"]));			
				break;
		}
	}

	//recivimos datos enviados para modificarlos
	public function recivirDatos (){
		$this->datosMod = array();
		switch($this->tipoTabla){
			case "admin":
				if (isset($_POST["nombre"]) and strlen($_POST["nombre"])>0){ 	
					$this->datosMod["nombre"] = "'".addslashes(trim($_POST["nombre"]))."'";	
				}
				if (isset($_POST["login"]) and strlen($_POST["login"])>0){ 	
					$this->datosMod["login"] = "'".addslashes(trim($_POST["login"]))."'";	
				}
				if (isset($_POST["pass"]) and strlen($_POST["pass"])>0){ 	
					$this->datosMod["pwd"] = "'".addslashes(trim($_POST["pass"]))."'";	
				}
				if (isset($_POST["datos"]) and strlen($_POST["datos"])>0){ 	
					$this->datosMod["datos"] = "'".addslashes(trim($_POST["datos"]))."'";	
				}
				if (isset($_POST["tel"]) and strlen($_POST["tel"])>0){ 		
					$this->datosMod["telefono"] = "'".addslashes(trim($_POST["tel"]))."'";	
				}
				if (isset($_POST["email"]) and strlen($_POST["email"])>0){ 	
					$this->datosMod["mail1"] = "'".addslashes(trim($_POST["email"]))."'";	
				}
				if (isset($_POST["email2"]) and strlen($_POST["email2"])>0){ 	
					$this->datosMod["mail2"] = "'".addslashes(trim($_POST["email2"]))."'";	
				}
				if (isset($_POST["email3"]) and strlen($_POST["email3"])>0){ 	
					$this->datosMod["mail3"] = "'".addslashes(trim($_POST["email3"]))."'";	
				}
				if (isset($_POST["cel"]) and strlen($_POST["cel"])>0){ 		
					$this->datosMod["cel1"] = "'".addslashes(trim($_POST["cel"]))."'";	
				}
				if (isset($_POST["cel2"]) and strlen($_POST["cel2"])>0){ 	
					$this->datosMod["cel2"] = "'".addslashes(trim($_POST["cel2"]))."'";	
				}
				$this->datosMod["md5"] = "'".md5(trim($_POST["pass"]))."'";
				break;	
			case "gps":				
				if (isset($_POST["usr"]) and strlen($_POST["usr"])>0){ 		
					$this->datosMod["usr"] = trim((int)$_POST["usr"]);	
				}
				if (isset($_POST["descripcion"]) and strlen($_POST["descripcion"])>0){ 		
					$this->datosMod["descripcion"] = "'".addslashes(trim($_POST["descripcion"]))."'";	
				}
				break;	
			case "userLow":
				if (isset($_POST["nombre"]) and strlen($_POST["nombre"])>0){ 	
					$this->datosMod["nombre"] = "'".addslashes(trim($_POST["nombre"]))."'";	
				}
				if (isset($_POST["login"]) and strlen($_POST["login"])>0){ 	
					$this->datosMod["login"] = "'".addslashes(trim($_POST["login"]))."'";	
				}
				if (isset($_POST["pass"]) and strlen($_POST["pass"])>0){ 	
					$this->datosMod["pwd"] = "'".addslashes(trim($_POST["pass"]))."'";	
				}
				if (isset($_POST["datos"]) and strlen($_POST["datos"])>0){ 	
					$this->datosMod["datos"] = "'".addslashes(trim($_POST["datos"]))."'";	
				}
				if (isset($_POST["tel"]) and strlen($_POST["tel"])>0){ 		
					$this->datosMod["telefono"] = "'".addslashes(trim($_POST["tel"]))."'";	
				}
				if (isset($_POST["email"]) and strlen($_POST["email"])>0){ 	
					$this->datosMod["mail1"] = "'".addslashes(trim($_POST["email"]))."'";	
				}
				if (isset($_POST["email2"]) and strlen($_POST["email2"])>0){ 	
					$this->datosMod["mail2"] = "'".addslashes(trim($_POST["email2"]))."'";	
				}
				if (isset($_POST["email3"]) and strlen($_POST["email3"])>0){ 	
					$this->datosMod["mail3"] = "'".addslashes(trim($_POST["email3"]))."'";	
				}
				if (isset($_POST["cel"]) and strlen($_POST["cel"])>0){ 		
					$this->datosMod["cel1"] = "'".addslashes(trim($_POST["cel"]))."'";	
				}
				if (isset($_POST["cel2"]) and strlen($_POST["cel2"])>0){ 	
					$this->datosMod["cel2"] = "'".addslashes(trim($_POST["cel2"]))."'";	
				}
				$this->datosMod["md5"] = "'".md5(trim($_POST["pass"]))."'";
				break;	
			case "viajes":				
				if (isset($_POST["ruta"]) and strlen($_POST["ruta"])>0){ 	
					$this->datosMod["ruta"] = (int)addslashes(trim($_POST["ruta"]));	
				}
				if (isset($_POST["origen"]) and strlen($_POST["origen"])>0){ 	
					$this->datosMod["origen"] = "'".addslashes(trim($_POST["origen"]))."'";	
				}
				if (isset($_POST["destino"]) and strlen($_POST["destino"])>0){ 	
					$this->datosMod["destino"] = "'".addslashes(trim($_POST["destino"]))."'";	
				}
				if (isset($_POST["mercancia"]) and strlen($_POST["mercancia"])>0){ 	
					$this->datosMod["mercancia"] = "'".addslashes(trim($_POST["mercancia"]))."'";	
				}
				if (isset($_POST["vel"]) and strlen($_POST["vel"])>0){ 	
					$this->datosMod["vel"] = addslashes(trim((int)$_POST["vel"]));	
				}
				if (isset($_POST["tiempomuerto"]) and strlen($_POST["tiempomuerto"])>0){ 	
					$this->datosMod["tiempomuerto"] = addslashes(trim((int)$_POST["tiempomuerto"]));	
				}				
				break;		
			case "ruta":
				if (isset($_POST["nombre"]) and strlen($_POST["nombre"])>0){ 	
					$this->datosMod["nombre"] = "'".addslashes(trim($_POST["nombre"]))."'";	
				}
				if (isset($_POST["descripcion"]) and strlen($_POST["descripcion"])>0){ 	
					$this->datosMod["descripcion"] = "'".addslashes(trim($_POST["descripcion"]))."'";	
				}
				break;	
			case "areasControl":	
				if (isset($_POST["geocontrol"]) and strlen($_POST["geocontrol"])>0){ 			
					$this->datosMod["geocontrol"] = addslashes(trim((int)$_POST["geocontrol"]));	
				}
				if (isset($_POST["tipo"]) and strlen($_POST["tipo"])>0){ 			
					$this->datosMod["tipo"] = addslashes(trim((int)$_POST["tipo"]));					
				}
				if (isset($_POST["date1"]) and strlen($_POST["date1"])>0){ 	
					$this->datosMod["fechaInicio"] = "'".addslashes(trim(substr($_POST["date1"],6,4).'-'.substr($_POST["date1"],3,2).'-'.substr($_POST["date1"],0,2).'T'.$_POST["selectHI"].':'.$_POST["selectMI"].':00'))."'";
				}
				if (isset($_POST["date2"]) and strlen($_POST["date2"])>0){ 	
					$this->datosMod["fechafin"] = "'".addslashes(trim(substr($_POST["date2"],6,4).'-'.substr($_POST["date2"],3,2).'-'.substr($_POST["date2"],0,2).'T'.$_POST["selectHF"].':'.$_POST["selectMF"].':00'))."'";
				}
								
				$this->datosMod["activo"] = 1;				
				break;		
		}
	}
	
	
	//valida los datos en comparacion a la BD dependiendo del tipo de form
	function validarDatos(){
		global $rs, $conn;
		
		switch($this->tipoTabla){
			case "admin":
				$rs->Open("SELECT $this->campoID FROM $this->tabla WHERE $this->campoID <> $this->id AND login=".$this->datosMod["login"], $conn);  // AND ID <> THISid
				$datos=fetch_assoc($rs);
				$rs->Close();
				
				if (count($datos) == 0){
					return true;
				}else{
					return false;
				}
				
				break;
			case "gps":
				return true;
				break;
			case "userLow":
				$rs->Open("SELECT $this->campoID FROM $this->tabla WHERE $this->campoID <> $this->id AND login=".$this->datosMod["login"], $conn);  // AND ID <> THISid
				$datos=fetch_assoc($rs);
				$rs->Close();
				
				if (count($datos) == 0){
					return true;
				}else{
					return false;
				}
				
				break;
			case "viajes":
				return true;
				break;
			case "areasControl":
				if (!isset($_POST["geocontrol"]{0})){
					return false;
				}
				
				foreach($_POST as $key => $value){					
					if (substr(trim($key), 0, 3) == "gps"){
						return true;
						break;
					}
				}
				
				return false;				
				break;
			case "ruta":
				global $u;
				$rs->Open("SELECT $this->campoID FROM $this->tabla WHERE $this->campoID <> $this->id AND owner = $u->id_user AND nombre = ".$this->datosMod["nombre"], $conn);  // AND ID <> THISid
				$datos=fetch_assoc($rs);
				$rs->Close();
				
				if (count($datos) == 0){
					return true;
				}else{
					return false;
				}
				
				break;
		}
	}
	
	
	//crea la sentencia sql dados los datos
	public function sentenciaSQL(){
		//creamos sentencia sql
		$sql = "";
		$lenDatos = count($this->datosMod);
		$a = 0;
		
		foreach($this->datosMod as $key => $valor){
			$coma = (($a == $lenDatos-1)?'':', ');
			$sql .= $key."=".$valor.$coma;
			++$a;
		}
		
		return $sql;
	}
	
	
	//modifica los datos de un registro
	public function modDatos(){
		$this->recivirDatos();
		if ($this->validarDatos()){		
			global $rc, $conn;
			$sql = $this->sentenciaSQL(); // creamos sentencia de campos y valores
			
			if ($this->tipoTabla == "areasControl"){
				$llegoActualGps = false;
				$newGpss = array();
				
				foreach($_POST as $key => $value){
					$key = trim($key);
					
					if ($key == "gps_x"){
						$llegoActualGps = true;
					}elseif (substr($key, 0, 3) == "gps"){
						//gps recibido
 						array_push($newGpss, $value);
					}
				}
				
				if ($llegoActualGps){
					//actualizamos el gps que ya habia
					$rc->CommandText = "UPDATE $this->tabla SET $sql WHERE $this->campoID = $this->id"; 
				}else{
					//desactivamos el gps que ya habia
					$rc->CommandText = "UPDATE $this->tabla SET activo = 0 WHERE $this->campoID = $this->id"; 
				}
				
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
				
				//checamos si hay nuevos gps para agregar
				for ($i = 0; $i < count($newGpss); $i++){
					//insertamos nuevas areas de control para los nuevos gps's seleccionados
					$rc->CommandText = "INSERT INTO $this->tabla (gps,geocontrol,fechaInicio,activo,fechafin,tipo) 
										VALUES (".$newGpss[$i].",".$this->datosMod["geocontrol"].",".$this->datosMod["fechaInicio"].",1,".$this->datosMod["fechafin"].",".$this->datosMod["tipo"].")"; 
					$rc->CommandType = 1;
					$rc->ActiveConnection = $conn;
					$rc->Execute;
				}
			}else{
				//actualizamos datos
				$rc->CommandText = "UPDATE $this->tabla SET $sql WHERE $this->campoID = $this->id"; 
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
			}
		}
	}
		
}//end class
?>