<?php
include_once("conexion.php");
	include_once("funciones.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript"
    src="http://maps.google.com/maps/api/js?v=3.2&sensor=false">
</script>
<style>
table {border-collapse:collapse;}
table td{border-collapse:collapse;text-align:center;}
table tr:nth-child(even){background:#E6E6E6;}
tr#h td{color:White;background-color:#006699;font-weight:bold;}
</style>
<script type='text/javascript'>
var lats=[];
var lngs=[];
var n;
function getAddress(lats,lngs,campo)
{
n=campo;
if(campo<lats.length)
{
var latlng=new google.maps.LatLng(lats[campo],lngs[campo]);
var geocoder=new google.maps.Geocoder();
var td=document.getElementById("dir"+campo);
geocoder.geocode({'latLng': latlng}, function(results, status) {
 if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
		td.innerHTML=results[0].formatted_address;
	} else {
		td.innerHTML='No Disponible';
        }
	} else {
       // alert('Geocoder failed due to:' + status);
		td.innerHTML='No Disponible';
      }});
n+=1;
setTimeout('getAddress(lats,lngs,n)',1200);
}
}
</script>
</head>

<?php
if(isset($_GET['d']) && strlen($_GET['d'])>0)
{

	$serieGps = (int)myDencr(trim($_GET['d']));
	$it = (int)trim($_GET["it"]);
	list($di,$mi,$ai)=explode("/",$_GET['fi']);
	list($df,$mf,$af)=explode("/",$_GET['ff']);
	$fechaI=new DateTime(date("Y-m-d H:i:s",mktime($_GET['hi'],$_GET['mi'],0,$mi,$di,$ai)));
	$fechaF=new DateTime(date("Y-m-d H:i:s",mktime($_GET['hf'],$_GET['mf'],0,$mf,$df,$af)));

	//$fechaI->modify("+".horarioVerano()." hours");
	//$fechaF->modify("+".horarioVerano()." hours");
	$delete=array();
	$control=0;
	$query_datos="
	SELECT * FROM datosghe
		WHERE
			gps=".$serieGps."
		AND
			fecharecv BETWEEN '".$fechaI->format("Y-m-d\TH:i:s")."' AND '".$fechaF->format("Y-m-d\TH:i:s")."' ORDER BY fechasend ASC";
	$qD=new COM("ADODB.Recordset");
	$qD->open($query_datos,$conn);
	$losDatos=fetch_assoc($qD);
	$qD->close();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------

#	$fechaC=new DateTime($fechaI);
#	$fechaC->modify("-".$it." minutes");

	$script="<script type='text/javascript'>";
		for($i=0;$i<count($losDatos);$i++)
		{

	$fechaA=new DateTime($losDatos[$control]['fechasend']);
	$fechaS=new DateTime($losDatos[$i]['fechasend']);
	$minutosA=hToM($fechaA->format("H:i"));
	$minutosS=hToM($fechaS->format("H:i"));
	$fechaS->modify("-".horarioVerano()." hours");
	if($i==0)
	{
		$script.='lats.push('.$losDatos[$i]['lat'].');';
		$script.='lngs.push('.$losDatos[$i]['lon'].');';
	}
	else
	{
	if(abs($minutosS-$minutosA)>$it)
	{
		$script.='lats.push('.$losDatos[$i]['lat'].');';
		$script.='lngs.push('.$losDatos[$i]['lon'].');';
		$control=$i-1;
	}else
	{
	array_push($delete,$i);
	}
	}
	}

for($i=0;$i<count($delete);$i++)
{
	unset($losDatos[$delete[$i]]);//elmino los datos segun las posiciones en el vector $delete
}
$losDatos=array_values($losDatos);
$script.="</script>";
echo $script;

}
?>
<body onload='getAddress(lats,lngs,0);'>
<table>
<tr id="h">
<td>Fecha/Hora</td>
<td>Latitud</td>
<td>Longitud</td>
<td>Velocidad</td>
<td>Direcci&oacute;n</td>
</tr>
<?php
for($i=0;$i<count($losDatos);$i++)
{
$fecha=new DateTime($losDatos[$i]['fechasend']);
$fecha->modify("-".horarioVerano()." hours");
$tabla.="<tr><td>".$fecha->format("Y-m-d H:i")."</td>";
$tabla.="<td>".$losDatos[$i]['lat']."</td>";
$tabla.="<td>".$losDatos[$i]['lon']."</td>";
$tabla.="<td>".$losDatos[$i]['velocidad']."</td>";
$tabla.="<td style='text-align:left;' id=dir".$i."></td></tr>";
}
echo $tabla;
?>
</table>
</body>
</html>


<?php
function hToM($h)
{
$t=explode(":",$h);
$h=$t[0];
if (isset($t[1])) {
                $m = $t[1];
            } else {
                $m = 0;
            }
$tm=($h*60)+$m;
return $tm;
}

function horarioVerano() {
    date_default_timezone_set('UTC');
    $year = date("Y");
    $ahora =strtotime(date("d-m-Y"));
    $inicio_invierno = strtotime("last Sunday April $year");
    $fin_invierno = strtotime("last Sunday November $year");
    if ($ahora > $inicio_invierno && $ahora <= $fin_invierno) $num = 5;
    else $num = 6;
    return $num;
}
?>