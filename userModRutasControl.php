<?
ob_start();
include("conexion.php");
$rs = New COM("ADODB.Recordset");
$rc = New COM("ADODB.Command");

include("funciones.php");
include("head.php");
$u = new User();
if ($u->isLogued){
	if (isset($_GET["a"]) and strlen(trim($_GET["a"]))>0){
		//sacamos ruta de control actual
		$idRuta = (int)trim($_GET["a"]);
?> 
		<title>Montecristo Data Mining - Tracking Tampering Technology - Modificar Rutas de Control</title>
		<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
		<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/jquerySortable.js"></script>
		<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/interface.js"></script>
		<script type="text/javascript" language="javascript" charset="iso-8859-1">
			var browser = "<?=browser(); ?>";
			var tipo_user = '<?=$u->tipo_user; ?>';
			var thisUrlAll = encodeURIComponent('<?=thisUrlAll();?>');
			var gpsSelect = '<?=$u->gpsSelect(); ?>';		
			var gpsSelectInUse = '<?=$u->gpsSelectInUse(); ?>';		
			var gpsSelectClosed = '<?=$u->gpsSelectClosed(); ?>';		
			var listGeocercas = '<?=$u->printIconList(); ?>';
			var rutasSelect = '<?=$u->rutaSelectForFindForm(); ?>';
			var rutaSelectForViaje = '<?=$u->rutaSelectForViajeForm(); ?>';		
			var rutaSelectForFindViaje = '<?=$u->rutaSelectForFindViaje(); ?>';	
			var rutaSelectForFindHistorial = '<?=$u->rutaSelectForFindHistorial(); ?>';		
			var gpsSelectForReporte = '<?=$u->selectGpsForReporte(); ?>';		
			var gpsLibresAreasControl = '<?=$u->gpsLibresAreasControl(); ?>';	
			var gpsAreasControl = '<?=$u->gpsAreasControl(); ?>';
			var geoareaSelect = '<?=$u->geoareaSelect(); ?>';
			
			//codigo de aqu�
			var a;
			var idRutaOld = <?=$idRuta; ?>;
			var barra2 = new Array(<?=$u->geoArray2InJSForModRutaControl($idRuta); ?>);  
			var barra1 = new Array(<?=$u->geoArrayInJSForRutaControl(); ?>);			
		</script>
		<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/modRutasControl.js"></script>
	  </head>
	  <body class="bodyAdmin">
		<div id="fondoAdminAll">
		  <div id="fondoAdmin">
			<div id="headAdmin">
				<div id="headAdminLeft"></div>
				<div id="headAdmincenter"></div>
				<div id="headAdminright">
					<div id="opt_buscar" onClick="showFind();"></div>
					<div id="opt_agregar" onClick="showAdd();"></div>
				</div>
				<div class="corte"></div>
			</div>
			<div id="bodyAdmin">
				<div id="bodyAdminLeft">
					<div class="MenuHover"><div id="M_inicio" onClick="location.href = 'index.php';"></div></div>
					<div class="MenuHover"><div id="M_sesion" onClick="location.href = 'login.php';"></div></div>
					<?=str_replace('MenuHover"><div id="M_rutascontrol', 'MenuSelected"><div id="M_rutascontrol', $u->menu_user()); ?>
					<div id="M_line_bottom"></div>
				</div>
				<div id="bodyAdminRight">
					<div id="bodyAdminRightContentHead"></div>
					<div id="bodyAdminRightContentBody">
						<div id="desplegable" style="display:none;"></div>
						<div id="msjError"></div>
						<div id="msjWarning"></div>
						<div id="msjInfo"></div>
						<a name="top"></a>
						<div id="addRutaControl">
							<div id="listaBases">
								<div class="divForm">
									<div class="Fseleccion">
										<label class="Flbl">Ruta</label><br/>
										<select class="Ftxt" name="ruta" id="ruta">
											<?=$u->rutaSelectForMod($idRuta); ?>
										</select>
										<input type="button" name="enviarBases" class="fButton fButtonAdd" onClick="enviar(); return false;" value="Modificar Ruta de Ctrl." />
										<input type="button" name="cancelar" class="fButton" value="Cancelar" onClick="document.location = 'userRutasControl.php';" />
									</div>
									<div id="barrasIzquierda">
										<label class="Flbl">Geocercas</label><br/>
										<form id="formBarra1">
											<div id="barra1" class="geoList">
												<?=$u->geoListForRutaControl(); ?>										
											</div>
										</form>
									</div>	
									<div id="barrasDerecha">
										<label class="Flbl">Geocercas Seleccionadas</label><br/>
										<form id="formBarra2">
											<div id="barra2" class="geoList">
												<?=$u->geoListForModRutaControl($idRuta); ?>
											</div>
										</form>									
									</div>	
									<div class="corte"></div>
								</div>							
							</div>
						</div>
						<div id="desplegableDown" style="display:none;"></div>
					</div>
					<div id="bodyAdminRightContentFeet"></div>
				</div>
				<div class="corte"></div>
			</div>		
			<?
			if (isset($_GET["mok"]) and trim($_GET["mok"])=="1"){			
				msj("i", 10, "Datos agregados satisfactoriamente!");
			}				
			?>
			<div id="feetAdmin">
				<div id="feetAdminLeft"></div>
				<div id="feetAdmincenter">
					<div id="creditos"></div>
				</div>
				<div id="feetAdminright"></div>
				<div class="corte"></div>
			</div>
		  </div>
		</div>
	  </body>
	</html>
<?php
	}
}else{
	header("Location: login.php");
}//endif ($u->isLogued)
ob_end_flush();
?>