<?php
ob_start();

if (isset($_GET["d"]) and strlen($_GET["d"])>0){
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	$u = new User();	
	if ($u->isLogued){
?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head profile="http://gmpg.org/xfn/11">
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
			<meta http-equiv="pragma" content="no-cache" />
			<meta http-equiv="cache-control" content="no-cache" />	
			<meta name="description" content="T3 es la forma sencilla y moderna de rastrear un contenedor en tiempo real y detectar su apertura en el momento en que esta ocurre, con un bajo costo de operaci&oacute;n." />
			<meta name="keywords" content="Rastreo,Sistema,Colima,GPS,Geoposicionamiento,Mexico,Contenedores,Rastreo Satelital" />
			<meta http-equiv="content-language" content="es" />
			<meta name="Language" content="Spanish" />
			<meta name="Distribution" content="Global" />
			<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
			<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/jquery.js"></script>
			<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/funciones.js"></script>	

<?
		$serieGps = (int)myDencr(trim($_GET["d"]));
		
		if ($serieGps>0){
			//procesamos datos					
			$vm = 100;
			if (isset($_GET["vm"]) and strlen(trim($_GET["vm"]))>0){
				$vm = (int)trim($_GET["vm"]);
			}
						
			//SACAMOS EL PUNTO MAS ACTUAL
			$rs->Open("	SELECT lat, lon
						FROM datosghe 
						WHERE 
							gps = ".$serieGps." AND
							id = (
								SELECT MAX(id) 
								FROM datosghe 
								WHERE 
									gps = ".$serieGps."
							)", $conn);
			$datos = fetch_assoc($rs); 
			$rs->Close();
			
			//modificamos el formato de las coordenadas
			$lat = $datos[0]["lat"];	  
			switch(substr($lat,strlen($lat)-1,1)){
				case 'S': (double)$lat*=-1; break;
				case 'N': (double)$lat*=1; break;
			}
			
			$lon = $datos[0]["lon"];  
			switch(substr($lon,strlen($lon)-1,1)){
				case 'W': (double)$lon*=-1; break;
				case 'E': (double)$lon*=1; break;
			}			
	?>			
			<title>Montecristo Data Mining - Tracking Tampering Technology - Viajes - Punto Actual</title>
			<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>


			<script type="text/javascript">  
				//<![CDATA[
				var map;
				var band_now=0;
				var PointsNow=[];
				var polylineNow;
				var band_polyNow=0;
				var marcadorNow;	
				var miIcono1;
				var OffOn;
				var msj = 'No abierta';		
				var geocoder;	
				var dir;										
				
				function load() {
					/*if (GBrowserIsCompatible()) {  	  
						map = new GMap2(document.getElementById("map"));
						map.setCenter(new GLatLng(<?=$lat; ?>, <?=$lon; ?>), 18, G_HYBRID_MAP);	  						
						map.addControl(new GSmallMapControl());
						map.addControl(new GMapTypeControl());
						map.enableScrollWheelZoom();
						geocoder = new GClientGeocoder();
						
						posicionNow(); //iniciamos actualizaciones
					}*/

                   var canvas=document.getElementById("map");
                    $("#map").css("height",$(window).height()-80);
                    
                      
                     <?php
                       echo "var latn=".count($datos).";";
                       if(!isset($lat)){
                         echo "alert('No hay Datos');";
                         $lat=19.3;
                         $lon=-103.2;
                         }
                     ?>

                     var latlng = new google.maps.LatLng(<?=$lat; ?>, <?=$lon; ?>);
                 
                     var settings = {
                       zoom: 12,
                       center: latlng,
                       mapTypeControl: true,
                       mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                       navigationControl: true,
                       navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                       mapTypeId: google.maps.MapTypeId.HYBRID
                     };
                     map = new google.maps.Map(canvas, settings);

                     $(window).resize(function() {
                      $("#map").css("height",$(window).height()-80);
                      });


                        google.maps.event.addListener(map, 'click', function(event) {
 	                       getCalle1(event.latLng,1);
                         });
                     if(latn!=0)
                     posicionNow();

				}


              


var info1 = new google.maps.InfoWindow();
var maker1;
var primer=false;

function getCalle1(latlng,iden){
	//obtengo
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          info1.setMap(null);
          if(iden==1){
         

            if(!primer){          	
                         marker1 = new google.maps.Marker({
                         position: latlng,
                         draggable:false,
                         map: map
                         });

            primer=true;
           }
            else
             marker1.setPosition(latlng);
         
     
           
           info1.setContent(results[1].formatted_address);
           info1.setPosition(latlng);
           info1.open(map);
           
         }else if(iden==2) {

           info1.setContent(results[1].formatted_address);
           info1.setPosition(latlng);
           info1.open(map);
                      
         }

        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });

   
 }




				function direccion(){
					
                   /*$("#tablaMapsOnePoint").html('<table cellpadding="0" cellspacing="0"><tr class="headTabla"><td>Modulo</td><td>Fecha</td><td>Fecha UTC</td><td>Fibra</td><td>Aperturas</td><td>Velocidad</td><td>Temperatura</td><td>Tiempo parado</td><td>Direcci&oacute;n</td></tr></table>');
							
							//actualizamos tabla
							$(".headTabla").after('<tr class="bodyTable0"><td>'+(datosNow[10].split('COMA').join(','))+'</td><td>'+datosNow[1]+'</td><td>'+datosNow[2]+'</td><td>'+msj+'</td><td>'+datosNow[12]+'</td><td>'+datosNow[7]+'</td><td>'+datosNow[5]+'</td><td>'+((datosNow[11]!='0')?datosNow[11]:'0 min')+'</td><td>'+(($("#calle_0").val())?$("#calle_0").val():'Cargando...')+'</td></tr>');

                 */
				}
				function posicionNow(){					
				 	
					PointsNow=[];	//inicializamos los puntos para hacer polyline de la vista actual
					band_polyNow=0;										
				    					
					mostrarNow();	//comenzamos a mostrar la posicion actual

				}
				
				function mostrarNow(){ 
						
					$.ajax({
						type: "GET",
						url: "posicionActualGMforUser_actualizar.php",
						data: "xt=<?=$serieGps; ?>",
						dataType: "html",
						contentType: "application/x-www-form-urlencoded",
						success: function(datos){
							
                          
							datos = trim(datos);
							
							//recivimos datos nuevos y los metemos en un array
							var datosNow = datos.split(", ");	
							
							if (band_now==0){ //primera vez que vemos la posicion actual
								band_now=1; 
							}else{  //borramos el markador anterior
								marcadorNow.setMap(null);			
							}
							


							
							//guardamos punto en vector para hacer polyline de vista actual
							PointsNow.push(new google.maps.LatLng(datosNow[3], datosNow[4]));
							getCalle1(new google.maps.LatLng(datosNow[3], datosNow[4]), 0);
							//si ya habia una polyline entonces la kitamos y ponemos la nueva
							if (band_polyNow==1){ 
								//map.removeOverlay(polylineNow);
								
								polylineNow.setMap(null);
							}
							
							//creamos polyline de vista actual
							if (PointsNow.length > 1){ //si existe mas de un punto
								polylineNow = new google.maps.Polyline({
                                 path: PointsNow
                                 , map: map
                                 , strokeColor: '#F1365E'
                                 , strokeWeight: 3
                                 , strokeOpacity: 0.7
                                 
                                 });



								/*polylineNow = new GPolyline(PointsNow, "#F1365E", 3, 0.7);
								map.addOverlay(polylineNow); */
								band_polyNow=1; 
								
							}
							
							//CREAMOS MARKADOR
							//verificamos que la alarma este apagada
							if (datosNow[9]==1 || datosNow[13]==1 || datosNow[9]==21 || datosNow[9]==50 ){ //fibra abierta, ponemos icono de alerta
								miIcono1 = "http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png"; 
								msj = 'Abierta';
							}else if (datosNow[7].substring(0, datosNow[7].length-5)><?=$vm; ?>){ //va a exeso de velocidad
								miIcono1 = "http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png"; 
							}else{
								//ponemos iconos normales
								if (datosNow[7].substring(0, datosNow[7].length-5)==0){ //si esta parado 
									OffOn="Off"; 
								}else{ 
									OffOn="On"; 
								}
								
								miIcono1 = "http://www.montecristodm.com/viajes/t3/Imagenes/" + datosNow[6] + OffOn + ".png";
							}
							
							/*miIcono1.iconSize = new GSize(16, 16);
							miIcono1.iconAnchor = new GPoint(8, 8);*/
							//miIcono1.infoWindowAnchor = new GPoint(8, 8);
							
							/*marcadorNow = new GMarker(new GLatLng(datosNow[3], datosNow[4]), { icon:miIcono1, title: (datosNow[10].split('COMA').join(','))});
							marcadorNow.value = datosNow[0];*/

                            
                            marcadorNow = new google.maps.Marker({
                            position: new google.maps.LatLng(datosNow[3], datosNow[4]),
                            map: map,
                            icon:miIcono1,
                            title: (datosNow[10].split('COMA').join(',')),
                            tag:datosNow[0]
                            }); 

					
							//obtenemos la calle de este punto
							
							var info = new google.maps.InfoWindow();
							 
							//le agregamos el evento "click" para desplegar información
							google.maps.event.addListener(marcadorNow,"click", function() { 		     
								var myHtml1 = '<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Modulo: <b>'+ (datosNow[10].split('COMA').join(','))+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha: <b>'+ datosNow[1]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha UTC: <b>'+ datosNow[2]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">GPS: <b>'+ datosNow[14]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fibra: <b>'+ msj+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Aperturas: <b>'+ datosNow[12]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Orientaci&oacute;n: <b>'+ datosNow[6] +'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Velocidad: <b>'+ datosNow[7]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Temperatura: <b>'+ datosNow[5]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Bater&iacute;a: <b>'+ datosNow[8]+'</b></div><br/>'+ ((datosNow[11]!='0')?('<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Tiempo parado: <b>'+ datosNow[11]+'</b></div><br/>'):'')+'<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Direcci&oacute;n: <b>'+(($("#calle_0").val())?$("#calle_0").val():'Cargando...')+'</b></div><br/>';
								
                                info.setMap(null);
                                info.setContent(myHtml1);
                                info.setPosition(new google.maps.LatLng(datosNow[3], datosNow[4]));
							    info.open(map);
							});
							
							//agregamos el marcador
							//map.addOverlay(marcadorNow);
							
							//movemos el mapa a la posicion actual
							//map.panTo(new GLatLng(datosNow[3], datosNow[4]));  //estas coordenadas estan mal
							
							//inicializamos tabla									
							
							$("#tablaMapsOnePoint").html('<table cellpadding="0" cellspacing="0"><tr class="headTabla"><td>Modulo</td><td>Fecha</td><td>Fecha UTC</td><td>Fibra</td><td>Aperturas</td><td>Velocidad</td><td>Temperatura</td><td>Tiempo parado</td><td>Direcci&oacute;n</td></tr></table>');
							
							//actualizamos tabla
							$(".headTabla").after('<tr class="bodyTable0"><td>'+(datosNow[10].split('COMA').join(','))+'</td><td>'+datosNow[1]+'</td><td>'+datosNow[2]+'</td><td>'+msj+'</td><td>'+datosNow[12]+'</td><td>'+datosNow[7]+'</td><td>'+datosNow[5]+'</td><td>'+((datosNow[11]!='0')?datosNow[11]:'0 min')+'</td><td>'+(($("#calle_0").val())?$("#calle_0").val():'Cargando...')+'</td></tr>');							
							//creamos recursividad
							if (band_now==1){ //volvemos mandar llamar la peticion ajax 
								setTimeout ("mostrarNow()", 30000);  //en 30 segundos se vuelve mandar llamar la funcion 
							}							
						}
					});

            
				}				
				//]]>
			</script>
			</head>
			<body onLoad="load()" onUnload="GUnload()">
				<div id="calles"></div>	
				<div id="map" style="width:100%; height:615px;"></div>
				<div id="tablaMapsOnePoint">
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Modulo</td>
						<td>Fecha</td>
						<td>Fecha UTC</td>
						<td>Fibra</td>
						<td>Aperturas</td>
						<td>Velocidad</td>
						<td>Temperatura</td>
						<td>Tiempo parado</td>
						<td>Direcci&oacute;n</td>
					</tr>
				</table>
				</div>
			</body>
			</html>
<?php		
		}//end if ($serieGps>0)
	}//end if ($u->isLogued)
}//end if (isset($_GET["idViaje"]) and strlen($_GET["idViaje"])>0)
ob_end_flush();
?>