<?php
ob_start();
header("Content-type: text/html; charset=iso-8859-1");

if (isset($_POST["llego"]) and $_POST["llego"] = "si"){
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	$rc = New COM("ADODB.Command");
	
	//verificamos si el user ya inici� sesion
	$u = new User();
	if ($u->isLogued){
		//recivimos forms
		switch(trim($_POST["tipoForm"])){
			case "user":
				//recivimos datos
				$saveDatos = new addDatos(trim($_POST["tipoForm"]), $u->id_user);
				
				if ($saveDatos->error == ""){
					//procesamos datos
					if ($saveDatos->validarDatos()){
						$saveDatos->addDatosToBD();
						echo "true";
					}else{
						echo "userYaExiste";
					}
				}else{
					echo $saveDatos->error;
				}
								
				break;
			case "gps":
				//recivimos datos
				$saveDatos = new addDatos(trim($_POST["tipoForm"]), $u->id_user);
				
				if ($saveDatos->error == ""){
					//procesamos datos
					if ($saveDatos->validarDatos()){
						$saveDatos->addDatosToBD();
						echo "true";
					}else{
						echo "gpsYaExiste";
					}
				}else{
					echo $saveDatos->error;
				}
								
				break;
			case "userLow":
				//recivimos datos
				$saveDatos = new addDatos(trim($_POST["tipoForm"]), $u->id_user);
				
				if ($saveDatos->error == ""){
					//procesamos datos
					if ($saveDatos->validarDatos()){
						$saveDatos->addDatosToBD();
						echo "true";
					}else{
						echo "userYaExiste";
					}
				}else{
					echo $saveDatos->error;
				}
								
				break;
			case "contrato":
				//recivimos datos
				$saveDatos = new addDatos(trim($_POST["tipoForm"]), $u->id_user);
				
				if ($saveDatos->error == ""){
					//procesamos datos
					if ($saveDatos->validarDatos()){
						$saveDatos->addDatosToBD();
						echo "true";
					}else{
						echo "contratoYaExiste";
					}
				}else{
					echo $saveDatos->error;
				}
								
				break;
			case "viaje":             
				//recivimos datos
				$saveDatos = new addDatos(trim($_POST["tipoForm"]), $u->id_user);
				
				if ($saveDatos->error == ""){
					//procesamos datos
					if ($saveDatos->validarDatos()){
						$saveDatos->addDatosToBD();
						echo "true";
					}else{
						echo "viajeYaExiste";
					}
				}else{
					echo $saveDatos->error;
				}
								
				break;
			case "ruta":             
				//recivimos datos
				$saveDatos = new addDatos(trim($_POST["tipoForm"]), $u->id_user);
				
				if ($saveDatos->error == ""){
					//procesamos datos
					if ($saveDatos->validarDatos()){
						$saveDatos->addDatosToBD();
						echo "true";
					}else{
						echo "rutaYaExiste";
					}
				}else{
					echo $saveDatos->error;
				}
								
				break;
			case "areasControl":             
				//recivimos datos
				$saveDatos = new addDatos(trim($_POST["tipoForm"]), $u->id_user);
				
				if ($saveDatos->error == ""){
					//procesamos datos
					if ($saveDatos->validarDatos()){
						$saveDatos->addDatosToBD();
						echo "true";
					}
				}else{
					echo $saveDatos->error;
				}
								
				break;
		}
		
		unset($rs, $rc);
	}else{
		echo "false";
	}
}else{
	echo "false";
}

ob_end_flush();
?>