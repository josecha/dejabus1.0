<?
ob_start();
include("conexion.php");
$rs = New COM("ADODB.Recordset");
$rc = New COM("ADODB.Command");

include("funciones.php");
include("head.php");
$u = new User();
?> 
	<title>Montecristo Data Mining - Tracking Tampering Technology - Inicio</title>
   	<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
	<script type="text/javascript" language="javascript" charset="iso-8859-1">
		var browser = "<?=browser(); ?>";
		var thisUrlAll = encodeURIComponent('<?=thisUrlAll();?>');
		<? if ($u->isLogued) { ?>
			var usersSelect = '<?=$u->usersSelect(); ?>';
			var gpsList = '<?=$u->gpsList(); ?>';
			var tipo_user = '<?=$u->tipo_user; ?>';
			var gpsSelect = '<?=$u->gpsSelect(); ?>';		
			var gpsSelectInUse = '<?=$u->gpsSelectInUse(); ?>';					
			var gpsSelectClosed = '<?=$u->gpsSelectClosed(); ?>';		
			var listGeocercas = '<?=$u->printIconList(); ?>';
			var rutasSelect = '<?=$u->rutaSelectForFindForm(); ?>';
			var rutaSelectForViaje = '<?=$u->rutaSelectForViajeForm(); ?>';		
			var rutaSelectForFindViaje = '<?=$u->rutaSelectForFindViaje(); ?>';		
			var rutaSelectForFindHistorial = '<?=$u->rutaSelectForFindHistorial(); ?>';	
			var gpsSelectForReporte = '<?=$u->selectGpsForReporte(); ?>';		
			var gpsLibresAreasControl = '<?=$u->gpsLibresAreasControl(); ?>';	
			var gpsAreasControl = '<?=$u->gpsAreasControl(); ?>';
			var geoareaSelect = '<?=$u->geoareaSelect(); ?>';
		<? } ?>
	</script>
  </head>
  <body class="bodyAdmin">
  	<a name="top"></a>
	<div id="fondoAdminAll">
      <div id="fondoAdmin">
    	<div id="headAdmin">
        	<div id="headAdminLeft"></div>
            <div id="headAdmincenter"></div>
            <div id="headAdminright">
				<? if ($u->isLogued) { ?><div id="opt_buscar" onClick="showFind();"></div>
				<div id="opt_agregar" onClick="showAdd();"></div><? } ?>
			</div>
            <div class="corte"></div>
        </div>
        <div id="bodyAdmin">
        	<div id="bodyAdminLeft">
				<div class="MenuSelected"><div id="M_inicio" onClick="location.href = 'index.php';"></div></div>
                <div class="MenuHover"><div id="M_sesion" onClick="location.href = 'login.php';"></div></div>
				<? if ($u->isLogued) { echo $u->menu_user(); } ?>
                <div id="M_line_bottom"></div>
            </div>
            <div id="bodyAdminRight">
            	<div id="bodyAdminRightContentHead"></div>
                <div id="bodyAdminRightContentBody">
					<div id="desplegable" style="display:none;"></div>
                	<div id="msjError"></div>
                    <div id="msjWarning"></div>
                    <div id="msjInfo"></div>
					
					<div style="font-family: 'Lucida grande', Tahoma, Verdana, Arial, sans-serif; font-size: 13px; padding-left:15px; padding-right:15px;">    
    <p style="font-size: 14px; font-weight: bold;">Aviso Legal</p>
    <p align="justify">A todos los usuarios   del sistema de rastreo de MONTECRISTO se les informa que la se&ntilde;al utilizada   por nuestro equipo es provista por una empresa ajena (proveedora),   por lo tanto en caso de fallas de se&ntilde;al en los equipos de rastreo MONTECRISTO   no se hace responsable, ya que su obligaci&oacute;n se limita a realizar las   gestiones pertinentes ante la empresa proveedora para la pronta reactivaci&oacute;n   de la se&ntilde;al. De igual manera MONTECRISTO no se hace responsable por   el servicio o acceso a Internet (ni sus accesorios) necesario para la   visualizaci&oacute;n del rastreo, por ser &aacute;mbito de competencia de terceros. &nbsp;</p>
    <p align="center"><br />
      Por su preferencia y   atenci&oacute;n, gracias. <br />
      MONTECRISTO DATA MINING   S.A. DE C.V.</p>
                    </div>
					                          
                </div>
                <div id="bodyAdminRightContentFeet"></div>
            </div>
            <div class="corte"></div>
        </div>
        <div id="feetAdmin">
        	<div id="feetAdminLeft"></div>
            <div id="feetAdmincenter">
            	<div id="creditos"></div>
            </div>
            <div id="feetAdminright"></div>
            <div class="corte"></div>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
ob_end_flush();
?>