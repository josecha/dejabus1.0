<?
class Login{
	//PROPIEDADES
	public $tabla = "users";		//tabla donde estan almacenados los datos del usuario
	public $campoUser = "login";	//campo de la tabla del nick o user
	public $campoPass = "md5";		//campo de la ttabla del password MD5
	public $pfx = "";				//prefijo para el nombre de las coockies
	public $sesionTime = 86400;		//duraci�n de la sesion
	public $ckUser = ""; 			//cookie de user
	public $ckPass = "";			//cookie de pass
	public $nameUser = "";			//nombre que se mostrar� al dar bienvenida
	
	
	//CONSTRUCTOR
	public function __construct ($pfx = ""){
		$this->pfx = $pfx;
	}
	
		
	//METODOS	
	//obtenemos cookies
	public function getCookies(){
		if (empty($_COOKIE[$this->pfx."Nick"]) and !isset($_COOKIE[$this->pfx."Nick"])){ 
			$this->ckUser = ""; 
		}else{ 
			$this->ckUser = $_COOKIE[$this->pfx."Nick"];
		}
	
		if (empty($_COOKIE[$this->pfx."Pass"]) and !isset($_COOKIE[$this->pfx."Pass"])){
			$this->ckPass = ""; 
		}else{ 
			$this->ckPass = $_COOKIE[$this->pfx."Pass"];
		}
		return true;
	}
	
	//asignamos cookies
	private function setCookies($u = "", $p = ""){
		setcookie($this->pfx."Nick", $u, time() + $this->sesionTime);
	    setcookie($this->pfx."Pass", $p, time() + $this->sesionTime);
		  
		return true;
	}
	
	//verifica si un usuario ya ha iniciado sesion
	public function isLogued(){
		//obtenemos cookies
		$this->getCookies();
		
		//checamos en BD si ya est� logueado
		if ($this->checarBD($this->ckUser, $this->ckPass)){
			return true;
		}else{
			return false;
		}
	}
	
	//checamos si es correcto el user y pass enviado
	public function checarDatos($u, $p){
		//checamos en BD si ya est� logueado
		if ($this->checarBD($u, $p)){
			//ponemos cookies
			$this->setCookies($u, $p);
			
			return true;
		}else{
			return false;
		}
	}
	
	//checamos en BD
	public function checarBD($u, $p){
		//checamos en BD si ya est� logueado
		//$sql = mysql_query("SELECT $this->campoUser FROM $this->tabla WHERE $this->campoUser = '$u' AND $this->campoPass = '$p'");
		global $conn;
		$rs = New COM("ADODB.Recordset");
		$rs->Open("SELECT $this->campoUser FROM $this->tabla WHERE $this->campoUser = '$u' AND $this->campoPass = '$p'", $conn); 
		$res = fetch_assoc($rs); 
		$rs->Close();
		
		//if (mysql_num_rows($sql) != 0){
		if (count($res)>0){
			$this->nameUser = $res[0][$this->campoUser];
			return true;
		}else{
			return false;
		}
	}
	
	//cerramos sesion 
	public function logOut(){
		$this->setCookies();
		return true;
	}
}//end class
?>