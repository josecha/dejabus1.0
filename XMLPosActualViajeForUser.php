<?php
ob_start();

if (isset($_GET["d"]) and strlen($_GET["d"])>0){
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	$u = new User();
	if ($u->isLogued){
		//procesamos datos					
		$vm = 100;
		if (isset($_GET["vm"]) and strlen(trim($_GET["vm"]))>0){
			$vm = (int)trim($_GET["vm"]);
		}
		
		$Kml = '<?xml version="1.0" encoding="ISO-8859-1"?>
		<kml xmlns="http://earth.google.com/kml/2.0">
		<NetworkLink>
		  <name>Punto Actual '.date("d-m-Y h:i a").'</name>
		  <Url>
			<href>http://www.montecristodm.com/viajes/t3/PosActualViajeForUser.php?n='.encrNum("".time()).'&amp;d='.encondeSpecialChars(urlencode(trim($_GET["d"]))).'&amp;iduser='.$u->id_user.'&amp;vel='.$vm.'</href>
			<refreshMode>onInterval</refreshMode>
			<refreshInterval>30</refreshInterval>
		  </Url>
		</NetworkLink>
		</kml>';
		
		header("Pragma: public"); 
		header("Expires: 0");       
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		header("Content-Type: application/vnd.google-earth.kml+xml kml; charset=ISO-8859-1");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".strlen($Kml));
		header("Pragma: no-cache");
		header("Content-disposition: attachment; filename=Posicion_Actual_".date("d-m-Y_h-i_a").".kml"); 
		
		echo $Kml;
	}//endif ($u->isLogued)
}//endif (isset($_GET["idviaje"]) and strlen($_GET["idviaje"])>0)
ob_end_flush();
?>