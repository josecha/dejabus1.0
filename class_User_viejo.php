<?
class User extends pagination{
	//PROPIEDADES
	public $id_user = "";
	public $user = "";
	public $tipo_user = "";
	public $isLogued = false;
	public $datosTabla = array();
	public $TablaName = "";
	public $thisUrl = "";
		
	
	//CONSTRUCTORES
	public function __construct (){
		//paginacion
		$this->nextLabel('<strong>Siguiente</strong>');
		$this->prevLabel('<strong>Anterior</strong>');
		$this->nextIcon('&rsaquo;');
		$this->prevIcon('&lsaquo;');
		$this->limit(15);
		$this->thisUrl = thisUrlAll(); //basename($_SERVER["REQUEST_URI"]);
				
		//verificamos login
		$this->isLogued();
	}
	
	
	//MÉTODOS
	//verifica si un usuario ya ha iniciado sesion
	public function isLogued (){
		if (empty($_COOKIE["Nick"]) and !isset($_COOKIE["Nick"])){ 
			$n=''; 
		}else{ 
			$n=$_COOKIE["Nick"];
		}
	
		if (empty($_COOKIE["Pass"]) and !isset($_COOKIE["Pass"])){
			$p=''; 
		}else{ 
			$p=$_COOKIE["Pass"];
		}
		
		global $conn;
		$rs = New COM("ADODB.Recordset");
		$rs->Open("SELECT id, nombre, tipo FROM users WHERE login = '$n' AND md5 = '$p'", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		if (count($datos) != 0){
			$this->user = $datos[0]["nombre"];
			$this->tipo_user = $datos[0]["tipo"];
			$this->id_user = $datos[0]["id"];
			$this->isLogued = true;	
			return true;
		}else{
			return false;
		}
	}
	
	
	//regresa el menu lateral
	public function menu_user(){
		switch($this->tipo_user){
			case "0": //root
				return '
				<div class="MenuHover"><div id="M_salir" onClick="logOutAll();"></div></div>
				<div class="Menu_div"></div>
				<div class="MenuHover"><div id="M_admin" onClick="location.href=\'rootAdmin.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_gps" onClick="location.href=\'rootGps.php\'; return false;"></div></div>';
				break;
			case "1": //admin
				return '
				<div class="MenuHover"><div id="M_salir" onClick="logOutAll();"></div></div>
				<div class="Menu_div"></div>
				<div class="MenuHover"><div id="M_usuarios" onClick="location.href=\'adminUsers.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_status" onClick="location.href=\'adminStatus.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_contratos" onClick="location.href=\'adminContratos.php\'; return false;"></div></div>				
				<div class="MenuHover"><div id="M_contrCerrados" onClick="location.href=\'adminContratosCerrados.php\'; return false;"></div></div>
				<div class="Menu_div"></div>
				<div class="MenuHover"><div id="M_misdatos" onClick="location.href=\'adminMisDatos.php\'; return false;"></div></div>';				
				break;
			case "2": //user
				return '
				<div class="MenuHover"><div id="M_salir" onClick="logOutAll();"></div></div>
				<div class="Menu_div"></div>				
				<div class="MenuHover"><div id="M_viajes" onClick="location.href=\'userViajes.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_historial" onClick="location.href=\'userHistorial.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_geocercas" onClick="location.href=\'userGeocercas.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_geoareas" onClick="location.href=\'userGeoareas.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_areascontrol" onClick="location.href=\'userAreasControl.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_rutas_" onClick="location.href=\'userRutas.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_rutascontrol" onClick="location.href=\'userRutasControl.php\'; return false;"></div></div>
				<div class="MenuHover"><div id="M_reportes" onClick="verReporteStatus(); return false;"></div></div>
				<div class="MenuHover"><div id="M_misdatos" onClick="location.href=\'userMisDatos.php\'; return false;"></div></div>';
				break;
		}
	}
	
	
	//regresa una lista formateada para un SELECT de usuarios
	public function usersSelect(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");
				
		switch($this->tipo_user){
			case "0":
				//sacamos usuarios admin				
				$rs->Open("SELECT id, nombre FROM users WHERE tipo = 1", $conn); 
				$datos = fetch_assoc($rs); 
				$rs->Close();
				$opt = "";
				
				for ($a=0; $a<count($datos); $a++){
					$opt .= '<option value="'.$datos[$a]["id"].'" '.(($datos[$a]["id"]==4)?'selected="selected"':'').'>'.str_replace("'", "", $datos[$a]["nombre"]).'</option>';
				}
				
				break;
			case "1":
				//sacamos usuarios admin					
				$rs->Open("SELECT id, nombre FROM users WHERE tipo = 2 AND admin = $this->id_user", $conn); 
				$datos = fetch_assoc($rs); 
				$rs->Close();
				$opt = "";
				
				for ($a=0; $a<count($datos); $a++){
					$opt .= '<option value="'.$datos[$a]["id"].'" '.(($datos[$a]["id"]==4)?'selected="selected"':'').'>'.str_replace("'", "", $datos[$a]["nombre"]).'</option>';
				}
				
				break;					
		}			
			
		return $opt;
	}
	
	
	//regresa una lista formateada para un DIV de GPS´s disponibles para un contrato
	public function gpsList(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("SELECT id, serie, descripcion FROM GPs WHERE usr = $this->id_user AND activo=1 order by serie", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<div><input type="checkbox" name="gps[]" value="'.$datos[$a]["id"].'" /><label>'.$datos[$a]["serie"].' - '.substr(str_replace("'", "", $datos[$a]["descripcion"]),0,20).((strlen($datos[$a]["descripcion"])>20)?'...':'').'</label></div>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un SELECT de GPS´s disponibles para un contrato
	public function gpsSelect(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("		
		SELECT 
			GPs.id, 
			GPs.descripcion
		FROM  GPs
		INNER JOIN gpscontratos 
		ON (GPs.id = gpscontratos.gps)  
		WHERE
			(GPs.id NOT IN(
				SELECT gps
				FROM   viajes
				WHERE 
					(activo = 1) AND (usr = ".$this->id_user."))) AND(
						gpscontratos.contrato IN(
							SELECT id FROM contratos
							WHERE 
								(activo = 1) AND (usr = ".$this->id_user."))) order by gps.descripcion", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.substr(str_replace("'", "", $datos[$a]["descripcion"]),0,20).((strlen($datos[$a]["descripcion"])>20)?'...':'').'</option>';
		}
						
		return $opt;
	}
	
	//regresa una lista formateada para un divList de GPS´s disponibles para una área de control
	public function gpsLibresAreasControl(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("
			SELECT 
				GPs.id, 
				GPs.descripcion
			FROM  GPs
			INNER JOIN gpscontratos 
			ON (GPs.id = gpscontratos.gps)  
			WHERE
				(GPs.id NOT IN(
					SELECT GPs.id 
					FROM GPs 
					INNER JOIN geoControlT3 ON (geoControlT3.gps = GPs.id) 
					INNER JOIN geoControl ON (geoControl.id = geoControlT3.geocontrol)
					WHERE geoControl.owner = $this->id_user AND geoControlT3.activo = 1)) AND(
							gpscontratos.contrato IN(
								SELECT id FROM contratos
								WHERE 
									(activo = 1) AND (usr = $this->id_user)))", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<div><input id="gps_'.$a.'" name="gps_'.$a.'" type="checkbox" value="'.$datos[$a]["id"].'" class="gpsDivList"/><label>'.str_replace("'", "", $datos[$a]["descripcion"]).'</label></div>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un divList con todos los GPS´s de un contrato
	public function gpsAreasControl(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("
			SELECT 
				GPs.id, 
				GPs.descripcion
			FROM  GPs
			INNER JOIN gpscontratos 
			ON (GPs.id = gpscontratos.gps)  
			WHERE
				(GPs.id IN(
					SELECT GPs.id 
					FROM GPs 
					INNER JOIN geoControlT3 ON (geoControlT3.gps = GPs.id) 
					INNER JOIN geoControl ON (geoControl.id = geoControlT3.geocontrol)
					WHERE geoControl.owner = $this->id_user AND geoControlT3.activo = 1)) AND(
							gpscontratos.contrato IN(
								SELECT id FROM contratos
								WHERE 
									(activo = 1) AND (usr = $this->id_user)))", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.str_replace("'", "", $datos[$a]["descripcion"]).'</option>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un SELECT de geoareas
	public function geoareaSelect(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("		
		SELECT id, nombre FROM geoControl WHERE owner = $this->id_user", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.substr(str_replace("'", "", $datos[$a]["nombre"]),0,20).((strlen($datos[$a]["nombre"])>20)?'...':'').'</option>';
		}
						
		return $opt;
	}
	

	//regresa una lista formateada para un SELECT de GPS´s que estan en un viaje
	public function gpsSelectInUse(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("		
		SELECT 
			GPs.id, 
			GPs.descripcion
		FROM  GPs
		INNER JOIN gpscontratos 
		ON (GPs.id = gpscontratos.gps)  
		WHERE
			(GPs.id IN(
				SELECT gps
				FROM   viajes
				WHERE 
					(activo = 1) AND (usr = ".$this->id_user."))) AND(
						gpscontratos.contrato IN(
							SELECT id FROM contratos
							WHERE 
								(activo = 1) AND (usr = ".$this->id_user.")))", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.substr(str_replace("'", "", $datos[$a]["descripcion"]),0,20).((strlen($datos[$a]["descripcion"])>20)?'...':'').'</option>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un SELECT de GPS´s que estan en un viaje cerrado
	public function gpsSelectClosed(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("		
			SELECT
				GPs.id,
				GPs.descripcion,
				viajes.clave
			FROM GPs
			INNER JOIN viajes
			ON (GPs.id = viajes.gps)
			WHERE
				viajes.activo = 0 AND 
				viajes.usr = ".$this->id_user, $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.substr(str_replace("'", "", $datos[$a]["descripcion"]),0,20).((strlen($datos[$a]["descripcion"])>20)?'...':'').'</option>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un SELECT de GPS´s que estan en un viaje
	public function gpsSelectForUser(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("		
		SELECT 
			GPs.serie, 
			GPs.descripcion			
		FROM GPs 
		INNER JOIN gpscontratos 
		ON (GPs.id = gpscontratos.gps) 
		WHERE (
			gpscontratos.contrato IN (
				SELECT id 
				FROM contratos 
				WHERE 
					activo = 1 AND 
					usr = ".$this->id_user."
			) AND gpscontratos.activo = 1
		)
		ORDER BY GPs.descripcion ASC", $conn); 								
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.urlencode(myEncr($datos[$a]["serie"])).'">'.substr(str_replace("'", "", $datos[$a]["descripcion"]),0,20).((strlen($datos[$a]["descripcion"])>20)?'...':'').'</option>';
		}
						
		return $opt;
	}	
	
	
	//obtiene la pagina actual
	public function paginaNow(){
		$page = 1;
		if (isset($_GET['page']) and strlen($_GET['page'])>0 and is_numeric($_GET['page'])){ 
			$page = $_GET['page'];
		}
		$this->currentPage($page);
	}


	//contruye el filtro para los datos de las tablas
	private function filtroTabla($tabla){
		$filtro = '';
		switch ($tabla){
			case "admin":
				$condiciones = array();
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["nombre"]) and strlen($_GET["nombre"])>0){ 
					$condiciones[] = " ".$this->TablaName.".nombre LIKE '%".addslashes(strip_tags(trim($_GET["nombre"])))."%' ";
				}
				if (isset($_GET["login"]) and strlen($_GET["login"])>0){ 
					$condiciones[] = " ".$this->TablaName.".login LIKE '%".addslashes(strip_tags(trim($_GET["login"])))."%' ";
				}
				if (isset($_GET["datos"]) and strlen($_GET["datos"])>0){ 
					$condiciones[] = " ".$this->TablaName.".datos LIKE '%".addslashes(strip_tags(trim($_GET["datos"])))."%' ";
				}
				if (isset($_GET["tel"]) and strlen($_GET["tel"])>0){ 
					$condiciones[] = " ".$this->TablaName.".telefono LIKE '%".addslashes(strip_tags(trim($_GET["tel"])))."%' ";
				}
				if (isset($_GET["email"]) and strlen($_GET["email"])>0){ 
					$condiciones[] = " ".$this->TablaName.".mail1 LIKE '%".addslashes(strip_tags(trim($_GET["email"])))."%' ";
				}
				if (isset($_GET["email2"]) and strlen($_GET["email2"])>0){ 
					$condiciones[] = " ".$this->TablaName.".mail2 LIKE '%".addslashes(strip_tags(trim($_GET["email2"])))."%' ";
				}
				if (isset($_GET["email3"]) and strlen($_GET["email3"])>0){ 
					$condiciones[] = " ".$this->TablaName.".mail3 LIKE '%".addslashes(strip_tags(trim($_GET["email3"])))."%' ";
				}
				if (isset($_GET["cel"]) and strlen($_GET["cel"])>0){ 
					$condiciones[] = " ".$this->TablaName.".cel1 LIKE '%".addslashes(strip_tags(trim($_GET["cel"])))."%' ";
				}
				if (isset($_GET["cel2"]) and strlen($_GET["cel2"])>0){ 
					$condiciones[] = " ".$this->TablaName.".cel2 LIKE '%".addslashes(strip_tags(trim($_GET["cel2"])))."%' ";
				}
				$condiciones[] = " ".$this->TablaName.".tipo = 1";				
				break;
			case "gps":
				$condiciones = array();
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["serie"]) and strlen($_GET["serie"])>0){ 
					$condiciones[] = " ".$this->TablaName.".serie = ".addslashes(strip_tags(trim((int)$_GET["serie"])));
				}
				if (isset($_GET["descripcion"]) and strlen($_GET["descripcion"])>0){ 
					$condiciones[] = " ".$this->TablaName.".descripcion LIKE '%".addslashes(strip_tags(trim($_GET["descripcion"])))."%' ";
				}
				if (isset($_GET["usr"]) and strlen($_GET["usr"])>0){ 
					if (trim($_GET["usr"])!='*'){
						$condiciones[] = " ".$this->TablaName.".usr = ".trim((int)$_GET["usr"]);
					}
				}
				if (isset($_GET["tipo"]) and strlen($_GET["tipo"])>0){ 
					if (trim($_GET["tipo"])!='*'){
						$condiciones[] = " ".$this->TablaName.".tipo = ".trim((int)$_GET["tipo"]);
					}
				}
				if (isset($_GET["activo"]) and strlen($_GET["activo"])>0){ 
					if (trim($_GET["activo"])!='*'){
						$condiciones[] = " ".$this->TablaName.".activo = ".trim((int)$_GET["activo"]);
					}
				}
				if (isset($_GET["celular"]) and strlen($_GET["celular"])>0){ 
					$condiciones[] = " ".$this->TablaName.".celular LIKE '%".addslashes(strip_tags(trim($_GET["celular"])))."%' ";
				}							
					
				break;		
			case "userLow":
				$condiciones = array();
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["nombre"]) and strlen($_GET["nombre"])>0){ 
					$condiciones[] = " ".$this->TablaName.".nombre LIKE '%".addslashes(strip_tags(trim($_GET["nombre"])))."%' ";
				}
				if (isset($_GET["login"]) and strlen($_GET["login"])>0){ 
					$condiciones[] = " ".$this->TablaName.".login LIKE '%".addslashes(strip_tags(trim($_GET["login"])))."%' ";
				}
				if (isset($_GET["datos"]) and strlen($_GET["datos"])>0){ 
					$condiciones[] = " ".$this->TablaName.".datos LIKE '%".addslashes(strip_tags(trim($_GET["datos"])))."%' ";
				}
				if (isset($_GET["tel"]) and strlen($_GET["tel"])>0){ 
					$condiciones[] = " ".$this->TablaName.".telefono LIKE '%".addslashes(strip_tags(trim($_GET["tel"])))."%' ";
				}
				if (isset($_GET["email"]) and strlen($_GET["email"])>0){ 
					$condiciones[] = " ".$this->TablaName.".mail1 LIKE '%".addslashes(strip_tags(trim($_GET["email"])))."%' ";
				}
				if (isset($_GET["email2"]) and strlen($_GET["email2"])>0){ 
					$condiciones[] = " ".$this->TablaName.".mail2 LIKE '%".addslashes(strip_tags(trim($_GET["email2"])))."%' ";
				}
				if (isset($_GET["email3"]) and strlen($_GET["email3"])>0){ 
					$condiciones[] = " ".$this->TablaName.".mail3 LIKE '%".addslashes(strip_tags(trim($_GET["email3"])))."%' ";
				}
				if (isset($_GET["cel"]) and strlen($_GET["cel"])>0){ 
					$condiciones[] = " ".$this->TablaName.".cel1 LIKE '%".addslashes(strip_tags(trim($_GET["cel"])))."%' ";
				}
				if (isset($_GET["cel2"]) and strlen($_GET["cel2"])>0){ 
					$condiciones[] = " ".$this->TablaName.".cel2 LIKE '%".addslashes(strip_tags(trim($_GET["cel2"])))."%' ";
				}
				$condiciones[] = " ".$this->TablaName.".tipo = 2";	
				$condiciones[] = " ".$this->TablaName.".admin = ".$this->id_user;						
				break;
			case "contrato":
				$condiciones = array();
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["date1"]) and strlen($_GET["date1"])>0){ 
					$condiciones[] = " ".$this->TablaName.".finicio > '".addslashes(strip_tags(trim(substr($_GET["date1"],6,4).'-'.substr($_GET["date1"],3,2).'-'.substr($_GET["date1"],0,2).'T'.(($_GET["selectHI"]!="*")?$_GET["selectHI"]:'00').':'.(($_GET["selectMI"]!="*")?$_GET["selectMI"]:'00').':00')))."' ";
				}				
				if (isset($_GET["usr"]) and $_GET["usr"]!="*"){ 
					$condiciones[] = " ".$this->TablaName.".usr = ".addslashes(strip_tags(trim((int)$_GET["usr"])));
				}
				if (isset($_GET["numero"]) and strlen($_GET["numero"])>0){ 
					$condiciones[] = " ".$this->TablaName.".numero LIKE '%".addslashes(strip_tags(trim($_GET["numero"])))."%' ";
				}
				if (isset($_GET["descripcion"]) and strlen($_GET["descripcion"])>0){ 
					$condiciones[] = " ".$this->TablaName.".descripcion LIKE '%".addslashes(strip_tags(trim($_GET["descripcion"])))."%' ";
				}								
				$condiciones[] = " ".$this->TablaName.".owner = ".$this->id_user;	
				$condiciones[] = " ".$this->TablaName.".activo = 1";	
				$condiciones[] = " GPs.activo = 0";	
				
				break;
			case "contratoCerrado":
				$condiciones = array();
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["date1"]) and strlen($_GET["date1"])>0){ 
					$condiciones[] = " ".$this->TablaName.".finicio >= '".addslashes(strip_tags(trim(substr($_GET["date1"],6,4).'-'.substr($_GET["date1"],3,2).'-'.substr($_GET["date1"],0,2).'T'.(($_GET["selectHI"]!="*")?$_GET["selectHI"]:'00').':'.(($_GET["selectMI"]!="*")?$_GET["selectMI"]:'00').':00')))."' ";
				}
				if (isset($_GET["date2"]) and strlen($_GET["date2"])>0){ 										
					$condiciones[] = " ".$this->TablaName.".ffin <= '".addslashes(strip_tags(trim(substr($_GET["date2"],6,4).'-'.substr($_GET["date2"],3,2).'-'.substr($_GET["date2"],0,2).'T'.(($_GET["selectHF"]!="*")?$_GET["selectHF"]:'00').':'.(($_GET["selectMF"]!="*")?$_GET["selectMF"]:'00').':00')))."' ";					
				}
				if (isset($_GET["usr"]) and $_GET["usr"]!="*"){ 
					$condiciones[] = " ".$this->TablaName.".usr = ".addslashes(strip_tags(trim((int)$_GET["usr"])));
				}
				if (isset($_GET["numero"]) and strlen($_GET["numero"])>0){ 
					$condiciones[] = " ".$this->TablaName.".numero LIKE '%".addslashes(strip_tags(trim($_GET["numero"])))."%' ";
				}
				if (isset($_GET["descripcion"]) and strlen($_GET["descripcion"])>0){ 
					$condiciones[] = " ".$this->TablaName.".descripcion LIKE '%".addslashes(strip_tags(trim($_GET["descripcion"])))."%' ";
				}								
				$condiciones[] = " ".$this->TablaName.".owner = ".$this->id_user;	
				$condiciones[] = " ".$this->TablaName.".activo = 0";	
				
				break;
			case "viajes":
				$condiciones = array();
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["clave"]) and strlen($_GET["clave"])>0){ 
					$condiciones[] = " ".$this->TablaName.".clave LIKE '%".addslashes(strip_tags(trim($_GET["clave"])))."%' ";
				}
				if (isset($_GET["gps"]) and strlen($_GET["gps"])>0 and $_GET["gps"]!='*'){ 
					$condiciones[] = " ".$this->TablaName.".gps = ".addslashes(strip_tags(trim((int)$_GET["gps"]))); 
				}
				if (isset($_GET["ruta"]) and strlen($_GET["ruta"])>0 and $_GET["ruta"]!='*'){ 
					$condiciones[] = " ".$this->TablaName.".ruta = ".(((int)$_GET["ruta"]<=0)?("0"):(addslashes(strip_tags(trim((int)$_GET["ruta"]))))); 
				}
				if (isset($_GET["origen"]) and strlen($_GET["origen"])>0){ 
					$condiciones[] = " ".$this->TablaName.".origen LIKE '%".addslashes(strip_tags(trim($_GET["origen"])))."%' ";
				}
				if (isset($_GET["destino"]) and strlen($_GET["destino"])>0){ 
					$condiciones[] = " ".$this->TablaName.".destino LIKE '%".addslashes(strip_tags(trim($_GET["destino"])))."%' ";
				}
				if (isset($_GET["mercancia"]) and strlen($_GET["mercancia"])>0){ 
					$condiciones[] = " ".$this->TablaName.".mercancia LIKE '%".addslashes(strip_tags(trim($_GET["mercancia"])))."%' ";
				}
				if (isset($_GET["date1"]) and strlen($_GET["date1"])>0){ 
					$condiciones[] = " ".$this->TablaName.".finicial >= '".addslashes(strip_tags(trim(substr($_GET["date1"],6,4).'-'.substr($_GET["date1"],3,2).'-'.substr($_GET["date1"],0,2).'T'.(($_GET["selectHI"]!="*")?$_GET["selectHI"]:'00').':'.(($_GET["selectMI"]!="*")?$_GET["selectMI"]:'00').':00')))."' ";
				}							
				$condiciones[] = " ".$this->TablaName.".activo = 1";	
				$condiciones[] = " ".$this->TablaName.".usr = ".$this->id_user;				
				break;				
			case "historial":
				$condiciones = array();
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["clave"]) and strlen($_GET["clave"])>0){ 
					$condiciones[] = " ".$this->TablaName.".clave LIKE '%".addslashes(strip_tags(trim($_GET["clave"])))."%' ";
				}
				if (isset($_GET["gps"]) and strlen($_GET["gps"])>0 and $_GET["gps"]!='*'){ 
					$condiciones[] = " ".$this->TablaName.".gps = ".addslashes(strip_tags(trim((int)$_GET["gps"]))); 
				}
				if (isset($_GET["ruta"]) and strlen($_GET["ruta"])>0 and $_GET["ruta"]!='*'){ 
					$condiciones[] = " ".$this->TablaName.".ruta = ".addslashes(strip_tags(trim((int)$_GET["ruta"]))); 
				}
				if (isset($_GET["origen"]) and strlen($_GET["origen"])>0){ 
					$condiciones[] = " ".$this->TablaName.".origen LIKE '%".addslashes(strip_tags(trim($_GET["origen"])))."%' ";
				}
				if (isset($_GET["destino"]) and strlen($_GET["destino"])>0){ 
					$condiciones[] = " ".$this->TablaName.".destino LIKE '%".addslashes(strip_tags(trim($_GET["destino"])))."%' ";
				}
				if (isset($_GET["mercancia"]) and strlen($_GET["mercancia"])>0){ 
					$condiciones[] = " ".$this->TablaName.".mercancia LIKE '%".addslashes(strip_tags(trim($_GET["mercancia"])))."%' ";
				}
				if (isset($_GET["date1"]) and strlen($_GET["date1"])>0){ 
					$condiciones[] = " ".$this->TablaName.".finicial >= '".addslashes(strip_tags(trim(substr($_GET["date1"],6,4).'-'.substr($_GET["date1"],3,2).'-'.substr($_GET["date1"],0,2).'T'.(($_GET["selectHI"]!="*")?$_GET["selectHI"]:'00').':'.(($_GET["selectMI"]!="*")?$_GET["selectMI"]:'00').':00')))."' ";
				}
				if (isset($_GET["date2"]) and strlen($_GET["date2"])>0){ 
					$condiciones[] = " ".$this->TablaName.".ffinal <= '".addslashes(strip_tags(trim(substr($_GET["date2"],6,4).'-'.substr($_GET["date2"],3,2).'-'.substr($_GET["date2"],0,2).'T'.(($_GET["selectHF"]!="*")?$_GET["selectHF"]:'00').':'.(($_GET["selectMF"]!="*")?$_GET["selectMF"]:'00').':00')))."' ";
				}							
				$condiciones[] = " ".$this->TablaName.".activo = 0";	
				$condiciones[] = " ".$this->TablaName.".usr = ".$this->id_user;				
				break;			
			case "geocerca":
				$condiciones = array();					
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["nombre"]) and strlen($_GET["nombre"])>0){ 
					$condiciones[] = " ".$this->TablaName.".nombre LIKE '%".addslashes(strip_tags(trim($_GET["nombre"])))."%' ";
				}
				if (isset($_GET["descripcion"]) and strlen($_GET["descripcion"])>0){ 
					$condiciones[] = " ".$this->TablaName.".descripcion LIKE '%".addslashes(strip_tags(trim($_GET["descripcion"])))."%' ";
				}
				if (isset($_GET["tipo"]) and strlen($_GET["tipo"])>0 and trim($_GET["tipo"])!='*'){ 
					$condiciones[] = " ".$this->TablaName.".tipo = ".trim($_GET["tipo"]);
				}
				$condiciones[] = " ".$this->TablaName.".owner = ".$this->id_user;				
				break;	
			case "geoarea":
				$condiciones = array();					
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["nombre"]) and strlen($_GET["nombre"])>0){ 
					$condiciones[] = " ".$this->TablaName.".nombre LIKE '%".addslashes(strip_tags(trim($_GET["nombre"])))."%' ";
				}
				if (isset($_GET["descripcion"]) and strlen($_GET["descripcion"])>0){ 
					$condiciones[] = " ".$this->TablaName.".descripcion LIKE '%".addslashes(strip_tags(trim($_GET["descripcion"])))."%' ";
				}
				$condiciones[] = " ".$this->TablaName.".owner = ".$this->id_user;				
				break;		
			case "areasControl":
				$condiciones = array();					
								
				//recivimos filtros y los asignamos a la cadena SQL
				if (isset($_GET["gps"]) and strlen($_GET["gps"])>0 and $_GET["gps"]!='*'){ 
					$condiciones[] = " ".$this->TablaName.".gps = ".addslashes(strip_tags(trim((int)$_GET["gps"]))); 
				}
				if (isset($_GET["geocontrol"]) and strlen($_GET["geocontrol"])>0 and $_GET["geocontrol"]!='*'){ 
					$condiciones[] = " ".$this->TablaName.".gps = ".addslashes(strip_tags(trim((int)$_GET["geocontrol"]))); 
				}
				if (isset($_GET["date1"]) and strlen($_GET["date1"])>0){ 
					$condiciones[] = " ".$this->TablaName.".fechaInicio >= '".addslashes(strip_tags(trim(substr($_GET["date1"],6,4).'-'.substr($_GET["date1"],3,2).'-'.substr($_GET["date1"],0,2).'T'.(($_GET["selectHI"]!="*")?$_GET["selectHI"]:'00').':'.(($_GET["selectMI"]!="*")?$_GET["selectMI"]:'00').':00')))."' ";
				}
				if (isset($_GET["date2"]) and strlen($_GET["date2"])>0){ 
					$condiciones[] = " ".$this->TablaName.".fechafin <= '".addslashes(strip_tags(trim(substr($_GET["date2"],6,4).'-'.substr($_GET["date2"],3,2).'-'.substr($_GET["date2"],0,2).'T'.(($_GET["selectHF"]!="*")?$_GET["selectHF"]:'00').':'.(($_GET["selectMF"]!="*")?$_GET["selectMF"]:'00').':00')))."' ";
				}
				if (isset($_GET["tipo"]) and strlen($_GET["tipo"])>0 and $_GET["tipo"]!='*'){ 
					$condiciones[] = " ".$this->TablaName.".tipo = ".addslashes(strip_tags(trim($_GET["tipo"])));
				}
				$condiciones[] = " ".$this->TablaName.".activo = 1";	
				$condiciones[] = " geoControl.owner = ".$this->id_user;				
				break;				
			case "ruta":
				$condiciones = array();
								
				//recivimos filtros y los asignamos a la cadena SQL											
				if (isset($_GET["nombre"]) and strlen($_GET["nombre"])>0){ 
					$condiciones[] = " ".$this->TablaName.".nombre LIKE '%".addslashes(strip_tags(trim($_GET["nombre"])))."%' ";
				}
				if (isset($_GET["descripcion"]) and strlen($_GET["descripcion"])>0){ 
					$condiciones[] = " ".$this->TablaName.".descripcion LIKE '%".addslashes(strip_tags(trim($_GET["descripcion"])))."%' ";
				}								
				$condiciones[] = " ".$this->TablaName.".owner = ".$this->id_user;					
				break;	
			case "rutaControl":
				$condiciones = array();
								
				//recivimos filtros y los asignamos a la cadena SQL											
				if (isset($_GET["idRuta"]) and strlen(trim($_GET["idRuta"]))>0 and trim($_GET["idRuta"])!="*"){ 
					$condiciones[] = " ".$this->TablaName.".idRuta = ".trim($_GET["idRuta"]);
				}	
				
				$condiciones[] = " RutaControl.owner = ".$this->id_user;															
				break;				
		}
		
		//creamos WHERE SQL 
		for ($a=0; $a<count($condiciones); $a++){
			if ($a == 0){ 
				$filtro .= " WHERE "; //si es el primero
			}
			
			$filtro .= $condiciones[$a];
			if ($a != count($condiciones) - 1){ 
				$filtro .= " AND ";  //mientras NO sea el ultimo
			}
		}
		
		return $filtro;
	}
	
	
	//saca los datos de las tablas y los procesa
	public function datosTabla($tabla){
		global $conn;
		$rs = New COM("ADODB.Recordset");
		$datos = array();
		$this->paginaNow();
		
		//metemos target
		$target = preg_replace("(page=([0-9]*))", "", preg_replace("(&page=([0-9]*))", "", $this->thisUrl));
		if (substr($target, strlen($target)-5)=='.php?'){ $target = substr($target, 0, -1); }
		$this->target($target);
							
		switch ($tabla){
			case "admin": //tabla de administradores
				$this->TablaName = "users";
				$sqlFiltro = $this->filtroTabla($tabla);
				
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.nombre, 
							newtbl.datos, 
							newtbl.login, 
							newtbl.nomAdmin, 
							newtbl.telefono, 
							newtbl.mail1, 							
							newtbl.cel1 
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".nombre, 
								".$this->TablaName.".datos, 
								".$this->TablaName.".login, 
								".$this->TablaName.".telefono, 
								".$this->TablaName.".mail1, 
								".$this->TablaName.".cel1, 
								adminTable.nombre AS nomAdmin 
							FROM ".$this->TablaName." 
							INNER JOIN ".$this->TablaName." adminTable 
							ON (adminTable.id = ".$this->TablaName.".admin)
							" .$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
				
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM ".$this->TablaName." 
							INNER JOIN ".$this->TablaName." adminTable 
							ON (adminTable.id = ".$this->TablaName.".admin)" .$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;	
			case "gps": //tabla de gps
				$this->TablaName = "GPs";
				$sqlFiltro = $this->filtroTabla($tabla);
				
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.serie, 
							newtbl.descripcion, 
							newtbl.activo, 
							newtbl.nombre, 
							newtbl.tipo, 
							newtbl.celular
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".serie, 
								".$this->TablaName.".descripcion, 
								".$this->TablaName.".activo, 
								users.nombre, 
								".$this->TablaName.".tipo, 
								".$this->TablaName.".celular
							FROM ".$this->TablaName." 
							INNER JOIN users 
							ON (users.id = ".$this->TablaName.".usr)
							" .$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
					
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM ".$this->TablaName." 
							INNER JOIN users 
							ON (users.id = ".$this->TablaName.".usr)" .$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;		
			case "userLow": //tabla de usuarios tipo 1
				$this->TablaName = "users";
				$sqlFiltro = $this->filtroTabla($tabla);
				
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.nombre, 
							newtbl.datos, 
							newtbl.login, 
							newtbl.nomAdmin, 
							newtbl.telefono, 
							newtbl.mail1, 							
							newtbl.cel1 
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".nombre, 
								".$this->TablaName.".datos, 
								".$this->TablaName.".login, 
								".$this->TablaName.".telefono, 
								".$this->TablaName.".mail1, 
								".$this->TablaName.".cel1, 
								adminTable.nombre AS nomAdmin 
							FROM ".$this->TablaName." 
							INNER JOIN ".$this->TablaName." adminTable 
							ON (adminTable.id = ".$this->TablaName.".admin)
							" .$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
				
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM ".$this->TablaName." 
							INNER JOIN ".$this->TablaName." adminTable 
							ON (adminTable.id = ".$this->TablaName.".admin)" .$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;	
			case "contrato": //tabla de contrato
				$this->limit(20);
				$this->TablaName = "contratos";
				$sqlFiltro = $this->filtroTabla($tabla);
								
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.finicio, 
							newtbl.ffin, 
							newtbl.numero, 
							newtbl.descripcion, 
							newtbl.nombre,
							newtbl.serie,
							newtbl.tipo,
							newtbl.idGPS,
							newtbl.celular
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".finicio, 
								".$this->TablaName.".ffin, 
								".$this->TablaName.".numero, 
								".$this->TablaName.".descripcion,
								users.nombre, 
								GPs.serie,
								GPs.id AS idGPS,
								GPs.tipo,
								GPs.celular
							FROM (((".$this->TablaName." 
							INNER JOIN users 
							ON (users.id = ".$this->TablaName.".usr))
							INNER JOIN gpscontratos
							ON (gpscontratos.contrato = ".$this->TablaName.".id))
							INNER JOIN GPs
							ON (GPs.id = gpscontratos.gps))
							" .$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 					
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
								
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM (((".$this->TablaName." 
							INNER JOIN users 
							ON (users.id = ".$this->TablaName.".usr))
							INNER JOIN gpscontratos
							ON (gpscontratos.contrato = ".$this->TablaName.".id))
							INNER JOIN GPs
							ON (GPs.id = gpscontratos.gps))
							" .$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;	
			case "contratoCerrado": //tabla de contratos cerrados
				$this->limit(20);
				$this->TablaName = "contratos";
				$sqlFiltro = $this->filtroTabla($tabla);
								
				//sacamos datos				
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.finicio, 
							newtbl.ffin, 
							newtbl.numero, 
							newtbl.descripcion, 
							newtbl.nombre,
							newtbl.serie,
							newtbl.tipo,
							newtbl.idGPS,
							newtbl.celular
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".finicio, 
								".$this->TablaName.".ffin, 
								".$this->TablaName.".numero, 
								".$this->TablaName.".descripcion,
								users.nombre, 
								GPs.serie,
								GPs.id AS idGPS,
								GPs.tipo,
								GPs.celular
							FROM (((".$this->TablaName." 
							INNER JOIN users 
							ON (users.id = ".$this->TablaName.".usr))
							INNER JOIN gpscontratos
							ON (gpscontratos.contrato = ".$this->TablaName.".id))
							INNER JOIN GPs
							ON (GPs.id = gpscontratos.gps))
							" .$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 					
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
								
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM (((".$this->TablaName." 
							INNER JOIN users 
							ON (users.id = ".$this->TablaName.".usr))
							INNER JOIN gpscontratos
							ON (gpscontratos.contrato = ".$this->TablaName.".id))
							INNER JOIN GPs
							ON (GPs.id = gpscontratos.gps))
							" .$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;	
			case "viajes": //tabla de viajes
				$this->TablaName = "viajes";
				$sqlFiltro = $this->filtroTabla($tabla);		
				
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.idviaje, 
							newtbl.clave, 
							newtbl.serie, 
							newtbl.descripcion, 
							newtbl.origen, 
							newtbl.destino, 
							newtbl.mercancia, 	
							newtbl.nombreRuta, 							
							newtbl.finicial 
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".idviaje, 
								".$this->TablaName.".clave, 
								GPs.serie, 
								GPs.descripcion,
								".$this->TablaName.".origen, 
								".$this->TablaName.".destino, 
								".$this->TablaName.".mercancia, 
								RutaControl.nombre AS nombreRuta, 
								".$this->TablaName.".finicial
							FROM (".$this->TablaName." 
							INNER JOIN GPs
							ON (GPs.id = ".$this->TablaName.".gps))
								LEFT JOIN RutaControl
								ON (RutaControl.id = ".$this->TablaName.".ruta)
							" .$sqlFiltro . "
							ORDER BY ".$this->TablaName.".idviaje ASC)as newtbl 
						ORDER BY newtbl.idviaje DESC)as newtbl2 
					ORDER BY newtbl2.idviaje ASC", $conn); 
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
											
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM (".$this->TablaName." 
							INNER JOIN GPs
							ON (GPs.id = ".$this->TablaName.".gps))
								LEFT JOIN RutaControl
								ON (RutaControl.id = ".$this->TablaName.".ruta)
							" .$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;			
			case "historial": //tabla de viajes cerrados
				$this->TablaName = "viajes";
				$sqlFiltro = $this->filtroTabla($tabla);		
										
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.idviaje, 
							newtbl.clave, 
							newtbl.serie, 
							newtbl.owner, 
							newtbl.nombreRuta, 
							newtbl.descripcion, 
							newtbl.origen, 
							newtbl.destino, 
							newtbl.mercancia, 							
							newtbl.finicial,
							newtbl.ffinal 
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".idviaje, 
								".$this->TablaName.".clave, 
								GPs.serie, 
								GPs.descripcion,
								RutaControl.nombre AS nombreRuta, 
								RutaControl.owner,
								".$this->TablaName.".origen, 
								".$this->TablaName.".destino, 
								".$this->TablaName.".mercancia, 
								".$this->TablaName.".finicial,
								".$this->TablaName.".ffinal
							FROM (".$this->TablaName." 
							INNER JOIN GPs
							ON (GPs.id = ".$this->TablaName.".gps))
								LEFT JOIN RutaControl
								ON (RutaControl.id = ".$this->TablaName.".ruta)
							" .$sqlFiltro . "
							ORDER BY ".$this->TablaName.".idviaje ASC)as newtbl 
						ORDER BY newtbl.idviaje DESC)as newtbl2 
					ORDER BY newtbl2.idviaje ASC", $conn); 
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
											
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM (".$this->TablaName." 
							INNER JOIN GPs
							ON (GPs.id = ".$this->TablaName.".gps))
								LEFT JOIN RutaControl
								ON (RutaControl.id = ".$this->TablaName.".ruta)
							" .$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;			
			case "geocerca": //tabla de geocercas
				$this->limit(12);
				$this->TablaName = "geocercas";
				$sqlFiltro = $this->filtroTabla($tabla);		
															
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.nombre, 
							newtbl.descripcion, 
							newtbl.longitud, 
							newtbl.latitud, 
							newtbl.tipo, 							
							newtbl.icono
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".nombre, 
								".$this->TablaName.".descripcion, 
								".$this->TablaName.".longitud, 
								".$this->TablaName.".latitud, 
								geocercastipo.tipo,
								geocercastipo.icono
							FROM ".$this->TablaName." 
							INNER JOIN geocercastipo
							ON (geocercastipo.id = ".$this->TablaName.".tipo)
							" .$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
											
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM ".$this->TablaName." 
							INNER JOIN geocercastipo
							ON (geocercastipo.id = ".$this->TablaName.".tipo)
							" .$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;	
			case "geoarea": //tabla de geocercas
				$this->limit(12);
				$this->TablaName = "geoControl";
				$sqlFiltro = $this->filtroTabla($tabla);		
															
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.nombre, 
							newtbl.descripcion
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".nombre, 
								".$this->TablaName.".descripcion
							FROM ".$this->TablaName
							.$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
											
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM ".$this->TablaName
							.$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;			
			case "areasControl": //tabla de areasControl
				$this->limit(12);
				$this->TablaName = "geoControlT3";
				$sqlFiltro = $this->filtroTabla($tabla);		
															
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.fechaInicio, 
							newtbl.activo,
							newtbl.fechafin,
							newtbl.tipo,
							newtbl.nombreGeoControl,
							newtbl.modulo
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".fechaInicio, 
								".$this->TablaName.".activo,
								".$this->TablaName.".fechafin, 
								".$this->TablaName.".tipo, 
								geoControl.nombre as nombreGeoControl,
								GPs.serie as modulo
							FROM ".$this->TablaName." 
								INNER JOIN geoControl ON (geoControl.id = ".$this->TablaName.".geocontrol) 
								INNER JOIN GPs ON (GPs.id = ".$this->TablaName.".gps) "
							.$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
											
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM ".$this->TablaName." 
								INNER JOIN geoControl ON (geoControl.id = ".$this->TablaName.".geocontrol) 
								INNER JOIN GPs ON (GPs.id = ".$this->TablaName.".gps) "
							.$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;		
			case "ruta": //tabla de rutas
				$this->limit(20);
				$this->TablaName = "RutaControl";
				$sqlFiltro = $this->filtroTabla($tabla);
									
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.nombre, 
							newtbl.descripcion
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".nombre, 
								".$this->TablaName.".descripcion
							FROM ".$this->TablaName
							.$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 					
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
								
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM ".$this->TablaName
							.$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;		
			case "rutaControl": //tabla de ruta de control				
				$this->TablaName = "RutaGeocercas";
				$sqlFiltro = $this->filtroTabla($tabla);
								
				//sacamos datos
				$rs->Open("
					SELECT * FROM (
						SELECT TOP " . $this->limit . " 
							newtbl.id, 
							newtbl.idRuta,
							newtbl.nombre, 
							newtbl.descripcion,
							newtbl.geoNombre,
							newtbl.geoDescripcion,
							newtbl.secuencia,
							newtbl.tipo
						FROM (
							SELECT TOP " . ($this->page * $this->limit) ." 
								".$this->TablaName.".id, 
								".$this->TablaName.".idRuta, 
								".$this->TablaName.".secuencia,
								RutaControl.nombre, 
								RutaControl.descripcion, 
								geocercas.nombre AS geoNombre,
								geocercas.descripcion AS geoDescripcion,
								geocercastipo.tipo
							FROM ((".$this->TablaName." 
							INNER JOIN RutaControl
							ON (RutaControl.id = ".$this->TablaName.".idRuta))
								INNER JOIN geocercas
								ON (geocercas.id = ".$this->TablaName.".idGeocerca))
									INNER JOIN geocercastipo
									ON (geocercastipo.id = geocercas.tipo)
							".$sqlFiltro . "
							ORDER BY ".$this->TablaName.".id ASC)as newtbl 
						ORDER BY newtbl.id DESC)as newtbl2 
					ORDER BY newtbl2.id ASC", $conn); 					
				$this->datosTabla = fetch_assoc($rs); 
				$rs->Close();
								
				//sacamos total de datos
				$rs->Open(" SELECT count(*) as total 
							FROM ((".$this->TablaName." 
							INNER JOIN RutaControl
							ON (RutaControl.id = ".$this->TablaName.".idRuta))
								INNER JOIN geocercas
								ON (geocercas.id = ".$this->TablaName.".idGeocerca))
									INNER JOIN geocercastipo
									ON (geocercastipo.id = geocercas.tipo)
							".$sqlFiltro, $conn); 
				$totalDatos = fetch_assoc($rs); 
				$this->Items($totalDatos[0]["total"]);
				$rs->Close();	
				
				break;		
		}//end switch   
		
		//imprimimos tabla
		$this->imprimirTabla($tabla);
		
		if ($totalDatos > 0){ 
			$this->show(); 
		}
	}
	
	
	//imprime los datos de las tablas
	private function imprimirTabla($tabla){
		switch ($tabla){
			case "admin":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Nombre</td>
						<td>Login</td>
						<td>Datos</td>
						<td>Creado por</td>
						<td>Telefono</td>
						<td>Correo 1</td>
						<td>Celular 1</td>
						<td></td>
					</tr>';
				for ($a=0; $a<count($this->datosTabla); $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.'" onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">
						<td>'.$this->datosTabla[$a]['nombre'].'</td>
						<td>'.$this->datosTabla[$a]['login'].'</td>
						<td>'.$this->datosTabla[$a]['datos'].'</td>
						<td>'.$this->datosTabla[$a]['nomAdmin'].'</td>
						<td>'.$this->datosTabla[$a]['telefono'].'</td>
						<td>'.$this->datosTabla[$a]['mail1'].'</td>
						<td>'.$this->datosTabla[$a]['cel1'].'</td>
					</tr>';
				}//end for
							
				echo '</table>';
				break;
			case "gps":		
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Serie</td>
						<td>Descripcion</td>
						<td>Activo</td>
						<td>Usuario</td>
						<td>Tipo</td>
						<td>Celular</td>
						<td></td>
					</tr>';
				for ($a=0; $a<count($this->datosTabla); $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.' rowGps_'.$this->datosTabla[$a]['serie'].'">
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">'.$this->datosTabla[$a]['serie'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">'.$this->datosTabla[$a]['descripcion'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">'.(($this->datosTabla[$a]['activo'] == 1)?'Si':'No').'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">'.$this->datosTabla[$a]['nombre'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">'.(($this->datosTabla[$a]['tipo'] == 1)?'Autonomo':'Regulador').'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">'.$this->datosTabla[$a]['celular'].'</td>
						<td class="tdImgMasMin_'.$this->datosTabla[$a]['serie'].'"><img class="btnHistorial" id="btnHistorial_'.$this->datosTabla[$a]['serie'].'" src="Imagenes/plus.gif" title="Ver Historial" onclick="verHistorial('.$this->datosTabla[$a]['serie'].');" /></td>
					</tr>';
				}//end for
							
				echo '</table>';
				break;
			case "userLow":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Nombre</td>
						<td>Login</td>
						<td>Datos</td>
						<td>Creado por</td>
						<td>Telefono</td>
						<td>Correo 1</td>
						<td>Celular 1</td>
						<td></td>
					</tr>';
				for ($a=0; $a<count($this->datosTabla); $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.'" onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">
						<td>'.$this->datosTabla[$a]['nombre'].'</td>
						<td>'.$this->datosTabla[$a]['login'].'</td>
						<td>'.$this->datosTabla[$a]['datos'].'</td>
						<td>'.$this->datosTabla[$a]['nomAdmin'].'</td>
						<td>'.$this->datosTabla[$a]['telefono'].'</td>
						<td>'.$this->datosTabla[$a]['mail1'].'</td>
						<td>'.$this->datosTabla[$a]['cel1'].'</td>
					</tr>';
				}//end for
							
				echo '</table>';
				break;
			case "contrato":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Id</td>
						<td>Numero</td>
						<td>Fecha Inicial</td>
						<td>Usuario</td>						
						<td>Descripci&oacute;n</td>					
						<td></td>
					</tr>';
				$totFilas = count($this->datosTabla);
				for ($a=0; $a<$totFilas; $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable1">
						<td>'.$this->datosTabla[$a]['id'].'</td>
						<td>'.$this->datosTabla[$a]['numero'].'</td>
						<td>'.$this->datosTabla[$a]['finicio'].'</td>
						<td>'.$this->datosTabla[$a]['nombre'].'</td>						
						<td>'.substr($this->datosTabla[$a]['descripcion'],0,30).((strlen($this->datosTabla[$a]['descripcion'])>30)?'...':'').'</td>						
						<td><img border="0" src="Imagenes/script_delete.png" title="Cerrar Contrato" onClick="javascript: verTxtContratoEnd('.$this->datosTabla[$a]['id'].');"/></td>
					</tr>';
					
					$band=0;
					while ($band==0 or ($a<$totFilas and $this->datosTabla[$a]['id'] == $this->datosTabla[$a-1]['id'])){
						$isLast = ((($a==$totFilas-1) or ($a+1<=$totFilas-1 and $this->datosTabla[$a]['id'] != $this->datosTabla[$a+1]['id']))?true:false);
						echo '
						<tr class="filGPSContr'.(($isLast)?' isLast':'').'" id="'.$this->datosTabla[$a]['idGPS'].'">
							<td class="'.(($isLast)?'imgPoints':'imgPointsLong').'"></td>
							<td>'.$this->datosTabla[$a]['serie'].'</td>
							<td>'.(($this->datosTabla[$a]['tipo']==8)?'Regulador':'Autonomo').'</td>
							<td colspan="2">'.$this->datosTabla[$a]['celular'].'</td>
							<td><img border="0" src="Imagenes/cross.png" title="Quitar GPS del Contrato" onClick="javascript: quitarGps('.$this->datosTabla[$a]['id'].', '.$this->datosTabla[$a]['idGPS'].');"/></td>										
						</tr>'; //fin de fila	
						$band = 1;
						++$a;
					}
					--$a;
				}//end for
							
				echo '</table>';
				break;
			case "contratoCerrado":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Id</td>
						<td>Numero</td>
						<td>Fecha Inicial</td>
						<td>Fecha Final</td>
						<td>Usuario</td>						
						<td>Descripci&oacute;n</td>					
					</tr>';
				$totFilas = count($this->datosTabla);
				for ($a=0; $a<$totFilas; $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable1">
						<td>'.$this->datosTabla[$a]['id'].'</td>
						<td>'.$this->datosTabla[$a]['numero'].'</td>
						<td>'.$this->datosTabla[$a]['finicio'].'</td>
						<td>'.$this->datosTabla[$a]['ffin'].'</td>
						<td>'.$this->datosTabla[$a]['nombre'].'</td>						
						<td>'.substr($this->datosTabla[$a]['descripcion'],0,30).((strlen($this->datosTabla[$a]['descripcion'])>30)?'...':'').'</td>						
					</tr>';
					
					$band=0;
					while ($band==0 or ($a<$totFilas and $this->datosTabla[$a]['id'] == $this->datosTabla[$a-1]['id'])){
						$isLast = ((($a==$totFilas-1) or ($a+1<=$totFilas-1 and $this->datosTabla[$a]['id'] != $this->datosTabla[$a+1]['id']))?true:false);
						echo '
						<tr class="filGPSContr'.(($isLast)?' isLast':'').'" id="'.$this->datosTabla[$a]['idGPS'].'">
							<td class="'.(($isLast)?'imgPoints':'imgPointsLong').'"></td>
							<td>'.$this->datosTabla[$a]['serie'].'</td>
							<td>'.(($this->datosTabla[$a]['tipo']==8)?'Regulador':'Autonomo').'</td>
							<td colspan="3">'.$this->datosTabla[$a]['celular'].'</td>																
						</tr>'; //fin de fila	
						$band = 1;
						++$a;
					}
					--$a;
				}//end for
							
				echo '</table>';
				break;
			case "viajes":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Clave</td>
						<td>Modulo</td>
						<td>Fecha Inicial</td>
						<td>Ruta de Control</td>
						<td>Origen</td>
						<td>Destino</td>
						<td>Mercancia</td>	
						<td></td>
						<td></td>
						<td></td>	
						<td></td>	
						<td></td>					
					</tr>';
				for ($a=0; $a<count($this->datosTabla); $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.'">
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['idviaje'].');">'.$this->datosTabla[$a]['clave'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['idviaje'].');">'.$this->datosTabla[$a]['descripcion'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['idviaje'].');">'.$this->datosTabla[$a]['finicial'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['idviaje'].');">'.$this->datosTabla[$a]['nombreRuta'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['idviaje'].');">'.$this->datosTabla[$a]['origen'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['idviaje'].');">'.$this->datosTabla[$a]['destino'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['idviaje'].');">'.$this->datosTabla[$a]['mercancia'].'</td>
						<td><img border="0" src="Imagenes/lorry_delete.png" title="Cerrar Viaje" onClick="javascript: verTxtViajeEnd('.$this->datosTabla[$a]['idviaje'].');"/></td>
						<td><img border="0" src="Imagenes/posActual.png" title="Ver Posici&oacute;n Actual en Google Maps" onClick="javascript: verPosActInGM('.$this->datosTabla[$a]['idviaje'].');"/></td>
						<td><img border="0" src="Imagenes/rutaCompleta.png" title="Ver Recorrido Actual en Google Maps" onClick="javascript: verFormVelTmToGM('.$this->datosTabla[$a]['idviaje'].',1);"/></td>
						<td><a href="XMLPosActualViaje.php?idviaje='.$this->datosTabla[$a]['idviaje'].'"><img border="0" src="Imagenes/posActualEarth.png" title="Descargar Posici&oacute;n Actual a Google Earth"/></a></td>
						<td><img border="0" src="Imagenes/rutaAllEarth.png" title="Descargar Recorrido Actual a Google Earth" onClick="javascript: verFormVelTm('.$this->datosTabla[$a]['idviaje'].');" /></td>
					</tr>';
				}//end for
							
				echo '</table>';
				break;
			case "historial":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Clave</td>
						<td>Modulo</td>
						<td>Fecha Inicial</td>
						<td>Fecha Final</td>
						<td>Ruta de Control</td>
						<td>Origen</td>
						<td>Destino</td>
						<td>Mercancia</td>	
						<td></td>
						<td></td>
						<td></td>					
					</tr>';
				for ($a=0; $a<count($this->datosTabla); $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.'">
						<td>'.$this->datosTabla[$a]['clave'].'</td>
						<td>'.$this->datosTabla[$a]['descripcion'].'</td>
						<td>'.$this->datosTabla[$a]['finicial'].'</td>
						<td>'.$this->datosTabla[$a]['ffinal'].'</td>
						<td>'.$this->datosTabla[$a]['nombreRuta'].'</td>
						<td>'.$this->datosTabla[$a]['origen'].'</td>
						<td>'.$this->datosTabla[$a]['destino'].'</td>
						<td>'.$this->datosTabla[$a]['mercancia'].'</td>						
						<td><img border="0" src="Imagenes/rutaCompleta.png" title="Ver Recorrido en Google Maps" onClick="javascript: verFormVelTmToGMForClosed('.$this->datosTabla[$a]['idviaje'].',1);"/></td>						
						<td><img border="0" src="Imagenes/rutaAllEarth.png" title="Descargar Recorrido a Google Earth" onClick="javascript: verFormVelTmForClosed('.$this->datosTabla[$a]['idviaje'].');" /></td>
						<td>'.((strlen(trim($this->datosTabla[$a]['owner'])) > 0)?'<img border="0" src="Imagenes/reporte.png" title="Ver Reporte" onClick="javascript: verReporteViajesCerrados('.$this->datosTabla[$a]['idviaje'].');" />':'').'</td>
					</tr>';
				}//end for 
							
				echo '</table>';
				break;
			case "geocerca":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Id</td>
						<td>Nombre</td>
						<td>Descripci&oacute;n</td>
						<td>Tipo</td>
						<td>Longitud</td>
						<td>Latitud</td>
						<td>Icono</td>	
						<td></td>	
						<td></td>					
					</tr>';
				for ($a=0; $a<count($this->datosTabla); $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.'">
						<td>'.$this->datosTabla[$a]['id'].'</td>
						<td>'.$this->datosTabla[$a]['nombre'].'</td>
						<td>'.$this->datosTabla[$a]['descripcion'].'</td>
						<td>'.$this->datosTabla[$a]['tipo'].'</td>
						<td>'.$this->datosTabla[$a]['longitud'].'</td>
						<td>'.$this->datosTabla[$a]['latitud'].'</td>
						<td><img style="cursor:default;" width="16" src="'.((strlen(trim($this->datosTabla[$a]['icono']))>0)?$this->datosTabla[$a]['icono']:'Imagenes/flagG.png').'" title="Icono" /></td>						
						<td><img border="0" src="Imagenes/googleEarth.png" title="Ver en Mapa" onClick="javascript: verEnMapa('.$this->datosTabla[$a]['latitud'].', '.$this->datosTabla[$a]['longitud'].', \''.addslashes($this->datosTabla[$a]['nombre']).'\');"/></td>	
						<td><img border="0" src="Imagenes/edit.png" title="Modificar Base '.$this->datosTabla[$a]['id'].'" onClick="javascript: modBaseInGm('.$this->datosTabla[$a]['id'].');"/></td>												
					</tr>';
				}//end for
							
				echo '</table>';
				break;
			case "geoarea":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Id</td>
						<td>Nombre</td>
						<td>Descripci&oacute;n</td>
						<td></td>	
						<td></td>					
					</tr>';
				for ($a=0; $a<count($this->datosTabla); $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.'">
						<td>'.$this->datosTabla[$a]['id'].'</td>
						<td>'.$this->datosTabla[$a]['nombre'].'</td>
						<td>'.$this->datosTabla[$a]['descripcion'].'</td>					
						<td><img border="0" src="Imagenes/googleEarth.png" title="Ver en Mapa" onClick="javascript: verEnMapa('.$this->datosTabla[$a]['id'].');"/></td>	
						<td><img border="0" src="Imagenes/edit.png" title="Modificar Geo&aacute;rea '.$this->datosTabla[$a]['id'].'" onClick="javascript: modGAInGm('.$this->datosTabla[$a]['id'].');"/></td>												
					</tr>';
				}//end for
							
				echo '</table>';
				break;
			case "areasControl":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Id</td>
						<td>Modulo</td>
						<td>Geo&aacute;rea</td>
						<td>Fecha Inicial</td>					
						<td>Fecha Final</td>	
						<td>tipo</td>	
						<td></td>	
						<td></td>
						<td></td>				
					</tr>';
				$totFilas = count($this->datosTabla);
				for ($a=0; $a<$totFilas; $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.'">
						<td>'.$this->datosTabla[$a]['id'].'</td>
						<td>'.$this->datosTabla[$a]['modulo'].'</td>
						<td>'.$this->datosTabla[$a]['nombreGeoControl'].'</td>
						<td>'.$this->datosTabla[$a]['fechaInicio'].'</td>
						<td>'.$this->datosTabla[$a]['fechafin'].'</td>
						<td>'.(($this->datosTabla[$a]['tipo'] == '1')?'Entrada':'Salida').'</td>
						<td><img border="0" src="Imagenes/reporte.png" title="Ver Reporte del &Aacute;rea de Control '.$this->datosTabla[$a]['id'].'" onClick="javascript: verReporteAreaControl('.$this->datosTabla[$a]['id'].');"/></td>
						<td><img border="0" src="Imagenes/edit.png" title="Modificar &Aacute;rea de Control '.$this->datosTabla[$a]['id'].'" onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');"/></td>	
						<td><img border="0" src="Imagenes/cross.png" title="Eliminar &Aacute;rea de Control '.$this->datosTabla[$a]['id'].'" onClick="if (confirm(\'Esta seguro que desea eliminar el &Aacute;rea de Control '.$this->datosTabla[$a]['id'].'?\')){location.href=\'userQuitarAreaControl.php?a='.$this->datosTabla[$a]['id'].'\';}"/></td>	
					</tr>';					
				}//end for
							
				echo '</table>';
				break;
			case "ruta":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Id</td>
						<td>Nombre</td>					
						<td>Descripci&oacute;n</td>	
						<td></td>				
					</tr>';
				$totFilas = count($this->datosTabla);
				for ($a=0; $a<$totFilas; $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.'">
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">'.$this->datosTabla[$a]['id'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">'.$this->datosTabla[$a]['nombre'].'</td>
						<td onClick="javascript: modificar(\''.$tabla.'\', '.$this->datosTabla[$a]['id'].');">'.$this->datosTabla[$a]['descripcion'].'</td>
						<td><img border="0" src="Imagenes/cross.png" title="Eliminar Ruta '.str_replace("'","",$this->datosTabla[$a]['nombre']).'" onClick="if (confirm(\'Esta seguro que desea eliminar la Ruta '.str_replace("'","",$this->datosTabla[$a]['nombre']).'?\')){location.href=\'userDelRutas.php?a='.$this->datosTabla[$a]['id'].'\';}"/></td>	
					</tr>';					
				}//end for
							
				echo '</table>';
				break;
			case "rutaControl":
				echo '
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>Id</td>
						<td>Secuencia</td>
						<td>Ruta</td>					
						<td>Descripci&oacute;n Ruta</td>		
						<td>Geocerca</td>	
						<td>Descripci&oacute;n Geocerca</td>	
						<td>Tipo</td>	
						<td></td>	
						<td></td>				
					</tr>';
				$totFilas = count($this->datosTabla);
				for ($a=0; $a<$totFilas; $a++){
					$mod = $a%2;
					echo '
					<tr class="bodyTable'.$mod.'">
						<td>'.$this->datosTabla[$a]['id'].'</td>
						<td>'.($this->datosTabla[$a]['secuencia']+1).'</td>
						<td>'.$this->datosTabla[$a]['nombre'].'</td>
						<td>'.$this->datosTabla[$a]['descripcion'].'</td>
						<td>'.$this->datosTabla[$a]['geoNombre'].'</td>
						<td>'.$this->datosTabla[$a]['geoDescripcion'].'</td>
						<td>'.$this->datosTabla[$a]['tipo'].'</td>
						<td><img border="0" src="Imagenes/edit.png" title="Modificar Ruta de Control '.$this->datosTabla[$a]['id'].'" onClick="location.href=\'userModRutasControl.php?a='.$this->datosTabla[$a]['idRuta'].'\';"/></td>	
						<td><img border="0" src="Imagenes/cross.png" title="Eliminar Ruta de Control '.$this->datosTabla[$a]['id'].'" onClick="if (confirm(\'Esta seguro que desea eliminar la Ruta de Control '.str_replace("'","",$this->datosTabla[$a]['nombre']).'?\')){location.href=\'userDelRutasControl.php?a='.$this->datosTabla[$a]['idRuta'].'\';}"/></td>	
					</tr>';					
				}//end for
							
				echo '</table>';
				break;
		}
	}
	
	
	//actualizamos datos del usuario enviados desde un form
	public function updateDatosUsr(){
		if (isset($_POST["enviar"]) and $_POST["enviar"] == "Modificar Datos"){			
			$datos["nombre"] = 		addslashes(strip_tags(trim($_POST["nombre"])));
			$datos["pwd"] = 		addslashes(strip_tags(trim($_POST["pass"])));
			$datos["md5"] = 		md5($datos["pwd"]);
			$datos["datos"] = 		addslashes(strip_tags(trim($_POST["datos"])));
			$datos["telefono"] = 	addslashes(strip_tags(trim($_POST["tel"])));
			$datos["mail1"] = 		addslashes(strip_tags(trim($_POST["email"])));
			$datos["mail2"] = 		addslashes(strip_tags(trim($_POST["email2"])));
			$datos["mail3"] = 		addslashes(strip_tags(trim($_POST["email3"])));
			$datos["cel1"] = 		addslashes(strip_tags(trim($_POST["cel"])));
			$datos["cel2"] = 		addslashes(strip_tags(trim($_POST["cel2"])));
			
			//actualizamos pass guadado en coockie
			setcookie("Pass", $datos["md5"], time() + 86400);
			
			global $conn, $rc;
			$rc->CommandText = "UPDATE users SET nombre = '".$datos["nombre"]."', pwd = '".$datos["pwd"]."', md5 = '".$datos["md5"]."', datos = '".$datos["datos"]."', telefono = '".$datos["telefono"]."', mail1 = '".$datos["mail1"]."', mail2 = '".$datos["mail2"]."', mail3 = '".$datos["mail3"]."', cel1 = '".$datos["cel1"]."', cel2 = '".$datos["cel2"]."'	WHERE id = ".$this->id_user;
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
			
			msj("i", 10, "Datos actualizados satisfactoriamente!");
		}
	}
		
	
	//imprime un form con los datos del user
	public function misDatos(){
		$this->updateDatosUsr();
		
		global $conn, $rs;

		//sacamos datos del user
		$rs->Open(" SELECT * FROM users WHERE id = ".$this->id_user, $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();	
		
		echo '
		<form name="FormUser" method="post" action="'.(($this->tipo_user=='1')?'adminMisDatos.php':'userMisDatos.php').'"  onsubmit="return validarFormUser(document.FormUser);">
			<div class="divForm">
				<div class="Fseleccion">
					<label class="Flbl">Nombre</label><br/>
					<input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="30" value="'.$datos[0]["nombre"].'" />
				</div>
				<div class="Fseleccion">
					<label class="Flbl">Contrase&ntilde;a</label><br/>
					<input class="Ftxt" type="password" name="pass" id="pass" maxlength="10" value="'.$datos[0]["pwd"].'" />
				</div>
				<div class="Fseleccion">
					<label class="Flbl">Re-Contrase&ntilde;a</label><br/>
					<input class="Ftxt" type="password" name="repass" id="repass" maxlength="10" value="'.$datos[0]["pwd"].'" />
				</div>
				<div class="Fseleccion">
					<label class="Flbl">Datos Adicionales</label><br/>
					<input class="Ftxt" type="text" name="datos" id="datos" maxlength="50" value="'.$datos[0]["datos"].'" />
				</div>
				<div class="Fseleccion">
					<label class="Flbl">Telefono</label><br/>
					<input class="Ftxt" type="text" name="tel" id="tel" maxlength="12" value="'.$datos[0]["telefono"].'" />
				</div>
				<div class="Fseleccion">
					<label class="Flbl">Correo</label><br/>
					<input class="Ftxt" type="text" name="email" id="email" maxlength="50" value="'.$datos[0]["mail1"].'" />
				</div>
				<div class="Fseleccion">
					<label class="Flbl">Correo 2 (Opcional)</label><br/>
					<input class="Ftxt" type="text" name="email2" id="email2" maxlength="50" value="'.$datos[0]["mail2"].'" />
				</div>
				<div class="Fseleccion">
					<label class="Flbl">Correo 3 (Opcional)</label><br/>
					<input class="Ftxt" type="text" name="email3" id="email3" maxlength="50" value="'.$datos[0]["mail3"].'" />
				</div>
				<div class="Fseleccion">
					<label class="Flbl">Celular</label><br/>
					<input type="text" class="Ftxt" name="cel" id="cel" maxlength="12" value="'.$datos[0]["cel1"].'" />
				</div>
				<div class="Fseleccion">
					<label class="Flbl">Celular 2 (Opcional)</label><br/>
					<input type="text" class="Ftxt" name="cel2" id="cel2" maxlength="12" value="'.$datos[0]["cel2"].'" />
				</div>
				<div class="Fseleccion">
					<div class="divButton">
						<input type="submit" name="enviar" value="Modificar Datos" />
					</div>
				</div>
			</div>
		</form>';
	}	
	
	
	//imprime el ultimo dato de cada gps
	public function statusGps(){
		global $conn, $rs;

		//sacamos datos del user
		$rs->Open("	SELECT 
						gps.serie,
						gps.descripcion,
						tbl1.fechasend,
						tbl1.fecharecv,
						tbl1.bateria,
						tbl1.valido
					FROM(
						SELECT *
						FROM datosghe
						WHERE(
							id IN(
								SELECT ID
								FROM ultimaTransmisiondatosGhe() ultimaTransmisiondatosGhe
							)
						)AND(
							gps IN(
								SELECT serie
								FROM gps
								WHERE usr = $this->id_user
							)
						)
					) AS tbl1				
					INNER JOIN gps
					ON (gps.serie = tbl1.gps)", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();	
				
		echo '
		<table cellpadding="0" cellspacing="0">
			<tr class="headTabla">
				<td>Serie</td>
				<td>Fecha Envio</td>
				<td>Fecha Recibido</td>
				<td>Bater&iacute;a</td>
				<td>Valido</td>
				<td>Descripci&oacute;n</td>
			</tr>';
		for ($a=0; $a<count($datos); $a++){
			$mod = $a%2;
			echo '
			<tr class="bodyTable'.$mod.'">
				<td>'.$datos[$a]['serie'].'</td>
				<td>'.$datos[$a]['fechasend'].'</td>
				<td>'.$datos[$a]['fecharecv'].'</td>
				<td>'.$datos[$a]['bateria'].'</td>
				<td>'.(($datos[$a]['valido'])?'Si':'No').'</td>
				<td>'.$datos[$a]['descripcion'].'</td>
			</tr>';
		}//end for
					
		echo '</table>';
	}
	
	
	//agrega una geocerca a la base de datos
	public function addGeoToDB(){			
		//recivimos datos
		$nombre = addslashes(strip_tags(trim($_POST["nombre"])));
		$tipo = $_POST["tipo"];
		
		$descripcion = "";
		if (isset($_POST["descripcion"]) and strlen($_POST["descripcion"])>0){
			$descripcion = addslashes(strip_tags(trim($_POST["descripcion"])));		
		}
				
		//coordenadas del centro y radio
		$latCentro = (float)trim($_POST["latCentro"]);
		$lonCentro = (float)trim($_POST["lonCentro"]);
		$latRadio = (float)trim($_POST["latRadio"]);
		$lonRadio = (float)trim($_POST["lonRadio"]);
		
		global $conn, $rc; 		
		$rc->CommandText = "INSERT INTO geocercas (nombre, owner, descripcion, tipo, longitud, latitud, radiolat, radiolon) VALUES ('$nombre', $this->id_user, '$descripcion', $tipo, $lonCentro, $latCentro, $latRadio, $lonRadio)";
		$rc->CommandType = 1;
		$rc->ActiveConnection = $conn;
		$rc->Execute;
		
		echo "OK";
	}
	
	
	//agrega una geoareas a la base de datos
	public function addGeoAreaToDB(){			
		//recivimos datos
		$nombre = addslashes(strip_tags(trim($_POST["nombre"])));
		
		$descripcion = "";
		if (isset($_POST["descripcion"]) and strlen($_POST["descripcion"])>0){
			$descripcion = addslashes(strip_tags(trim($_POST["descripcion"])));		
		}
				
		//coordenadas de los puntos
		$puntos = array();
		if (isset($_POST["puntos"]{0})){
			$allPuntos = split("#", trim($_POST["puntos"]));
			
			for ($a = 0; $a < count($allPuntos); $a++){
				list($puntos[$a]["lat"], $puntos[$a]["lng"]) = split("_", $allPuntos[$a]);
			}
		}
		
		global $conn, $rc; 
		
		//guardamos geoarea		
		$rc->CommandText = "INSERT INTO geoControl (nombre, owner, descripcion) VALUES ('$nombre', $this->id_user, '$descripcion')";
		$rc->CommandType = 1;
		$rc->ActiveConnection = $conn;
		$rc->Execute;
		
		//obtenemos id de esta geoarea		
		$rs = New COM("ADODB.Recordset");
		$rs->Open("SELECT MAX(id) as idGeoarea FROM geoControl", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		//guardamos puntos de la geoarea	
		for ($a = 0; $a < count($puntos); $a++){	
			$rc->CommandText = "INSERT INTO geoControlData (geocontrol, secuencia, latitud, longitud) VALUES (".$datos[0]["idGeoarea"].", $a, ".$puntos[$a]["lat"].", ".$puntos[$a]["lng"].")";
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
		}
				
		echo "OK";
	}
	
	
	//modifica una geocerca a la base de datos
	public function modGeoInDB(){			
		//recivimos datos
		$nombre = addslashes(strip_tags(trim($_POST["nombre"])));
		$id = (float)trim($_POST["idBase"]);
		$tipo = (int)$_POST["tipo"];
		
		$descripcion = "";
		if (isset($_POST["descripcion"]) and strlen($_POST["descripcion"])>0){
			$descripcion = addslashes(strip_tags(trim($_POST["descripcion"])));		
		}
				
		//coordenadas del centro y radio
		$latCentro = (float)trim($_POST["latCentro"]);
		$lonCentro = (float)trim($_POST["lonCentro"]);
		$latRadio = (float)trim($_POST["latRadio"]);
		$lonRadio = (float)trim($_POST["lonRadio"]);
		
		global $conn, $rc;
		$rc->CommandText = "UPDATE geocercas SET nombre = '$nombre', owner = $this->id_user, descripcion = '$descripcion', tipo = ".$tipo.", longitud = $lonCentro, latitud = $latCentro, radiolat = $latRadio, radiolon = $lonRadio WHERE id = $id";
		$rc->CommandType = 1;
		$rc->ActiveConnection = $conn;
		$rc->Execute;
		
		echo "OK";
	}
	
	
	//modifica una geoarea a la base de datos
	public function modGeoAreaInDB(){			
		//recivimos datos
		$nombre = addslashes(strip_tags(trim($_POST["nombre"])));
		$id = trim($_POST["idGA"]);
		
		$descripcion = "";
		if (isset($_POST["descripcion"]) and strlen($_POST["descripcion"])>0){
			$descripcion = addslashes(strip_tags(trim($_POST["descripcion"])));		
		}
			
		global $conn, $rc;
		$rc->CommandText = "UPDATE geoControl SET nombre = '$nombre', owner = $this->id_user, descripcion = '$descripcion' WHERE id = $id";
		$rc->CommandType = 1;
		$rc->ActiveConnection = $conn;
		$rc->Execute;
		
		//coordenadas de los puntos
		$puntos = array();
		if (isset($_POST["puntos"]{0})){
			$allPuntos = split("#", trim($_POST["puntos"]));
			
			for ($a = 0; $a < count($allPuntos); $a++){
				list($puntos[$a]["lat"], $puntos[$a]["lng"]) = split("_", $allPuntos[$a]);
			}
		}
		
		//eliminamos antiguas coordenadas
		$rc->CommandText = "DELETE FROM geoControlData WHERE geocontrol = $id";
		$rc->CommandType = 1;
		$rc->ActiveConnection = $conn;
		$rc->Execute;
		
		//guardamos nuevos puntos de la geoarea	
		for ($a = 0; $a < count($puntos); $a++){	
			$rc->CommandText = "INSERT INTO geoControlData (geocontrol, secuencia, latitud, longitud) VALUES (".$id.", $a, ".$puntos[$a]["lat"].", ".$puntos[$a]["lng"].")";
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
		}
				
		echo "OK";	
	}
	
	
	//imprime una lista de los iconos para las geocercas
	public function printIconList(){
		if ((int)$this->tipo_user > 1){
			global $conn;
			$rs = New COM("ADODB.Recordset");
	
			//sacamos usuarios admin				
			$rs->Open("SELECT * FROM geocercastipo", $conn); 
			$datos = fetch_assoc($rs); 
			$rs->Close();
			$opt = "";
			
			for ($a=0; $a<count($datos); $a++){
				$opt .= '<option value="'.$datos[$a]["id"].'">'.str_replace("'", "", $datos[$a]["tipo"]).'</option>';
			}
							
			return $opt;
		}else{
			return "";
		}
	}
	
	
	//regresa una lista formateada para un SELECT de rutas disponibles
	public function rutaSelect(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("SELECT id, nombre FROM RutaControl WHERE owner = $this->id_user AND id NOT IN (SELECT idRuta FROM RutaGeocercas)", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.$datos[$a]["nombre"].'</option>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un SELECT de rutas en uso
	public function rutaSelectForFindForm(){		
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos rutas			
		$rs->Open("SELECT id, nombre FROM RutaControl WHERE owner = $this->id_user AND id IN (SELECT idRuta FROM RutaGeocercas)", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();		
		
		$opt = '<option value="*" selected="selected">Cualquiera</option>';		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.str_replace("'", "", $datos[$a]["nombre"]).'</option>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un DIV de GPS´s disponibles para un contrato
	public function rutaSelectForMod($idRuta){
		$opt = "";
		global $conn, $rs;
		
		//sacamos ruta actual
		$rs->Open("SELECT nombre FROM RutaControl WHERE id = $idRuta", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$nombreRutaActual = $datos[0]['nombre'];

		//sacamos rutas que aun no son usadas			
		$rs->Open("SELECT id, nombre FROM RutaControl WHERE owner = $this->id_user AND id NOT IN (SELECT idRuta FROM RutaGeocercas)", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = '<option value="'.$idRuta.'" selected="selected">'.$nombreRutaActual.'</option>';
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.$datos[$a]["nombre"].'</option>';
		}
						
		return $opt;
	}
		
	
	//regresa una lista formateada para un SELECT de geocercas
	public function geoListForRutaControl(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos usuarios admin				
		$rs->Open("SELECT id, nombre FROM geocercas WHERE owner = $this->id_user", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<div><input id="'.$a.'" type="checkbox" onclick="javascript:checkBarra(this.id);" name="N'.$datos[$a]["id"].'"/><label>'.$datos[$a]["nombre"].'</label></div>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un SELECT de geocercas ya usadas
	public function geoListForModRutaControl($idRuta){
		$opt = "";
		global $conn, $rs;

		//sacamos geocercas		
		$rs->Open("SELECT geocercas.nombre, geocercas.id FROM RutaGeocercas INNER JOIN geocercas ON (geocercas.id = RutaGeocercas.idGeocerca) WHERE RutaGeocercas.idRuta = $idRuta ORDER BY RutaGeocercas.secuencia ASC", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		$opt = "";
		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<div class="sortableitem"><input id="'.$a.'" type="checkbox" checked="checked" onclick="javascript:checkBarra(this.id);" name="N'.$datos[$a]["id"].'"/><label>'.$datos[$a]["nombre"].'</label></div>';
		}
						
		return $opt;
	}
	
	//regresa una lista formateada para un array de JS de geocercas
	public function geoArrayInJSForRutaControl(){
		$opt = "";
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos geocercas		
		$rs->Open("SELECT id, nombre FROM geocercas WHERE owner = $this->id_user", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		$opt = "";			
		$totalGeocercas = count($datos);
		
		//guardamos geocercas en array JS
		for ($a=0; $a<$totalGeocercas; $a++){
			$opt .= "'<input type=\"checkbox\" name=\"N".$datos[$a]['id']."\" id=\"".$a."\" onclick=\"javascript:checkBarra(this.id);\" /><label>".str_replace("'","", $datos[$a]['nombre'])."</label>'";
			
			if ($a != $totalGeocercas-1){ 
				$opt .= ',';
			}
		}
								
		return $opt;
	}
	
	
	//regresa una lista formateada para un segundo array de JS de geocercas para modiificar
	public function geoArray2InJSForModRutaControl($idRuta){
		$opt = "";
		global $conn, $rs;

		//sacamos geocercas		
		$rs->Open("SELECT geocercas.nombre, geocercas.id FROM RutaGeocercas INNER JOIN geocercas ON (geocercas.id = RutaGeocercas.idGeocerca) WHERE RutaGeocercas.idRuta = $idRuta ORDER BY RutaGeocercas.secuencia ASC", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		$opt = "";			
		$totalGeocercas = count($datos);
		
		//guardamos geocercas en array JS
		for ($a=0; $a<$totalGeocercas; $a++){
			$opt .= "'<input type=\"checkbox\" name=\"N".$datos[$a]['id']."\" id=\"".$a."\" onclick=\"javascript:checkBarra(this.id);\" /><label>".str_replace("'","", $datos[$a]['nombre'])."</label>'";
			
			if ($a != $totalGeocercas-1){ 
				$opt .= ',';
			}
		}
								
		return $opt;
	}
	
	
	//agrega una ruta de control a la base de datos
	public function addRutaControlToDB(){	
		global $conn, $rc, $rs; 				
		$Ruta = $_POST["Ruta"];  //recivimos ruta
	   
	    $rs->Open("SELECT idRuta FROM RutaGeocercas WHERE idRuta = ".$Ruta, $conn); 
        $datos=fetch_assoc($rs); 
		$rs->Close();
	    
		if (count($datos) == 0){ //si Aun no existe esta ruta
			//obtenemos nombres individuales
			$AllBases = split(",",trim($_POST["AllBases"]));
			
			for ($a=0; $a<count($AllBases); $a++){
				$BaseInd=array();
				$BaseInd = split('_',trim($AllBases[$a]));
				
				$rc->CommandText = "INSERT INTO RutaGeocercas (idRuta, idGeocerca, secuencia) VALUES (".$Ruta.", ". substr(trim($BaseInd[0]),1) . ", " . trim($BaseInd[1]) .")";
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
			}
			
			echo "OK"; 
		}else{ 
			echo "RutaYaExiste";
		}
	}
	
	
	//modifica una ruta de control de la base de datos
	public function modRutaControlToDB(){	
		global $conn, $rc; 				
		$Ruta = (int)$_POST["Ruta"];  //recivimos ruta
		$oldRuta = (int)$_POST["idRutaOld"];  //recivimos ruta vieja
	   
	   	//eliminamos ruta de control actual, para reemplazarla por la nueva
		$rc->CommandText = "DELETE FROM RutaGeocercas WHERE idRuta = $oldRuta";
		$rc->CommandType = 1;
		$rc->ActiveConnection = $conn;
		$rc->Execute;
				
		//obtenemos nombres individuales
		$AllBases = split(",",trim($_POST["AllBases"]));
		
		for ($a=0; $a<count($AllBases); $a++){
			$BaseInd=array();
			$BaseInd = split('_',trim($AllBases[$a]));
			
			$rc->CommandText = "INSERT INTO RutaGeocercas (idRuta, idGeocerca, secuencia) VALUES (".$Ruta.", ". substr(trim($BaseInd[0]),1) . ", " . trim($BaseInd[1]) .")";
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
		}
		
		echo "OK"; 
	}
	
	
	//elimina una ruta de control de la base de datos
	public function deleteRutaControlFromDB(){	
		global $conn, $rc, $rs; 				
		$Ruta = (int)$_GET["a"];  //recivimos ruta de control
		
		//checamos si esta ruta es de este usuario
		$rs->Open("SELECT id FROM RutaControl WHERE owner = $this->id_user AND id = $Ruta", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		if (count($datos)>0){	   
			//eliminamos ruta de control
			$rc->CommandText = "DELETE FROM RutaGeocercas WHERE idRuta = $Ruta";
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
			
			return true;
		}		
		
		return false;
	}
	
	
	//elimina una ruta de la base de datos
	public function deleteRutaFromDB(){	
		global $conn, $rc, $rs; 				
		$idRuta = (int)$_GET["a"];  //recivimos ruta de control
		
		//checamos si esta ruta es de este usuario
		$rs->Open("SELECT id FROM RutaControl WHERE owner = $this->id_user AND id = $idRuta", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		if (count($datos)>0){	   
			//eliminamos ruta de control
			$rc->CommandText = "DELETE FROM RutaControl WHERE id = $idRuta";
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
			
			return true;
		}		
		
		return false;
	}
	
	
	//regresa una lista formateada para un SELECT de rutas en uso para seleccionar en viajes
	public function rutaSelectForViajeForm(){		
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos rutas			
		$rs->Open("SELECT id, nombre FROM RutaControl WHERE owner = $this->id_user AND id IN (SELECT idRuta FROM RutaGeocercas)", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();		
		
		$opt = '<option value="0" selected="selected">Sin Ruta de Control</option>';		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.str_replace("'", "", $datos[$a]["nombre"]).'</option>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un SELECT de rutas en uso para seleccionar en viajes
	public function rutaSelectForFindViaje(){		
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos rutas			
		$rs->Open("SELECT id, nombre FROM RutaControl WHERE owner = $this->id_user AND id IN (SELECT ruta FROM viajes)", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();		
		
		$opt = '<option value="*" selected="selected">Cualquiera</option><option value="0">Sin Ruta</option>';		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.str_replace("'", "", $datos[$a]["nombre"]).'</option>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista formateada para un SELECT de rutas en uso para seleccionar en historial
	public function rutaSelectForFindHistorial(){		
		global $conn;
		$rs = New COM("ADODB.Recordset");

		//sacamos rutas			
		$rs->Open("SELECT id, nombre FROM RutaControl WHERE owner = $this->id_user AND id IN (SELECT ruta FROM viajes WHERE activo = 0)", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();		
		
		$opt = '<option value="*" selected="selected">Cualquiera</option><option value="0">Sin Ruta</option>';		
		for ($a=0; $a<count($datos); $a++){
			$opt .= '<option value="'.$datos[$a]["id"].'">'.str_replace("'", "", $datos[$a]["nombre"]).'</option>';
		}
						
		return $opt;
	}
	
	
	//regresa una lista para SELECT de gps para el usuario USER
	public function selectGpsForReporte(){
		global $conn, $rs;
		//sacamos gps de este user		
		$rs->Open("		
		SELECT 
			GPs.serie, 
			GPs.descripcion			
		FROM GPs 
		INNER JOIN gpscontratos 
		ON (GPs.id = gpscontratos.gps) 
		WHERE (
			gpscontratos.contrato IN (
				SELECT id 
				FROM contratos 
				WHERE 
					activo = 1 AND 
					usr = ".$this->id_user.")) ", $conn); 	
		$GPSs = fetch_assoc($rs); 
		$rs->Close();
		
		$opt = "";
		for ($a=0; $a<count($GPSs); $a++){
			$opt .= '<option value="'.$GPSs[$a]["serie"].'">'.$GPSs[$a]["descripcion"].'</option>';
		}
		
		return $opt;
	}
		
		
	//filtro para los reportes de status
	public function filtroReporteStatus(){
		$condiciones = "";
		
		//condiciones para primer consulta
		if (isset($_GET["serie"]) and strlen(trim($_GET["serie"]))>0 and trim($_GET["serie"])!="*"){ 
			$condiciones .= "AND GPs.serie = ".(int)$_GET["serie"];
		}	
				
		return $condiciones;
	}
	
	
	//imprime una tabla de reporte de los gps y su status y demas datos
	public function printReporte(){		
		global $conn, $rs;
		//sacamos gps de este user		
		$rs->Open("		
		SELECT 
			GPs.serie, 
			GPs.descripcion			
		FROM GPs 
		INNER JOIN gpscontratos 
		ON (GPs.id = gpscontratos.gps) 
		WHERE (
			gpscontratos.contrato IN (
				SELECT id 
				FROM contratos 
				WHERE 
					activo = 1 AND 
					usr = ".$this->id_user.")) ". $this->filtroReporteStatus(), $conn); 	
		$GPSs = fetch_assoc($rs); 
		$rs->Close();
		
		//imprimimos inicio de tabla
		echo '
		<table cellpadding="0" cellspacing="0" class="tablaReporte">
			<tr class="headTabla">
				<td>Descripci&oacute;n</td>
				<td>Fecha</td>					
				<td>Latitud</td>		
				<td>Longitud</td>	
				<td>Velocidad</td>	
				<td>Bater&iacute;a</td>	
				<td>Temperatura</td>	
				<td>Tiempo Parado</td>
				<td>Gps</td>
				<td>Ubicaci&oacute;n</td>
			</tr>';
		
		$datosGps = "";
		$totalGps = count($GPSs);
		for ($a=0; $a<$totalGps; $a++){
			$xt = (int)trim($GPSs[$a]["serie"]);	
									
			//SACAMOS EL PUNTO MAS ACTUAL
			$rs->Open("	SELECT 
							GPs.descripcion,
							datosghe.id,
							datosghe.fecharecv,							
							CONVERT(CHAR(19),datosghe.fechasend,120) AS fechasend,
							datosghe.lat,
							datosghe.lon,
							datosghe.tambiental,
							datosghe.alarma,
							datosghe.velocidad,
							datosghe.bateria,
							datosghe.rumbo,
							datosghe.valido
						FROM datosghe 
						INNER JOIN GPs
						ON (GPs.serie = datosghe.gps)
						WHERE 
							datosghe.gps = ".$xt." AND
							datosghe.id = (
								SELECT MAX(datosghe.id) 
								FROM datosghe 
								WHERE 
									datosghe.gps = ".$xt."
							) ", $conn);
			$datos = fetch_assoc($rs); 
			$rs->Close();
						
			if (count($datos) > 0){
				//si la velocidad es 0 sacamos el tiempo total que lleva parado			
				$tiempoParado = 0;
				if ($datos[0]["velocidad"] == "0"){
					$rs->Open("	SELECT  
									CONVERT(CHAR(19),fechasend,120) AS fechasend
								FROM datosghe 						
								WHERE id = (
									SELECT 
										MAX(id)
									FROM datosghe
									WHERE velocidad > 3 AND gps = ".$xt." 
								)", $conn);
					$datosTiempoParado = fetch_assoc($rs); 
					$rs->Close();
					
					if (count($datosTiempoParado)){
						//calculo los minutos transcurridos en velocidad 0				
						$lastFechaMoving = date_parse(trim($datosTiempoParado[0]['fechasend']));
						$thisFechaStop = date_parse(trim($datos[0]['fechasend']));
						
						$tiempoParado = 				
								mktime(
									$thisFechaStop['hour'],
									$thisFechaStop['minute'],
									$thisFechaStop['second'],
									$thisFechaStop['month'],
									$thisFechaStop['day'],
									$thisFechaStop['year']
								) -				
								mktime(
									$lastFechaMoving['hour'],
									$lastFechaMoving['minute'],
									$lastFechaMoving['second'],
									$lastFechaMoving['month'],
									$lastFechaMoving['day'],
									$lastFechaMoving['year']
								);		
					}//end if
					
					if ((float)$tiempoParado<0){
						$tiempoParado = 0;
					}else{
						$tiempoParado = segMinHrs($tiempoParado);
					}
				}
				
				//modificamos el formato de las coordenadas
				$lat = $datos[0]["lat"];	  
				switch(substr($lat,strlen($lat)-1,1)){
					case 'S': (double)$lat*=-1; break;
					case 'N': (double)$lat*=1; break;
				}
				
				$lon = $datos[0]["lon"];  
				switch(substr($lon,strlen($lon)-1,1)){
					case 'W': (double)$lon*=-1; break;
					case 'E': (double)$lon*=1; break;
				}
					
				//imprimimos fila
				$mod = $a%2;
				echo '
				<tr class="bodyTable'.$mod.'">			
					<td>'.trim($GPSs[$a]["descripcion"]).'</td>
					<td>'.$datos[0]['fecharecv'].'</td>
					<td class="lat'.$a.'">'.$lat.'</td>
					<td class="lon'.$a.'">'.$lon.'</td>
					<td>'.$datos[0]['velocidad'].' km/h</td>
					<td>'.$datos[0]['bateria'].'</td>	
					<td>'.$datos[0]['tambiental'].'&deg; C</td>	
					<td>'.$tiempoParado.'</td>	
					<td>'.(($datos[0]['valido'] == '0')?'X':'Ok').'</td>	
					<td class="calle'.$a.'">Cargando...</td>				
				</tr>';	
			}//end if						
		}//end for	
		
		//fin de tabla
		echo '</table><input type="hidden" id="totFilas" value="'.$totalGps.'" />';
	}
	
	
	//imprime un reporte de las rutas cerradas
	public function printReporteViajesCerrados($idviaje){
		global $conn, $rs;

		$rs->Open("	SELECT
						viajes.clave, 						
						viajes.origen, 
						viajes.destino, 
						viajes.mercancia, 	
						viajes.vel,
						viajes.tiempomuerto,
						CONVERT(CHAR(19),viajes.finicial,120) AS fechaInicial,
						CONVERT(CHAR(19),viajes.ffinal,120) AS fechaFinal,						
						viajes.finicial,
						viajes.ffinal,			
						GPs.descripcion,
						GPs.serie,
						RutaControl.nombre,
						RutaControl.id AS idRuta
					FROM (viajes
					INNER JOIN RutaControl
					ON (RutaControl.id = viajes.ruta))
						INNER JOIN GPs
						ON (GPs.id = viajes.gps)
					WHERE
						viajes.usr = ".$this->id_user." AND
						viajes.idviaje = ".$idviaje, $conn); 	
		$datosViaje = fetch_assoc($rs); 
		$rs->Close();	
		
		//formateamos la fecha inicial
		$fi = date_parse($datosViaje[0]["fechaInicial"]);
		$fechaInicial = $fi['year'].'-'.twoDigit($fi['month']).'-'.twoDigit($fi['day']).'T'.twoDigit($fi['hour']).':'.twoDigit($fi['minute']).':'.twoDigit($fi['second']);
		
		//formateamos la fecha final
		$ff = date_parse($datosViaje[0]["fechaFinal"]);
		$fechaFinal = $ff['year'].'-'.twoDigit($ff['month']).'-'.twoDigit($ff['day']).'T'.twoDigit($ff['hour']).':'.twoDigit($ff['minute']).':'.twoDigit($ff['second']);	
	
		//imprimimos datos del viaje
		echo '
		<table cellpadding="0" cellspacing="0" class="tablaReporteV" style="margin-bottom:20px;">
			<tr class="headTabla">
				<td>Viaje</td>
				<td>Modulo</td>	
				<td>Ruta</td>	
				<td>Fecha Inicial</td>					
				<td>Fecha Final</td>		
				<td>Origen</td>	
				<td>Destino</td>	
				<td>Mercancia</td>	
				<td>Vel. M&aacute;x. Perm.</td>	
				<td>Inmovilidad Max. Perm.</td>			
			</tr>
			<tr class="bodyTable0">
				<td>'.$datosViaje[0]['clave'].'</td>
				<td>'.$datosViaje[0]['descripcion'].'</td>
				<td>'.$datosViaje[0]['nombre'].'</td>
				<td>'.$datosViaje[0]['finicial'].'</td>
				<td>'.$datosViaje[0]['ffinal'].'</td>
				<td>'.$datosViaje[0]['origen'].'</td>	
				<td>'.$datosViaje[0]['destino'].'</td>	
				<td>'.$datosViaje[0]['mercancia'].'</td>	
				<td>'.((strlen(trim($datosViaje[0]['vel'])) > 0)?$datosViaje[0]['vel']:'0').' km/h</td>	
				<td>'.((strlen(trim($datosViaje[0]['tiempomuerto']))==0 or (int)$datosViaje[0]['tiempomuerto']==0)?'0 seg':segMinHrs($datosViaje[0]['tiempomuerto'])).'</td>				
			</tr>
		</table>';
		
		//sacamos las bases
		$rs->Open("	SELECT 
						RutaGeocercas.id, 
						RutaGeocercas.secuencia,
						geocercas.nombre, 
						geocercas.descripcion,
						geocercas.norte,
						geocercas.oeste,
						geocercas.radio,
						geocercastipo.tipo
					FROM ((RutaGeocercas 
						INNER JOIN RutaControl
						ON (RutaControl.id = RutaGeocercas.idRuta))
							INNER JOIN geocercas
							ON (geocercas.id = RutaGeocercas.idGeocerca))
								INNER JOIN geocercastipo
								ON (geocercastipo.id = geocercas.tipo)
					WHERE 
						RutaControl.id = ".$datosViaje[0]['idRuta']."
					ORDER BY RutaGeocercas.secuencia ASC", $conn); 	
		$bases = fetch_assoc($rs); 
		$rs->Close();	
		
		/** fecha inicial en movimiento **/
		//sacamos primer fecha entre la fechaInicial y fechaFinal que estuvo en movimiento 
		$rs->Open("	SELECT 
						CONVERT(CHAR(19),fecharecv,120) AS fecharecv
					FROM datosghe
					WHERE datosghe.id = (
						SELECT MIN(id)
						FROM datosghe
						WHERE 
							gps = ".$datosViaje[0]['serie']." AND
							fecharecv >= '".$fechaInicial."' AND	
							fecharecv <= '".$fechaFinal."' AND		
							velocidad > 3 AND
							valido = 1				
					)", $conn); 	
		$fechaIMovimiento = fetch_assoc($rs); 
		$rs->Close();	
		
		//formateamos esta fecha
		$fi = date_parse($fechaIMovimiento[0]['fecharecv']);
		$fechaIMovimiento = $fi['year'].'-'.twoDigit($fi['month']).'-'.twoDigit($fi['day']).'T'.twoDigit($fi['hour']).':'.twoDigit($fi['minute']).':'.twoDigit($fi['second']);				
		
		/** fecha final en movimiento **/		
		//sacamos ultima fecha entre la fechaInicial y fechaFinal que estuvo en movimiento 
		$rs->Open("	SELECT 
						CONVERT(CHAR(19),fecharecv,120) AS fecharecv
					FROM datosghe
					WHERE datosghe.id = (
						SELECT MAX(id)
						FROM datosghe
						WHERE 
							gps = ".$datosViaje[0]['serie']." AND
							fecharecv >= '".$fechaInicial."' AND	
							fecharecv <= '".$fechaFinal."' AND		
							velocidad > 3 AND
							valido = 1				
					)", $conn); 	
		$fechaFMovimiento = fetch_assoc($rs); 
		$rs->Close();	
		
		//formateamos esta fecha
		$ff = date_parse($fechaFMovimiento[0]['fecharecv']);
		$fechaFMovimiento = $ff['year'].'-'.twoDigit($ff['month']).'-'.twoDigit($ff['day']).'T'.twoDigit($ff['hour']).':'.twoDigit($ff['minute']).':'.twoDigit($ff['second']);		
		
		//sacamos los puntos de todo el viaje con los que construiremos el reporte
		$rs->Open("	SELECT 
						id,
						fechasend,
						fecharecv,
						CONVERT(CHAR(19),fechasend,120) AS fechasendFormated,
						CONVERT(CHAR(19),fecharecv,120) AS fecharecvFormated,	
						North,
						West,
						alarma,
						tambiental,
						aperturas,
						velocidad,
						bateria,
						rumbo
					FROM datosghe
					WHERE 
						fecharecv >= '".$fechaIMovimiento."' AND
						fecharecv <= '".$fechaFMovimiento."' AND
						lat <> 0 AND
						lon <> 0 AND 
						valido = 1						
					ORDER BY fechasend ASC", $conn); 	
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		//CALCULAMOS TIEMPO TOTAL DEL RECORRIDO
		$fi = date_parse($fechaIMovimiento); 
		$ff = date_parse($fechaFMovimiento); 	
		
		$tiempoTotalRecorrido = segMinHrs(
			mktime(
				$ff['hour'], 
				$ff['minute'], 
				$ff['second'], 
				$ff['month'], 
				$ff['day'], 
				$ff['year']
			) - 
			mktime(
				$fi['hour'], 
				$fi['minute'], 
				$fi['second'], 
				$fi['month'], 
				$fi['day'], 
				$fi['year']
			)
		);
		
		//CALCULAMOS MAXIMO TIEMPO PARADO	
		$cerosSeguidos = 0;
		$con = 0;
		$maxTimeStop = array();
		 
		for ($a = 1; $a < count($datos); $a++){
			if ($datos[$a]['velocidad'] == '0' and $datos[$a - 1]['velocidad'] == '0'){
				if ($cerosSeguidos == 0){
					$maxTimeStop[$con][$cerosSeguidos++] = $a - 1;
				}
				
				$maxTimeStop[$con][$cerosSeguidos++] = $a;
			}elseif ($cerosSeguidos > 0){
				$cerosSeguidos = 0;
				$con++;
			}
		}//end for  
		
		
		//caculamos tiempos para cada parada continua
		$tiempoPorParada = array();
		$con = 0;
		
		for ($a = 1; $a < count($maxTimeStop); $a++){
			$tiempoPorParada[$con]['Time'] = fechaToSeg($datos[$maxTimeStop[$a][count($maxTimeStop[$a]) - 1]]['fechasendFormated']) - fechaToSeg($datos[$maxTimeStop[$a][0]]['fechasendFormated']);
			
			$tiempoPorParada[$con++]['Fecha'] = $datos[$maxTimeStop[$a][0]]['fechasend'];
		}//end for
		
		//sacamos el maximo tiempo en cero
		$maxTime['Time'] = $tiempoPorParada[0]['Time'];
		$maxTime['Fecha'] = $tiempoPorParada[0]['Fecha'];
		
		for ($a = 1; $a < count($tiempoPorParada); $a++){
			if ($tiempoPorParada[$a]['Time'] > $maxTime['Time']){
				$maxTime['Time'] = $tiempoPorParada[$a]['Time'];
				$maxTime['Fecha'] = $tiempoPorParada[$a]['Fecha'];
			}
		}//end for
		
		$tiempoMaxParado = segMinHrs($maxTime['Time']).' el '.$maxTime['Fecha']; 
		
		//sacamos vel max
		$rs->Open("	SELECT 
						velocidad,
						fecharecv
					FROM datosghe
					WHERE 
						velocidad = (
							SELECT 
								MAX(velocidad)
							FROM datosghe
							WHERE
								fecharecv >= '".$fechaIMovimiento."' AND
								fecharecv <= '".$fechaFMovimiento."' AND
								lat <> 0 AND
								lon <> 0 AND 
								valido = 1	
						)", $conn); 	
		$velMax = fetch_assoc($rs); 
		$rs->Close();
				
		$tb = 0;
		$tv = 0;	  
		$bb = 0;  //contador de la base buscada actual
		$secMax = count($bases);  //secuencia maxima existente de bases
		$secB = 0;
		$TBC = 0; //total de bases comparadas...
		$basesPasadas = array(); // array que tendrá las bases por las que fue pasando y los datos del punto que pasó en cada base
		
		//recorremos todos los puntos
		for ($a = 1; $a < count($datos); $a++){
			$TBC = 0; //reiniciamos el total de bases comparadas
			
			//sacamos coordenadas (X,Y) del camion ANTES Y AHORA
			$puntoOld['x'] = $datos[$a-1]['West'];
			$puntoOld['y'] = $datos[$a-1]['North'];
			$puntoNew['x'] = $datos[$a]['West'];
			$puntoNew['y'] = $datos[$a]['North'];
			
			$bandBF = 0;
			//mientras no se encuentre una base y mientras no se alla dado una vuelta entera a las bases
			while ($bandBF == 0){ 
				//estaba en alguna BASE 
				if (inCircle($puntoOld['x'], $puntoOld['y'], $bases[$secB]['oeste'], $bases[$secB]['norte'], $bases[$secB]['radio']) == 1){
					$inBxOld = 1;
				}else{
					$inBxOld = 0;
				}
				
				//esta en alguna BASE 
				if (inCircle($puntoNew['x'], $puntoNew['y'], $bases[$secB]['oeste'], $bases[$secB]['norte'], $bases[$secB]['radio']) == 1){
					$inBxNew = 1;
				}else{
					$inBxNew = 0;
				}
				
				//SALIO DE UNA BASE 
				if ($inBxOld == 1 and $inBxNew == 0){     
					$basesPasadas[$tb]['sec'] = $bases[$secB]['secuencia'];
					$basesPasadas[$tb]['IO'] = 'O';
					$basesPasadas[$tb++]['id'] = $a;
					
					--$secB; //decremntamos porque despues se vuelte a incrementar y asi buscara denuevo en esta base
					$bandBF = 1;  //nos salimos del ciclo
				}elseif ($inBxOld == 0 and $inBxNew == 1){ //ENTRO A UNA BASE 
					$basesPasadas[$tb]['sec'] = $bases[$secB]['secuencia'];
					$basesPasadas[$tb]['IO'] = 'I';
					$basesPasadas[$tb++]['id'] = $a;
					
					--$secB; //decremntamos porque despues se vuelte a incrementar y asi buscara denuevo en esta base
					$bandBF = 1;  //nos salimos del ciclo
				}
				
				++$secB; //incrementamos la secuencia buscada
				
				//si ya se llego al final de las bases, nos vamos al inicio
				if ($secB == $secMax){ 
					$secB = 0; 
				}  
				++$TBC;  //incrementamos las bases comparadas
				
				//si ya comparo toodas las bases, nos salimos del ciclo
				if ($TBC == $secMax){ 
					$bandBF = 1; 
				}  
			}//end while
		}//end for		
				
		//SIMPLIFICAMOS BASES (ELIMINAMOS DUPLICADOS) 	
		$basesOK = array();
		$tbOK = 0;
		
		//la primer base va por default
		$basesOK[$tbOK]['sec'] = $basesPasadas[0]['sec'];
		$basesOK[$tbOK]['IO'] = $basesPasadas[0]['IO'];
		$basesOK[$tbOK]['id'] = $basesPasadas[0]['id'];
		++$tbOK;
		
		// bandera que nos dice si ya se encontro la entrada y salida de una base		
		$bandIO = 0;
		
		for ($a = 1; $a < count($basesPasadas); $a++){
			//la anterior que se agregó es igual que esta base
			if ($basesPasadas[$a]['sec'] == $basesOK[$tbOK-1]['sec']){ 	
				//si aun no se han guardado la entrada y salida
				if ($bandIO == 0){
					//si el anterior era una entrada y este es una salida entonces lo guardamos
					if (($basesOK[$tbOK-1]['IO'] == 'I' and $basesPasadas[$a]['IO'] == 'O')){
						$basesOK[$tbOK]['sec'] = $basesPasadas[$a]['sec'];
						$basesOK[$tbOK]['IO'] = $basesPasadas[$a]['IO'];
						$basesOK[$tbOK]['id'] = $basesPasadas[$a]['id'];
						++$tbOK;
						$bandIO = 1; // ya encontramos la entrada y salida
					}
				}
			}else{ 
				//ya cambio de base
				$basesOK[$tbOK]['sec'] = $basesPasadas[$a]['sec'];
				$basesOK[$tbOK]['IO'] = $basesPasadas[$a]['IO'];
				$basesOK[$tbOK]['id'] = $basesPasadas[$a]['id'];
				++$tbOK;
				$bandIO = 0;
			}			
		}//end for		
		
		unset($basesPasadas);			
								
		//ACOMODAMOS ARRAY: CADA BASE EN UNA FILA
		$basesPorFila = array();
		$tbOK = 0;
		
		//la primer base va por default
		$basesPorFila[$tbOK]['sec'] = $basesOK[0]['sec'];
		if ($basesOK[0]['IO'] == 'I'){
			$basesPorFila[$tbOK]['I'] = $basesOK[0]['id'];
		}else{
			$basesPorFila[$tbOK]['O'] = $basesOK[0]['id'];
		}	
			
		for ($a = 1; $a < count($basesOK); $a++){
			//la anterior que se agregó es igual que esta base
			if ($basesOK[$a]['sec'] != $basesOK[$a-1]['sec']){ 										
				++$tbOK;					
				$basesPorFila[$tbOK]['sec'] = $basesOK[$a]['sec'];
			}		
						
			if ($basesOK[$a]['IO'] == 'I'){
				$basesPorFila[$tbOK]['I'] = $basesOK[$a]['id'];
			}else{
				$basesPorFila[$tbOK]['O'] = $basesOK[$a]['id'];
			}	
		}//end for					
		
		//SACAMOS DEMAS DATOS PARA EL REPORTE
		unset($basesOK);		
		$filas = array();
		$cf = 0;
		$a = 0;
		
		//** sacamos datos de fila 0 **//
		//datos de bases
		$filas[$cf]['id'] = $bases[$basesPorFila[$a]['sec']]['secuencia'] + 1; //id de base (secuencia)
		$filas[$cf]['nombre'] = $bases[$basesPorFila[$a]['sec']]['nombre']; //nombre de base
		$filas[$cf]['descripcion'] = $bases[$basesPorFila[$a]['sec']]['descripcion']; //descripción de base		
		
		//entrada
		if (isset($basesPorFila[$a]['I'])){
			$filas[$cf]['fechaInputUTC'] = $datos[$basesPorFila[$a]['I']]['fechasend'];
			$filas[$cf]['fechaInput'] = $datos[$basesPorFila[$a]['I']]['fecharecv'];
		}else{			
			//asignamos fecha de inicio de movimiento   			
			//fecha UTC = fecha - 6hrs					       
			$fechaIMovimiento = date_parse($fechaIMovimiento); 	
						
			$filas[$cf]['fechaInputUTC'] = date('d/m/Y h:i:s a', mktime($fechaIMovimiento['hour'], $fechaIMovimiento['minute'], $fechaIMovimiento['second'], $fechaIMovimiento['month'], $fechaIMovimiento['day'], $fechaIMovimiento['year']) - 21600);
						
			$filas[$cf]['fechaInput'] = date('d/m/Y h:i:s a', mktime($fechaIMovimiento['hour'], $fechaIMovimiento['minute'], $fechaIMovimiento['second'], $fechaIMovimiento['month'], $fechaIMovimiento['day'], $fechaIMovimiento['year']));						
		}
		
		//salida
		if (isset($basesPorFila[$a]['O'])){
			$filas[$cf]['fechaOutputUTC'] = $datos[$basesPorFila[$a]['O']]['fechasend'];
			$filas[$cf]['fechaOutput'] = $datos[$basesPorFila[$a]['O']]['fecharecv'];
		}else{
			if (isset($basesPorFila[$a + 1]['I'])){
				//agarramos la entrada del siguiente como salida de este
				$filas[$cf]['fechaOutputUTC'] = $datos[$basesPorFila[$a + 1]['I']]['fechasend'];
				$filas[$cf]['fechaOutput'] = $datos[$basesPorFila[$a + 1]['I']]['fecharecv'];
			}else{
				//agarramos la salida del siguiente como salida de este
				$filas[$cf]['fechaOutputUTC'] = $datos[$basesPorFila[$a + 1]['O']]['fechasend'];
				$filas[$cf]['fechaOutput'] = $datos[$basesPorFila[$a + 1]['O']]['fecharecv'];
			}
		}
		
		$cf++;		
		
		//** sacamos datos de las demas filas **// NOTA: estamos llegando hasta la PENULTIMA BASE
		for ($a = 1; $a < count($basesPorFila) - 1; $a++){
			//datos de bases
			$filas[$cf]['id'] = $bases[$basesPorFila[$a]['sec']]['secuencia'] + 1; //id de base (secuencia)
			$filas[$cf]['nombre'] = $bases[$basesPorFila[$a]['sec']]['nombre']; //nombre de base
			$filas[$cf]['descripcion'] = $bases[$basesPorFila[$a]['sec']]['descripcion']; //descripción de base			
			
			//entrada
			if (isset($basesPorFila[$a]['I'])){
				$filas[$cf]['fechaInputUTC'] = $datos[$basesPorFila[$a]['I']]['fechasend'];
				$filas[$cf]['fechaInput'] = $datos[$basesPorFila[$a]['I']]['fecharecv'];
			}else{
				if (isset($basesPorFila[$a - 1]['O'])){
					//agarramos la salida del anterior como entrada de este
					$filas[$cf]['fechaInputUTC'] = $datos[$basesPorFila[$a - 1]['O']]['fechasend'];
					$filas[$cf]['fechaInput'] = $datos[$basesPorFila[$a - 1]['O']]['fecharecv'];
				}else{
					//agarramos la entrada del anterior como entrada de este
					$filas[$cf]['fechaInputUTC'] = $datos[$basesPorFila[$a - 1]['I']]['fechasend'];
					$filas[$cf]['fechaInput'] = $datos[$basesPorFila[$a - 1]['I']]['fecharecv'];
				}
			}
			
			//salida
			if (isset($basesPorFila[$a]['O'])){
				$filas[$cf]['fechaOutputUTC'] = $datos[$basesPorFila[$a]['O']]['fechasend'];
				$filas[$cf]['fechaOutput'] = $datos[$basesPorFila[$a]['O']]['fecharecv'];
			}else{
				if (isset($basesPorFila[$a + 1]['I'])){
					//agarramos la entrada del siguiente como salida de este
					$filas[$cf]['fechaOutputUTC'] = $datos[$basesPorFila[$a + 1]['I']]['fechasend'];
					$filas[$cf]['fechaOutput'] = $datos[$basesPorFila[$a + 1]['I']]['fecharecv'];
				}else{
					//agarramos la salida del siguiente como salida de este
					$filas[$cf]['fechaOutputUTC'] = $datos[$basesPorFila[$a + 1]['O']]['fechasend'];
					$filas[$cf]['fechaOutput'] = $datos[$basesPorFila[$a + 1]['O']]['fecharecv'];
				}
			}			
			
			$cf++;
		}//end for
		
		//** sacamos datos de fila final **//
		//datos de bases
		$filas[$cf]['id'] = $bases[$basesPorFila[$a]['sec']]['secuencia'] + 1; //id de base (secuencia)
		$filas[$cf]['nombre'] = $bases[$basesPorFila[$a]['sec']]['nombre']; //nombre de base
		$filas[$cf]['descripcion'] = $bases[$basesPorFila[$a]['sec']]['descripcion']; //descripción de base
		
		//entrada
		if (isset($basesPorFila[$a]['I'])){
			$filas[$cf]['fechaInputUTC'] = $datos[$basesPorFila[$a]['I']]['fechasend'];
			$filas[$cf]['fechaInput'] = $datos[$basesPorFila[$a]['I']]['fecharecv'];
		}else{	
			if (isset($basesPorFila[$a - 1]['O'])){
				//agarramos la salida del anterior como entrada de este
				$filas[$cf]['fechaInputUTC'] = $datos[$basesPorFila[$a - 1]['O']]['fechasend'];
				$filas[$cf]['fechaInput'] = $datos[$basesPorFila[$a - 1]['O']]['fecharecv'];
			}else{
				//agarramos la entrada del anterior como entrada de este
				$filas[$cf]['fechaInputUTC'] = $datos[$basesPorFila[$a - 1]['I']]['fechasend'];
				$filas[$cf]['fechaInput'] = $datos[$basesPorFila[$a - 1]['I']]['fecharecv'];
			}			
		}
		
		//salida
		if (isset($basesPorFila[$a]['O'])){
			$filas[$cf]['fechaOutputUTC'] = $datos[$basesPorFila[$a]['O']]['fechasend'];
			$filas[$cf]['fechaOutput'] = $datos[$basesPorFila[$a]['O']]['fecharecv'];
		}else{
			//asignamos fecha de fin de movimiento   			
			//fecha UTC = fecha - 6hrs					       
			$fechaFMovimiento = date_parse($fechaFMovimiento); 	
						
			$filas[$cf]['fechaOutputUTC'] = date('d/m/Y h:i:s a', mktime($fechaFMovimiento['hour'], $fechaFMovimiento['minute'], $fechaFMovimiento['second'], $fechaFMovimiento['month'], $fechaFMovimiento['day'], $fechaFMovimiento['year']) - 21600);
						
			$filas[$cf]['fechaOutput'] = date('d/m/Y h:i:s a', mktime($fechaFMovimiento['hour'], $fechaFMovimiento['minute'], $fechaFMovimiento['second'], $fechaFMovimiento['month'], $fechaFMovimiento['day'], $fechaFMovimiento['year']));					
		}
				
		//CALCULAMOS TIEMPOS EN CADA BASE Y DATOS TOTALES
		for ($a = 0; $a < count($filas); $a++){
			$fi = date_parse($filas[$a]['fechaInputUTC']); 
			$ff = date_parse($filas[$a]['fechaOutputUTC']); 	
			
			$filas[$a]['tiempoTotal'] = segMinHrs(
				mktime(
					$ff['hour'], 
					$ff['minute'], 
					$ff['second'], 
					$ff['month'], 
					$ff['day'], 
					$ff['year']
				) - 
				mktime(
					$fi['hour'], 
					$fi['minute'], 
					$fi['second'], 
					$fi['month'], 
					$fi['day'], 
					$fi['year']
				)
			);
		}//end for
		
		//IMPRIMIMOS REPORTE
		echo '
		<table cellpadding="0" cellspacing="0" class="tablaReporteV" style="margin-bottom:20px;">
			<tr class="headTabla">
				<td>Id</td>
				<td>Nombre</td>	
				<td>Descripci&oacute;n</td>	
				<td>Fecha Llegada</td>	
				<td>Fecha Llegada UTC</td>		
				<td>Fecha Salida</td>		
				<td>Fecha Salida UTC</td>	
				<td>Duraci&oacute;n</td>	
			</tr>';
		
		for ($a = 0; $a < count($filas); $a++){
			echo '
			<tr class="bodyTable'.($a%2).'">
				<td>'.$filas[$a]['id'].'</td>
				<td>'.$filas[$a]['nombre'].'</td>
				<td>'.substr($filas[$a]['descripcion'], 0, 29).((isset($filas[$a]['descripcion'][30]))?'...':'').'</td>
				<td>'.$filas[$a]['fechaInput'].'</td>
				<td>'.$filas[$a]['fechaInputUTC'].'</td>	
				<td>'.$filas[$a]['fechaOutput'].'</td>
				<td>'.$filas[$a]['fechaOutputUTC'].'</td>	
				<td>'.$filas[$a]['tiempoTotal'].'</td>					
			</tr>';
		
		}//end for
		
		echo '			
		</table>';		
				
		//IMPRIMIMOS DATOS TOTALES
		echo '
		<table cellpadding="0" cellspacing="0" class="tablaReporteV" style="margin-bottom:20px;">
			<tr class="headTabla">				
				<td>Velocidad Max.</td>	
				<td>Tiempo Parado Max.</td>	
				<td>Duraci&oacute;n Total</td>	
			</tr>
			<tr class="bodyTable'.($a%2).'">
				<td>'.$velMax[0]['velocidad'].' km/h el '.$velMax[0]['fecharecv'].'</td>
				<td>'.$tiempoMaxParado.'</td>
				<td>'.$tiempoTotalRecorrido.'</td>
			</tr>';
	}
		
}//end class
?>
