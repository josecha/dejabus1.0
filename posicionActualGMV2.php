<?php
ob_start();

if (isset($_GET["idViaje"]) and strlen($_GET["idViaje"])>0){
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	$u = new User();
	if ($u->isLogued){
		include('head.php');
		$idViaje = (int)trim($_GET["idViaje"]);
		
		//verificamos que el viaje corresponda con el usuario
		$rs->Open("SELECT idviaje FROM viajes WHERE activo = 1 AND idviaje = ".$idViaje." AND usr = ".$u->id_user, $conn);
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		if (count($datos)>0){ //el viaje si es del usuario y aun esta activo		
			//RECUPERAMOS DATOS DE VIAJE
			$rs->Open("	SELECT 
							viajes.clave, 
							viajes.origen,
							viajes.destino,
							viajes.mercancia,
							viajes.ruta,
							viajes.vel,
							GPs.serie
						FROM viajes 
						INNER JOIN GPs
						ON (GPs.id = viajes.gps)
						WHERE viajes.idviaje = ".$idViaje, $conn);
			$datosViaje = fetch_assoc($rs);  
			$rs->Close();
			
			//si esque hay bases
			$bases = array();
			if (strlen(trim($datosViaje[0]['ruta'])) > 0 and (int)$datosViaje[0]['ruta'] != 0){
				//sacamos ruta de control
				$rs->Open("	SELECT 
								nombre, 
								descripcion
							FROM RutaControl 
							WHERE 
								id = ".(int)$datosViaje[0]['ruta'], $conn);
				$rutaControl = fetch_assoc($rs);  
				$rs->Close();
			
				//sacamos todas las bases
				$rs->Open("	SELECT     
								geocercas.nombre,
								geocercas.descripcion,
								geocercastipo.tipo,
								geocercastipo.icono,
								geocercas.longitud,
								geocercas.latitud
							FROM geocercas 
							INNER JOIN geocercastipo
							ON (geocercastipo.id = geocercas.tipo)
							WHERE geocercas.id IN(
								SELECT idGeocerca
								FROM RutaGeocercas
								WHERE RutaGeocercas.idRuta = ".(int)$datos[0]['ruta']."
								GROUP BY idGeocerca
								HAVING (COUNT(RutaGeocercas.idGeocerca) > 0)
							)", $conn);
				$bases = fetch_assoc($rs);  
				$rs->Close();
			}//end if
			
			//procesamos datos
			$origen = 'Sin especificar';
			if (strlen($datosViaje[0]['origen'])>0){
				$origen = $datosViaje[0]['origen'];
			}
			
			$destino = 'Sin especificar';
			if (strlen($datosViaje[0]['destino'])>0){
				$destino = $datosViaje[0]['destino'];
			}
			
			$mercancia = 'Sin especificar';
			if (strlen($datosViaje[0]['mercancia'])>0){
				$mercancia = $datosViaje[0]['mercancia'];
			}
			
			$vm = 100;
			if ((isset($datosViaje[0]['vel'])) and (strlen(trim($datosViaje[0]['vel']))>0) and ((int)trim($datosViaje[0]['vel'])>1)){
				$vm = (int)trim($datosViaje[0]['vel']);
			}
						
			//SACAMOS EL PUNTO MAS ACTUAL
			$rs->Open("	SELECT lat, lon
						FROM datosghe 
						WHERE 
							gps = ".$datosViaje[0]['serie']." AND
							id = (
								SELECT MAX(id) 
								FROM datosghe 
								WHERE 
									gps = ".$datosViaje[0]['serie']."
							)", $conn);
			$datos = fetch_assoc($rs); 
			$rs->Close();
			
			//modificamos el formato de las coordenadas
			$lat = $datos[0]["lat"];	  
			switch(substr($lat,strlen($lat)-1,1)){
				case 'S': (double)$lat*=-1; break;
				case 'N': (double)$lat*=1; break;
			}
			
			$lon = $datos[0]["lon"];  
			switch(substr($lon,strlen($lon)-1,1)){
				case 'W': (double)$lon*=-1; break;
				case 'E': (double)$lon*=1; break;
			}			
?>			
			<title>Montecristo Data Mining - Tracking Tampering Technology - Viajes - Punto Actual</title>
		       <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
               <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

				<script type="text/javascript">  
				//<![CDATA[
				var map;
				var band_now=0;
				var PointsNow=[];
				var polylineNow;
				var band_polyNow=0;
				var marcadorNow;	
					
				var miIconoBase;
				var markerOptionsBase;	
				var marcadorBase;												
				
				function load() {
					/*if (GBrowserIsCompatible()) {  	  
						map = new GMap2(document.getElementById("map"));
						map.setCenter(new GLatLng(<?=$lat; ?>, <?=$lon; ?>), 18, G_HYBRID_MAP);	  						
						map.addControl(new GSmallMapControl());
        				map.addControl(new GMapTypeControl());
						map.enableScrollWheelZoom(); */


                     var canvas=document.getElementById("map");
                     $("#map").css("height",$(window).height()-20);
                    
                      var latlng ;
                     //var lat=<?=$lat; ?>;
                     //alert(lat);
                     //var latlng = new google.maps.LatLng(<?=$lat; ?>, <?=$lon; ?>);
                     
                     <?php
                       echo "var latn=".count($datos).";";
                       if(!isset($lat)){
                         echo "alert('No hay Datos');";
                         $lat=19.3;
                         $lon=103.2;
                         }
                     ?>

                     
                         latlng = new google.maps.LatLng(<?=$lat; ?>, <?=$lon; ?>);
                      


                     var settings = {
                       zoom: 12,
                       center: latlng,
                       mapTypeControl: true,
                       mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                       navigationControl: true,
                       navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                       mapTypeId: google.maps.MapTypeId.HYBRID
                     };
                     map = new google.maps.Map(canvas, settings);

                     $(window).resize(function() {
                      $("#map").css("height",$(window).height()-20);
                      });


												
/*<?php





						//imprimimos bases
						echo 'miIconoBase = new GIcon();';
						for ($a = 0; $a < count($bases); $a++){
							echo '
							//icono de base
							miIconoBase = new GIcon();
							miIconoBase.image = "'.$bases[$a]["icono"].'";
							miIconoBase.iconSize = new GSize(20, 20);
							miIconoBase.iconAnchor = new GPoint(10, 10);
							miIconoBase.infoWindowAnchor = new GPoint(10, 10);
							markerOptionsBase = { icon:miIconoBase, title:"'.$bases[$a]['nombre'].'" };
							
							//marcador de base
							marcadorBase = new GMarker(new GLatLng('.$bases[$a]['latitud'].', '.$bases[$a]['longitud'].'), markerOptionsBase);
							marcadorBase.value = '.$a.';
							
							//le agregamos el evento "click" para desplegar información
							GEvent.addListener(marcadorBase,"click", function() { 		     
								var myHtml1 = \'<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Base: <b>'.$bases[$a]['nombre'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Descripci&oacute;n de base: <b>'.$bases[$a]['descripcion'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Tipo: <b>'.$bases[$a]['tipo'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Ruta de Control: <b>'.$rutaControl[0]['nombre'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Descripci&oacute;n de ruta de control: <b>'.$rutaControl[0]['descripcion'].'</b></div><br/>\';
								map.openInfoWindowHtml(new GLatLng('.$bases[$a]['latitud'].', '.$bases[$a]['longitud'].'), myHtml1);
							});
							
							map.addOverlay(marcadorBase);
							'."\n\n";
						}
?>	*/					
					


         <?php
                        //echo 'miIconoBase = new GIcon();';
						for ($a = 0; $a < count($bases); $a++){
							
                            
							echo '
							 alert('.$bases[$a]['longitud'].');
                            marcadorBase = new google.maps.Marker({
                            position: new google.maps.LatLng('.$bases[$a]['latitud'].', '.$bases[$a]['longitud'].'),
                            title:"'.$bases[$a]['nombre'].'",
                            icon:"'.$bases[$a]["icono"].'",
                            map: map,                           
                            tag:'.$a.'
                            });

                            alert("hola1");
                            google.maps.event.addListener(marcadorBase,"click", function() {
						        var myHtml1 = \'<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Base: <b>'.$bases[$a]['nombre'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Descripci&oacute;n de base: <b>'.$bases[$a]['descripcion'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Tipo: <b>'.$bases[$a]['tipo'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Ruta de Control: <b>'.$rutaControl[0]['nombre'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Descripci&oacute;n de ruta de control: <b>'.$rutaControl[0]['descripcion'].'</b></div><br/>\';    
						
                                info.setContent(myHtml1);
                                info.setPosition(new google.maps.LatLng('.$bases[$a]['latitud'].', '.$bases[$a]['longitud'].'));
							    info.open(map);
                            
						      //map.openInfoWindowHtml(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), myHtml);
					        });
							
							';
						}		

        ?>



                          google.maps.event.addListener(map, 'click', function(event) {
 	                      
 	                       getCalle1(event.latLng,1);
                        }); 
                       if(latn!=0)
						posicionNow(); //iniciamos actualizaciones
					//}
				}
				
				function posicionNow(){					
					PointsNow=[];	//inicializamos los puntos para hacer polyline de la vista actual
					band_polyNow=0;										
					mostrarNow();	//comenzamos a mostrar la posicion actual
				}
				
				function mostrarNow(){ 
					$.ajax({
						type: "GET",
						url: "posicionActualGM_actualizar.php",
						data: "xt=<?=$datosViaje[0]['serie']; ?>&idViaje=<?=$idViaje; ?>",
						dataType: "html",
						contentType: "application/x-www-form-urlencoded",
						success: function(datos){
							datos = trim(datos);
							
							//recivimos datos nuevos y los metemos en un array
							var datosNow = datos.split(", ");	
							
							if (band_now==0){ //primera vez que vemos la posicion actual
								band_now=1; 
							}else{  //borramos el markador anterior
								//map.removeOverlay(marcadorNow);	
								marcadorNow.setMap(null);		
							}
							
							//guardamos punto en vector para hacer polyline de vista actual
							//PointsNow.push(new GLatLng(datosNow[3], datosNow[4]));
							PointsNow.push(new google.maps.LatLng(datosNow[3], datosNow[4]));
							//si ya habia una polyline entonces la kitamos y ponemos la nueva
							if (band_polyNow==1){ 
								//map.removeOverlay(polylineNow); 
						        polylineNow.setMap(null);
							}
							
							//creamos polyline de vista actual
							if (PointsNow.length > 1){ //si existe mas de un punto
								polylineNow = new google.maps.Polyline({
                                 path: PointsNow
                                 , map: map
                                 , strokeColor: '#F1365E'
                                 , strokeWeight: 3
                                 , strokeOpacity: 0.7
                                 
                                 });


								//polylineNow = new GPolyline(PointsNow, "#F1365E", 3, 0.7);
								//map.addOverlay(polylineNow); 
								band_polyNow=1; 
							}
							
							//CREAMOS MARKADOR
							// Personalizamos el nuevo icono
							var miIcono1;
							var OffOn;
							var msj = 'No abierta';
							
							//verificamos que la alarma este apagada
							if (datosNow[9]==1 || datosNow[13]==1){ //fibra abierta, ponemos icono de alerta
								miIcono1 = "http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png"; 
								msj = 'Abierta';
							}else if (datosNow[7].substring(0, datosNow[7].length-5)><?=$vm; ?>){ //va a exeso de velocidad
								miIcono1="http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png"; 
							}else{
								//ponemos iconos normales
								if (datosNow[7].substring(0, datosNow[7].length-5)==0){ //si esta parado 
									OffOn="Off"; 
								}else{ 
									OffOn="On"; 
								}
								
								miIcono1 = "http://www.montecristodm.com/viajes/t3/Imagenes/" + datosNow[6] + OffOn + ".png";
							}
							
							//miIcono1.iconSize = new GSize(16, 16);
							//miIcono1.iconAnchor = new GPoint(8, 8);
							//miIcono1.infoWindowAnchor = new GPoint(8, 8);
							// Lo metemos a las configuraciones del marcador
							//markerOptions1 = { icon:miIcono1 };
							
							//marcadorNow = new GMarker(new GLatLng(datosNow[3], datosNow[4]), markerOptions1);
							//marcadorNow.value = datosNow[0];
							
                            marcadorNow = new google.maps.Marker({
                            position: new google.maps.LatLng(datosNow[3], datosNow[4]),
                            map: map,
                            icon:miIcono1,
                            tag:datosNow[0]
                            }); 







							//obtenemos la calle de este punto
							getCalle1(new google.maps.LatLng(datosNow[3], datosNow[4]), 0);
							//getCalle(datosNow[3], datosNow[4], 0);
							
							//le agregamos el evento "click" para desplegar información
							/*GEvent.addListener(marcadorNow,"click", function() { 		     
								var myHtml1 = '<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Modulo: <b>'+ (datosNow[10].split('COMA').join(','))+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha: <b>'+ datosNow[1]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha UTC: <b>'+ datosNow[2]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fibra: <b>'+ msj+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Aperturas: <b>'+ datosNow[12]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Origen: <b><?=$origen; ?></b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Destino: <b><?=$destino; ?></b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Orientaci&oacute;n: <b>'+ datosNow[6] +'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Mercancia: <b><?=$mercancia; ?></b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Velocidad: <b>'+ datosNow[7]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Temperatura: <b>'+ datosNow[5]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Bater&iacute;a: <b>'+ datosNow[8]+'</b></div><br/>'+ ((datosNow[11]!='0')?('<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Tiempo parado: <b>'+ datosNow[11]+'</b></div><br/>'):'')+'<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Direcci&oacute;n: <b>'+(($("#calle_0").val())?$("#calle_0").val():'Cargando...')+'</b></div><br/>';
								map.openInfoWindowHtml(new GLatLng(datosNow[3], datosNow[4]), myHtml1);
							});*/
							
							//agregamos el marcador
							//map.addOverlay(marcadorNow);
							
							//movemos el mapa a la posicion actual
							
							//map.panTo(new GLatLng(datosNow[3], datosNow[4]));  //estas coordenadas estan mal
							
							//creamos recursividad
							
                            var info = new google.maps.InfoWindow();
							 
							//le agregamos el evento "click" para desplegar información
							google.maps.event.addListener(marcadorNow,"click", function() { 		     
                                var myHtml1 = '<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Modulo: <b>'+ (datosNow[10].split('COMA').join(','))+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha: <b>'+ datosNow[1]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha UTC: <b>'+ datosNow[2]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fibra: <b>'+ msj+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Aperturas: <b>'+ datosNow[12]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Origen: <b><?=$origen; ?></b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Destino: <b><?=$destino; ?></b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Orientaci&oacute;n: <b>'+ datosNow[6] +'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Mercancia: <b><?=$mercancia; ?></b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Velocidad: <b>'+ datosNow[7]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Temperatura: <b>'+ datosNow[5]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Bater&iacute;a: <b>'+ datosNow[8]+'</b></div><br/>'+ ((datosNow[11]!='0')?('<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Tiempo parado: <b>'+ datosNow[11]+'</b></div><br/>'):'')+'<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Direcci&oacute;n: <b>'+(($("#calle_0").val())?$("#calle_0").val():'Cargando...')+'</b></div><br/>';
												
                                info.setMap(null);
                                info.setContent(myHtml1);
                                info.setPosition(new google.maps.LatLng(datosNow[3], datosNow[4]));
							    info.open(map);
							});

							if (band_now==1){ //volvemos mandar llamar la peticion ajax 
								setTimeout ("mostrarNow()", 30000);  //en 30 segundos se vuelve mandar llamar la funcion 
							}							
						}
					});
                    
                  
				}

                var info1 = new google.maps.InfoWindow();
var maker1;
var primer=false;

function getCalle1(latlng,iden){
	//obtengo
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          info1.setMap(null);
          if(iden==1){
         

            if(!primer){          	
                         marker1 = new google.maps.Marker({
                         position: latlng,
                         draggable:false,
                         map: map
                         });

            primer=true;
           }
            else
             marker1.setPosition(latlng);
         
     
           
           info1.setContent(results[1].formatted_address);
           info1.setPosition(latlng);
           info1.open(map);
           
         }else if(iden==2) {

           info1.setContent(results[1].formatted_address);
           info1.setPosition(latlng);
           info1.open(map);
                      
         }

        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });

   
 }





				//]]>
			</script>
			</head>
			<body onLoad="load()" onUnload="GUnload()">
				<div id="calles"></div>	
				<div id="map" style="width:100%; height:615px;"></div>
			</body>
			</html>
<?php
		}//end if (count($datos)>0)
	}//end if ($u->isLogued)
}//end if (isset($_GET["idViaje"]) and strlen($_GET["idViaje"])>0)
ob_end_flush();
?>