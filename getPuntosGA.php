<?php
ob_start();
header("Content-type: text/html; charset=iso-8859-1");
include("conexion.php");
include("funciones.php");
$rs = New COM("ADODB.Recordset");
	
if (isset($_POST["idGeoarea"]{0})){ 
	//verificamos si el user ya inici� sesion
	$u = new User();
	if ($u->isLogued){
		//obtenemos puntos
		$rs->Open("SELECT latitud, longitud FROM geoControlData WHERE geocontrol = ".trim($_POST["idGeoarea"])." ORDER BY secuencia ASC", $conn); 					
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		if (count($datos) == 0){
			echo "false";
		}else{
			//imprimo puntos
			$strPuntos = "";
			
			for ($a = 0; $a < count($datos); $a++){
				$strPuntos .= trim($datos[$a]["latitud"])."_".trim($datos[$a]["longitud"]).(($a != count($datos)-1)?"#":"");
			}
			
			echo trim($strPuntos);
		}
	}else{
		echo "false";
	}
}else{
	echo "false";
}

ob_end_flush();
?>