<?php
ob_start();

if (isset($_GET["d"]) and strlen($_GET["d"])>0){
	ini_set("memory_limit","32M");
	set_time_limit(240);

	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");

	$u = new User();
	if ($u->isLogued){
		include('head.php');
		$serieGps = (int)myDencr(trim($_GET["d"]));

		//obtenemos velocidad
		$vm = 100;
		if (isset($_GET["vm"]) and strlen(trim($_GET["vm"]))>0){
			$vm = (int)trim($_GET["vm"]);
		}

		//obtenemos tiempo muerto
		$tm = 60;
		if (isset($_GET["tm"]) and strlen(trim($_GET["tm"]))>0){
			$tm = (int)trim($_GET["tm"]);
		}

		//sacamos fecha inicial de contrato
		$rs->Open("	SELECT
					CONVERT(CHAR(19),contratos.finicio,120) AS fechaI
					FROM (contratos
					INNER JOIN gpscontratos
					ON (gpscontratos.contrato = contratos.id))
						INNER JOIN GPs
						ON (gpscontratos.gps = GPs.id)
						WHERE
							contratos.usr = ".$u->id_user." AND
							contratos.activo = 1 AND
							GPs.serie = ".$serieGps, $conn);
		$datos = fetch_assoc($rs);
		$rs->Close();

		//obtenemos fecha inicial
		$fechaInicial = date('Y-m-d\TH:i:s');

		//si este usuario si pertenece a este contrato con gps
		if (count($datos)>0){
			//fecha inicial del contrato
			$fechaI = date_parse(trim($datos[0]['fechaI']));
			$fechaI = mktime(
						$fechaI['hour'],
						$fechaI['minute'],
						$fechaI['second'],
						$fechaI['month'],
						$fechaI['day'],
						$fechaI['year']
					);

			if (isset($_GET["fi"]) and strlen($_GET["fi"])>0){
				//fecha inicial de selecci�n
				$fechaISeleccion = mktime(
							((isset($_GET["hi"]) and strlen($_GET["hi"])>0)?$_GET["hi"]:00),
							((isset($_GET["mi"]) and strlen($_GET["mi"])>0)?$_GET["mi"]:00),
							00,
							substr($_GET["fi"],3,2),
							substr($_GET["fi"],0,2),
							substr($_GET["fi"],6,4)
						);

				//comparo fechas para valdiar que no se pase de la fecha de inicio de contrato
				if ($fechaISeleccion >= $fechaI){
					$fechaInicial = $fechaISeleccion; //fecha dentro del contrato, todo ok
				}else{
					$fechaInicial = $fechaI; //fecha fuera del contrato, ponemos por default, la fecha del inicio del contrato
				}
			}else{
				$fechaInicial = $fechaI; //no hay con que comparar, asi que ponemos fecha del inicio de contrato
			}

			$fechaInicial = date('Y-m-d\TH:i:s', $fechaInicial);	//le damos formato dal datestamp
		}

		//obtenemos fecha final
		if (isset($_GET["ff"]) and strlen($_GET["ff"])>0){
			$fechaFinal = trim(substr($_GET["ff"],6,4).'-'.substr($_GET["ff"],3,2).'-'.substr($_GET["ff"],0,2).'T'.((isset($_GET["hf"]) and strlen($_GET["hf"])>0)?$_GET["hf"]:00).':'.((isset($_GET["mf"]) and strlen($_GET["mf"])>0)?$_GET["mf"]:00).':00');
		}else{
			$fechaFinal = date('Y-m-d\TH:i:s');
		}

		//sacamos todos los puntos
		$rs->Open("	SELECT
						GPs.descripcion,
						datosghe.gps,
						CONVERT(CHAR(19),dateadd(hour,DATEDIFF(Hour,getUTCDate(),getDate()),datosghe.fechasend),120) AS fechasend,
						CONVERT(CHAR(19),datosghe.fecharecv,120) AS fecharecv,
						datosghe.lat,
						datosghe.lon,
						datosghe.alarma,
						datosghe.tambiental,
						datosghe.velocidad,
						datosghe.valido,
						datosghe.bateria,
						datosghe.aperturas,
						datosghe.rumbo
					FROM datosghe
					INNER JOIN GPs
					ON (GPs.serie = datosghe.gps)
					WHERE
						datosghe.gps = $serieGps AND
						datosghe.fecharecv >= '$fechaInicial' AND
						datosghe.fecharecv <= '$fechaFinal' AND
						datosghe.lat <> 0 AND
						datosghe.lon <> 0 AND
						datosghe.valido = 1
					ORDER BY datosghe.fechasend ASC", $conn);
		$datos = fetch_assoc($rs);
		$rs->Close();
?>
			<title>Montecristo Data Mining - Tracking Tampering Technology - Viajes - Recorrido Actual</title>
		    <script src="http://maps.google.com/maps/api/js?libraries=geometry&sensor=false" type="text/javascript"></script>
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	  
			<script type="text/javascript">
				//<![CDATA[
           
				var map;
				var a;
				var vi=0;
				var band_vi=1;
				var band_rep=0;
				var band_now=0;
				var ruta_all=1;
				var marcadores=[];
				var PointsNow=[];
				var polylineNow;
				var band_polyNow=0;
				var marcadorNow;
				var polyline;
				var Finicial=[];
				var Ffinal=[];
				var Tiempo=[];
				var Dtotal=0;
				var distancias = new Array();
				var polylineD;
				var geocoder;
				var address;
			    //geocoder = new GClientGeocoder();
				var markersArray=[];

<?php
				echo '
				//OBTENEMOS DATOS DE LA DB
				var datosdb = new Array([';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["gps"].',';
					}
					echo $datos[count($datos)-1]["gps"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo '"'.$datos[$a]["fecharecv"].'",';
					}
					echo '"'.$datos[count($datos)-1]["fecharecv"];
					echo '"],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo '"'.$datos[$a]["fechasend"].'",';
					}
					echo '"'.$datos[count($datos)-1]["fechasend"];
					echo '"],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						//19.2432N, 103.7011W (latitud,longitud) (Y,X) ->  19.2432, -103.7011
						$lat=$datos[$a]["lat"];
						switch(substr($lat,strlen($lat)-1,1)){
							case 'S': (double)$lat*=-1; break;
							case 'N': (double)$lat*=1; break;
						}
						//guardamos puntos en formato decimal
						echo '"'.$lat.'",';
					}  //end for

					$lat=$datos[count($datos)-1]["lat"];
					switch(substr($lat,strlen($lat)-1,1)){
						case 'S': (double)$lat*=-1; break;
						case 'N': (double)$lat*=1; break;
					}
					//guardamos puntos en formato decimal
					echo '"'.$lat;
					echo '"],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						//19.2432N, 103.7011W (latitud,longitud) (Y,X) ->  19.2432, -103.7011
						$lon=$datos[$a]["lon"];
						switch(substr($lon,strlen($lon)-1,1)){
							case 'W': (double)$lon*=-1; break;
							case 'E': (double)$lon*=1; break;
						}
						//guardamos puntos en formato decimal
						echo '"'.$lon.'",';
					}
					$lon=$datos[count($datos)-1]["lon"];
					switch(substr($lon,strlen($lon)-1,1)){
						case 'W': (double)$lon*=-1; break;
						case 'E': (double)$lon*=1; break;
					}
					echo '"'.$lon;
					echo '"],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["alarma"].',';
					}
					echo $datos[count($datos)-1]["alarma"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["tambiental"].',';
					}
					echo $datos[count($datos)-1]["tambiental"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["velocidad"].',';
					}
					echo $datos[count($datos)-1]["velocidad"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["valido"].',';
					}
					echo $datos[count($datos)-1]["valido"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["bateria"].',';
					}
					echo $datos[count($datos)-1]["bateria"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["rumbo"].',';
					}
					echo $datos[count($datos)-1]["rumbo"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo '"'.$datos[$a]["descripcion"].'",';
					}
					echo '"'.$datos[count($datos)-1]["descripcion"].'"';
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["aperturas"].',';
					}
					echo $datos[count($datos)-1]["aperturas"];
					echo ']
				);';
?>

				var largo=datosdb[0].length; //tama�o total de cada array dentro de "datosdb"
                
				function misdatos(micampo, minumero){  //funcion solo para tener mayor legibilidad
					var midato;
					switch(micampo){
						case "gps": midato=datosdb[0][minumero]; break;
						case "fecharecv": midato=datosdb[1][minumero]; break;
						case "fechasend": midato=datosdb[2][minumero]; break;
						case "lat": midato=datosdb[3][minumero]; break;
						case "lon": midato=datosdb[4][minumero]; break;
						case "alarma": midato=datosdb[5][minumero]; break;
						case "tambiental": midato=datosdb[6][minumero]; break;
						case "velocidad": midato=datosdb[7][minumero]; break;
						case "valido": midato=datosdb[8][minumero]; break;
						case "bateria": midato=datosdb[9][minumero]; break;
						case "rumbo": midato=datosdb[10][minumero]; break;
						case "descripcion": midato=datosdb[11][minumero]; break;
						case "aperturas": midato=datosdb[12][minumero]; break;
					}
					return midato;
				}

				function GetRumbo(numeroR){
					var elrumbo;
					switch(misdatos("rumbo",numeroR)){
						case 0: elrumbo="Norte"; break;
						case 1: elrumbo="Noreste"; break;
						case 2: elrumbo="Este"; break;
						case 3: elrumbo="Sureste"; break;
						case 4: elrumbo="Sur"; break;
						case 5: elrumbo="Suroeste"; break;
						case 6: elrumbo="Oeste"; break;
						case 7: elrumbo="Noroeste"; break;
					}

					return elrumbo;
				}

				//Obtenemos distancias
				distancias[0]=0;

				

<?php
				//obtenemos distancia entre este punto y el punto anterior
				for ($a=1; $a<count($datos); $a++){
					

					echo 'var point1=[];
                    point1.push(new google.maps.LatLng(misdatos("lat",'.($a-1).'), misdatos("lon",'.($a-1).')));
                    point1.push(new google.maps.LatLng(misdatos("lat",'.$a.'), misdatos("lon",'.$a.')));
                    Dtotal += google.maps.geometry.spherical.computeDistanceBetween(point1[0], point1[1]);
					distancias['.$a.'] = Dtotal;
                    
					'; 

            }        
				
?>
         


				function crearMarcador(idDato, Opciones){
					var msjStop = '';
					var marker;
					if (Opciones == 1){

                            marker = new google.maps.Marker({
                            position: new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)),
                            map: map,
                            icon:markerOptionsStop,
                            tag:idDato
                            });

							msjStop = '<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Duraci&oacute;n de inactividad: <b>'+ Tiempo[idDato]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Desde: <b>'+ Finicial[idDato]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Hasta: <b>'+ Ffinal[idDato]+'</b></div><br/>';
					}else if (Opciones == 2){
				
                            marker = new google.maps.Marker({
                            position: new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)),
                            map: map,
                            icon:markerOptionsAlarma,
                            tag:idDato
                            });
						
						//var marker = new GMarker(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), markerOptionsAlarma);
				
					}else if (Opciones == 3){
					        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)),
                            map: map,
                            icon:markerOptionsFast,
                            tag:idDato
                            });	

						//var marker = new GMarker(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), markerOptionsFast);
					
					}else{ //Opciones==0
						
                        marker = new google.maps.Marker({
                        position: new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)),
                        map: map,
                        icon: markerOptions,
                        tag:idDato
                        });	

						//var marker = new GMarker(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), markerOptions);
					}

					//marker.value = idDato;
					//le agregamos el evento "click" para desplegar informaci�n
					
                   
					google.maps.event.addListener(marker,"click", function() {
						var myHtml = '<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Modulo: <b>'+ misdatos('descripcion',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha: <b>'+ misdatos('fecharecv',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha UTC: <b>'+misdatos('fechasend',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fibra: <b>'+ ((misdatos('alarma',idDato)==1)?'Abierta':'No abierta')+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Aperturas: <b>'+ misdatos('aperturas',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Direcci&oacute;n: <b>'+ GetRumbo(idDato) +'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Velocidad: <b>'+misdatos('velocidad',idDato)+' km/h</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Temperatura: <b>'+ misdatos('tambiental',idDato)+'&deg; C</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Bater&iacute;a: <b>'+ misdatos('bateria',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;"><b>'+ convertirM_KM(distancias[idDato])+'</b> recorridos</div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;"><b>'+ convertirM_KM(Dtotal)+'</b> totales</div><br/>'+trim(msjStop);

						
                                info.setContent(myHtml);
                                info.setPosition(new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)));
							    info.open(map);

						//map.openInfoWindowHtml(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), myHtml);
					});

					marcadores.push(marker);
				}
                 var info = new google.maps.InfoWindow();
				//funcion que guarda en variables globales el tiempo que transcurrio un camion en velocidad 0
				function crearMarcadorAux(idDato, Fini, Ffin, Tiem) {
					Finicial[idDato]=Fini;
				   	Ffinal[idDato]=Ffin;
				   	Tiempo[idDato]=Tiem;
				   	crearMarcador(idDato, 1);
				} 


				function load() {
										


                     var canvas=document.getElementById("map");
                     $("#map").css("height",$(window).height()-20);
      
                     var latlng;
                    
                    if(misdatos("lat",0)==""){
                    	latlng = new google.maps.LatLng(19.3,-103.3);
                    	alert("No hay datos");
                    }else{

                    	latlng = new google.maps.LatLng(misdatos("lat",0),misdatos("lon",0));             
                    }
                     var settings = {
                       zoom: 13,
                       center: latlng,
                       mapTypeControl: true,
                       mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                       navigationControl: true,
                       navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                       mapTypeId: google.maps.MapTypeId.HYBRID
                     };
                     map = new google.maps.Map(canvas, settings);

                     $(window).resize(function() {
                      $("#map").css("height",$(window).height()-20);
                      });


                      
                      
						/*map = new GMap2(document.getElementById("map"));
						map.setCenter(new GLatLng(misdatos("lat",0), misdatos("lon",0)), 13);
						map.addControl(new GSmallMapControl());
						map.addControl(new GMapTypeControl());
						map.enableScrollWheelZoom();*/
				
                       /*  polylineNow = new google.maps.Polyline({
                                 path: PointsNow
                                 , map: map
                                 , strokeColor: '#F1365E'
                                 , strokeWeight: 3
                                 , strokeOpacity: 0.7
                                 
                                 });*/



                        var point=[];
                     
                       <?php //echo 'var datoPoligono=[';
						for ($a=0; $a < count($datos)-1; $a++){
							//echo 'alert(misdatos("lat",'.$a.'));';
							echo 'point.push(new google.maps.LatLng(misdatos("lat",'.$a.'), misdatos("lon",'.$a.')));';
						}?>                      
						 
                       
                        polyline = new google.maps.Polyline({
                                 path: point
                                 , map: map
                                 , strokeColor: '#CC0000'
                                 , strokeWeight: 2
                                 , strokeOpacity: 0.6
                                 
                             });
                
				                        

						/*<?php echo 'polyline = new GPolyline([';
						for ($a=0; $a < count($datos)-1; $a++){
							echo 'new GLatLng(misdatos("lat",'.$a.'), misdatos("lon",'.$a.')),';
						}?>
						new GLatLng(misdatos("lat",largo-1), misdatos("lon",largo-1))
						], "#CC0000", 2, 0.6);
						map.addOverlay(polyline);*/



						// Personalizamos el nuevo icono
					/*	var miIcono = new GIcon();
						miIcono.image = "http://www.montecristodm.com/viajes/t3/Imagenes/point.png";
						miIcono.iconSize = new GSize(16, 16);
						miIcono.iconAnchor = new GPoint(8, 8);
						miIcono.infoWindowAnchor = new GPoint(8, 8);

						// Personalizamos el icono de Stop
						var miIconoStop = new GIcon();
						miIconoStop.image = "http://www.montecristodm.com/viajes/t3/Imagenes/tm.png";
						miIconoStop.iconSize = new GSize(16, 16);
						miIconoStop.iconAnchor = new GPoint(8, 8);
						miIconoStop.infoWindowAnchor = new GPoint(8, 8);

						// Personalizamos el icono Alarma
						var miIconoAlarma = new GIcon();
						miIconoAlarma.image = "http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png";
						miIconoAlarma.iconSize = new GSize(16, 16);
						miIconoAlarma.iconAnchor = new GPoint(8, 8);
						miIconoAlarma.infoWindowAnchor = new GPoint(8, 8);

						// Personalizamos el icono Exeso de Velocidad
						var miIconoFast = new GIcon();
						miIconoFast.image = "http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png";
						miIconoFast.iconSize = new GSize(16, 16);
						miIconoFast.iconAnchor = new GPoint(8, 8);
						miIconoFast.infoWindowAnchor = new GPoint(8, 8);*/

						// Lo metemos a las configuraciones del marcador

						var miIcono = "http://www.montecristodm.com/viajes/t3/Imagenes/point.png";
						var miIconoStop = "http://www.montecristodm.com/viajes/t3/Imagenes/tm.png";
						var miIconoAlarma = "http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png";
						var miIconoFast = "http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png";

						markerOptions =  miIcono;
						markerOptionsStop = miIconoStop;
						markerOptionsAlarma = miIconoAlarma;
						markerOptionsFast = miIconoFast;

						<?php
						//creamos markadores
						for ($a=0; $a<count($datos); $a++){
							//checamos si hubo cambio de aperturas
							$cambioAperturas = false;
							if ($a > 0){
								if ($datos[$a]["aperturas"] != $datos[$a-1]["aperturas"]){
									$cambioAperturas = true;
								}
							}

							//procesamos icono
							if ($datos[$a]["alarma"] == 1 or $cambioAperturas == true){
								echo 'crearMarcador('.$a.',2);'; //fibra abierta
							}else{
								//fibra cerrada

								if ($datos[$a]["velocidad"] != 0){
									if ((int)$datos[$a]["velocidad"] > $vm){ //mostramos alarma de velocidad
										echo 'crearMarcador('.$a.',3);';
									}else{
										echo 'crearMarcador('.$a.',0);';
									}
								}else{
									//estuvo parado un tiempo... veremos si fue mas del limite
									if ($a < count($datos)-1){  //si no esta en el ultimo registro
										if ($datos[$a+1]['velocidad']!=0){ //si solo era un cero unico (sin serie de ceros)
											echo 'crearMarcador('.$a.',0);';
										}else{
											//serie de ceros: recorremos hasta encontrar el fin de la serie e imprimimos marcador
											$b = $a;

											//sacamos fecha del primer cero
											$fechaCero11 = date_parse($datos[$b]['fechasend']);
											$fechaCero11 = date('m/d/Y h:i:s A', mktime(
																					$fechaCero11['hour'],
																					$fechaCero11['minute'],
																					$fechaCero11['second'],
																					$fechaCero11['month'],
																					$fechaCero11['day'],
																					$fechaCero11['year']
																				));
																				//) - 21600);

											//convertimos fecha del primer cero a segundos para verificar si estuvo mas de 10 minutos
											$fechaCero1 = date_parse($datos[$b]['fechasend']);
											$fechaCero1 = mktime(
															$fechaCero1['hour'],
															$fechaCero1['minute'],
															$fechaCero1['second'],
															$fechaCero1['month'],
															$fechaCero1['day'],
															$fechaCero1['year']
														);

											$band_while = 0;
											while (($datos[$b]['velocidad'] == 0) and ($band_while == 0)){  //posible error de desbordamiento
												++$b;
												if ($b == count($datos)){ //desbordado, entonces decrementamos y  encendemos bandera
													--$b;
													$band_while = 1;
												}
											} //$b contendra el id del siguiente registro con velocidad diferente de cero

											//sacamos fecha del siguiente registro despues del ultimo cero
											$fechaCero22 = date_parse($datos[$b]['fechasend']);
											$fechaCero22 = date('m/d/Y h:i:s A', mktime(
																					$fechaCero22['hour'],
																					$fechaCero22['minute'],
																					$fechaCero22['second'],
																					$fechaCero22['month'],
																					$fechaCero22['day'],
																					$fechaCero22['year']
																				));
																				//) - 21600);

											//convertimos fecha del siguiente registro despues del ultimo cero, a segundos para verificar si estuvo mas de 10 minutos
											$fechaCero2 = date_parse($datos[$b]['fechasend']);
											$fechaCero2 = mktime(
															$fechaCero2['hour'],
															$fechaCero2['minute'],
															$fechaCero2['second'],
															$fechaCero2['month'],
															$fechaCero2['day'],
															$fechaCero2['year']
														);

											if (($fechaCero2 - $fechaCero1) >= ($tm * 60)){ //ponemos icono de stop
												echo 'crearMarcadorAux('.$a.', \''.$fechaCero11.'\', \''.$fechaCero22.'\', SegMinHrs('.($fechaCero2 - $fechaCero1).'));';
											}else{ //ponemos icono normal
												echo 'crearMarcador('.$a.',0);';
											}

											//movemos el puntero ($a) a la posicion del ultimo cero, para que a la siguiente vuelta se incremente solo
											$a = $b - 1;
										}
									}else{
										echo 'crearMarcador('.$a.',0);';
									}
								}
							}
						}
						?>

						//imprimimos markadores
						/*for (a=0; a < marcadores.length; a++){
							map.addOverlay(marcadores[a]);

						}*/
// aqui empiezale


//GEvent.addListener(map,'click',placeMarker);


 google.maps.event.addListener(map, 'click', function(event) {
 	     getCalle1(event.latLng,1);
     });


var info1 = new google.maps.InfoWindow();
var maker1;




function getCalle1(latlng,iden){
	//obtengo direcciones de este marcador				
	
   
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          info1.setMap(null);
          if(iden==1){
         
          if(markersArray.length>0)
          { markersArray[markersArray.length-1].setMap(null);}
           
            marker1 = new google.maps.Marker({
                         position: latlng,
                         draggable:true,
                         map: map
                         });


             google.maps.event.addListener(marker1, 'dragend', function(event) {
 	             getCalle1(marker1.getPosition(),2);
             });

          
          
           markersArray.push(marker1);
           

           info1.setContent(results[1].formatted_address);
           info1.setPosition(latlng);
           info1.open(map);
           
         }else if(iden==2) {

           info1.setContent(results[1].formatted_address);
           info1.setPosition(latlng);
           info1.open(map);
                      
         }

        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });


 }

/*function showAddress(response){

if (!response || response.Status.code != 200) {
    alert("Status Code:" + response.Status.code);
  } else {
    place =response.Placemark[0];
    point = new GLatLng(place.Point.coordinates[1],place.Point.coordinates[0]);
    if(markersArray.length>0)
     {map.removeOverlay(markersArray[markersArray.length-1]);}
      marker = new GMarker(point);
      markersArray.push(marker);
      map.addOverlay(marker);
      marker.openInfoWindowHtml(place.address);
}
}*/
//de aqui no pases
		
				}






				//]]>
	    </script>



		</head>
		<body onLoad="load()" onUnload="GUnload()">
			<div id="map" style="width:100%; height:615px;"></div>
		</body>
		</html>
<?php
	}//end if ($u->isLogued)
} //end if (isset($_GET["d"]) and strlen($_GET["d"])>0)
ob_end_flush();
?>