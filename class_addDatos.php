<?
class addDatos{
	//PROPIEDADES
	public $error = "";
	public $datos = array();
	public $gps = array();
	public $id_root = "";
	public $tipoForm = "";
			
	
	//CONSTRUCTORES
	public function __construct ($tipoForm, $id_root){
		$this->tipoForm = $tipoForm;
		
		if (strlen(trim($id_root))>0){
			$this->id_root = $id_root;
			$this->getVars();
		}else{
			$this->error = "id_root no especificado.";
		}
	}
	
	
	//M�TODOS	
	//recive las variables enviadas por Post
	public function getVars (){
		$this->datos = array();
		switch($this->tipoForm){
			case "user":
				if (isset($_POST["nombre"]) and strlen($_POST["nombre"])>0){ 	
					$this->datos["nombre"] = "'".addslashes(trim($_POST["nombre"]))."'";	
				}
				if (isset($_POST["login"]) and strlen($_POST["login"])>0){ 	
					$this->datos["login"] = "'".addslashes(trim($_POST["login"]))."'";	
				}
				if (isset($_POST["pass"]) and strlen($_POST["pass"])>0){ 	
					$this->datos["pwd"] = "'".addslashes(trim($_POST["pass"]))."'";	
				}
				if (isset($_POST["datos"]) and strlen($_POST["datos"])>0){ 	
					$this->datos["datos"] = "'".addslashes(trim($_POST["datos"]))."'";	
				}
				if (isset($_POST["tel"]) and strlen($_POST["tel"])>0){ 		
					$this->datos["telefono"] = "'".addslashes(trim($_POST["tel"]))."'";	
				}
				if (isset($_POST["email"]) and strlen($_POST["email"])>0){ 	
					$this->datos["mail1"] = "'".addslashes(trim($_POST["email"]))."'";	
				}
				if (isset($_POST["email2"]) and strlen($_POST["email2"])>0){ 	
					$this->datos["mail2"] = "'".addslashes(trim($_POST["email2"]))."'";	
				}
				if (isset($_POST["email3"]) and strlen($_POST["email3"])>0){ 	
					$this->datos["mail3"] = "'".addslashes(trim($_POST["email3"]))."'";	
				}
				if (isset($_POST["cel"]) and strlen($_POST["cel"])>0){ 		
					$this->datos["cel1"] = "'".addslashes(trim($_POST["cel"]))."'";	
				}
				if (isset($_POST["cel2"]) and strlen($_POST["cel2"])>0){ 	
					$this->datos["cel2"] = "'".addslashes(trim($_POST["cel2"]))."'";	
				}
				$this->datos["md5"] = "'".md5(trim($_POST["pass"]))."'";
				$this->datos["tipo"] = 1;
				$this->datos["admin"] = $this->id_root;
				break;
			case "gps":
				if (isset($_POST["serie"]) and strlen($_POST["serie"])>0){ 		
					$this->datos["serie"] = addslashes(trim((int)$_POST["serie"]));	
				}
				if (isset($_POST["descripcion"]) and strlen($_POST["descripcion"])>0){ 	
					$this->datos["descripcion"] = "'".addslashes(trim($_POST["descripcion"]))."'";	
				}
				if (isset($_POST["usr"]) and strlen($_POST["usr"])>0){ 			
					$this->datos["usr"] = trim((int)$_POST["usr"]);	
				}
				if (isset($_POST["tipo"]) and strlen($_POST["tipo"])>0){ 		
					$this->datos["tipo"] = trim((int)$_POST["tipo"]);	
				}
				if (isset($_POST["celular"]) and strlen($_POST["celular"])>0){ 		
					$this->datos["celular"] = "'".addslashes(trim($_POST["celular"]))."'";	
				}
				$this->datos["activo"] = 1; //por default, estara disponible para ser usado por un admin
				break;
			case "userLow":
				if (isset($_POST["nombre"]) and strlen($_POST["nombre"])>0){ 	
					$this->datos["nombre"] = "'".addslashes(trim($_POST["nombre"]))."'";	
				}
				if (isset($_POST["login"]) and strlen($_POST["login"])>0){ 	
					$this->datos["login"] = "'".addslashes(trim($_POST["login"]))."'";	
				}
				if (isset($_POST["pass"]) and strlen($_POST["pass"])>0){ 	
					$this->datos["pwd"] = "'".addslashes(trim($_POST["pass"]))."'";	
				}
				if (isset($_POST["datos"]) and strlen($_POST["datos"])>0){ 	
					$this->datos["datos"] = "'".addslashes(trim($_POST["datos"]))."'";	
				}
				if (isset($_POST["tel"]) and strlen($_POST["tel"])>0){ 		
					$this->datos["telefono"] = "'".addslashes(trim($_POST["tel"]))."'";	
				}
				if (isset($_POST["email"]) and strlen($_POST["email"])>0){ 	
					$this->datos["mail1"] = "'".addslashes(trim($_POST["email"]))."'";	
				}
				if (isset($_POST["email2"]) and strlen($_POST["email2"])>0){ 	
					$this->datos["mail2"] = "'".addslashes(trim($_POST["email2"]))."'";	
				}
				if (isset($_POST["email3"]) and strlen($_POST["email3"])>0){ 	
					$this->datos["mail3"] = "'".addslashes(trim($_POST["email3"]))."'";	
				}
				if (isset($_POST["cel"]) and strlen($_POST["cel"])>0){ 		
					$this->datos["cel1"] = "'".addslashes(trim($_POST["cel"]))."'";	
				}
				if (isset($_POST["cel2"]) and strlen($_POST["cel2"])>0){ 	
					$this->datos["cel2"] = "'".addslashes(trim($_POST["cel2"]))."'";	
				}
				$this->datos["md5"] =	"'".md5(trim($_POST["pass"]))."'";
				$this->datos["tipo"] = 2;
				$this->datos["admin"] = $this->id_root;
				break;
			case "contrato":
				if (isset($_POST["usr"]) and strlen($_POST["usr"])>0){ 			
					$this->datos["usr"] = addslashes(trim((int)$_POST["usr"]));	
				}
				if (isset($_POST["numero"]) and strlen($_POST["numero"])>0){ 		
					$this->datos["numero"] = "'".addslashes(trim($_POST["numero"]))."'";	
				}
				if (isset($_POST["descripcion"]) and strlen($_POST["descripcion"])>0){ 	
					$this->datos["descripcion"] = "'".addslashes(trim($_POST["descripcion"]))."'";	
				}
				if (isset($_POST["date1"]) and strlen($_POST["date1"])>0){ 	
					$this->datos["finicio"] = "'".addslashes(trim(substr($_POST["date1"],6,4).'-'.substr($_POST["date1"],3,2).'-'.substr($_POST["date1"],0,2).'T'.$_POST["selectHI"].':'.$_POST["selectMI"].':00'))."'";
				}
				
				$this->datos["activo"] = 1;
				$this->datos["owner"] = $this->id_root;								
				break;	
				
			case "viaje":
				if (isset($_POST["gps"]) and strlen($_POST["gps"])>0){ 			
					$this->datos["gps"] = addslashes(trim((int)$_POST["gps"]));	
				}
				if (isset($_POST["ruta"]) and strlen($_POST["ruta"])>0){ 			
					$this->datos["ruta"] = addslashes(trim((int)$_POST["ruta"]));					
				}
				if (isset($_POST["clave"]) and strlen($_POST["clave"])>0){ 		
					$this->datos["clave"] = "'".addslashes(trim($_POST["clave"]))."'";	
				}
				if (isset($_POST["origen"]) and strlen($_POST["origen"])>0){ 	
					$this->datos["origen"] = "'".addslashes(trim($_POST["origen"]))."'";	
				}
				if (isset($_POST["date1"]) and strlen($_POST["date1"])>0){ 	
					$this->datos["finicial"] = "'".addslashes(trim(substr($_POST["date1"],6,4).'-'.substr($_POST["date1"],3,2).'-'.substr($_POST["date1"],0,2).'T'.$_POST["selectHI"].':'.$_POST["selectMI"].':00'))."'";
				}
				if (isset($_POST["destino"]) and strlen($_POST["destino"])>0){ 	
					$this->datos["destino"] = "'".addslashes(trim($_POST["destino"]))."'";	
				}
				if(isset($_POST['envCorreo'])){
					if($_POST['envCorreo']=="si"){
							$this->datos["enviarAlarmas"]=1;
							if(isset($_POST["cCorreo"]) and strlen($_POST["cCorreo"])>0){
							//a�adimos los correos
							$this->datos["obs"] = "':".$_POST["cCorreo"]."'";										
							}
					}else{
							$this->datos["enviarAlarmas"]=0;
						}	
				}if (isset($_POST["mercancia"]) and strlen($_POST["mercancia"])>0){ 	
					$this->datos["mercancia"] = "'".addslashes(trim($_POST["mercancia"]))."'";	
				}
				if (isset($_POST["vel"]) and strlen($_POST["vel"])>0){ 	
					$this->datos["vel"] = addslashes(trim((int)$_POST["vel"]));	
				}
				if (isset($_POST["tiempomuerto"]) and strlen($_POST["tiempomuerto"])>0){ 	
					$this->datos["tiempomuerto"] = addslashes(trim((int)$_POST["tiempomuerto"]));	
				}
				
				//sacamos el contrato
				global $rs, $conn;
				$rs->Open("SELECT id FROM contratos WHERE activo = 1 AND usr = ".$this->id_root, $conn); 
				$datos=fetch_assoc($rs);
				$rs->Close();
								
				$this->datos["contrato"] = $datos[0]["id"];
				$this->datos["activo"] = 1;
				$this->datos["usr"] = (int)$this->id_root;									
				break;	
			case "ruta":			
				if (isset($_POST["nombre"]) and strlen($_POST["nombre"])>0){ 		
					$this->datos["nombre"] = "'".addslashes(trim($_POST["nombre"]))."'";	
				}
				if (isset($_POST["descripcion"]) and strlen($_POST["descripcion"])>0){ 	
					$this->datos["descripcion"] = "'".addslashes(trim($_POST["descripcion"]))."'";	
				}
				
				$this->datos["owner"] = $this->id_root;								
				break;	
			case "areasControl":
				if (isset($_POST["geocontrol"]) and strlen($_POST["geocontrol"])>0){ 			
					$this->datos["geocontrol"] = addslashes(trim((int)$_POST["geocontrol"]));	
				}
				if (isset($_POST["tipo"]) and strlen($_POST["tipo"])>0){ 			
					$this->datos["tipo"] = addslashes(trim((int)$_POST["tipo"]));					
				}
				if (isset($_POST["date1"]) and strlen($_POST["date1"])>0){ 	
					$this->datos["fechaInicio"] = "'".addslashes(trim(substr($_POST["date1"],6,4).'-'.substr($_POST["date1"],3,2).'-'.substr($_POST["date1"],0,2).'T'.$_POST["selectHI"].':'.$_POST["selectMI"].':00'))."'";
				}
				if (isset($_POST["date2"]) and strlen($_POST["date2"])>0){ 	
					$this->datos["fechafin"] = "'".addslashes(trim(substr($_POST["date2"],6,4).'-'.substr($_POST["date2"],3,2).'-'.substr($_POST["date2"],0,2).'T'.$_POST["selectHF"].':'.$_POST["selectMF"].':00'))."'";
				}
								
				$this->datos["activo"] = 1;							
				break;			
		}
	}
	
		
	//valida los datos en comparacion a la BD dependiendo del tipo de form
	function validarDatos(){
		global $rs, $conn;
		
		switch($this->tipoForm){
			case "user":
				$rs->Open("SELECT id FROM users WHERE login=".$this->datos["login"], $conn); 
				$datos=fetch_assoc($rs);
				$rs->Close();
				
				if (count($datos) == 0){
					return true;
				}else{
					return false;
				}
				
				break;
			case "gps":
				$rs->Open("SELECT id FROM GPs WHERE serie=".$this->datos["serie"], $conn); 
				$datos=fetch_assoc($rs);
				$rs->Close();
				
				if (count($datos) == 0){
					return true;
				}else{
					return false;
				}
				
				break;
			case "userLow":
				$rs->Open("SELECT id FROM users WHERE login=".$this->datos["login"], $conn); 
				$datos=fetch_assoc($rs);
				$rs->Close();
				
				if (count($datos) == 0){
					return true;
				}else{
					return false;
				}
				
				break;
			case "contrato":
				return true;
				
				break;
			case "viaje":
				return true;
				
				break;
			case "areasControl":
				return true;
				
				break;
			case "ruta":		
				$rs->Open("SELECT id FROM RutaControl WHERE nombre = ".$this->datos["nombre"]." AND owner = ".$this->id_root, $conn); 
				$datos=fetch_assoc($rs);
				$rs->Close();
				
				if (count($datos) == 0){
					return true;
				}else{
					return false;
				}
				
				break;
		}
	}
	
	//crea la sentencia sql dados los datos
	public function sentenciaSQL(){
		//creamos sentencia sql
		$sql["campos"] = $sql["values"] = "";
		$lenDatos = count($this->datos);
		$a = 0;
		
		foreach($this->datos as $key => $valor){
			$coma = (($a == $lenDatos-1)?'':', ');
			$sql["campos"] .= $key . $coma;
			$sql["values"] .= $valor . $coma;
			++$a;
		}
		
		return $sql;
	}
	
	//guarda los datos en la BD
	public function addDatosToBD(){
		global $rc, $conn;
		$sql = $this->sentenciaSQL(); // creamos sentencia de campos y valores
		
		switch($this->tipoForm){
			case "user":
				//guardamos datos
				$rc->CommandText = "INSERT INTO users (".$sql["campos"].") VALUES (".$sql["values"].")"; 
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
				
				break;
			case "gps":
				//guardamos datos	
				$rc->CommandText = "INSERT INTO GPs (".$sql["campos"].") VALUES (".$sql["values"].")"; 
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
				
				break;
			case "userLow":
				//guardamos datos
				$rc->CommandText = "INSERT INTO users (".$sql["campos"].") VALUES (".$sql["values"].")"; 
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
				
				break;
			case "contrato":
				//guardamos datos
				$rc->CommandText = "INSERT INTO contratos (".$sql["campos"].") VALUES (".$sql["values"].")"; 
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
				
				//guardamos GPS�s
				if (isset($_POST["gps"])){
					global $rs;
					$rs->Open("SELECT MAX(id) as idMax FROM contratos", $conn); 
					$datos=fetch_assoc($rs);
					$rs->Close();
					
					for ($a=0; $a<count($_POST["gps"]); $a++){
						$rc->CommandText = "INSERT INTO gpscontratos (gps, contrato, activo) VALUES (".$_POST["gps"][$a].", ".$datos[0]["idMax"].", 1)"; 
						$rc->CommandType = 1;
						$rc->ActiveConnection = $conn;
						$rc->Execute;
						
						//deshabilitamos gps
						$rc->CommandText = "UPDATE GPs SET activo = 0 WHERE id = ".$_POST["gps"][$a]; 
						$rc->CommandType = 1;
						$rc->ActiveConnection = $conn;
						$rc->Execute;
					}
				}
				
				break;
			case "viaje":
				//guardamos datos
				$rc->CommandText = "INSERT INTO viajes (".$sql["campos"].") VALUES (".$sql["values"].")"; 
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
							
				//insertamos comando
				/*$rc->CommandText = "INSERT INTO CMDS (comando, hora, minuto, segundo, modulo, enviado, fecha) SELECT 'B', 0, 0, 0, serie, 0, '".fechaNow()."' FROM GPS WHERE id = ".$this->datos["gps"]; 
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;*/
				//$rc->Execute;
				
				break;
			case "ruta":
				//guardamos datos
				$rc->CommandText = "INSERT INTO RutaControl (".$sql["campos"].") VALUES (".$sql["values"].")"; 
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
				
				break;
			case "areasControl":
				if (isset($_POST["gpsCheckList"]) and strlen($_POST["gpsCheckList"])>0){ 	
					$gpsList = split("_", trim($_POST["gpsCheckList"]));
					
					for ($i = 0; $i < count($gpsList); $i++){
						//guardamos datos
						$rc->CommandText = "INSERT INTO geoControlT3 (".$sql["campos"].", gps) VALUES (".$sql["values"].", ".$gpsList[$i].")"; 
						$rc->CommandType = 1;
						$rc->ActiveConnection = $conn;
						$rc->Execute;
					}
				}
				
				break;	
		}
	}
		
}//end class
?>