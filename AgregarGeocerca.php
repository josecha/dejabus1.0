<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />	
	<meta name="description" content="T3 es la forma sencilla y moderna de rastrear un contenedor en tiempo real y detectar su apertura en el momento en que esta ocurre, con un bajo costo de operaci&oacute;n." />
	<meta name="keywords" content="Rastreo,Sistema,Colima,GPS,Geoposicionamiento,Mexico,Contenedores,Rastreo Satelital" />
	<meta http-equiv="content-language" content="es" />
	<meta name="Language" content="Spanish" />
	<meta name="Distribution" content="Global" />
	<script type="text/javascript" language="javascript" charset="iso-8859-1">		
		//banderas de menu desplegable
		var stAdd;
		var stFind;
		var AddNow=0;
		var FindNow=0;
		var ModNow=0;
		
		//ID�s de los timers
		var stE = "";
		var stW = "";
		var stI = "";
		
		//banderas que detectan alguna modificacion en una variable como usersSelect y gpsList
		var gpsListChange = false;
		var usersSelectChange = false;
		var usersSelectChange = false;
		var gpsListAreaControlChange = false;
	</script>
	<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/jquery.js"></script>
	<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/funciones.js"></script>
	<script type="text/javascript" src="Js/jquery.datePicker.js"></script>
	<!-- Plugin DatePicker -->
	<script type="text/javascript" language="javascript" src="Js/date.js"></script>
				
	<title>Montecristo Data Mining - Tracking Tampering Technology - Agregar Geocerca</title>
	<link href="Estilos/estilo.css" type="text/css" rel="stylesheet" />
	<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAS8qcIPtV0x0MIRmvjcEpwxQyZyC6eC3MnCpFWkAtXr1SV4-GRBTWQBPqeRYxIRE7Di0Eumx97i0eSQ" type="text/javascript"></script>

<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0px; padding: 0px }
  #map_canvas { height: 100% }
</style>
<script type="text/javascript"
    src="https://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
var contCentro = 0;
var contRadio = 0;
var centro2;
var radio2;
  function initialize() {
    var latlng = new google.maps.LatLng(23.644524, -101.99707);	 
    var myOptions = {
      zoom: 6,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
		
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  
  google.maps.event.addListener(map, 'zoom_changed', function() {
    setTimeout(moveToDarwin, 3000);
  });

        google.maps.event.addListener(map, 'click', function (event) {
            ponerCentro(event.latLng);
        });

	  google.maps.event.addListener(map, 'rightclick', function (event) {
            ponerRadio(event.latLng);
        });
		
}
  
function ponerCentro(posicion) {
if (contCentro == 1){
centro2.setMap(null);
contCentro = 0; 
 }
  
if (contCentro == 0){
 var Centro = new google.maps.Marker({
      position: posicion, 
      map: map,
	  icon: 'centroMarker.png'
  });
  map.setCenter(posicion);
  contCentro = 1;
  centro2 = Centro;
  }
}

function ponerRadio(posicion) {
if (contRadio == 1){
radio2.setMap(null);
contRadio = 0;
}

if (contRadio == 0){
var Radio  = new google.maps.Marker({
      position: posicion, 
      map: map,
	  icon: 'radioMarker.png',
  });
  map.setCenter(posicion);
  contRadio = 1;
  radio2= Radio;
}
}

function addGeocerca(){
var nombre = document.getElementById("nombre").value;
var des = document.getElementById("descripcion").value;
var tipo = document.getElementById("icono").value;

if ((nombre =="") || (des =="")) {
alert("Llenar todos los campos")
}
else{
if ((contCentro == 0) || (contRadio == 0)){
alert ("Establecer un centro y un radio para la geocerca");
 }
 else{
 
document.getElementById("latCentro").value = centro2.getPosition().lat();
document.getElementById("lonCentro").value = centro2.getPosition().lng();
 document.getElementById("latRadio").value = radio2.getPosition().lat();
 document.getElementById("lonRadio").value = radio2.getPosition().lng();
 
 /*  Parte del envio */	

 	$.ajax({
					type: "POST",
					url: "addGeocercasToDB.php",
					data: "nombre="+encodeURIComponent($("#nombre").val())+"&descripcion="+encodeURIComponent($("#descripcion").val())+"&latCentro="+$("#latCentro").val()+"&lonCentro="+$("#lonCentro").val()+"&latRadio="+$("#latRadio").val()+"&lonRadio="+$("#lonRadio").val()+"&tipo="+$("#icono").val(),
					dataType: "html",
					contentType: "application/x-www-form-urlencoded",
                    success: function(datos){
						if (trim(datos)=='OK'){
							//eliminamos markador del centro
			alert("Geocerca agregada satisfactoriamente!");
			$("#nombre").val("");
							$("#descripcion").val("");
									document.getElementById("nombre").focus();
							
							}
						}					
				});

	/*$.ajax({
					type: "POST",
					url: "addGeocercasToDB.php",
					data: "nombre="+document.getElementById("nombre").value+"&descripcion="+document.getElementById("descripcion").value+"&latCentro="+document.getElementById("latCentro").value+"&lonCentro="+document.getElementById("lonCentro").value+"&latRadio="+document.getElementById("latRadio").value+"&lonRadio="+document.getElementById("lonRadio").value+"&tipo="+document.getElementById("icono").value,
					dataType: "html",
					contentType: "application/x-www-form-urlencoded",
					success: function(datos){
						if (trim(datos)=='OK'){
							//eliminamos markador del centro
			
							}
						}
				});
				*/
/* limpiada */ 

centro2.setMap(null);
contCentro = 0; 
radio2.setMap(null);
contRadio = 0;
document.getElementById("nombre").focus();

 }
}

 

}
  
</script>
</head>
<body onload="initialize()">
	<div class="formAddGeocercas">
			<label class="Flbl">Nombre</label>
			<input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="20" />
			<label class="Flbl">Descripci&oacute;n</label>					
			<input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="50" />
			<label class="Flbl">Tipo</label>
			<select class="Ftxt" name="icono" id="icono"><option value="1">Base</option><option value="2">Bodega</option><option value="3">Mercado</option><option value="4">Caseta</option><option value="5">Gasolinera</option><option value="6">Central de Taxis</option><option value="7">Estacionamiento</option><option value="8">Estacion de Camiones</option><option value="9">Gasolinera</option><option value="10">Taller Mecanico</option></select>
			<input class="buttonAddGeo" type="button" value="Agregar Geocerca" onClick="addGeocerca(); return false;" />
			<div>Presiona click izquierdo en el mapa para poner el centro de la geocerca; despu&eacute;s presiona click derecho para poner el radio.</div>
			<input type="hidden" name="latCentro" id="latCentro" value="0" />
			<input type="hidden" name="lonCentro" id="lonCentro" value="0" />
			<input type="hidden" name="latRadio" id="latRadio" value="0" />
			<input type="hidden" name="lonRadio" id="lonRadio" value="0" />
		</div>	
  <div id="map_canvas" style="width:80%; height:80%"></div>
</body>
</html>