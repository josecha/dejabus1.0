<?
ob_start();
include("conexion.php");
$rs = New COM("ADODB.Recordset");
$rc = New COM("ADODB.Command");

include("funciones.php");
include("head.php");
$u = new User();
if ($u->isLogued){
?> 
	<title>Montecristo Data Mining - Tracking Tampering Technology - Mis Datos</title>
   	<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
	<script type="text/javascript" language="javascript" charset="iso-8859-1">
		var browser = "<?=browser(); ?>";
		var usersSelect = '<?=$u->usersSelect(); ?>';
		var gpsList = '<?=$u->gpsList(); ?>';
		var tipo_user = '<?=$u->tipo_user; ?>';
		var thisUrlAll = encodeURIComponent('<?=thisUrlAll();?>');
	</script>
  </head>
  <body class="bodyAdmin">
	<div id="fondoAdminAll">
      <div id="fondoAdmin">
    	<div id="headAdmin">
        	<div id="headAdminLeft"></div>
            <div id="headAdmincenter"></div>
            <div id="headAdminright">
				<div id="opt_buscar" onClick="showFind();"></div>
				<div id="opt_agregar" onClick="showAdd();"></div>
			</div>
            <div class="corte"></div>
        </div>
        <div id="bodyAdmin">
        	<div id="bodyAdminLeft">
				<div class="MenuHover"><div id="M_inicio" onClick="location.href = 'index.php';"></div></div>
                <div class="MenuHover"><div id="M_sesion" onClick="location.href = 'login.php';"></div></div>
				<?=str_replace('MenuHover"><div id="M_misdatos', 'MenuSelected"><div id="M_misdatos', $u->menu_user()); ?>
                <div id="M_line_bottom"></div>
            </div>
            <div id="bodyAdminRight">
            	<div id="bodyAdminRightContentHead"></div>
                <div id="bodyAdminRightContentBody">
					<div id="desplegable" style="display:none;"></div>
                	<div id="msjError"></div>
                    <div id="msjWarning"></div>
                    <div id="msjInfo"></div>
					<a name="top"></a>
						<? $u->misDatos(); ?>
					<div id="desplegableDown" style="display:none;"></div>
                </div>
                <div id="bodyAdminRightContentFeet"></div>
            </div>
            <div class="corte"></div>
        </div>
		<?
		if (isset($_GET["mok"]) and trim($_GET["mok"])=="1"){			
			msj("i", 10, "Datos actualizados satisfactoriamente!");
		}
		?>
        <div id="feetAdmin">
        	<div id="feetAdminLeft"></div>
            <div id="feetAdmincenter">
            	<div id="creditos"></div>
            </div>
            <div id="feetAdminright"></div>
            <div class="corte"></div>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
}else{
	header("Location: login.php");
}//endif ($u->isLogued)
ob_end_flush();
?>