<?php
ob_start();
header("Content-type: text/html; charset=iso-8859-1");
include("conexion.php");
include("funciones.php");
$rs = New COM("ADODB.Recordset");
$rc = New COM("ADODB.Command");
	
if (isset($_POST["idContrato"]) and strlen(trim($_POST["idContrato"]))>0){
	//verificamos si el user ya inici� sesion
	$u = new User();
	if ($u->isLogued){
		//sacamos GPSs de este contrato
		$rs->Open("SELECT gps FROM gpscontratos WHERE contrato = ".trim($_POST["idContrato"]), $conn); 					
		$datos = fetch_assoc($rs); 
		$rs->Close();
						
		if (count($datos)>0){
			//quitamos gps de contrato
			$rc->CommandText = "UPDATE gpscontratos SET activo = 0 WHERE contrato = ".trim($_POST["idContrato"]); 
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
						
			//habilitamos todos los gps de este contrato, para otros contratos
			for ($a=0; $a<count($datos); $a++){
				$rc->CommandText = "UPDATE GPs SET activo = 1 WHERE id = ".$datos[$a]["gps"]; 
				$rc->CommandType = 1;
				$rc->ActiveConnection = $conn;
				$rc->Execute;
			}
			
			//cierro los viajes
			$rc->CommandText = "UPDATE viajes SET activo = 0, ffinal = '".fechaNow()."', obs = 'Cerrado por sistema' WHERE contrato = ".trim($_POST["idContrato"]); 
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
			
			//cierro el contrato 			
			$rc->CommandText = "UPDATE contratos SET activo = 0, ffin = '".fechaNow()."', comentarios = '".((isset($_POST["comentEnd"]) and strlen($_POST["comentEnd"])>0)?addslashes(strip_tags(trim($_POST["comentEnd"]))):'')."' WHERE id = ".trim($_POST["idContrato"]); 
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
			
			$urlRedir = urldecode(trim($_POST["urlRedir"]));
			header("Location: ".$urlRedir.((strpos($urlRedir, '?')===false)?'?ccok=1':'&ccok=1'));							
		}
	}else{
		header("Location: login.php");
	}
}

ob_end_flush();
?>