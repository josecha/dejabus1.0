<?php
$mlMensajes = array(
	0 => /*FalloConexionBD*/ 		"It was no possible to be connected to the data base",
	1 => /*FalloEjecucionSql*/ 		"It was no possible to execute the sql sentence",
	2 => /*ErrorEnTransaccion*/ 	"There was an error in some sentence of the transaction",
	3 => /*ErrorEnFecha*/ 			"There was an error in the date",
	4 => /*TimeStampInvalido*/ 		"The date of type TimeStamp is invalid",
	5 => /*FechaDateTimeInvalida*/ 	"The date of type DateTime is invalid",
	6 => /*FechaDateInvalida*/ 		"The date of type Date is invalid",
	7 => /*FechaNoValida*/ 			"The date is invalid or is out of range",
	8 => /*FechaTimeInvalida*/ 		"The date of type Time is invalid",
	9 => /*FechaYearInvalida*/ 		"The date of type Year is invalid",
	10 => /*UpdateODeleteSinWhere*/ "This trying itself to update or to eliminate a whole table without a Where",
	11 => /*CampoNoExiste*/ 		"The field doesn´t exist in the table",
	12 => /*CampoNoPuedeSerNull*/ 	"The field can´t be Null",
	12 => /*FuncionPreLoadSqlNoExiste*/ "The function PreLoadSql doesn´t exist"
);
?>