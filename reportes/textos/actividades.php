<?php
require_once("inicio.php");
require_once("header.php");
?>

<div id="contenidoActividades">
	<div id="actividadesBody">
		<div id="actividadesHorarios">
			<img onclick="$('#horarioTabla').fadeIn('slow');" src="media/actividades/horarios.jpg" />
		</div>
		<div id="actividadesImagenes">
			<div id="actividadesBallet"></div>
			<div id="actividadesBasquet"></div>
			<div id="actividadesFutbol"></div>
			<div id="actividadesKarate"></div>
			<div id="actividadesVolei"></div>
			<div id="actividadesAjedres"></div>
			<div id="actividadesImgDeportes"></div>
		</div>
		<div>
			<span class="actividadesLabel txtHide" id="txtBallet">Ballet</span>
			<span class="actividadesLabel txtHide" id="txtBasquet">Basket Ball</span>
			<span class="actividadesLabel txtHide" id="txtFutbol">Soccer</span>
			<span class="actividadesLabel txtHide" id="txtKarate">Karate</span>
			<span class="actividadesLabel txtHide" id="txtVolei">Volleyball</span>
			<span class="actividadesLabel txtHide" id="txtAjedres">Ajedres</span>
		</div>
	</div>
	<div id="horarioTabla">
		<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<th>Actividad</th>
				<th>D�a</th>
				<th>Horario</th>
			</tr>
			<tr>
				<td class="firstRow">Volley Ball</td>
				<td class="firstRow">Lunes y Jueves</td>
				<td class="firstRow">2:30 - 3:30pm</td>
			</tr>
			<tr>
				<td>Basquetball</td>
				<td>Martes</td>
				<td>2:30 - 3:30pm</td>
			</tr>
			<tr>
				<td></td>
				<td>Jueves</td>
				<td>3:30 - 4:30pm</td>
			</tr>
			<tr>
				<td>Soccer</td>
				<td>Mi�rcoles y Viernes</td>
				<td>2:30 - 3:30pm</td>
			</tr>
			<tr>
				<td>Baile</td>
				<td>Mi�rcoles</td>
				<td>2:30 - 3:30pm</td>
			</tr>
			<tr>
				<td>Porras</td>
				<td>Viernes</td>
				<td>Por definir</td>
			</tr>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		mostrarMenu();
	});
</script>

<?php
require_once("footer.php");
require_once("fin.php");
?>