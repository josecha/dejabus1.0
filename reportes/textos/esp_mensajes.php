<?php
$mlMensajes = array(
	0 => /*FalloConexionBD*/ 		"No se pudo conectar a la base de datos",
	1 => /*FalloEjecucionSql*/ 		"No se pudo ejecutar la sentencia sql",
	2 => /*ErrorEnTransaccion*/ 	"Hubo un error en alguna sentencia de la transacción",
	3 => /*ErrorEnFecha*/ 			"Hay un error en la fecha",
	4 => /*TimeStampInvalido*/ 		"La fecha de tipo TimeStamp es invalida",
	5 => /*FechaDateTimeInvalida*/ 	"La fecha de tipo DateTime es invalida",
	6 => /*FechaDateInvalida*/ 		"La fecha de tipo Date es invalida",
	7 => /*FechaNoValida*/ 			"La fecha no es valida o esta fuera de rango",
	8 => /*FechaTimeInvalida*/ 		"La fecha de tipo Time es invalida",
	9 => /*FechaYearInvalida*/ 		"La fecha de tipo Year es invalida",
	10 => /*UpdateODeleteSinWhere*/ "Se esta intentando actualizar o eliminar una tabla entera sin un Where",
	11 => /*CampoNoExiste*/ 		"El campo no existe en la tabla",
	12 => /*CampoNoPuedeSerNull*/ 	"El campo no puede ser Null"
);
?>