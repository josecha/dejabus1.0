<?php
class cTxt{
	private $textos = array(
		"dichaPor" => "Said by"
	);

	public function __get($nombre){
		return $this->textos[$nombre];
	}


	/**
	 * Obtiene un texto reemplazando los parametros enviados como varargs
	 * @param 0 -> nombre del texto
	 * @param 1...N -> parametros a reemplazar en orden
	 * @return texto como los valores reemplazados
	 * @example
	 * 	$txt->totalUsuarios = "hay {0} usuarios online y {1} offline"
	 * 	$txt->get("totalUsuarios", 3, 7);
	 *  //regresa "hay 3 usuarios online y 7 offline"
	 */
	public function get(){
		$retorno = "";
		$numArgs = func_num_args();

		if ($numArgs > 1){
			$nombre = func_get_arg(0);
			if (isset($this->textos[$nombre])){
				$vals = func_get_args();

				$patrones = array();
				$valores = array();

				//obtenemos valores
				for ($a = 1; $a < $numArgs; $a++){
					$valores[$a-1] = $vals[$a];
					$patrones[$a-1] = "{".($a-1)."}";
				}

				$retorno = str_replace($patrones, $valores, $this->textos[$nombre]);
			}
		}

		return $retorno;
	}
}
?>