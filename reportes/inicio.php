<?php
/*
 * Todo este codigo es escencial y necesario para el correcto funcionamiento del frameWork
 */

ob_start();
session_start();

function __autoload($className) {
    @include_once('clases/class.'.$className . '.php');
}

//cargamos textos multiIdiomas
require_once("textos/".sys::$idioma."_mensajes.php");
require_once("textos/".sys::$idioma."_textos.php");

/*
 * Agregár el codigo que se requiera, despues de esta linea.
 */
?>