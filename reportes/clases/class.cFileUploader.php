<?
class cFileUploader{
	/*	 * Propiedades	 */
	public $nameIn = "";    	//nombre de la foto a subir o a redimensionar (incluyendo ruta y extencion. ejemplo: fotos/foto1.jpg)
	public $nameOut = "";   	//nombre deseado de la foto (incluyendo ruta de carpeta: ejemplo: fotos/foto1) [sin extension]	public $nameInputForm = "";	//nombre del <input type="file"> del formulario que se esta enviando 
	public $size = 0;			//tama�o del archivo original
	public $tipo = "";			//tipo del archivo ejemplo: .doc .zip .jpg
	public $maxSize = 5242880; 	//imagen de 5 MB como maximo
	public $archivosPermitidos = array(
		"doc",	"docx",
		"zip",
		"rar",
		"pdf",
		"ai",
		"ppt",	"pptx", "pps",
		"xl",	"xls",	"xlsx",
		"mdb",	"mda",	"accdb",
		"jpg",	"jpeg",
		"gif",
		"png",
		"txt", "rtf"
	);

	/*	 * Constructor	 */
	public function cFileUploader($nameInputForm, $nameOut, $maxSize = 5242880){
		$this->maxSize = $maxSize;
		$this->nameInputForm = $nameInputForm;
		$nameIn = $_FILES[$this->nameInputForm]['tmp_name'];
		if ($nameIn == ""){
			throw new cError("Introduc� el nombre del archivo original");
		}elseif (!@file_exists($nameIn) and substr(trim($nameIn), 0, 4) != 'http'){
			throw new cError("El archivo no se carg&oacute; correctamente. Trata de nuevo.");
		}else{
			$this->nameIn = $nameIn;
						//sacamos datos del archivo original
			$this->getDatos();
			
			//subimos archivo
			$this->procesar($nameOut);
		}
	}
	/*	 * Metodos	 */
	//sube un archivo al servidor
	protected function procesar($nameOut){
		if ($nameOut != "" and $this->nameOut = $nameOut){
			if (!@copy($this->nameIn, $this->nameOut.$this->tipo)){
				throw new cError("Ocurri&oacute; un error al mover el archivo al servidor. Trata de nuevo.");
			}
			
			return true;
		}else{
			throw new cError("Introduce un nombre para el archivo de salida");
		}
	}
	
	//obtiene y valida los datos del archivo subido
	private function getDatos(){
		$datos = pathinfo($_FILES[$this->nameInputForm]['name']);
		$datos['extension'] = strtolower($datos['extension']);
		
		//sacamos tipo de archivo
		if (array_search($datos['extension'], $this->archivosPermitidos)){
			$this->tipo = ".".$datos['extension'];
		}else{
			throw new cError("Solo se admiten los siguientes formatos: ".join(", ",$this->archivosPermitidos));
		}
		//verificamos tama�o
		$this->size = @filesize($this->nameIn);
		if ($this->size > $this->maxSize){
			throw new cError("El archivo NO puede ser mayor a ".($this->maxSize / 1048576)." MB.");
		}
		return true;
	}
	
	//regresa la direccion del nuevo archivo pero ya con extencion: ejemplo: Archivos/archivo.doc
	public function src(){
		return $this->nameOut.$this->tipo;
	}
}
?>