package t3.web.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

public class WebUtil {
	/**
	 * Convierte un queryString en un Hashtable. NOTA: Se asume que los valores ya van codificados
	 * Por ejemplo, en el nombre o valor de las variables no deber�an existir caracteres como '&' o '='.
	 * Para hacer eso se recomienda usar WebUtil.encriptarParaQuery()
	 * @param queryString. parametros de una url con formato nombre1=valor1&nombre2=valor2
	 * @return Hashtable de las variables con sus valores: hash.get("nombre1");
	 */
	public static Hashtable<String, String> queryStringToHashtable(String queryString){
		Hashtable<String, String> hash = new Hashtable<String, String>();
		ArrayList<String> keyValuePairs = new ArrayList<String>(Arrays.asList(queryString.split("&")));
		
		String[] arrPar;
		for (String par : keyValuePairs){
			arrPar = par.split("=");
			
			if (arrPar.length == 2){
				hash.put(arrPar[0], arrPar[1]);
			}
		}
		
		return hash;
	}
	
	
	
	/**
	 * @param txt. Texto al que se le eliminar�n los signos de igualdad '=' y '&'
	 * @return String. Texto sin signos '=' y '&'
	 */
	public static String encriptarParaQuery(String txt){
		return txt.replace("=", "/ig/").replace("&", "/ap/");
	}
	
	
	/**
	 * @param txt. Texto al que se le agregaran los signos de igualdad '=' y '&'
	 * @return String. Texto con signos '=' y '&'
	 */
	public static String desencriptarDeQuery(String txt){
		return txt.replace("/ig/", "=").replace("/ap/", "&");
	}
}