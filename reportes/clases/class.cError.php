<?php
class cError extends Exception{
	/*
	 * Propiedades
	 */
	protected $msjOpcional = "";
	protected $queryFallida = "";


	public function getQueryFallida(){
		return $this->queryFallida;
	}


	public function setQueryFallida($queryFallida){
		$this->queryFallida = $queryFallida;
	}


	public function getMsjOpcional(){
		return $this->msjOpcional;
	}


	public function setMsjOpcional($msjOpcional){
		$this->msjOpcional = $msjOpcional;
	}


	/*
	 * Contructores
	 */

	public function cError($msj, $msjOpcional = "", $queryFallida = ""){
		//si esta enviando un codigo de msj
		if (is_int($msj)){
			$msj = sys::obtenerMensaje($msj);
		}

		parent::__construct($msj, 0);
		$this->setMsjOpcional($msjOpcional);
		$this->setQueryFallida($queryFallida);
	}


	/*
	 * Metodos
	 */

	public function __toString(){
		return json_encode(array("error" => $this->getMessage()." --- ".
		(($this->getMsjOpcional() != "")?$this->getMsjOpcional():"").
		((sys::$depurando)?(" ------".$this->getTraceAsString()):"").
		((sys::$depurando and $this->getQueryFallida() != "")?(" --- Consulta [".$this->getQueryFallida()."]"):"")));
	}
}
?>