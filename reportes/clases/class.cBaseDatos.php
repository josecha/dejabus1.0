<?php
class cBaseDatos{
	/*
	 * Propiedades
	 */

	public static $bdClass = null;
	private $datosBd = null;
	public $hayTransaccion = false;
	public $hayErrorEnTransaccion = false;

	/* variables para conexiones */
	protected $mysqli = null;
	protected $sqlServer = null;
	protected $sqlServerRS = null; //para obtener datos
	protected $sqlServerCM = null; //para ejecutar comandos sql


	public function getDatosBd(){
		return $this->datosBd;
	}


	public function setDatosBd($datos){
		$this->datosBd = $datos;
	}
	
	
	protected function getSqlServerRS(){
		if ($this->sqlServerRS === null){
			$this->sqlServerRS = New COM("ADODB.Recordset");
		}
		
		return $this->sqlServerRS;
	}
	
	
	protected function getSqlServerCM(){
		if ($this->sqlServerCM === null){
			$this->sqlServerCM = New COM("ADODB.Command");
			$this->sqlServerCM->CommandType = 1;
			$this->sqlServerCM->ActiveConnection = $this->sqlServer;
		}
		
		return $this->sqlServerCM;
	}


	/*
	 * Constructores
	 */

	public function cBaseDatos($tipoBaseDatos = cTipoBase::MySql, $usuario = "", $contrasenia = "", $baseDatos = "", $servidor = "", $puerto = ""){
		$this->setDatosBd(new cDatosConexion($tipoBaseDatos, $usuario, $contrasenia, $baseDatos, $servidor, $puerto));
	}

	
	/*
	 * Métodos
	 */

	/**
	 * Funcion principal, la cual inicia una unica instancia de la clase cBaseDatos
	 * @return instancia cBaseDatos
	 */
	public static function iniciar($tipoBaseDatos = cTipoBase::MySql, $usuario = "", $contrasenia = "", $baseDatos = "", $servidor = "", $puerto = ""){
		if (self::$bdClass == null){
			self::$bdClass = new cBaseDatos($tipoBaseDatos, $usuario, $contrasenia, $baseDatos, $servidor, $puerto);
		}

		self::$bdClass->conectar();
		Return self::$bdClass;
	}


	public function conectar(){
		switch ($this->getDatosBd()->getTipoBaseDatos()){
			case cTipoBase::MySql:
				$this->mysqli = new mysqli(
				$this->getDatosBd()->getServidor(),
				$this->getDatosBd()->getUsuario(),
				$this->getDatosBd()->getContrasenia(),
				$this->getDatosBd()->getBaseDatos()
				);

				if (mysqli_connect_error()){
					throw new cError(cMsj::FalloConexionBD, mysqli_connect_error());
				}

				break;
			case cTipoBase::Access:
				break;
			case cTipoBase::FireBird:
				break;
			case cTipoBase::Oracle:
				break;
			case cTipoBase::Postgres:
				break;
			case cTipoBase::SqlServer:
				$this->sqlServer = new COM('ADODB.Connection');
				$this->sqlServer->Open($this->getDatosBd()->ObtenerCadenaConexion());
								
				break;
		}
	}


	public function desconectar(){
		switch ($this->getDatosBd()->getTipoBaseDatos()){
			case cTipoBase::MySql:
				if ($this->mysqli){
					$this->mysqli->close();
				}
				break;
			case cTipoBase::Access:
				break;
			case cTipoBase::FireBird:
				break;
			case cTipoBase::Oracle:
				break;
			case cTipoBase::Postgres:
				break;
			case cTipoBase::SqlServer:
				if ($this->sqlServerRS){
					if ($this->sqlServerRS->State != 0){
						$this->sqlServerRS->Close();
					}
				}
				
				if ($this->sqlServer){
					$this->sqlServer->Close();
				}
				break;
		}
	}


	public function obtenerDatos($consultaSql){
		$tabla = new cDatosTabla();

		switch ($this->getDatosBd()->getTipoBaseDatos()){
			case cTipoBase::MySql:
				if ($datos = $this->mysqli->query($consultaSql)){
					//obtenemos detalles de las columnas
					for ($i = 0; $i < $datos->field_count; $i++){
						$detalles = $datos->fetch_field_direct($i);
						$tabla->columnas[$detalles->name] = new cDatosColumna(
							sys::tipoDatoMysqlToCTipoDato($detalles->type),
							$detalles->name,
							$detalles->orgname,
							$detalles->table,
							$detalles->orgtable
						);
					}
						
					//obtenemos filas
					while ($fila = $datos->fetch_assoc()) {
						//buscamos campos de fechas y los convertimos a DateTime
						foreach($fila as $nombre => $valor){
							if ($tabla->columnas[$nombre]->getTipo() == cTipoDato::Fecha){
								//si es tipo Fecha, lo convertimos a FechaHora
								$fila[$nombre] = cFecha::FechaToFechaHora($valor);
							}
						}

						$tabla->addFila($fila);
					}

					$datos->close();
				}else{
					if ($this->hayTransaccion){
						$this->hayErrorEnTransaccion = true;
					}
					throw new cError(cMsj::FalloEjecucionSql, $mysqli->error, $consultaSql);
				}

				break;
			case cTipoBase::Access:
				break;
			case cTipoBase::FireBird:
				break;
			case cTipoBase::Oracle:
				break;
			case cTipoBase::Postgres:
				break;
			case cTipoBase::SqlServer:
				try{
					$rs = $this->getSqlServerRS();
					$rs->Open($consultaSql, $this->sqlServer); 		
					
					//obtenemos detalles de las columnas
					if ($rs->RecordCount > 0){
						for($campo = 0; $campo < $rs->Fields->Count(); $campo++){
							$tabla->columnas[$rs->Fields[$campo]->Name] = new cDatosColumna(
								sys::tipoDatoSqlServerToCTipoDato($rs->MetaType($rs->FetchField($campo)->type)),
								$rs->Fields[$campo]->Name,
								$rs->Fields[$campo]->Name,
								"",
								""
							);
				      	}
					}	
					
					//obtenemos datos
					$assoc_array=array();
					while (!$rs->EOF){ //mientras no llegue al final de los registros
						for($campo = 0; $campo < $rs->Fields->Count(); $campo++){
							$assoc_array[$rs->Fields[$campo]->Name] = $rs->Fields[$campo]->Value;
				      	}
				      	
				     	//saltamos a la siguiente linea
				     	$tabla->addFila($assoc_array);
				     	$rs->MoveNext();
					}					
					 
					$rs->Close();
				}catch(Exception $e){
					throw new cError(cMsj::FalloEjecucionSql, $e->__toString(), $consultaSql);
				}
				
				break;
		}

		return $tabla;
	}


	public function obtenerDato($consultaSql){
		$valor = "";
		
		switch ($this->getDatosBd()->getTipoBaseDatos()){
			case cTipoBase::MySql:
				if ($datos = $this->mysqli->query($consultaSql)){
					//obtenemos el tipo de dato de este campo
					$tipoDatos = sys::tipoDatoMysqlToCTipoDato($datos->fetch_field_direct(0)->type);
						
					//obtenemos la primer fila
					if ($datos->num_rows > 0){
						$fila = $datos->fetch_assoc();
							
						foreach($fila as $nombre => $val){
							$valor = $val;
	
							if ($tipoDatos == cTipoDato::Fecha){
								//si es tipo Fecha, lo convertimos a FechaHora
								$valor = cFecha::FechaToFechaHora($valor);
							}
								
							break;
						}
					}
						
					$datos->close();
				}else{
					if ($this->hayTransaccion){
						$this->hayErrorEnTransaccion = true;
					}
						
					throw new cError(cMsj::FalloEjecucionSql, $mysqli->error, $consultaSql);
				}

				break;
			case cTipoBase::Access:
				break;
			case cTipoBase::FireBird:
				break;
			case cTipoBase::Oracle:
				break;
			case cTipoBase::Postgres:
				break;
			case cTipoBase::SqlServer:
				try{
					$rs = $this->getSqlServerRS();
					$rs->Open($consultaSql, $this->sqlServer); 		
					
					while (!$rs->EOF){ //mientras no llegue al final de los registros
						if ($rs->Fields->Count() > 0){
							$valor = $rs->Fields[0]->Value;
				      	}
				      	
				      	break;
					}	
					
					$rs->Close();
				}catch(Exception $e){
					if ($this->hayTransaccion){
						$this->hayErrorEnTransaccion = true;
					}
					
					throw new cError(cMsj::FalloEjecucionSql, $e->__toString(), $consultaSql);
				}
				
				break;
		}
		
		return $valor;
	}


	public function ejecutarComando($consultaSql){
		$resultado = 0;
		
		if ((stripos($consultaSql, "UPDATE") !== false or stripos($consultaSql, "DELETE") !== false) and stripos($consultaSql, "WHERE") === false){
			throw new cAviso(cMsj::UpdateODeleteSinWhere, "", $consultaSql);
		}

		switch ($this->getDatosBd()->getTipoBaseDatos()){
			case cTipoBase::MySql:
				if ($this->mysqli->query($consultaSql)){
					if (stripos($consultaSql, "INSERT") !== false){
						$resultado = $this->mysqli->insert_id;
					}else{
						$resultado = $this->mysqli->affected_rows;
					}
				}else{
					if ($this->hayTransaccion){
						$this->hayErrorEnTransaccion = true;
					}
						
					throw new cError(cMsj::FalloEjecucionSql, $mysqli->error, $consultaSql);
				}

				break;
			case cTipoBase::Access:
				break;
			case cTipoBase::FireBird:
				break;
			case cTipoBase::Oracle:
				break;
			case cTipoBase::Postgres:
				break;
			case cTipoBase::SqlServer:
				try{
					$cm = $this->getSqlServerCM();
					
					if (stripos($consultaSql, "INSERT") !== false){
						$cm->CommandText = $consultaSql;
						$cm->Execute();
					}else{
						$cm->CommandText = $consultaSql;
						$cm->Execute(&$affectedRows);
						$resultado = $affectedRows;
					}
				}catch(Exception $e){
					if ($this->hayTransaccion){
						$this->hayErrorEnTransaccion = true;
					}
						
					throw new cError(cMsj::FalloEjecucionSql, $e->__toString(), $consultaSql);
				}

				break;
		}

		return $resultado;
	}


	public function limpiarValor($valor){
		switch ($this->getDatosBd()->getTipoBaseDatos()){
			case cTipoBase::MySql:
				$valor = $this->mysqli->real_escape_string($valor);
				break;
			case cTipoBase::Access:
				break;
			case cTipoBase::FireBird:
				break;
			case cTipoBase::Oracle:
				break;
			case cTipoBase::Postgres:
				break;
			case cTipoBase::SqlServer:
				break;
		}

		return $valor;
	}


	/**
	 * Regresa la fecha actual de la BD del servidor
	 * @return <cFecha> Fecha actual
	 */
	public function obtenerFechaBD(){
		$fecha = "";

		switch ($this->getDatosBd()->getTipoBaseDatos()){
			case cTipoBase::MySql:
				$fecha = new cFecha($this->obtenerDato("SELECT NOW()"),cTipoFecha::DateTime);
				break;
			case cTipoBase::Access:
				break;
			case cTipoBase::FireBird:
				break;
			case cTipoBase::Oracle:
				break;
			case cTipoBase::Postgres:
				break;
			case cTipoBase::SqlServer:
				$fecha = new cFecha($this->obtenerDato("SELECT CONVERT(CHAR(19),getDate(),120) AS fecha"),cTipoFecha::DateTime);
				break;
		}

		return $fecha;
	}


	/**
	 * Inicia una transacción sql. AVISO: [MySql: Las transacciones solo funcionan en tablas que son <Type = InnoDB>
	 * Para lograr esto es necesario hacer algo como "ALTER TABLE miTabla Type = InnoDB"]
	 * @return void
	 */
	public function iniciarTransaccion(){
		if (!$this->hayTransaccion){
			switch ($this->getDatosBd()->getTipoBaseDatos()){
				case cTipoBase::MySql:
					$this->mysqli->autocommit(false);
					break;
				case cTipoBase::Access:
					break;
				case cTipoBase::FireBird:
					break;
				case cTipoBase::Oracle:
					break;
				case cTipoBase::Postgres:
					break;
				case cTipoBase::SqlServer:
					$this->sqlServer->BeginTrans();
					break;
			}
				
			$this->hayTransaccion = true;
			$this->hayErrorEnTransaccion = false;
		}
	}


	public function terminarTransaccion(){
		switch ($this->getDatosBd()->getTipoBaseDatos()){
			case cTipoBase::MySql:
				$this->mysqli->autocommit(true);
				break;
			case cTipoBase::Access:
				break;
			case cTipoBase::FireBird:
				break;
			case cTipoBase::Oracle:
				break;
			case cTipoBase::Postgres:
				break;
			case cTipoBase::SqlServer:
				break;
		}
			
		$this->hayErrorEnTransaccion = false;
		$this->hayTransaccion = false;
	}


	public function hacerTransaccion(){
		if ($this->hayTransaccion){
			if ($this->hayErrorEnTransaccion){
				$this->deshacerTransaccion();

				throw new cError(cMsj::ErrorEnTransaccion, $this->mysqli->error);
			}else{
				switch ($this->getDatosBd()->getTipoBaseDatos()){
					case cTipoBase::MySql:
						$this->mysqli->commit();
						break;
					case cTipoBase::Access:
						break;
					case cTipoBase::FireBird:
						break;
					case cTipoBase::Oracle:
						break;
					case cTipoBase::Postgres:
						break;
					case cTipoBase::SqlServer:
						$this->sqlServer->CommitTrans();
						break;
				}
			}
		}
	}


	public function deshacerTransaccion(){
		if ($this->hayTransaccion){
			switch ($this->getDatosBd()->getTipoBaseDatos()){
				case cTipoBase::MySql:
					$this->mysqli->rollback();
					break;
				case cTipoBase::Access:
					break;
				case cTipoBase::FireBird:
					break;
				case cTipoBase::Oracle:
					break;
				case cTipoBase::Postgres:
					break;
				case cTipoBase::SqlServer:
					$this->sqlServer->RollbackTrans();
					break;
			}
				
			$this->hayErrorEnTransaccion = false;
		}
	}
}
?>
