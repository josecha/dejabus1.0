<?php
class cReporteCamionColima{
	/**
	 * Propiedades
	 */
	protected $fechaInicial = null;
	protected $fechaFinal = null;
	public static $FECHA_INICIAL_DEFAULT = "2010-05-05T13:07:00";
	public static $FECHA_FINAL_DEFAULT = "2010-05-05T16:20:00";
	public static $MODULO = 2104;
	public static $ID_RUTA_CONTROL = 59;
	protected $datosReporte = null;
	
	
	/**
	 * Constructor
	 */
	public function cReporteCamionColima(){
		$this->fechaInicial = new cFecha();
		$this->fechaFinal = new cFecha();
	}
	
	
	/**
	 * Metodos
	 */
	
	public function setFechaInicial($fechaI){
		$this->fechaInicial = $fechaI;
	}
	
	
	public function getFechaInicial(){
		return $this->fechaInicial;
	}
	
	
	public function setFechaFinal($fechaF){
		$this->fechaFinal = $fechaF;
	}
	
	
	public function getFechaFinal(){
		return $this->fechaFinal;
	}
	
	
	//REGRESA 1 SI UN PUNTO (X, Y) ESTA DENTRO DE UN CIRCULO CON RADIO "R", DE LO CONTRARIO REGRESA 0
	public function inCircle($x, $y, $h, $k, $r){  
		if (sqrt(pow((double)$x-(double)$h, 2) + pow((double)$y-(double)$k, 2))  <= $r){
			return true;
		}
		
		return false;
	}
	
	
	public function segMinHrs($numeroAC){  	
		if ($numeroAC < 60){ //solo segundos
			$numeroTB_ok = $numeroAC." seg"; 
		}elseif ($numeroAC < 3600){ //solo minutos
			$numeroTB_ok = round($numeroAC / 60, 2)." min"; 
		}else{ 
			$numeroTB_ok = round($numeroAC / 3600, 2); 
			if ($numeroTB_ok == 1){ 
				$numeroTB_ok.=" hr"; 
			}else{ 
				$numeroTB_ok.=" hrs"; 
			}
		}
		
		return $numeroTB_ok;
	}
	
	
	public function recibirParametros(){
		//fecha inicial
		if (isset($_GET["fechaI"]{0})){
			try{
				$this->setFechaInicial(new cFecha(sys::limpiarTexto($_GET["fechaI"]), cTipoFecha::DateTime));
			}catch(Exception $e){
				$this->setFechaInicial(new cFecha(self::$FECHA_INICIAL_DEFAULT, cTipoFecha::DateTime));
			}
		}else{
			$this->setFechaInicial(new cFecha(self::$FECHA_INICIAL_DEFAULT, cTipoFecha::DateTime));
		}
		
		//fecha final
		if (isset($_GET["fechaF"]{0})){
			try{
				$this->setFechaFinal(new cFecha(sys::limpiarTexto($_GET["fechaF"]), cTipoFecha::DateTime));
			}catch(Exception $e){
				$this->setFechaFinal(new cFecha(self::$FECHA_FINAL_DEFAULT, cTipoFecha::DateTime));
			}
		}else{
			$this->setFechaFinal(new cFecha(self::$FECHA_FINAL_DEFAULT, cTipoFecha::DateTime));
		}
	}
	
	
public function crearReporte(){
		//sacamos las bases
		$bases = sys::$bd->obtenerDatos("	
			SELECT 
				RutaGeocercas.id, 
				RutaGeocercas.secuencia,
				geocercas.nombre, 
				geocercas.descripcion,
				geocercas.norte,
				geocercas.oeste,
				geocercas.radio,
				geocercastipo.tipo
			FROM ((RutaGeocercas 
				INNER JOIN RutaControl
				ON (RutaControl.id = RutaGeocercas.idRuta))
					INNER JOIN geocercas
					ON (geocercas.id = RutaGeocercas.idGeocerca))
						INNER JOIN geocercastipo
						ON (geocercastipo.id = geocercas.tipo)
			WHERE 
				RutaControl.id = ".self::$ID_RUTA_CONTROL."
			ORDER BY RutaGeocercas.secuencia ASC
		");	
		
		
		/** fecha inicial en movimiento **/
		//sacamos primer fecha entre la fechaInicial y fechaFinal que estuvo en movimiento 
		$fechaIMovimiento = sys::$bd->obtenerDato("
			SELECT 
				CONVERT(CHAR(19),fecharecv,120) AS fecharecv
			FROM datosghe
			WHERE datosghe.id = (
				SELECT MIN(id)
				FROM datosghe
				WHERE 
					gps = ".self::$MODULO." AND
					fecharecv >= '".$this->getFechaInicial()->toSqlDateTime()."' AND	
					fecharecv <= '".$this->getFechaFinal()->toSqlDateTime()."' AND		
					velocidad > 3 AND
					valido = 1				
			)
		"); 	
		
		
		/** fecha final en movimiento **/		
		//sacamos ultima fecha entre la fechaInicial y fechaFinal que estuvo en movimiento 
		$fechaFMovimiento = sys::$bd->obtenerDato("
			SELECT 
				CONVERT(CHAR(19),fecharecv,120) AS fecharecv
			FROM datosghe
			WHERE datosghe.id = (
				SELECT MAX(id)
				FROM datosghe
				WHERE 
					gps = ".self::$MODULO." AND
					fecharecv >= '".$this->getFechaInicial()->toSqlDateTime()."' AND	
					fecharecv <= '".$this->getFechaFinal()->toSqlDateTime()."' AND		
					velocidad > 3 AND
					valido = 1				
			)
		");	
		
		
		//sacamos los puntos de todo el viaje con los que construiremos el reporte
		$datos = sys::$bd->obtenerDatos("
			SELECT 
				id,
				fechasend,
				fecharecv,
				CONVERT(CHAR(19),fechasend,120) AS fechasendFormated,
				CONVERT(CHAR(19),fecharecv,120) AS fecharecvFormated,	
				North,
				West,
				alarma,
				tambiental,
				aperturas,
				velocidad,
				bateria,
				rumbo
			FROM datosghe
			WHERE 
				fecharecv >= '".$fechaIMovimiento."' AND
				fecharecv <= '".$fechaFMovimiento."' AND
				lat <> 0 AND
				lon <> 0 AND 
				valido = 1						
			ORDER BY fechasend ASC
		"); 	
		
		
		//CALCULAMOS TIEMPO TOTAL DEL RECORRIDO
		$fi = date_parse($fechaIMovimiento); 
		$ff = date_parse($fechaFMovimiento); 	
		
		$tiempoTotalRecorrido = $this->segMinHrs(
			mktime(
				$ff['hour'], 
				$ff['minute'], 
				$ff['second'], 
				$ff['month'], 
				$ff['day'], 
				$ff['year']
			) - 
			mktime(
				$fi['hour'], 
				$fi['minute'], 
				$fi['second'], 
				$fi['month'], 
				$fi['day'], 
				$fi['year']
			)
		);
		
		
		//inicializamos array donde guardaremos el primer punto de cada base
		$puntosEnBase = array();
		for ($b = 0; $b < $bases->largo(); $b++){
			$puntosEnBase[$bases->get($b, "secuencia")]["idPunto"] = -1;
			$puntosEnBase[$bases->get($b, "secuencia")]["idTablaBase"] = $b;
		}
		
		
		//recorremos todos los puntos
		for ($a = 0; $a < $datos->largo(); $a++){
			for ($b = 0; $b < $bases->largo(); $b++){
				$puntoX = $datos->get($a, 'West');
				$puntoY = $datos->get($a, 'North');
				
				$baseCentroX = $bases->get($b,'oeste');
				$baseCentroY = $bases->get($b,'norte');
				$baseRadio = $bases->get($b,'radio');
				
				//esta en una base
				if ($this->inCircle($puntoX, $puntoY, $baseCentroX, $baseCentroY, $baseRadio)){
					//si aun no hab�a pasado por esta base
					if ($puntosEnBase[$bases->get($b, "secuencia")]["idPunto"] == -1){
						$puntosEnBase[$bases->get($b, "secuencia")]["idPunto"] = $a;
						
						//nos vamos por el siguiente punto
						break;
					}
				}
			}
		}
		
		
		//imprimimos datos
		echo '
		<table cellpadding="0" cellspacing="0" class="tablaReporteV" style="margin-bottom:20px;">
			<tr class="headTabla">
				<td>Secuencia</td>
				<td>Base</td>
				<td>Descripci�n</td>
				<td>Fecha</td>
			</tr>
		';
		
		$a = 0;
		foreach ($puntosEnBase as $arr){
			//si si se encontr� un punto para esta base
			if ($arr["idPunto"] != -1){
				$fecha = new cFecha($datos->get($arr["idPunto"], "fechasendFormated"), cTipoFecha::DateTime);
				$fecha->restarHoras(5);
				
				echo '
				<tr class="bodyTable'.($a++ % 2).'">
					<td>'.$bases->get($arr["idTablaBase"], "secuencia").'</td>
					<td>'.$bases->get($arr["idTablaBase"], "nombre").'</td>
					<td>'.$bases->get($arr["idTablaBase"], "descripcion").'</td>
					<td>'.$fecha->toFechaFormateada(
							cFormatoFecha::DiaNumero."/".
							cFormatoFecha::MesNumero."/".
							cFormatoFecha::AnioLargo.'   '.
							cFormatoFecha::Hora12.":".
							cFormatoFecha::Minuto.":".
							cFormatoFecha::Segundo." ".
							cFormatoFecha::AmPmMinusculas
					).'</td>
				</tr>
				';
			}
		}
		
		echo '
		</table>
		';
	}
	
	
	public function crearReporte_old(){
		//sacamos las bases
		$bases = sys::$bd->obtenerDatos("	
			SELECT 
				RutaGeocercas.id, 
				RutaGeocercas.secuencia,
				geocercas.nombre, 
				geocercas.descripcion,
				geocercas.norte,
				geocercas.oeste,
				geocercas.radio,
				geocercastipo.tipo
			FROM ((RutaGeocercas 
				INNER JOIN RutaControl
				ON (RutaControl.id = RutaGeocercas.idRuta))
					INNER JOIN geocercas
					ON (geocercas.id = RutaGeocercas.idGeocerca))
						INNER JOIN geocercastipo
						ON (geocercastipo.id = geocercas.tipo)
			WHERE 
				RutaControl.id = ".self::$ID_RUTA_CONTROL."
			ORDER BY RutaGeocercas.secuencia ASC
		");	
		
		
		/** fecha inicial en movimiento **/
		//sacamos primer fecha entre la fechaInicial y fechaFinal que estuvo en movimiento 
		$fechaIMovimiento = sys::$bd->obtenerDato("
			SELECT 
				CONVERT(CHAR(19),fecharecv,120) AS fecharecv
			FROM datosghe
			WHERE datosghe.id = (
				SELECT MIN(id)
				FROM datosghe
				WHERE 
					gps = ".self::$MODULO." AND
					fecharecv >= '".$this->getFechaInicial()->toSqlDateTime()."' AND	
					fecharecv <= '".$this->getFechaFinal()->toSqlDateTime()."' AND		
					velocidad > 3 AND
					valido = 1				
			)
		"); 	
		
		
		/** fecha final en movimiento **/		
		//sacamos ultima fecha entre la fechaInicial y fechaFinal que estuvo en movimiento 
		$fechaFMovimiento = sys::$bd->obtenerDato("
			SELECT 
				CONVERT(CHAR(19),fecharecv,120) AS fecharecv
			FROM datosghe
			WHERE datosghe.id = (
				SELECT MAX(id)
				FROM datosghe
				WHERE 
					gps = ".self::$MODULO." AND
					fecharecv >= '".$this->getFechaInicial()->toSqlDateTime()."' AND	
					fecharecv <= '".$this->getFechaFinal()->toSqlDateTime()."' AND		
					velocidad > 3 AND
					valido = 1				
			)
		");	
		
		
		//sacamos los puntos de todo el viaje con los que construiremos el reporte
		$datos = sys::$bd->obtenerDatos("
			SELECT 
				id,
				fechasend,
				fecharecv,
				CONVERT(CHAR(19),fechasend,120) AS fechasendFormated,
				CONVERT(CHAR(19),fecharecv,120) AS fecharecvFormated,	
				North,
				West,
				alarma,
				tambiental,
				aperturas,
				velocidad,
				bateria,
				rumbo
			FROM datosghe
			WHERE 
				fecharecv >= '".$fechaIMovimiento."' AND
				fecharecv <= '".$fechaFMovimiento."' AND
				lat <> 0 AND
				lon <> 0 AND 
				valido = 1						
			ORDER BY fechasend ASC
		"); 	
		
		
		//CALCULAMOS TIEMPO TOTAL DEL RECORRIDO
		$fi = date_parse($fechaIMovimiento); 
		$ff = date_parse($fechaFMovimiento); 	
		
		$tiempoTotalRecorrido = $this->segMinHrs(
			mktime(
				$ff['hour'], 
				$ff['minute'], 
				$ff['second'], 
				$ff['month'], 
				$ff['day'], 
				$ff['year']
			) - 
			mktime(
				$fi['hour'], 
				$fi['minute'], 
				$fi['second'], 
				$fi['month'], 
				$fi['day'], 
				$fi['year']
			)
		);
		
				
		$tb = 0;
		$tv = 0;	  
		$bb = 0;  //contador de la base buscada actual
		$secMax = $bases->largo();  //secuencia maxima existente de bases
		$secB = 0;
		$TBC = 0; //total de bases comparadas...
		$basesPasadas = array(); // array que tendrá las bases por las que fue pasando y los datos del punto que pasó en cada base
		
		//recorremos todos los puntos
		for ($a = 1; $a < $datos->largo(); $a++){
			$TBC = 0; //reiniciamos el total de bases comparadas
			
			//sacamos coordenadas (X,Y) del camion ANTES Y AHORA
			$puntoOld['x'] = $datos->get($a-1, 'West');
			$puntoOld['y'] = $datos->get($a-1, 'North');
			$puntoNew['x'] = $datos->get($a, 'West');
			$puntoNew['y'] = $datos->get($a, 'North');
			
			$bandBF = 0;
			//mientras no se encuentre una base y mientras no se alla dado una vuelta entera a las bases
			while ($bandBF == 0){ 
				//estaba en alguna BASE 
				if ($this->inCircle($puntoOld['x'], $puntoOld['y'], $bases->get($secB,'oeste'), $bases->get($secB,'norte'), $bases->get($secB,'radio'))){
					$inBxOld = 1;
				}else{
					$inBxOld = 0;
				}
				
				//esta en alguna BASE 
				if ($this->inCircle($puntoNew['x'], $puntoNew['y'], $bases->get($secB,'oeste'), $bases->get($secB,'norte'), $bases->get($secB,'radio'))){
					$inBxNew = 1;
				}else{
					$inBxNew = 0;
				}
				
				//SALIO DE UNA BASE 
				if ($inBxOld == 1 and $inBxNew == 0){     
					$basesPasadas[$tb]['sec'] = $bases->get($secB,'secuencia');
					$basesPasadas[$tb]['IO'] = 'O';
					$basesPasadas[$tb++]['id'] = $a;
					
					--$secB; //decremntamos porque despues se vuelte a incrementar y asi buscara denuevo en esta base
					$bandBF = 1;  //nos salimos del ciclo
				}elseif ($inBxOld == 0 and $inBxNew == 1){ //ENTRO A UNA BASE 
					$basesPasadas[$tb]['sec'] = $bases->get($secB,'secuencia');
					$basesPasadas[$tb]['IO'] = 'I';
					$basesPasadas[$tb++]['id'] = $a;
					
					--$secB; //decremntamos porque despues se vuelte a incrementar y asi buscara denuevo en esta base
					$bandBF = 1;  //nos salimos del ciclo
				}
				
				++$secB; //incrementamos la secuencia buscada
				
				//si ya se llego al final de las bases, nos vamos al inicio
				if ($secB == $secMax){ 
					$secB = 0; 
				}  
				++$TBC;  //incrementamos las bases comparadas
				
				//si ya comparo toodas las bases, nos salimos del ciclo
				if ($TBC == $secMax){ 
					$bandBF = 1; 
				}  
			}//end while
		}//end for		
				
		//SIMPLIFICAMOS BASES (ELIMINAMOS DUPLICADOS) 	
		$basesOK = array();
		$tbOK = 0;
		
		//la primer base va por default
		$basesOK[$tbOK]['sec'] = $basesPasadas[0]['sec'];
		$basesOK[$tbOK]['IO'] = $basesPasadas[0]['IO'];
		$basesOK[$tbOK]['id'] = $basesPasadas[0]['id'];
		++$tbOK;
		
		// bandera que nos dice si ya se encontro la entrada y salida de una base		
		$bandIO = 0;
		
		for ($a = 1; $a < count($basesPasadas); $a++){
			//la anterior que se agregó es igual que esta base
			if ($basesPasadas[$a]['sec'] == $basesOK[$tbOK-1]['sec']){ 	
				//si aun no se han guardado la entrada y salida
				if ($bandIO == 0){
					//si el anterior era una entrada y este es una salida entonces lo guardamos
					if (($basesOK[$tbOK-1]['IO'] == 'I' and $basesPasadas[$a]['IO'] == 'O')){
						$basesOK[$tbOK]['sec'] = $basesPasadas[$a]['sec'];
						$basesOK[$tbOK]['IO'] = $basesPasadas[$a]['IO'];
						$basesOK[$tbOK]['id'] = $basesPasadas[$a]['id'];
						++$tbOK;
						$bandIO = 1; // ya encontramos la entrada y salida
					}
				}
			}else{ 
				//ya cambio de base
				$basesOK[$tbOK]['sec'] = $basesPasadas[$a]['sec'];
				$basesOK[$tbOK]['IO'] = $basesPasadas[$a]['IO'];
				$basesOK[$tbOK]['id'] = $basesPasadas[$a]['id'];
				++$tbOK;
				$bandIO = 0;
			}			
		}//end for		
		
		unset($basesPasadas);			
								
		//ACOMODAMOS ARRAY: CADA BASE EN UNA FILA
		$basesPorFila = array();
		$tbOK = 0;
		
		//la primer base va por default
		$basesPorFila[$tbOK]['sec'] = $basesOK[0]['sec'];
		if ($basesOK[0]['IO'] == 'I'){
			$basesPorFila[$tbOK]['I'] = $basesOK[0]['id'];
		}else{
			$basesPorFila[$tbOK]['O'] = $basesOK[0]['id'];
		}	
			
		for ($a = 1; $a < count($basesOK); $a++){
			//la anterior que se agregó es igual que esta base
			if ($basesOK[$a]['sec'] != $basesOK[$a-1]['sec']){ 										
				++$tbOK;					
				$basesPorFila[$tbOK]['sec'] = $basesOK[$a]['sec'];
			}		
						
			if ($basesOK[$a]['IO'] == 'I'){
				$basesPorFila[$tbOK]['I'] = $basesOK[$a]['id'];
			}else{
				$basesPorFila[$tbOK]['O'] = $basesOK[$a]['id'];
			}	
		}//end for					
		
		//SACAMOS DEMAS DATOS PARA EL REPORTE
		unset($basesOK);		
		$filas = array();
		$cf = 0;
		$a = 0;
		
		//** sacamos datos de fila 0 **//
		//datos de bases
		$filas[$cf]['id'] = $bases->get($basesPorFila[$a]['sec'],'secuencia') + 1; //id de base (secuencia)
		$filas[$cf]['nombre'] = $bases->get($basesPorFila[$a]['sec'],'nombre'); //nombre de base
		$filas[$cf]['descripcion'] = $bases->get($basesPorFila[$a]['sec'],'descripcion'); //descripción de base		
		
		//entrada
		if (isset($basesPorFila[$a]['I'])){
			$filas[$cf]['fechaInputUTC'] = $datos->get($basesPorFila[$a]['I'],'fechasend');
			$filas[$cf]['fechaInput'] = $datos->get($basesPorFila[$a]['I'],'fecharecv');
		}else{			
			//asignamos fecha de inicio de movimiento   			
			//fecha UTC = fecha - 6hrs					       
			$fechaIMovimiento = date_parse($fechaIMovimiento); 	
						
			$filas[$cf]['fechaInputUTC'] = date('d/m/Y H:i:s a', mktime($fechaIMovimiento['hour'], $fechaIMovimiento['minute'], $fechaIMovimiento['second'], $fechaIMovimiento['month'], $fechaIMovimiento['day'], $fechaIMovimiento['year']) - 21600);
			$filas[$cf]['fechaInput'] = date('d/m/Y h:i:s a', mktime($fechaIMovimiento['hour'], $fechaIMovimiento['minute'], $fechaIMovimiento['second'], $fechaIMovimiento['month'], $fechaIMovimiento['day'], $fechaIMovimiento['year']));						
		}
		
		//salida
		if (isset($basesPorFila[$a]['O'])){
			$filas[$cf]['fechaOutputUTC'] = $datos->get($basesPorFila[$a]['O'],'fechasend');
			$filas[$cf]['fechaOutput'] = $datos->get($basesPorFila[$a]['O'],'fecharecv');
		}else{
			if (isset($basesPorFila[$a + 1]['I'])){
				//agarramos la entrada del siguiente como salida de este
				$filas[$cf]['fechaOutputUTC'] = $datos->get($basesPorFila[$a + 1]['I'],'fechasend');
				$filas[$cf]['fechaOutput'] = $datos->get($basesPorFila[$a + 1]['I'],'fecharecv');
			}else{
				//agarramos la salida del siguiente como salida de este
				$filas[$cf]['fechaOutputUTC'] = $datos->get($basesPorFila[$a + 1]['O'],'fechasend');
				$filas[$cf]['fechaOutput'] = $datos->get($basesPorFila[$a + 1]['O'],'fecharecv');
			}
		}
		
		$cf++;		
		
		//** sacamos datos de las demas filas **// NOTA: estamos llegando hasta la PENULTIMA BASE
		for ($a = 1; $a < count($basesPorFila) - 1; $a++){
			//datos de bases
			$filas[$cf]['id'] = $bases->get($basesPorFila[$a]['sec'],'secuencia') + 1; //id de base (secuencia)
			$filas[$cf]['nombre'] = $bases->get($basesPorFila[$a]['sec'],'nombre'); //nombre de base
			$filas[$cf]['descripcion'] = $bases->get($basesPorFila[$a]['sec'],'descripcion'); //descripción de base			
			
			//entrada
			if (isset($basesPorFila[$a]['I'])){
				$filas[$cf]['fechaInputUTC'] = $datos->get($basesPorFila[$a]['I'],'fechasend');
				$filas[$cf]['fechaInput'] = $datos->get($basesPorFila[$a]['I'],'fecharecv');
			}else{
				if (isset($basesPorFila[$a - 1]['O'])){
					//agarramos la salida del anterior como entrada de este
					$filas[$cf]['fechaInputUTC'] = $datos->get($basesPorFila[$a - 1]['O'],'fechasend');
					$filas[$cf]['fechaInput'] = $datos->get($basesPorFila[$a - 1]['O'],'fecharecv');
				}else{
					//agarramos la entrada del anterior como entrada de este
					$filas[$cf]['fechaInputUTC'] = $datos->get($basesPorFila[$a - 1]['I'],'fechasend');
					$filas[$cf]['fechaInput'] = $datos->get($basesPorFila[$a - 1]['I'],'fecharecv');
				}
			}
			
			//salida
			if (isset($basesPorFila[$a]['O'])){
				$filas[$cf]['fechaOutputUTC'] = $datos->get($basesPorFila[$a]['O'],'fechasend');
				$filas[$cf]['fechaOutput'] = $datos->get($basesPorFila[$a]['O'],'fecharecv');
			}else{
				if (isset($basesPorFila[$a + 1]['I'])){
					//agarramos la entrada del siguiente como salida de este
					$filas[$cf]['fechaOutputUTC'] = $datos->get($basesPorFila[$a + 1]['I'],'fechasend');
					$filas[$cf]['fechaOutput'] = $datos->get($basesPorFila[$a + 1]['I'],'fecharecv');
				}else{
					//agarramos la salida del siguiente como salida de este
					$filas[$cf]['fechaOutputUTC'] = $datos->get($basesPorFila[$a + 1]['O'],'fechasend');
					$filas[$cf]['fechaOutput'] = $datos->get($basesPorFila[$a + 1]['O'],'fecharecv');
				}
			}			
			
			$cf++;
		}//end for
		
		//** sacamos datos de fila final **//
		//datos de bases
		$filas[$cf]['id'] = $bases->get($basesPorFila[$a]['sec'],'secuencia') + 1; //id de base (secuencia)
		$filas[$cf]['nombre'] = $bases->get($basesPorFila[$a]['sec'],'nombre'); //nombre de base
		$filas[$cf]['descripcion'] = $bases->get($basesPorFila[$a]['sec'],'descripcion'); //descripción de base
		
		//entrada
		if (isset($basesPorFila[$a]['I'])){
			$filas[$cf]['fechaInputUTC'] = $datos->get($basesPorFila[$a]['I'],'fechasend');
			$filas[$cf]['fechaInput'] = $datos->get($basesPorFila[$a]['I'],'fecharecv');
		}else{	
			if (isset($basesPorFila[$a - 1]['O'])){
				//agarramos la salida del anterior como entrada de este
				$filas[$cf]['fechaInputUTC'] = $datos->get($basesPorFila[$a - 1]['O'],'fechasend');
				$filas[$cf]['fechaInput'] = $datos->get($basesPorFila[$a - 1]['O'],'fecharecv');
			}else{
				//agarramos la entrada del anterior como entrada de este
				$filas[$cf]['fechaInputUTC'] = $datos->get($basesPorFila[$a - 1]['I'],'fechasend');
				$filas[$cf]['fechaInput'] = $datos->get($basesPorFila[$a - 1]['I'],'fecharecv');
			}			
		}
		
		//salida
		if (isset($basesPorFila[$a]['O'])){
			$filas[$cf]['fechaOutputUTC'] = $datos->get($basesPorFila[$a]['O'],'fechasend');
			$filas[$cf]['fechaOutput'] = $datos->get($basesPorFila[$a]['O'],'fecharecv');
		}else{
			//asignamos fecha de fin de movimiento   			
			//fecha UTC = fecha - 6hrs					       
			$fechaFMovimiento = date_parse($fechaFMovimiento); 	
						
			$filas[$cf]['fechaOutputUTC'] = date('d/m/Y h:i:s a', mktime($fechaFMovimiento['hour'], $fechaFMovimiento['minute'], $fechaFMovimiento['second'], $fechaFMovimiento['month'], $fechaFMovimiento['day'], $fechaFMovimiento['year']) - 21600);
						
			$filas[$cf]['fechaOutput'] = date('d/m/Y h:i:s a', mktime($fechaFMovimiento['hour'], $fechaFMovimiento['minute'], $fechaFMovimiento['second'], $fechaFMovimiento['month'], $fechaFMovimiento['day'], $fechaFMovimiento['year']));					
		}
				
		//CALCULAMOS TIEMPOS EN CADA BASE Y DATOS TOTALES
		for ($a = 0; $a < count($filas); $a++){
			$fi = date_parse($filas[$a]['fechaInputUTC']); 
			$ff = date_parse($filas[$a]['fechaOutputUTC']); 	
			
			$filas[$a]['tiempoTotal'] = $this->segMinHrs(
				mktime(
					$ff['hour'], 
					$ff['minute'], 
					$ff['second'], 
					$ff['month'], 
					$ff['day'], 
					$ff['year']
				) - 
				mktime(
					$fi['hour'], 
					$fi['minute'], 
					$fi['second'], 
					$fi['month'], 
					$fi['day'], 
					$fi['year']
				)
			);
		}//end for
		
		//IMPRIMIMOS REPORTE
		echo '
		<table cellpadding="0" cellspacing="0" class="tablaReporteV" style="margin-bottom:20px;">
			<tr class="headTabla">
				<td>Id</td>
				<td>Nombre</td>	
				<td>Descripci&oacute;n</td>	
				<td>Fecha Llegada</td>			
				<td>Fecha Llegada UTC</td>			
				<td>Fecha Salida</td>
				<td>Fecha Salida UTC</td>		
				<td>Duraci&oacute;n</td>	
			</tr>';
		
		for ($a = 0; $a < count($filas); $a++){
			
			//  5/5/2010 1:42:13 PM
			list($date, $time, $ampm) = explode(" ", trim($filas[$a]['fechaInputUTC']));
			list($dia, $mes, $anio) = explode("/", $date);
			list($hora, $minuto, $segundo) = explode(":", $time);
			if ($ampm == "PM") $hora += 12;
			
			$fInUtc = new cFecha();
			$fInUtc->setHora($hora);
			$fInUtc->setMinuto($minuto);
			$fInUtc->setSegundo($segundo);
			$fInUtc->setDia($dia);
			$fInUtc->setMes($mes);
			$fInUtc->setAnio($anio);
			$fInUtc->restarHoras(5);
			
			list($date, $time, $ampm) = explode(" ", trim($filas[$a]['fechaOutputUTC']));
			list($dia, $mes, $anio) = explode("/", $date);
			list($hora, $minuto, $segundo) = explode(":", $time);
			if ($ampm == "PM") $hora += 12;
			
			$fOutUtc = new cFecha();
			$fOutUtc->setHora($hora);
			$fOutUtc->setMinuto($minuto);
			$fOutUtc->setSegundo($segundo);
			$fOutUtc->setDia($dia);
			$fOutUtc->setMes($mes);
			$fOutUtc->setAnio($anio);
			$fOutUtc->restarHoras(5);

			echo '
			<tr class="bodyTable'.($a%2).'">
				<td>'.$filas[$a]['id'].'</td>
				<td>'.$filas[$a]['nombre'].'</td>
				<td>'.substr($filas[$a]['descripcion'], 0, 29).((isset($filas[$a]['descripcion'][30]))?'...':'').'</td>
				<td>'.$filas[$a]['fechaInput'].'</td>
				<td>'.$fInUtc.'</td>
				<td>'.$filas[$a]['fechaOutput'].'</td>
				<td>'.$fOutUtc.'</td>	
				<td>'.$filas[$a]['tiempoTotal'].'</td>					
			</tr>';
		}
		
		echo '			
		</table>';		
				
		//IMPRIMIMOS DATOS TOTALES
		echo '
		<table cellpadding="0" cellspacing="0" class="tablaReporteV" style="margin-bottom:20px;">
			<tr class="headTabla">		
				<td>Duraci&oacute;n Total</td>	
			</tr>
			<tr class="bodyTable'.($a%2).'">
				<td>'.$tiempoTotalRecorrido.'</td>
			</tr>
		</table>';
	}
}
?>