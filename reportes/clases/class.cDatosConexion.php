<?php
class cDatosConexion{
	/*
	 * Propiedades
	 */

	private $usuario = "";
	private $contrasenia = "";
	private $baseDatos = "";
	private $servidor = "";
	private $puerto = "";
	private $tipoBaseDatos = cTipoBase::MySql; //cTipoBase

	public function getUsuario(){
		return $this->usuario;
	}

	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}

	public function getContrasenia(){
		return $this->contrasenia;
	}

	public function setContrasenia($contrasenia){
		$this->contrasenia = $contrasenia;
	}

	public function getBaseDatos(){
		return $this->baseDatos;
	}

	public function setBaseDatos($baseDatos){
		$this->baseDatos = $baseDatos;
	}

	public function getServidor(){
		return $this->servidor;
	}

	public function setServidor($servidor){
		$this->servidor = $servidor;
	}

	public function getPuerto(){
		return $this->puerto;
	}

	public function setPuerto($puerto){
		$this->puerto = $puerto;
	}

	public function getTipoBaseDatos(){
		return $this->tipoBaseDatos;
	}

	public function setTipoBaseDatos($tipoBaseDatos){
		$this->tipoBaseDatos = $tipoBaseDatos;
	}

	/*
	 * Constructores
	 */

	public function cDatosConexion($tipoBaseDatos = TipoBase::MySql, $usuario = "", $contrasenia = "", $baseDatos = "", $servidor = "", $puerto = ""){
		$this->setTipoBaseDatos($tipoBaseDatos);
		$this->setUsuario($usuario);
		$this->setContrasenia($contrasenia);
		$this->setBaseDatos($baseDatos);
		$this->setServidor($servidor);
		$this->setPuerto($puerto);
	}

	/*
	 * Metodos
	 */

	public function ObtenerCadenaConexion(){
		$cadena = "";

		switch($this->getTipoBaseDatos()){
			case cTipoBase::SqlServer:
				$cadena = "DRIVER={SQL Server}; SERVER=".$this->getServidor().",".$this->getPuerto().";DATABASE=".$this->getBaseDatos().";UID=".$this->getUsuario().";PWD=".$this->getContrasenia().";Persist Security Info=True;";
				break;
			case cTipoBase::Access:
				break;
			case cTipoBase::Oracle:
				break;
			case cTipoBase::Postgres:
				break;
			case cTipoBase::FireBird:
				break;
		}
			
		return $cadena;
	}
}
?>