<?php
abstract class cMsj{
	const FalloConexionBD = 0;
	const FalloEjecucionSql = 1;
	const ErrorEnTransaccion = 2;
	const ErrorEnFecha = 3;
	const TimeStampInvalido = 4;
	const FechaDateTimeInvalida = 5;
	const FechaDateInvalida = 6;
	const FechaNoValida = 7;
	const FechaTimeInvalida = 8;
	const FechaYearInvalida = 9;
	const UpdateODeleteSinWhere = 10;
	const CampoNoExiste = 11;
	const CampoNoPuedeSerNull = 12;
	const FuncionPreLoadSqlNoExiste = 13;
}
?>