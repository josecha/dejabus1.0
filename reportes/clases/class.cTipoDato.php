<?php
abstract class cTipoDato{
	const Numero = 0;
	const Cadena = 1;
	const Fecha = 2;
	const FechaHora = 3;
	const TimeStamp = 4;
	const Hora = 5;
	const Bit = 6;
	const Null = 7;
	const Anio = 8;
	const Binary = 9;
}
?>