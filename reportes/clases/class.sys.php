<?php
/**
 * @author angel
 * Clase con funciones estaticas que realizan tareas especificas
 * En pocas palabras, es una librería de funciones
 */
class sys{
	/*
	 * Variables de configuración de la página
	 */
	public static $idioma = cIdiomas::espaniol;
	public static $navegador = cNavegadores::firefox;
	public static $versionNavegador = 0;
	public static $charset = "text/html; charset=ISO-8859-1";
	public static $descripcion = "";
	public static $keyWords = "";
	public static $bd = null;
	public static $actualizarTablas = true;
	public static $webUrl = "http://www.montecristodm.com/viajes/t3/reportes/";
	public static $webTitulo = "Reportes";
	public static $conectado = false;
	
	/**
	 * @var Boolean: Indica si todo el sistema estará funcionando en modo <Depuración> o en modo <Liberado>
	 * Deshabilitarlo cuando el sistema se encuentre en optimas condiciones para ser liberado
	 * Al Deshabilitarlo se dejarán de mostrar detalles de los errores como los querys sql entre otros.
	 */
	public static $depurando = true;
	
	
	public static function conectar(){
		try{
			if (!$conectado){
				self::$bd = cBaseDatos::iniciar(cTipoBase::SqlServer, "xtt3", "RASTREO", "rastreo", "148.213.1.38\SQLEXPRESS", 4638);
				$conectado = true;
			}
		}catch(Exception $e){
			echo $e;
		}
	}
	
	
	public static function desconectar(){
		try{
			if (self::$bd){
				self::$bd->desconectar();
			}
		}catch(Exception $e){
			echo $e;
		}
	}
	
	
	public static function getLenguaje(){
		$lenguaje = "Spanish";
		
		switch(self::$idioma){
			case cIdiomas::espaniol:
				$lenguaje = "Spanish";
				break;
			case cIdiomas::frances:
				$lenguaje = "French";
				break;
			case cIdiomas::ingles:
				$lenguaje = "English";
				break;
		}
		
		return $lenguaje;
	}
	

	/*
	 * @return
	 * 		regresa un string si $tipoLecturaArchivo = cTipoLecturaArchivo::string
	 * 		regresa las lineas en un array si $tipoLecturaArchivo = cTipoLecturaArchivo::arrayLineas
	 */
	public static function leerArchivo($urlArchivo, $tipoLecturaArchivo = cTipoLecturaArchivo::string){
		$archivo = "";
			
		if (file_exists($urlArchivo)){
			switch($tipoLecturaArchivo){
				case cTipoLecturaArchivo::string:
					$archivo = file_get_contents($urlArchivo);
					break;
				case cTipoLecturaArchivo::arrayLineas:
					$archivo = file($urlArchivo);
					break;
			}
		}

		return $archivo;
	}


	/*
	 * Forza un archivo a ser descargado por el navegador
	 */
	public static function forzarDescargaArchivo($urlArchivo){
		if (file_exists($urlArchivo)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($urlArchivo));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($urlArchivo));
			ob_clean();
			flush();
			readfile($urlArchivo);
		}
	}


	/*
	 * Obtiene el mensaje de error/aviso/información corresponiente del idioma
	 */
	public static function obtenerMensaje($codigoMsj){
		global $mlMensajes;
		return $mlMensajes[$codigoMsj];
	}


	public static function dosDigitos($numero){
		if (strlen((string)$numero) == 1){
			return ('0'.$numero);
		}else{
			return (string)$numero;
		}
	}


	/**
	 *Convierte el id numerico regresado por mysqli_result::fetch_field_direct::type a un tipo de dato de cTipoDato
	 *@return Elemento de cTipoDato. Ejemplo cTipoDato::Numero
	 */
	public static function tipoDatoMysqlToCTipoDato($tipoDatoMysql){
		switch($tipoDatoMysql){
			case (($tipoDatoMysql > -1 and $tipoDatoMysql < 6) || $tipoDatoMysql == 8 || $tipoDatoMysql == 9 || $tipoDatoMysql == 246):
				$tipoDatoMysql = cTipoDato::Numero;
				break;
			case ($tipoDatoMysql == 6):
				$tipoDatoMysql = cTipoDato::Null;
				break;
			case ($tipoDatoMysql == 7 || $tipoDatoMysql == 12):
				$tipoDatoMysql = cTipoDato::FechaHora;
				break;
			case ($tipoDatoMysql == 10):
				$tipoDatoMysql = cTipoDato::Fecha;
				break;
			case ($tipoDatoMysql == 11):
				$tipoDatoMysql = cTipoDato::Hora;
				break;
			case ($tipoDatoMysql == 13):
				$tipoDatoMysql = cTipoDato::Anio;
				break;
			case ($tipoDatoMysql == 16):
				$tipoDatoMysql = cTipoDato::Bit;
				break;
			default:
				$tipoDatoMysql = cTipoDato::Cadena;
				break;
		}

		return $tipoDatoMysql;
	}
	
	
	/**
	 *Convierte el caracter regresado por MetaType a un tipo de dato de cTipoDato
	 *@return Elemento de cTipoDato. Ejemplo cTipoDato::Numero
	 */
	public static function tipoDatoSqlServerToCTipoDato($tipoDatoSqlServer){
		switch($tipoDatoSqlServer){
			case ($tipoDatoSqlServer == 'I' || $tipoDatoSqlServer == 'N' || $tipoDatoSqlServer == 'L'):
				$tipoDatoSqlServer = cTipoDato::Numero;
				break;
			case ($tipoDatoSqlServer == 'D'):
				$tipoDatoSqlServer = cTipoDato::FechaHora;
				break;
			case ($tipoDatoSqlServer == 'T'):
				$tipoDatoSqlServer = cTipoDato::TimeStamp;
				break;
			case ($tipoDatoSqlServer == 'B'):
				$tipoDatoSqlServer = cTipoDato::Binary;
				break;
			default:
				$tipoDatoMysql = cTipoDato::Cadena;
				break;
		}

		return $tipoDatoSqlServer;
	}
	
	
	/**
	 *Convierte el el valor de information_schema.columns.data_type a un tipo de dato de cTipoDato
	 *@return Elemento de cTipoDato. Ejemplo cTipoDato::Numero
	 */
	public static function tipoDatoSchemaMysqlToCTipoDato($tipoDatoSchemaMysql){
		$tipos = array(
			"varchar" => 1,
			"char" => 1,
			"longtext" => 1,
			"text" => 1,
			"tinytext" => 1,
			"mediumtext" => 1,
			"int" => 0,
			"decimal" => 0,
			"bigint" => 0,
			"tinyint" => 0,
			"smallint" => 0,
			"mediumint" => 0,
			"float" => 0,
			"double" => 0,
			"bit" => 5,
			"datetime" => 3,
			"date" => 2,
			"timestamp" => 3,
			"time" => 4,
			"year" => 7,
			"binary" => 8,
			"varbinary" => 8
		);
		
		$tipo = 1; //cadena por default
		if (isset($tipos[$tipoDatoSchemaMysql])){
			$tipo = $tipos[$tipoDatoSchemaMysql];
		}
		
		return $tipo;
	}


	/**
	 * Regresa la dirección URL de la página actual
	 * @return String dirección URL
	 */
	public static function obtenerUrlActual(){
		$uri = $_SERVER['SCRIPT_NAME'];

		if ($_SERVER['QUERY_STRING'] != ''){
			$uri .= '?'.urlencode($_SERVER['QUERY_STRING']);
		}

		return basename($uri);
	}


	/**
	 * Regresa el nombre del archivo que se esta ejecutando
	 * @return String nombre del archio: ejemplo archivo.php
	 */
	public static function obtenerNombreArchivoActual(){
		return basename($_SERVER['SCRIPT_NAME']);
	}
	
	
	/**
	 * Regresa el query enviado por GET
	 * @return String serie de variables enviadas mediante GET {var1=val1&var2=val2}
	 */
	public static function obtenerQueryStringArchivoActual(){
		if (isset($_SERVER['QUERY_STRING']{0})){
			return '?'.$_SERVER['QUERY_STRING'];
		}
		
		return "";
	}


	/**
	 * Obtiene el tipo de navegador en el que se está ejecutando el script y
	 * lo asigna en la variable $browser
	 * @return void
	 */
	public static function obtenerNavegador(){
		$navegadores = array(
			"firefox", "msie", "opera", "chrome", "safari", "mozilla", 
			"seamonkey", "konqueror", "netscape", "gecko", "navigator", 
			"mosaic", "lynx", "amaya", "omniweb", "avant", "camino", "flock", "aol"
		);

		$agent = strtolower($_SERVER['HTTP_USER_AGENT']);
		foreach($navegadores as $id => $navegador){
			if (preg_match("#($navegador)[/ ]?([0-9.]*)#", $agent, $match)){
				self::$navegador = $id ;
				self::$versionNavegador = $match[2] ;
				break ;
			}
		}
	}


	/**
	 * Limpia una cadena
	 * @param $txt. Texto a ser limpiado
	 * @return String. Texto limpio
	 */
	public static function limpiarTexto($txt){
		return addslashes(self::quitComillas(strip_tags(trim($txt))));
	}
	
	
	/**
	 *Recorta un texto hasta $numMax y si sobrepasa $numMax, a $txt se le agregan 3 puntos al final
	 *@param $txt. String. texto a recortar
	 *@param $numMax. Int. Numero m�ximo permitido de caracteres
	 */
	public static function recortarTexto($txt, $numMax){
		return substr($txt, 0, $numMax).((strlen($txt)>$numMax)?"...":"");
	}
	
	
	/**
	 * @param $txt. Texto al que se le eliminarán las comillas
	 * @return String. Texto sin comillas
	 */
	public static function quitComillas($txt){
		return str_replace("'","`",str_replace('"','``',$txt));
	}


	/**
	 * @param $nombre. Nombre de la cookie. Puede ser también un array
	 * @param $valor. Valor de la cookie
	 * @param $tiempo. Tiempo de expiración <Default = 1 hora>
	 * @return si $valor es recibido, regresa void. De lo contrario regresa el valor de la cookie con el $nombre
	 * @example
	 * 	sys::cookie(array("nombre"=>"juan", "edad"=>13), 7200); 			//crea una serie de cookies durante 2 horas
	 * 	sys::cookie("nombres", array("juan", "pepe", "gabriel"), 3600);	//crea un cookie con un array interno
	 * 	sys::cookie("edad", 13); //crea una cookie simple durante una hora (default)
	 *
	 * 	$nombre = sys::cookie("nombre"); //regresa el valor de la cookie con nombre "nombre"
	 * 	$nombres = sys::cookie("nombres"); //regresa un array de valores que contenia la cookie "nombres"
	 */
	public static function cookie($nombre, $valor = "_getCookie_", $tiempo = 3600){
        //formateamos nombre
        $nombre = md5(self::$webUrl)."_".$nombre;
       
        if (isset($nombre)){
            if ($valor === "_getCookie_" and !is_array($nombre)){ //se esta intentando obtener el valor de una cookie
                $nombre = trim($nombre);

                if (isset($_COOKIE[$nombre])){
                    return $_COOKIE[$nombre];
                }else{
                    return null;
                }
            }else{ //se esta intentando asignar un valor a una cookie
                if (is_array($nombre)){
                    //sys::cookie(array("nombre"=>"juan", "edad"=>13), 7200);
                    if (is_numeric($valor) and $valor > -1){
                        $tiempo = $valor;
                    }
                    $tiempo += time();
                   
                    foreach ($nombre as $llave => $val){
                        setcookie($llave, $val, $tiempo, '/');
                    }
                }elseif (is_array($valor)){
                    $nombre = trim($nombre);
                    $tiempo += time();
                   
                    //sys::cookie("nombres", array("juan", "pepe", "gabriel"), 3600);
                    //sys::cookie("edades", array("juan" => 24, "pepe" => 54, "gabriel" => 23), 3600);
                    foreach ($valor as $llave => $val){
                        setcookie($nombre."[".$llave."]", $val, $tiempo, '/');
                    }
                }else{
                    //sys::cookie("edad", 13);
                    setcookie(trim($nombre), $valor, time() + $tiempo, '/');
                }
            }
        }
    }


	public static function eliminarCookie($nombre){
		if (isset($nombre{0})){
			//formateamos nombre
            $nombre = md5(self::$webUrl)."_".$nombre;
			setcookie(trim($nombre), "", time() - 1000);
		}
	}
	
	
	public static function redireccionar($url){
		header("Location:".$url);
	}
	
	
	public static function obtenerSO(){ 
		$Agente = $_SERVER['HTTP_USER_AGENT'];
		
		if      (ereg("Windows NT 5.1",  $Agente)) $sistemaOperativo = "Windows XP";
		elseif  (ereg("Windows NT 5.0",  $Agente)) $sistemaOperativo = "Windows 2000";
		elseif  (ereg("Win98     ",      $Agente)) $sistemaOperativo = "Windows 98"; 
		elseif  (ereg("Win",             $Agente)) $sistemaOperativo = "Windows ??"; 
		elseif  ( (ereg("Mac",           $Agente)) || 
			  (ereg("PPC", $Agente))) $sistemaOperativo = "Macintosh"; 
		elseif  (ereg("Debian",          $Agente)) $sistemaOperativo = "Debian"; 
		elseif  (ereg("Linux",           $Agente)) $sistemaOperativo = "Linux"; 
		elseif  (ereg("FreeBSD",         $Agente)) $sistemaOperativo = "FreeBSD"; 
		elseif  (ereg("SunOS",           $Agente)) $sistemaOperativo = "SunOS"; 
		elseif  (ereg("IRIX",            $Agente)) $sistemaOperativo = "IRIX"; 
		elseif  (ereg("BeOS",            $Agente)) $sistemaOperativo = "BeOS"; 
		elseif  (ereg("OS/2",            $Agente)) $sistemaOperativo = "OS/2"; 
		elseif  (ereg("AIX",             $Agente)) $sistemaOperativo = "AIX"; 
		else   $sistemaOperativo = "Desconocido"; 
	 
		return $sistemaOperativo; 
	}
}
?>