<?php
abstract class cTabla{
	/*
	 * Propiedades
	 */
	protected $campos = array();
	/*
	 * Array que usaremos como buffer para lograr hacer transacciones logicas
	 */
	protected $camposBuff = array();
	/*
	 * Array con los nombres (String) de los campos que son llaves. 
	 */
	protected $camposId = array();
	/*
	 * clase cBaseDatos usada para meter/sacar datos de la tabla. <Default = sys::$bd>
	 */
	public $bd = null; 
	private $nombreTabla = null;
	private $estado = cEstadoTabla::nuevo;
	/*
	 * Contiene el ultimo error lanzado por esta tabla. <cMsj>
	 */
	public $error = null;
	/*
	 * Bandera para verificar si se encontraron datos o no
	 */
	protected $hayDatos = false;
	/*
	 * Ultimo Id insertado
	 */
	protected $ultimoIdInsertado = 0;
	
	
	public function getUltimoIdInsertado(){
		return $this->ultimoIdInsertado;
	}
	
	
	public function setUltimoIdInsertado($ultimoIdInsertado){
		$this->ultimoIdInsertado = $ultimoIdInsertado;
	}
	
	
	public function getNombreTabla(){
		return $this->nombreTabla;
	}
	
	
	public function setNombreTabla($nombreTabla){
		$this->nombreTabla = $nombreTabla;
	}
	
	
	public function getEstado(){
		return $this->estado;
	}
	
	
	public function setEstado($estado){
		$this->estado = $estado;
	}
	
	
	public function setBD($bd){
		$this->bd = $bd;
	}
	
	
	public function getBD(){
		return $this->bd;
	}
	
	public function getNombreTablaSesion(){
		return $this->getBD()->getDatosBd()->getBaseDatos()."_".$this->getNombreTabla();
	}
	
	
	/**
	 * Constructor
	 * Caulquier subclase de esta clase debe primeramente mandar llamar el contructor padre con parent::cTabla();
	 * @param $obtenerDatos. boolean. Indica si la clase al crearse debe obtener los datos de la BD o no.
	 * @param $valoresLlave. array/string. Valores a introducir en los campos llaves de esta tabla para poder
	 * facilmente meter y sacar datos de la BD.
	 * @param $bd. cBaseDatos. base de datos usada para meter/sacar datos a esta tabla. <Default = sys::$bd>
	 * @example
	 * 	$clase = new tCliente(); //crea un cliente indefinido (es decir, sin datos que lo identifiquen)
	 * 	$clase = new tOficios(array("oficio"=>"programador", "departamento"=>14));
	 * 	$clase = new tCliente(148, true); //crea el cliente con la unica llave primaria igual a 148 
	 * 		y saca los datos de la BD
	 * 	$clase = new tCliente(null, false, $otraBaseDatos); //crea un cliente basandose en una base de datos 
	 * 															diferente de la del sistema
	 */
	public function	cTabla($valoresLlave = null, $obtenerDatos = false, $bd = null){
		if ($bd){
			$this->setBD($bd);
		}else{
			$this->setBD(sys::$bd);
		}
		
		if (isset($_SESSION[$this->getNombreTablaSesion()]) and $_SESSION[$this->getNombreTablaSesion()] != null and !sys::$actualizarTablas){
			$objTemp = unserialize($_SESSION[$this->getNombreTablaSesion()]);
			/*
			 * copiamos la estructura de la clase guardada en sesion
			 * aqui podemos copiar demás variables que sean necesarias futuramente
			 */
			$this->campos = $objTemp->campos;
			$this->camposId = $objTemp->camposId;
			
			unset($objTemp);
		}else{
			//obtenemos estructura desde la BD
			$this->obtenerEstructura();
			
			$_SESSION[$this->getNombreTablaSesion()] = serialize($this);
		}
		
		if ($valoresLlave){
			if (is_array($valoresLlave)){
				//hay varias llaves
				foreach($valoresLlave as $llave => $valor){
					$this->campo($llave, $valor);
				}
			}else{
				//solo hay una llave
				if (count($this->camposId) > 0){
					$this->campo($this->camposId[0], $valoresLlave);
				}
			}
		}
		
		if ($obtenerDatos){
			$this->obtenerDatos();
		}
	}
	
	
	/*
	 * Métodos
	 */
	protected function obtenerEstructura(){
		$this->getBD()->ejecutarComando("use information_schema");
		$datos = $this->getBD()->obtenerDatos("SELECT * FROM COLUMNS WHERE table_name = '".$this->getNombreTabla()."' AND TABLE_SCHEMA = '".$this->getBD()->getDatosBd()->getBaseDatos()."' ORDER BY ORDINAL_POSITION ASC");
		
		if ($datos->hayDatos()){
			$nombreCampo = "";
			$this->camposId = array();
			
			for ($a = 0; $a < $datos->largo(); $a++){
				$nombreCampo = $datos->get($a, "COLUMN_NAME");
				$tipoCampo = $datos->get($a, "COLUMN_KEY");
				
				$this->campos[$nombreCampo] = new cCampo($nombreCampo);
				$this->campos[$nombreCampo]->setTipo(sys::tipoDatoSchemaMysqlToCTipoDato($datos->get($a, "DATA_TYPE")));
				$this->campos[$nombreCampo]->setNombreTipo($datos->get($a, "DATA_TYPE"));
				$this->campos[$nombreCampo]->setDefault($datos->get($a, "COLUMN_DEFAULT"));
				$this->campos[$nombreCampo]->setPresicionNumerica($datos->get($a, "NUMERIC_PRECISION"));
				$this->campos[$nombreCampo]->setEscalaNumerica($datos->get($a, "NUMERIC_SCALE"));
				$this->campos[$nombreCampo]->setEsLlaveForanea(($tipoCampo == "MUL"));
				$this->campos[$nombreCampo]->setEsLlaveForanea(($tipoCampo == "UNI"));
				$this->campos[$nombreCampo]->setMaxLargo($datos->get($a, "CHARACTER_MAXIMUM_LENGTH"));
				$this->campos[$nombreCampo]->setUsaDefault(($datos->get($a, "COLUMN_DEFAULT") != null));
				$this->campos[$nombreCampo]->setEsNullable(($datos->get($a, "IS_NULLABLE") == "YES"));
				$this->campos[$nombreCampo]->setEsAutoIncrementable(($datos->get($a, "EXTRA") == "auto_increment"));
				
				if ($tipoCampo == "PRI"){
					$this->campos[$nombreCampo]->setEsLlavePrimaria(true);
					$this->camposId[] = $nombreCampo;
				}
			}
		}
		
		$this->getBD()->ejecutarComando("use ".$this->getBD()->getDatosBd()->getBaseDatos());
	}
	
	
	/**
	 *Obtiene/asigna un valor a un campo de la tabla
	 *@return <cTabla/mixed> Si se esta obtieniendo un campo, regresa el valor. De lo contrario regresa una referencia a este objeto
	 *@example
	 *	$cliente->campo("nombre", "juan");
	 *	$nombre = $cliente->campo("nombre");
	 */
	public function campo($nombre, $valor = "_getCampo_"){
		if (array_key_exists($nombre, $this->campos)){
			if ($valor === "_getCampo_"){
				return $this->campos[$nombre]->getValor();
			}else{
				$this->campos[$nombre]->setValor($valor);
				$this->setEstado(cEstadoTabla::modificado);
				
				return $this;
			}
		}else{
			throw new cError(cMsj::CampoNoExiste, "Campo: ". $nombre);
		}
	}
	
	
	/**
	 *Obtiene un objeto cCampo de la tabla
	 *@example
	 *	$tipoCampo = $cliente->objCampo("nombre")->getTipo();
	 */
	public function objCampo($nombre){
		if (array_key_exists($nombre, $this->campos)){
			return $this->campos[$nombre];
		}else{
			throw new cError(cMsj::CampoNoExiste, "Campo: ". $nombre);
		}
	}
	
	
	/**
	 * Obtiene/asigna algunos/todos los campos en un array asociativo
	 * @param $regresarObjCampos <opcional> <default = false> <boolean> 
	 * si es true, regresa un array de objetos tipo cCampo. 
	 * Si es false, regresa un array como nombre:valor
	 * Puede recibir un array especificando los campos que desea regresar/ingresar
	 * 	NOTA: si el array es asociativo, significa que se estan ingresando datos
	 * 		  si el array es de indice numerico, significa que se estan obteniendo los datos
	 * @param $nombreCampos. <Valido solo si $regresarObjCampos = true>. array de nombres de los campos que se desea obtener
	 * @return <cTabla/mixed> Si se esta obtieniendo una serie de campos, regresa los valores. De lo contrario regresa una referencia a este objeto
	 * @example
	 * 	list($nombre, $edad, $sexo) = $cliente->campos(array("nombre", "edad", "sexo"));
	 * 	$todosLosCampos = $cliente->campos();			$nombre = $todosLosCampos["nombre"];
	 * 	$todosLosObjCampos = $cliente->campos(true);	$tipoDatoNombre = $todosLosCampos["nombre"]->getTipo();
	 * 	$todosLosObjCampos = $cliente->campos(true, array("nombre", "edad"));	$tipoDatoNombre = $todosLosCampos["nombre"]->getTipo(); $tipoDatoEdad = $todosLosCampos["edad"]->getTipo();
	 * 	$cliente->campos(array("nombre"=>"juan", "sexo"=>"H", "edad"=>23)); //inserta los campos especificados a la tabla 
	 */
	public function campos($regresarObjCampos = false, $nombreCampos = null){
		if ($regresarObjCampos === true){
			//array de objetos cCampo
			if (is_array($nombreCampos)){
				$objCampos = array();
				
				foreach($nombreCampos as $campo){
					$objCampos[] = $this->objCampo($campo);
				}
				
				return $objCampos;
			}else{
				return $this->campos;
			}
		}elseif (is_array($regresarObjCampos)){
			if (array_key_exists(0, $regresarObjCampos)){
				//array numerico, se estan obteniendo campos
				$camp = array();
				
				foreach($regresarObjCampos as $campo){
					if (array_key_exists($campo, $this->campos)){
						$camp[$campo] = $this->campos[$campo]->getValor();
					}else{
						throw new cError(cMsj::CampoNoExiste, "Campo: ". $campo);
					}
				}

				return $camp;
			}else{
				//array asociativo, se estan insertando datos a los campos
				foreach($regresarObjCampos as $llave => $valor){
					$this->campo($llave, $valor);
				}
				
				return $this;
			}
		}else{
			//todos los campos
			$camp = array();
			foreach($this->campos as $campo){
				$camp[$campo->getNombre()] = $campo->getValor();
			}
			
			return $camp;
		}
	}
	
	
	/**
	 * Regresa un array con el nombre de todos los campos de la tabla
	 * @return array
	 */
	public function nombreCampos(){
		$nomCampos = array();
		foreach($this->campos as $campo){
			$nomCampos[] = $campo->getNombre();
		}
		
		return $nomCampos;
	}
	
	
	/**
	 * Verifica si la clase contiene datos o no
	 * return Boolean. True si se encontraron datos, de lo contrario regresa False
	 */
	public function hayDatos(){
		return $this->hayDatos;
	}
	
	
	/**
	 *@param $condicion <opcional>. String. condicion Sql para seleccionar los datos
	 *@return $this. <cTabla>
	 *@example
	 *	$clase->obtenerDatos();
	 *	$clase->obtenerDatos("nombre = 'jose'");
	 */
	public function obtenerDatos($condicion = null){
		if ($condicion == null){
			/*	la condicion sql será basada en los campos llave
			 * 	$clase->borrarDatos();
			 */
			$separador = "";
			foreach($this->camposId as $campoId){
				$condicion .= $separador.$this->campos[$campoId]->getNombre()." = ".$this->formatearValorSql($this->campos[$campoId]->getValor(), $this->campos[$campoId]->getTipo());
				$separador = " and ";
			}
		}
		
		//obtenemos datos;
		$datos = $this->getBD()->obtenerDatos("SELECT * FROM ".$this->getNombreTabla()." WHERE ".$condicion);
		$this->hayDatos = false;
		
		//guardamos los datos en sus respectivos campos
		if ($datos->hayDatos()){
			$this->hayDatos = true;
			
			for ($a = 0; $a < $datos->largo(); $a++){
				foreach ($datos->get($a) as $llave => $valor){
					$this->campo($llave, $valor);
				}
			}
			
			//modificamos estado
			$this->setEstado(cEstadoTabla::actualizado);
		}
		
		//regresamos referencia al mismo objeto
		return $this;
	}
	
	
	/**
	 *@param $condicion <opcional>. String. condicion Sql para seleccionar los objetos
	 *@return array<cTabla>
	 *@example
	 *	$personas = tPersona::obtenerObjetos("nombre LIKE '%angel%'");
	 *
	 *	foreach ($personas as $persona){
	 *		echo "Nombre: ".$persona->campo("nombre");
	 *		echo "Edad: ".$persona->campo("edad");
	 *	}
	 */
	public static function obtenerObjetos($nombreSubClaseTabla = "", $condicion = "1 = 1", $ordenarPor = null){
		//obtenemos datos;
		$objAux = new $nombreSubClaseTabla();
		$datos = sys::$bd->obtenerDatos("SELECT * FROM ".$objAux->getNombreTabla()." WHERE ".$condicion.(($ordenarPor!=null)?(" ORDER BY ".$ordenarPor):""));
		
		unset($objAux);
		$objetos = array();
		
		//guardamos los datos en sus respectivos campos
		if ($datos->hayDatos()){
			for ($a = 0; $a < $datos->largo(); $a++){
				$objetos[$a] = new $nombreSubClaseTabla($datos->get($a));
			}
		}
		
		//regresamos array de objetos cTabla
		return $objetos;
	}
		
	
	/**
	 *Le agrega comillas a los campos que lo requieran. Si es una fecha, $valor es cFecha
	 *@param $valor. cFecha/String/Numeric
	 *@param $tipo. cTipoDato
	 */
	protected function formatearValorSql($valor = "", $tipo = cTipoDato::Cadena){
		switch ($tipo){
			case cTipoDato::Cadena:
				$valor = "'".sys::limpiarTexto($valor)."'";
				break;
			case cTipoDato::Anio:
				$valor = "'".$valor->getAnio()."'";
				break;
			case cTipoDato::Fecha:
				$valor = "'".$valor->toSqlDate()."'";
				break;
			case cTipoDato::FechaHora:
				$valor = "'".$valor->toSqlDateTime()."'";
				break;
			case cTipoDato::Hora:
				$valor = "'".$valor->toSqlTime()."'";
				break;
		}
		
		return $valor;
	}
	
	
	/**
	 * Inserta en la BD los campos dentro de $this->campos
	 * Para ejecutar este metodo, es necesario tener todos los campos con su respectivo valor a exepción
	 * de los que son Nulleables
	 * @param $camposAInsertar. Lista de campos que se desean insertar.
	 * @return $this. <cTabla>
	 * @example:
	 * 	$cliente->insertarDatos();
	 * 	$cliente->insertarDatos(array("nombre", "apellido", "edad"));
	 */
	public function insertarDatos($camposAInsertar = null){
		$campos = "";
		$valores = "";
		$separador = "";
		$condicion = "";
		
		if ($camposAInsertar === null){
			$camposAInsertar = $this->campos;
		}else{
			$camposAInsertar = $this->campos(true, $camposAInsertar);
		}
		
		//creamos consulta sql de campos y valores
		foreach($camposAInsertar as $campo){
			$campos .= $separador.$campo->getNombre();
			
			if ($campo->getEsAutoIncrementable()){
				$valores .= $separador."0";
			}elseif ($campo->getValor() === null){
				if ($campo->getEsNullable()){
					$valores .= $separador."NULL";
				}else{
					throw new cError(cMsj::CampoNoPuedeSerNull, "Campo: ".$campo->getNombre());
				}
			}else{
				$valores .= $separador.$this->formatearValorSql($campo->getValor(), $campo->getTipo());
			}
			
			$separador = ", ";
		}
		
		//insertamos datos
		$this->setUltimoIdInsertado($this->getBD()->ejecutarComando("INSERT INTO ".$this->getNombreTabla()." ($campos) VALUES ($valores)"));
		
		//buscamos si existe un campo auto_increment. Si si existe, lo actualizamos
		foreach($this->campos as $campo){
			if ($campo->getEsAutoIncrementable()){
				$campo->setValor($this->getUltimoIdInsertado());
				break;
			}
		}
		
		//modificamos estado
		$this->setEstado(cEstadoTabla::actualizado);
		
		//regresamos referencia al mismo objeto
		return $this;
	}
	
	
	/**
	 *@param $campos <opcional>. array con los campos a modificar. Tambien puede ser un solo campo como String
	 *@param $condicion <opcional>. String. condicion Sql para modificar los campos
	 *@return $this. <cTabla>
	 *@example
	 *	$clase->modificarDatos();
	 *	$clase->modificarDatos(array("nombre", "edad", "sexo"));
	 *	$clase->modificarDatos(array("nombre", "edad", "sexo"), "nombre = 'jose'");
	 *	$clase->modificarDatos("nombre = 'jose'");
	 */
	public function modificarDatos($campos = "*", $condicion = null){
		//acomodamos los campos a modificar y creamos condicion sql
		if (is_string($campos) and $campos != "*"){
			/*	esta enviando una condicion y quiere guardar todos los campos
			 * 	$clase->modificarDatos("nombre = 'jose'");
			 */
			$condicion = $campos;
			$campos = $this->nombreCampos();
		}elseif ($condicion == null){
			/*	la condicion sql será basada en los campos llave
			 * 	$clase->modificarDatos();
			 * 	$clase->modificarDatos(array("nombre", "edad", "sexo"));
			 */
			$separador = "";
			foreach($this->camposId as $campoId){
				$condicion .= $separador.$this->campos[$campoId]->getNombre()." = ".$this->formatearValorSql($this->campos[$campoId]->getValor(), $this->campos[$campoId]->getTipo());
				$separador = " and ";
			}
			
			if ($campos == "*"){
				//quiere modificar todos los campos
				$campos = $this->nombreCampos();
			}
		}
		
		$datos = "";
		$separador = "";
		
		//creamos consulta sql de campos y valores
		foreach($campos as $nombreCampo){
			$campo = $this->objCampo($nombreCampo);
			$datos .= $separador.$campo->getNombre()." = ";
			
			if ($campo->getValor() == null){
				if ($campo->getEsNullable()){
					$datos .= "NULL";
				}else{
					throw new cError(cMsj::CampoNoPuedeSerNull, "Campo: ".$campo->getNombre());
				}
			}else{
				$datos .= $this->formatearValorSql($campo->getValor(), $campo->getTipo());
			}
			
			$separador = ", ";
		}
		
		//modificamos datos
		$this->getBD()->ejecutarComando("UPDATE ".$this->getNombreTabla()." SET $datos WHERE $condicion");
		
		//modificamos estado
		$this->setEstado(cEstadoTabla::actualizado);
		
		//regresamos referencia al mismo objeto
		return $this;
	}
	
	
	/**
	 *@param $condicion <opcional>. String. condicion Sql para eliminar los campos
	 *@return $this. <cTabla>
	 *@example
	 *	$clase->borrarDatos();
	 *	$clase->borrarDatos("nombre = 'jose'");
	 */
	public function borrarDatos($condicion = null){
		if ($condicion == null){
			/*	la condicion sql será basada en los campos llave
			 * 	$clase->borrarDatos();
			 */
			$separador = "";
			foreach($this->camposId as $campoId){
				$condicion .= $separador.$this->campos[$campoId]->getNombre()." = ".$this->formatearValorSql($this->campos[$campoId]->getValor(), $this->campos[$campoId]->getTipo());
				$separador = " and ";
			}
		}
		
		//modificamos datos
		$this->getBD()->ejecutarComando("DELETE FROM ".$this->getNombreTabla()." WHERE $condicion");
		
		//modificamos estado
		$this->setEstado(cEstadoTabla::nuevo);
		
		//regresamos referencia al mismo objeto
		return $this;
	}
	
	
	/**
     *Agrega una o varias validaciónes a uno o varios campos de la tabla
     *@param $campo. String/Array de nombres de campos
     *@param $validacion. Array de validaciones
     *@return $this. <cTabla>
     *@example
     *	$cliente->agregarValidacion(
     *		array("nombre", "apellido", "colonia"),
     *		array(
     *			"obligatorio" = true,
     *			"minLargo" = 4,
     *			"maxLargo" = 10
     *		)
     *	);
     *
     *	$cliente->agregarValidacion("correo",
     *		array(
     *			"obligatorio" = true,
     *			"esCorreo" = true
     *		)
     *	);
     *
	 */
	public function agregarValidacion($campo, $validacion){
		$campos = ((is_array($campo))?$campo:array($campo));
		
		foreach($campos as $camp){
			if (array_key_exists($camp, $this->campos)){
				foreach($validacion as $llave => $valor){
					$this->campos[$camp]->validaciones[$llave] = $valor;
				}
			}else{
				throw new cError(cMsj::CampoNoExiste, "Campo: ".$camp);
			}
		}
		
		//regresamos referencia al mismo objeto
		return $this;
	}
	
	
	/**
	 * verifica si la validación recibida se cumple o no en el campo recibido
	 * @param $valorCampo. Valor del campo a validar
	 * @param $validacion. Llave de la validacion a realizar. Ejemplo "esCorreo"
	 * @param $valor. Valor de la validacion. Ejemplo $validacion = "maxLargo". $valor = 14
	 * @return boolean. True si el campo es valido, de lo contrario regresa false
	 */
	protected function checarValidacion($valorCampo, $validacion, $valor = null){
		$valorCampo = trim($valorCampo);
		$validacion = strtolower($validacion);
		$valido = true;
		
		switch($validacion){
			case "minlargo":
				if (strlen($valorCampo) < $valor){
					$valido = false;
				}
				break;
			case "maxlargo":
				if (strlen($valorCampo) > $valor){
					$valido = false;
				}
				break;
			case "escorreo":
				if (!cValidacion::esCorreo($valorCampo)){
					$valido = false;
				}
				break;
			case "puedeservacio":
				if (!$valor and cValidacion::estaVacio($valorCampo)){
					$valido = false;
				}	
				break;
			case "obligatorio":
				if ($valor and cValidacion::estaVacio($valorCampo)){
					$valido = false;
				}	
				break;
		}
		
		return $valido;
	}
	
	
	/**
	 * Valida todos o una serie de campos.
	 * @param $campos. String/Array. serie de campos a validar. <Opcional>. 
	 * 	Si se deja vacio, se validan todos los campos de la tabla
	 * @throws cError
	 * @return $this. <cTabla>
	 * @example 
	 * 	$cliente->validarCampos();
	 * 	$cliente->validarCampos(array("edad", "sexo"));
	 */
	public function validarCampos($campos = null){
		if ($campos == null){
			//validamos todos los campos
			$campos = $this->nombreCampos();
		}else{
			//nos esta enviando un solo campo como string o una serie de campos dentro de un array
			$campos = ((is_array($campos))?$campos:array($campos));
		}
		
		//obtenemos objetos cCampo de estos nombres de campo
		$campos = $this->campos(true, $campos);
		
		//giramos atravez de los campos
		foreach($campos as $campo){
			//giramos atravez de las validaciones de este campo
			foreach($campo->validaciones as $validacion => $valor){
				if (!$this->checarValidacion($campo->getValor(), $validacion, $valor)){
					//ver textos/esp_mensajes.php para ingresar/modificar los mensajes lanzados en el idioma especificado
					global $mlMensajes;
					throw new cError($mlMensajes[strtolower($this->getNombreTabla())][strtolower($campo->getNombre())][strtolower($validacion)]);
				}
			}
		}
		
		//regresamos referencia al mismo objeto
		return $this;
	}
	
	
	/**
	 * Respalda los datos en los campos existentes
	 * @return $this. <cTabla>
	 * @example
	 * 	$cliente->respaldarCampos();
	 * 
	 * 	try{
	 * 		$cliente->campos(array(
	 * 			"nombre" => "jose", 
	 * 			"sexo" => "H", 
	 * 			"edad" => 43
	 * 		));
	 * 		//se validan los datos. Si se detecta un error, se lanza una excepción y los datos 
	 * 		//no se insertan/modifican en la BD
	 * 		$cliente->validarCampos();
	 * 
	 * 		$cliente->liberarRespaldo();  
	 * 		$cliente->insertarDatos();
	 * 	}catch(cError $e){
	 * 		$cliente->restaurarCampos();
	 * 		echo $e;
	 * 	}
	 */
	public function respaldarCampos(){
		//respaldamos datos actuales
		$this->camposBuff = $this->campos();
		
		//regresamos referencia al mismo objeto
		return $this;
	}

	
	/**
	 * Libera los recursos usados por el buffer de respaldo de datos
	 * @return $this. <cTabla>
	 */
	public function liberarRespaldo(){
		$this->camposBuff = array();
		
		//regresamos referencia al mismo objeto
		return $this;
	}


	/**
	 * Pasa el respaldo de datos a los datos originales. Volviendo asi a una version anterior del objeto
	 * @return $this. <cTabla>
	 */
	public function restaurarCampos(){
		//restauramos los datos
		$this->campos($this->camposBuff);
		
		//regresamos referencia al mismo objeto
		return $this;
	}
}
?>