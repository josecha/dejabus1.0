<?php
class cAviso extends Exception{
	/*
	 * Propiedades
	 */
	public $msjOpcional = "";


	public function getMsjOpcional(){
		return $this->msjOpcional;
	}


	public function setMsjOpcional($msjOpcional){
		$this->msjOpcional = $msjOpcional;
	}


	/*
	 * Contructores
	 */

	public function cAviso($msj, $msjOpcional = ""){
		//si esta enviando un codigo de msj
		if (is_int($msj)){
			$msj = sys::obtenerMensaje($msj);
		}

		parent::__construct($msj, 0);
		$this->setMsjOpcional($msjOpcional);
	}


	/*
	 * Metodos
	 */

	public function __toString(){
		return $this->getMessage()."<br/>".
		(($this->getMsjOpcional() != "")?$this->getMsjOpcional():"");
	}
}
?>