<?php
class cWebUtil{
	/**
	 * @param $txt. Texto al que se le eliminarán los signos de igualdad '=' y '&'
	 * @return String. Texto sin signos '=' y '&'
	 */
	public static function encriptarParaQuery($txt = ""){
		return str_replace("=", "/ig/", str_replace("&", "/ap/", $txt));
	}
	
	
	/**
	 * @param $txt. Texto al que se le agregaran los signos de igualdad '=' y '&'
	 * @return String. Texto con signos '=' y '&'
	 */
	public static function desencriptarDeQuery($txt = ""){
		return str_replace("/ig/", "=", str_replace("/ap/", "&", $txt));
	}
}
?>