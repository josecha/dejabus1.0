<?php
class cCampo{
	/*
	 * Propiedades
	 */

	private $nombre = null;
	private $valor = null;
	private $tipo = cTipoDato::Cadena;
	private $nombreTipo = null;
	private $default = null;
	private $maxLargo = null;
	private $presicionNumerica = null;
	private $escalaNumerica = null;
	private $esLlavePrimaria = false;
	private $esLlaveForanea = false;
	private $esAutoIncrementable = false;
	private $esUnico = false;
	private $usaDefault = false;
	private $esNullable = false;
	public $validaciones = null;


	public function getNombre(){
		return $this->nombre;
	}


	public function setNombre($nombre){
		$this->nombre = $nombre;
	}


	public function getValor(){
		return $this->valor;
	}


	public function setValor($valor){
		if (($this->getTipo() == cTipoDato::Cadena or $this->getTipo() == cTipoDato::Numero) and $this->getMaxLargo() != null and strlen($valor) > $this->getMaxLargo()){
			$valor = substr($valor, 0, $this->getMaxLargo());
		}
		
		if (!is_a($valor, "cFecha")){
			switch ($this->getTipo()){
				case cTipoDato::Anio:
					$valor = new cFecha($valor, cTipoFecha::Year);
					break;
				case cTipoDato::Fecha:
					$valor = new cFecha($valor, cTipoFecha::Date);
					break;
				case cTipoDato::FechaHora:
					$valor = new cFecha($valor, cTipoFecha::DateTime);
					break;
				case cTipoDato::Hora:
					$valor = new cFecha($valor, cTipoFecha::Time);
					break;
			}
		}
		
		$this->valor = $valor;
	}


	public function getTipo(){
		return $this->tipo;
	}


	public function setTipo($tipo){
		$this->tipo = $tipo;
	}
	
	
	public function getPresicionNumerica(){
		return $this->presicionNumerica;
	}


	public function setPresicionNumerica($presicionNumerica){
		$this->presicionNumerica = $presicionNumerica;
	}
	
	
	public function getEscalaNumerica(){
		return $this->escalaNumerica;
	}


	public function setEscalaNumerica($escalaNumerica){
		$this->escalaNumerica = $escalaNumerica;
	}
	
	
	public function getMaxLargo(){
		return $this->maxLargo;
	}


	public function setMaxLargo($maxLargo){
		$this->maxLargo = $maxLargo;
		
		//actualizamos el valor del campo dependiendo del maximo largo permitido
		if ($this->getValor() != null){
			$this->setValor($this->getValor());
		}
	}


	public function getNombreTipo(){
		return $this->nombreTipo;
	}


	public function setNombreTipo($nombreTipo){
		$this->nombreTipo = $nombreTipo;
	}
	
	
	public function getDefault(){
		return $this->default;
	}


	public function setDefault($default){
		$this->default = $default;
	}


	public function getEsLlavePrimaria(){
		return $this->esLlavePrimaria;
	}


	public function setEsLlavePrimaria($esLlavePrimaria){
		$this->esLlavePrimaria = $esLlavePrimaria;
	}


	public function getEsLlaveForanea(){
		return $this->esLlaveForanea;
	}


	public function setEsLlaveForanea($esLlaveForanea){
		$this->esLlaveForanea = $esLlaveForanea;
	}
	
	
	public function getEsAutoIncrementable(){
		return $this->esAutoIncrementable;
	}


	public function setEsAutoIncrementable($esAutoIncrementable){
		$this->esAutoIncrementable = $esAutoIncrementable;
	}
	
	
	public function getEsUnico(){
		return $this->esUnico;
	}


	public function setEsUnico($esUnico){
		$this->esUnico = $esUnico;
	}


	public function getUsaDefault(){
		return $this->usaDefault;
	}


	public function setUsaDefault($usaDefault){
		$this->usaDefault = $usaDefault;
	}
	
	
	public function getEsNullable(){
		return $this->esNullable;
	}


	public function setEsNullable($esNullable){
		$this->esNullable = $esNullable;
	}


	/*
	 * Constructor
	 */

	public function cCampo($nombre = null, $valor = null){
		$this->setNombre($nombre);
		$this->setValor($valor);
	}
}
?>