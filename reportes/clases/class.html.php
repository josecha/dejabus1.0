<?php
class html{
	const chGuion = '&ndash;';
	const chGuionLargo = '&mdash;';
	const chEclamacionInicial = '&iexcl;';
	const chPreguntaInicial = '&iquest;';
	const chDobleComilla = '&quot;';
	const chComillasInicial = '&ldquo;';
	const chComillasFinal = '&rdquo;';
	const chComillaInicial = '&lsquo;';
	const chComillaFinal = '&rsquo;';
	const chDobleFlechaIzq = '&laquo;';
	const chDobleFlechaDer = '&raquo;';
	const chAmpersand = '&amp;';
	const chCopyright = '&copy;';
	const chDivision = '&divide;';
	const chMayorQue = '&gt;';
	const chMenorQue = '&lt;';
	const chEuro = '&euro;';
	const chMarcaRegistrada = '&reg;';
	const chEspacio = '&nbsp;';
	
	
	public static function printLn($numeroLineas = 1){
		if ($numeroLineas == 1){
			echo "<br/>";
		}else{
			for ($a = 0; $a < $numeroLineas; $a++){
				echo "<br/>";
			}
		}
	}
	
	
	public static function printArray($elem, $max_level = 10, $print_nice_stack=array()){
		if(is_array($elem) || is_object($elem)){
			if(in_array(&$elem,$print_nice_stack,true)){
				echo "<font color=red>RECURSION</font>";
				return;
			}
			$print_nice_stack[]=&$elem;
			if($max_level<1){
				echo "<font color=red>nivel maximo alcanzado</font>";
				return;
			}
			$max_level--;
			echo "<table border=1 cellspacing=0 cellpadding=3 width=100%>";
			if(is_array($elem)){
				echo '<tr><td colspan=2 style="background-color:#333333;"><strong><font color=white>ARRAY</font></strong></td></tr>';
			}else{
				echo '<tr><td colspan=2 style="background-color:#333333;"><strong>';
				echo '<font color=white>OBJECT Type: '.get_class($elem).'</font></strong></td></tr>';
			}
			$color=0;
			foreach($elem as $k => $v){
				if($max_level%2){
					$rgb=($color++%2)?"#888888":"#BBBBBB";
				}else{
					$rgb=($color++%2)?"#8888BB":"#BBBBFF";
				}
				echo '<tr><td valign="top" style="width:40px;background-color:'.$rgb.';">';
				echo '<strong>'.$k."</strong></td><td>";
				self::printArray($v,$max_level,$print_nice_stack);
				echo "</td></tr>";
			}
			echo "</table>";
			return;
		}
		if($elem === null){
			echo "<font color=green>NULL</font>";
		}elseif($elem === 0){
			echo "0";
		}elseif($elem === true){
			echo "<font color=green>TRUE</font>";
		}elseif($elem === false){
			echo "<font color=green>FALSE</font>";
		}elseif($elem === ""){
			echo "<font color=green>EMPTY STRING</font>";
		}else{
			echo str_replace("\n","<strong><font color=red>*</font></strong><br>\n",$elem);
		}
	}
	
	
	public static function printObj($obj){
		echo "<pre>";
		var_dump($obj);
		echo "</pre>";
	}
	
	
	public static function printCss($urlCss){
		echo '<link rel="stylesheet" type="text/css" media="screen" href="'.$urlCss.'" />';
	}
	
	
	public static function printJs($urlJs){
		echo "\n<script type=\"text/javascript\" src=\"".$urlJs."\"></script>";
	}
	
	
	public static function insertarCss(){
		$css = 'FF';
		
		switch(sys::$navegador){
			case 1: //IE
				$css = 'IE';
				break;
			case 3: //CH
				$css = 'CH';
				break;
			case 4: //SA
				$css = 'SA';
				break;
			case 2: //OP
				$css = 'OP';
				break;
		}
		
		if ($css != "IE"){
			$so = sys::obtenerSO();
			if (strpos($so, "Windows") !== false){
				$css .= "_W";
			}
		}else{	
			if (eregi('(MSIE 6\.[0-9]+)', $_SERVER['HTTP_USER_AGENT'])){
				$css .= "6";
			}else if (eregi('(MSIE 7\.[0-9]+)', $_SERVER['HTTP_USER_AGENT'])){
				$css .= "7";
			}else{
				$css .= "8";
			}
		}
		
		self::printCss(sys::$webUrl.'estilos/estilo'.$css.'.css');
	}
	
	
	public static function isIE6(){
		$is = false;
		
		if (eregi('(MSIE 6\.[0-9]+)', $_SERVER['HTTP_USER_AGENT'])){
			$is = true;
		}
		
		return $is;
	}
	
	
	public static function mostrarInfo($msjPrincipal, $msjSecundario = "", $imprimir = true){
		$txt = '
			<div class="msjInfo">
				<span>'.$msjPrincipal.'</span><br/>'.$msjSecundario.'
			</div>
			<script type="text/javascript">
				setTimeout("ocultarMsj()", 10000);
			</script>
		';
		
		if ($imprimir){
			echo $txt;
		}else{
			return $txt;
		}
	}
	
	
	public static function mostrarError($msjPrincipal, $msjSecundario = "", $imprimir = true){
		$txt = '
			<div class="msjError">
				<span>'.$msjPrincipal.'</span><br/>'.$msjSecundario.'
			</div>
			<script type="text/javascript">
				setTimeout("ocultarMsj()", 10000);
			</script>
		';
		
		if ($imprimir){
			echo $txt;
		}else{
			return $txt;
		}
	}
	
	
	public static function mostrarAviso($msjPrincipal, $msjSecundario = "", $imprimir = true){
		$txt = '
			<div class="msjAviso">
				<span>'.$msjPrincipal.'</span><br/>'.$msjSecundario.'
			</div>
			<script type="text/javascript">
				setTimeout("ocultarMsj()", 10000);
			</script>
		';
		
		if ($imprimir){
			echo $txt;
		}else{
			return $txt;
		}
	}
}
?>