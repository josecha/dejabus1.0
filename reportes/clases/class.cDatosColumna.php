<?php
class cDatosColumna{
	/*
	 * Propiedades
	 */
	private $tipo = cTipoDato::Cadena;
	private $nombreOriginal = null;
	private $nombre = null;
	private $nombreTabla = null;
	private $nombreOriginalTabla = null;

	public function getTipo(){
		return $this->tipo;
	}


	public function setTipo($tipo){
		$this->tipo = $tipo;
	}


	public function getNombre(){
		return $this->nombre;
	}


	public function setNombre($nombre){
		$this->nombre = $nombre;
	}


	public function getNombreOriginal(){
		return $this->nombreOriginal;
	}


	public function setNombreOriginal($nombreOriginal){
		$this->nombreOriginal = $nombreOriginal;
	}


	public function getNombreOriginalTabla(){
		return $this->nombreOriginalTabla;
	}


	public function setNombreOriginalTabla($nombreOriginalTabla){
		$this->nombreOriginalTabla = $nombreOriginalTabla;
	}


	public function getNombreTabla(){
		return $this->nombreTabla;
	}


	public function setNombreTabla($nombreTabla){
		$this->nombreTabla = $nombreTabla;
	}


	/*
	 * Constructor
	 */

	public function cDatosColumna($tipo = cTipoDato::Cadena, $nombre = null, $nombreOriginal = null, $nombreTabla = null, $nombreOriginalTabla = null){
		$this->setTipo($tipo);
		$this->setNombre($nombre);
		$this->setNombreOriginal($nombreOriginal);
		$this->setNombreTabla($nombreTabla);
		$this->setNombreOriginalTabla($nombreOriginalTabla);
	}
}
?>