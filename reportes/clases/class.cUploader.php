<?
class cUploader{
	/*	 * Propiedades	 */
	public $nameIn = "";    	//nombre de la foto a subir o a redimensionar (incluyendo ruta y extencion. ejemplo: fotos/foto1.jpg)
	public $nameOut = "";   	//nombre deseado de la foto (incluyendo ruta de carpeta: ejemplo: fotos/foto1) [sin extension]	public $wIn = 0;			//ancho de imagen original
	public $hIn = 0;			//alto de imagen original
	public $wOut = 0;			//ancho de imagen redimensionada
	public $hOut = 0;			//alto de imagen redimensionada
	public $size = 0;			//tama�o de la imagen original
	public $tipo = "";			//tipo de la imagen: .gif, .jpg y .png
	public $maxW = 0;			//limite de ancho. Si una imagen sebasa el limite, ser� redimensionada
	public $maxH = 0;			//limite de alto. Si una imagen sebasa el limite, ser� redimensionada
	public $maxSize = 10485760; 	//imagen de 10 MB como maximo
	public $calidadImg = 90; 	//calidad de la nueva imagen[recomendado]
	public $estilo = "NO";
	/*
	estilo = "NO" -> no se redimensionar� (a menos que halla limites que cumplir)
	estilo = "E" -> se redimensiona como cuadro exacto pero SIN proporciones. La altura ser� igual a la anchura - no muy recomendado
	estilo = "C" -> se redimensiona como cuadro exacto pero CON proporciones. La altura ser� igual a la anchura - muy recomendado
	estilo = "P" -> se redimensiona proporcionalmente tomando una altura o una anchura inicial. Puede quedar una imagen rectangular.
	*/

	/*	 * Constructor	 */
	public function cUploader($nameIn, $maxSize = 10485760){
		$this->maxSize = $maxSize;
		if ($nameIn == ""){
			throw new cError("Introduc� el nombre de la imagen original");
		}elseif (!@file_exists($nameIn) and substr(trim($nameIn), 0, 4) != 'http'){
			throw new cError("La imagen no se carg&oacute; correctamente. Trata de nuevo.");
		}else{
			$this->nameIn = $nameIn;
						//sacamos datos de la imagen original
			$this->datosImg();
		}
	}
	/*	 * Metodos	 */
	//sube una foto al servidor
	public function procesar($nameOut, $estilo="NO", $wOut=0, $hOut=0, $maxW=0, $maxH=0){
		if ($nameOut != "" and $this->nameOut = $nameOut){
			$this->wOut = $wOut;
			$this->hOut = $hOut;
			$this->maxW = $maxW;
			$this->maxH = $maxH;
			$this->estilo = $estilo;
			//verificamos si es necesario redimensionar
			if ($this->estilo == "NO"){
				//verificamos si es necesario redimensionar debido al maxW
				if (($this->maxW == 0 and $this->maxH == 0) or ($this->maxH != 0 and $this->maxW == 0 and $this->hIn <= $this->maxH) or ($this->maxW != 0 and $this->maxH == 0 and $this->wIn <= $this->maxW) or ($this->maxW != 0 and $this->maxH != 0 and $this->wIn <= $this->maxW and $this->hIn <= $this->maxH)){
					/* Si entr� aqui es por una de las siguientes causas:
					-No hay limites
					-Solo hay limite de altura Pero la altura de la imagen no rebasa ese limite
					-Solo hay limite de anchura Pero la anchura de la imagen no rebasa ese limite
					-Hay limites de altura y anchura, Pero ni la altura ni la anchura de la imagen rebasan esos limites	*/
					if (!@copy($this->nameIn, $this->nameOut.$this->tipo)){
						throw new cError("Ocurri&oacute; un error al mover la imagen al servidor. Trata de nuevo.");
					}else{
						return true;
					}
				}else{ //redimensionamos debido a que hay limites que cumplir
					if ($this->maxW != 0 and $this->maxH == 0){ //limite en ancho
						$this->wOut = $this->maxW;
						$this->hOut = $this->wOut * $this->hIn / $this->wIn;
					}elseif ($this->maxH != 0 and $this->maxW == 0){ //limite en altura
						$this->hOut = $this->maxH;
						$this->wOut = $this->hOut * $this->wIn / $this->hIn;
					}else{ //limite en anchura y altura
						$this->wOut = $this->maxW;
						$this->hOut = $this->maxH;
					}
					$this->redim(); //redimensionamos
				}
			}else{
				$this->redim(); //redimensionamos
			}
			return true;
		}else{
			throw new cError("Introduce un nombre para la imagen de salida");
		}//endif ($nameOut != "" and $this->nameOut = $nameOut)
	}
	//obtiene y valida los datos de la imagen subida
	private function datosImg(){
		$datos = @getimagesize($this->nameIn);
		//sacamos medidas originales
		$this->wIn = $datos[0];
		$this->hIn = $datos[1];

		//sacamos tipo de archivo
		if ($datos[2]==1){
			$this->tipo = ".gif";
		}elseif ($datos[2]==2){
			$this->tipo = ".jpg";
		}elseif ($datos[2]==3){
			$this->tipo = ".png";
		}else{
			throw new cError("Solo se admiten im&aacute;genes con formato JPG, GIF y PNG");
		}
		//verificamos tama�o
		$this->size = @filesize($this->nameIn);
		if ($this->size > $this->maxSize){
			throw new cError("La imagen NO puede ser mayor a ".($this->maxSize / 1048576)." MB.");
		}
		return true;
	}
	protected function redim(){
		switch($this->estilo){
			case "E":
				if ($this->wOut != 0){ //redimensionamos respecto a anchura
					if ($this->wIn > $this->wOut or $this->hIn > $this->wOut){ //es necesario redimensionar
						$this->hOut = $this->wOut;
					}else{ //dejamos tama�o original
						$this->wOut = $this->wIn;
						$this->hOut = $this->hIn;
					}
				}elseif($this->hOut != 0){ //redimensionamos respecto a altura
					if ($this->wIn > $this->hOut or $this->hIn > $this->hOut){ //es necesario redimensionar
						$this->wOut = $this->hOut;
					}else{ //dejamos tama�o original
						$this->wOut = $this->wIn;
						$this->hOut = $this->hIn;
					}
				}else{
					throw new cError("Es necesario dar una anchura o altura a redimensionar");
				}
				break;
			case "C":
				if ($this->wOut != 0){
					if ($this->wIn == $this->hIn){ //anchura y altura orgiginal iguales
						if ($this->wIn > $this->wOut){ //es necesario redimensionar
							$this->hOut = $this->wOut;
						}else{ //dejamos tama�o original
							$this->wOut = $this->wIn;
							$this->hOut = $this->hIn;
						}
					}elseif ($this->hIn > $this->wIn){ //altura original mayor que anchura original
						if ($this->hIn > $this->wOut){ //es necesario redimensionar
							$this->hOut = $this->wOut;
							$this->wOut = $this->hOut * $this->wIn / $this->hIn;
						}else{ //dejamos tama�o original
							$this->wOut = $this->wIn;
							$this->hOut = $this->hIn;
						}
					}else{ //anchura original mayor que altura original
						if ($this->wIn > $this->wOut){ //es necesario redimensionar
							$this->hOut = $this->wOut * $this->hIn / $this->wIn;
						}else{ //dejamos tama�o original
							$this->wOut = $this->wIn;
							$this->hOut = $this->hIn;
						}
					}
				}else{
					throw new cError("Es necesario dar una anchura para tener una base a redimensionar");
				}				
				break;
			case "P":
				if ($this->wOut != 0){ //redimensionamos respecto a anchura
					if ($this->wIn > $this->wOut){ //es necesario redimensionar
						$this->hOut = $this->wOut * $this->hIn / $this->wIn;
					}else{ //dejamos tama�o original
						$this->wOut = $this->wIn;
						$this->hOut = $this->hIn;
					}
				}elseif($this->hOut != 0){ //redimensionamos respecto a altura
					if ($this->hIn > $this->hOut){ //es necesario redimensionar
						$this->wOut = $this->hOut * $this->wIn / $this->hIn;
					}else{ //dejamos tama�o original
						$this->wOut = $this->wIn;
						$this->hOut = $this->hIn;
					}
				}else{
					throw new cError("Es necesario dar una anchura o altura a redimensionar");
				}				
				break;
		}//end switch
			
		//comenzamos a crear bases para la imagen
		if ($this->tipo == ".jpg"){
			$img = @imagecreatefromjpeg($this->nameIn);
		}elseif ($this->tipo == ".gif"){
			$img = @imagecreatefromgif($this->nameIn);
		}else{ //png
			$img = @imagecreatefrompng($this->nameIn);
		}
		//creamos imagen miniatura nueva
		$thumb = @imagecreatetruecolor($this->wOut, $this->hOut);
		@imagecopyresampled ($thumb, $img, 0, 0, 0, 0, $this->wOut, $this->hOut, $this->wIn, $this->hIn);//redimensionar imagen original
		// guardar la nueva imagen redimensionada donde indicia $this->nameOut
		if ($this->tipo == ".jpg"){
			@imagejpeg($thumb, $this->nameOut.$this->tipo, $this->calidadImg);
		}elseif ($this->tipo == ".gif"){
			@imagegif($thumb, $this->nameOut.$this->tipo, $this->calidadImg);
		}else{ //png
			@imagepng($thumb, $this->nameOut.$this->tipo, ($this->calidadImg / 10) - 1);
		}
		return true;
	}
	//regresa la direccion de la nueva imagen pero ya con extencion: ejemplo: Fotos/foto.jpg
	public function src(){
		return $this->nameOut.$this->tipo;
	}
}
?>