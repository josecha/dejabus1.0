<?php
class tAviso extends cTablaEstatica{
	
	/*
	 * Constructor
	 */
	public function tAviso($valoresLlave = null, $obtenerDatos = false, $bd = null){
		$this->setNombreTabla("avisos");
		
		$nombreCampo = "id";
		$this->campos[$nombreCampo] = new cCampo($nombreCampo);
		$this->campos[$nombreCampo]->setTipo(cTipoDato::Numero);
		$this->campos[$nombreCampo]->setDefault(null);
		$this->campos[$nombreCampo]->setEsLlaveForanea(false);
		$this->campos[$nombreCampo]->setMaxLargo(11);
		$this->campos[$nombreCampo]->setUsaDefault(false);
		$this->campos[$nombreCampo]->setEsNullable(false);
		$this->campos[$nombreCampo]->setEsAutoIncrementable(true);
		$this->campos[$nombreCampo]->setEsLlavePrimaria(true);
		
		$nombreCampo = "fecha";
		$this->campos[$nombreCampo] = new cCampo($nombreCampo);
		$this->campos[$nombreCampo]->setTipo(cTipoDato::FechaHora);
		$this->campos[$nombreCampo]->setDefault(null);
		$this->campos[$nombreCampo]->setEsLlaveForanea(false);
		$this->campos[$nombreCampo]->setMaxLargo(null);
		$this->campos[$nombreCampo]->setUsaDefault(false);
		$this->campos[$nombreCampo]->setEsNullable(true);
		$this->campos[$nombreCampo]->setEsAutoIncrementable(false);
		$this->campos[$nombreCampo]->setEsLlavePrimaria(false);
		
		$nombreCampo = "aviso";
		$this->campos[$nombreCampo] = new cCampo($nombreCampo);
		$this->campos[$nombreCampo]->setTipo(cTipoDato::Cadena);
		$this->campos[$nombreCampo]->setDefault(null);
		$this->campos[$nombreCampo]->setEsLlaveForanea(false);
		$this->campos[$nombreCampo]->setMaxLargo(null);
		$this->campos[$nombreCampo]->setUsaDefault(false);
		$this->campos[$nombreCampo]->setEsNullable(true);
		$this->campos[$nombreCampo]->setEsAutoIncrementable(false);
		$this->campos[$nombreCampo]->setEsLlavePrimaria(false);
		
		parent::cTablaEstatica($valoresLlave, $obtenerDatos, $bd);
	}
}
?>