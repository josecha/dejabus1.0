<?php
class cDatosTabla{
	/*
	 * Propiedades
	 */
	private $varLargo = 0;
	private $datos = array();
	public $columnas = array();


	public function largo(){
		return $this->varLargo;
	}


	public function hayDatos(){
		$siHay = false;
		if ($this->largo() > 0){
			$siHay = true;
		}

		return $siHay;
	}


	/**
	 *Regresa un campo, dado el numero de fila y el nombre de la columna.
	 *Opcionalmente puede regresar toda una fila entera si solo se le envia el numero de fila
	 *Ejemplo:
	 *	$bd->get(3,"nombre"); //regresa "juan"
	 *	$bd->get(3); //regresa array("nombre"=>"juan", "edad"=>"4", "fecha"=>"2009-04-13 14:34:20")
	 */
	public function get($posicion, $llave = null){
		if ($llave == null){
			return $this->datos[$posicion];
		}else{
			return $this->datos[$posicion][$llave];
		}
	}


	public function addFila($fila = array()){
		$this->datos[$this->varLargo++] = $fila;
	}

	
	/**
	 *Regresa un array bidimensional para ser tratado en un foreach
	 *Ejemplo:
	 *	foreach ($tabla->datos() as $fila){
	 *		echo "Nombre: ".$fila["nombre"];
	 *		echo "Edad: ".$fila["edad"];
	 *	}
	 */
	public function toArray(){
		return $this->datos;
	}
}
?>