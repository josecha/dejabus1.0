<?php
abstract class cNavegadores{
	const firefox = 0;
	const msie = 1;
	const opera = 2;
	const chrome = 3;
	const safari = 4;
	const mozilla = 5;
	const seamonkey = 6;
	const konqueror = 7;
	const netscape = 8;
	const gecko = 9;
	const navigator = 10;
	const mosaic = 11;
	const lynx = 12;
	const amaya = 13;
	const omniweb = 14;
	const avant = 15;
	const camino = 16;
	const flock = 17;
	const aol = 18;
}
?>