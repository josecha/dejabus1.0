<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte Cami�n Colima</title>

<!-- jQuery -->
<script type="text/javascript" src="js/jquery-1.1.2.js"></script>

<!-- required plugins -->
<script type="text/javascript" src="js/date.js"></script>
<?
 if(ereg("MSIE", $_SERVER["HTTP_USER_AGENT"])) //si es IE
   echo '<script type="text/javascript" src="js/jquery.bgiframe.js"></script>';        
?>

<!-- jquery.datePicker.js -->
<script type="text/javascript" src="js/jquery.datePicker.js"></script>

<!-- datePicker styles -->
<link rel="stylesheet" type="text/css" media="screen" href="js/datePicker.css">

<style type="text/css">
/* located in demo.css and creates a little calendar icon
 * instead of a text link for "Choose date"
 */
a.dp-choose-date {
	float: left;
	width: 16px;
	height: 16px;
	padding: 0;
	margin: 5px 3px 0;
	display: block;
	text-indent: -2000px;
	overflow: hidden;
	background: url(imagenes/calendar.png) no-repeat; 
}
a.dp-choose-date.dp-disabled {
	background-position: 0 -20px;
	cursor: default;
}
/* makes the input field shorter once the date picker code
 * has run (to allow space for the calendar icon
 */
input.dp-applied {
	width: 100px;
	float: left;
}

/* ESTILO DE ESTA PAGINA */
#all{
  width:100%;
}

#contenido{
  margin:0 auto;
  width:400px;
  border:3px double #CCCCCC;
  padding:20px;
  background:#F4F4F4;
  font-family:Arial, Helvetica, sans-serif;
}

#contenido label{
  font-size:13px;
  color:#990000;
}

.corte{
  clear:both;
}

#lblFI{
  float:left;
  margin-right:11px;
  margin-top:2px;
}

#lblFF{
  float:left;
  margin-right:16px;
}

#lblHI{
  margin-left:10px;
}

#lblHF{
  margin-left:10px;
}

#divFI1{
  margin-bottom:15px;
}

.boton{
  width:140px;
  height:27px;
  font-size:12px;
  font-weight:bold;
  margin-right:5px;
  margin-top:15px;
}

#divC{
  margin-top:15px;
}

#lblC{
  margin-right:36px;
}

#divVehiculos{
	width:120px;
	font-family:"Lucida grande", Tahoma, Verdana, Arial, sans-serif;
	border-top:1px #BCBCBC groove;
	border-left:1px #BCBCBC groove;
	margin-bottom:7px;
	margin-top:7px;
	background:#FDFDFD none repeat scroll 0% 0%;
	overflow:auto;
	height:150px;
}

</style>
<script type="text/javascript" charset="utf-8" src="js/index.js"></script>
</head>

<body>
 <div id="all">
   <div id="contenido">
    <form method="get" id="frmFechas" name="frmFechas" action="reporte.php" onsubmit="return enviar();">
	 <div id="divFI1">
	   <label id="lblFI" for="date1">Fecha Inicial:</label>
	   <input type="hidden" name="fechaI" id="fechaI" />
	   <input name="date1" readonly="true" id="date1" class="date-pick" />
	   <label id="lblHI">Hora:</label>
	   <select id="selectHI" name="selectHI">
             <option value="00">00</option>
			 <option value="01">01</option>
			 <option value="02">02</option>
			 <option value="03">03</option>
			 <option value="04">04</option>
			 <option value="05">05</option>
			 <option value="06">06</option>
			 <option value="07">07</option>
			 <option value="08">08</option>
			 <option value="09">09</option>
		     <option value="10">10</option>
			 <option value="11">11</option>
			 <option value="12">12</option>	 
			 <option value="13" selected="selected">13</option>
			 <option value="14">14</option>
			 <option value="15">15</option>
			 <option value="16">16</option>
			 <option value="17">17</option>
			 <option value="18">18</option>
			 <option value="19">19</option>
			 <option value="20">20</option>
			 <option value="21">21</option>
			 <option value="22">22</option>
			 <option value="23">23</option>
	   </select>
	   <select id="selectMI" name="selectMI">
	         <option value="00">00</option>
			 <option value="01">01</option>
			 <option value="02">02</option>
			 <option value="03">03</option>
			 <option value="04">04</option>
			 <option value="05">05</option>
			 <option value="06">06</option>
			 <option value="07" selected="selected">07</option>
			 <option value="08">08</option>
			 <option value="09">09</option>
		     <option value="10">10</option>
			 <option value="11">11</option>
			 <option value="12">12</option>	 
			 <option value="13">13</option>
			 <option value="14">14</option>
			 <option value="15">15</option>
			 <option value="16">16</option>
			 <option value="17">17</option>
			 <option value="18">18</option>
			 <option value="19">19</option>
			 <option value="20">20</option>
			 <option value="21">21</option>
			 <option value="22">22</option>
			 <option value="23">23</option>
			 <option value="24">24</option>
			 <option value="25">25</option>
			 <option value="26">26</option>
			 <option value="27">27</option>
			 <option value="28">28</option>
			 <option value="29">29</option>
			 <option value="30">30</option>
			 <option value="31">31</option>
			 <option value="32">32</option>
			 <option value="33">33</option>
			 <option value="34">34</option>
			 <option value="35">35</option>
			 <option value="36">36</option>
			 <option value="37">37</option>
			 <option value="38">38</option>
			 <option value="39">39</option>
			 <option value="40">40</option>
			 <option value="41">41</option>
			 <option value="42">42</option>
			 <option value="43">43</option>
			 <option value="44">44</option>
			 <option value="45">45</option>
			 <option value="46">46</option>
			 <option value="47">47</option>
			 <option value="48">48</option>
			 <option value="49">49</option>
			 <option value="50">50</option>
			 <option value="51">51</option>
			 <option value="52">52</option>
			 <option value="53">53</option>
			 <option value="54">54</option>
			 <option value="55">55</option>
			 <option value="56">56</option>
			 <option value="57">57</option>
			 <option value="58">58</option>
			 <option value="59">59</option>
	   </select>
	 </div>
	 <div class="corte"></div>
	 
     <div id="divFF1">
	   <label id="lblFF" for="date2">Fecha Final:</label>
	   <input type="hidden" name="fechaF" id="fechaF" />
	   <input name="date2" readonly="true" id="date2" class="date-pick" />
	   <label id="lblHF">Hora:</label>
	   <select id="selectHF" name="selectHF">
             <option value="00">00</option>
			 <option value="01">01</option>
			 <option value="02">02</option>
			 <option value="03">03</option>
			 <option value="04">04</option>
			 <option value="05">05</option>
			 <option value="06">06</option>
			 <option value="07">07</option>
			 <option value="08">08</option>
			 <option value="09">09</option>
		     <option value="10">10</option>
			 <option value="11">11</option>
			 <option value="12">12</option>	 
			 <option value="13">13</option>
			 <option value="14">14</option>
			 <option value="15">15</option>
			 <option value="16" selected="selected">16</option>
			 <option value="17">17</option>
			 <option value="18">18</option>
			 <option value="19">19</option>
			 <option value="20">20</option>
			 <option value="21">21</option>
			 <option value="22">22</option>
			 <option value="23">23</option>
	   </select>
	   <select id="selectMF" name="selectMF">
	         <option value="00">00</option>
			 <option value="01">01</option>
			 <option value="02">02</option>
			 <option value="03">03</option>
			 <option value="04">04</option>
			 <option value="05">05</option>
			 <option value="06">06</option>
			 <option value="07">07</option>
			 <option value="08">08</option>
			 <option value="09">09</option>
		     <option value="10">10</option>
			 <option value="11">11</option>
			 <option value="12">12</option>	 
			 <option value="13">13</option>
			 <option value="14">14</option>
			 <option value="15">15</option>
			 <option value="16">16</option>
			 <option value="17">17</option>
			 <option value="18">18</option>
			 <option value="19">19</option>
			 <option value="20" selected="selected">20</option>
			 <option value="21">21</option>
			 <option value="22">22</option>
			 <option value="23">23</option>
			 <option value="24">24</option>
			 <option value="25">25</option>
			 <option value="26">26</option>
			 <option value="27">27</option>
			 <option value="28">28</option>
			 <option value="29">29</option>
			 <option value="30">30</option>
			 <option value="31">31</option>
			 <option value="32">32</option>
			 <option value="33">33</option>
			 <option value="34">34</option>
			 <option value="35">35</option>
			 <option value="36">36</option>
			 <option value="37">37</option>
			 <option value="38">38</option>
			 <option value="39">39</option>
			 <option value="40">40</option>
			 <option value="41">41</option>
			 <option value="42">42</option>
			 <option value="43">43</option>
			 <option value="44">44</option>
			 <option value="45">45</option>
			 <option value="46">46</option>
			 <option value="47">47</option>
			 <option value="48">48</option>
			 <option value="49">49</option>
			 <option value="50">50</option>
			 <option value="51">51</option>
			 <option value="52">52</option>
			 <option value="53">53</option>
			 <option value="54">54</option>
			 <option value="55">55</option>
			 <option value="56">56</option>
			 <option value="57">57</option>
			 <option value="58">58</option>
			 <option value="59">59</option>
	   </select>
	 </div>
	 <div class="corte"></div>
	 <div>
	   <input type="submit" class="boton" name="verReporte" value="Ver Reporte" />  
	 </div>
    </form>
   </div>
 </div>
</body>
</html>
