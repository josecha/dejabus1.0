<?php
ob_start();

if (isset($_GET["d"]) and strlen($_GET["d"])>0){
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	$u = new User();	
	if ($u->isLogued){
?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head profile="http://gmpg.org/xfn/11">
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
			<meta http-equiv="pragma" content="no-cache" />
			<meta http-equiv="cache-control" content="no-cache" />	
			<meta name="description" content="T3 es la forma sencilla y moderna de rastrear un contenedor en tiempo real y detectar su apertura en el momento en que esta ocurre, con un bajo costo de operaci&oacute;n." />
			<meta name="keywords" content="Rastreo,Sistema,Colima,GPS,Geoposicionamiento,Mexico,Contenedores,Rastreo Satelital" />
			<meta http-equiv="content-language" content="es" />
			<meta name="Language" content="Spanish" />
			<meta name="Distribution" content="Global" />
			<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />	
<?
		$serieGps = (int)myDencr(trim($_GET["d"]));
		
		if ($serieGps>0){
			//procesamos datos			
			$vm = 100;
			if (isset($_GET["vm"]) and strlen(trim($_GET["vm"]))>0){
				$vm = (int)trim($_GET["vm"]);
			}
			
			//obtenemos fecha inicial
			if (isset($_GET["fi"]) and strlen($_GET["fi"])>0){
				$fi = trim(substr($_GET["fi"],6,4).'-'.substr($_GET["fi"],3,2).'-'.substr($_GET["fi"],0,2).'T'.((isset($_GET["hi"]) and strlen($_GET["hi"])>0)?$_GET["hi"]:00).':'.((isset($_GET["mi"]) and strlen($_GET["mi"])>0)?$_GET["mi"]:00).':00');			
			}else{
				$fi = date('Y-m-d\TH:i:s');	
			}
			
			//obtenemos fecha final
			if (isset($_GET["ff"]) and strlen($_GET["ff"])>0){
				$ff = trim(substr($_GET["ff"],6,4).'-'.substr($_GET["ff"],3,2).'-'.substr($_GET["ff"],0,2).'T'.((isset($_GET["hf"]) and strlen($_GET["hf"])>0)?$_GET["hf"]:00).':'.((isset($_GET["mf"]) and strlen($_GET["mf"])>0)?$_GET["mf"]:00).':00');			
			}else{
				$ff = date('Y-m-d\TH:i:s');	
			}
?>			
			<title>Montecristo Data Mining - Tracking Tampering Technology - Viajes - Gr&aacute;fica de Velocidades Por D&iacute;a</title>
			</head>
			<body>
				<div id="pdf" style="margin:0 auto; padding-bottom:20px; text-align:center; margin-bottom:5px;">
					<span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; color:#666666;">Gr&aacute;ficos de velocidades por d&iacute;a</span>
				</div>
				
				<?
				echo '<div id="grafica" style="border:#C7C7C7 solid 1px; width:1008px; margin: 0 auto;"><img src="graficaVelocidad.php?fi='.$fi.'&ff='.$ff.'&xt='.$serieGps.'&vm='.$vm.'" title="Velocidades por d�a" alt="Velocidades por d�a" width="1000"/></div>';
				?>
			</body>
		</html>
<?php		
		}//end if ($serieGps>0)
	}//end if ($u->isLogued)
}//end if (isset($_GET["idViaje"]) and strlen($_GET["idViaje"])>0)
ob_end_flush();
?>