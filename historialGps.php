<?php
ob_start();
header("Content-type: text/html; charset=iso-8859-1");
include("conexion.php");
include("funciones.php");
$rs = New COM("ADODB.Recordset");
	
if (isset($_GET["id"]) and strlen(trim($_GET["id"]))>0){ //regresamos datos
	//verificamos si el user ya inici� sesion
	$u = new User();
	if ($u->isLogued){
		//sacamos datos y los regresamos
		$rs->Open("	SELECT users.nombre, bitacoraGPS.fecha 
					FROM bitacoraGPS 
					INNER JOIN users 
					ON (bitacoraGPS.admin = users.id)
					WHERE bitacoraGPS.gps=".trim($_GET["id"])."
					ORDER BY bitacoraGPS.fecha ASC", $conn); 
		$datos = fetch_assoc($rs);
		$rs->Close();		
		unset($rs);	
		
		//creamos datos formateados
		$dat = "";
		$totRegistros = count($datos);
		
		for ($a=0; $a<$totRegistros; $a++){
			$dat .= trim($datos[$a]["nombre"])."@co@".trim($datos[$a]["fecha"]).(($a != $totRegistros-1)?'@fi@':'');
		}
		
		//imprimimos datos formateados
		echo trim($dat);
	}else{
		echo "false";
	}
}else{
	echo "false";
}

ob_end_flush();
?>