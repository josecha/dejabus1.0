<?php
ob_start();

include("conexion.php");
include("funciones.php");
$rs = New COM("ADODB.Recordset");

$u = new User();
if ($u->isLogued){
	if (isset($_GET["ga"]{0})){
		include('head.php');	
		$idGA = (int)trim($_GET["ga"]);
		
		//sacamos datos de esta base
		$rs->Open("SELECT latitud, longitud, nombre, descripcion FROM geoControl INNER JOIN geoControlData ON (geoControl.id = geoControlData.geocontrol) WHERE geoControl.id = $idGA ORDER BY secuencia ASC", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
?>			
		<title>Montecristo Data Mining - Tracking Tampering Technology - Modificar Geo&aacute;rea</title>
		<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
		<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAi8mj96kyL8tl4LpmHvQWdBRf664DUNFDIWxt3GQfe0EgEFDFzxQQXv2MdIUYESSeDE37VxjSiN4nbQ" type="text/javascript"></script>
		<script type="text/javascript">  
			//<![CDATA[
			var map;
			var poly;
			var count = 0;
			var points = new Array();
			var markers = new Array();
			var lineColor = "#0000af";
			var fillColor = "#335599";
			var lineWeight = 3;
			var lineOpacity = .8;
			var fillOpacity = .2;
			var controlAreas = new Array(); // Array que almacena las Areas de control
			
			$(document).ready(function(){
				icon_url ="http://labs.google.com/ridefinder/images/";
				mapdiv = document.getElementById("map");
				buildMap(); // inicializa el mapa
				inicializar(); //colocamos poligono en mapa
			});
			
			function modGeoarea(){
				// Guarda en un array los puntos correspondientes al Area de control
				if (points.length < 4 || isVacio($("#nombre").val())){
					alert("Primero crea un �rea de m�nimo 3 puntos y despues escribe un nombre.");
				}else{
					//creamos vector de coordenadas para enviarlas mediante ajax
					var puntos = "";
					for (i = 0; i < points.length; i++){
						puntos += points[i].lat() + "_" + points[i].lng();
						
						if (i != points.length - 1){
							puntos += "#";
						} 
					}
					
					$.ajax({
						type: "POST",
						url: "modGeoareasToDB.php",
						data: "nombre="+encodeURIComponent($("#nombre").val())+"&descripcion="+encodeURIComponent($("#descripcion").val())+"&puntos="+puntos+"&idGA="+<?=$idGA ?>,
						dataType: "html",
						contentType: "application/x-www-form-urlencoded",
						success: function(datos){
							if (trim(datos)=='OK'){
								//limpiamos variables
								points = [];
								markers = [];
								count = 0;
								map.clearOverlays();
								poly = null;
								
								//limpiamos cuadros de texto
								$("#nombre").val("");
								$("#descripcion").val("");
															
								alert("Geo�rea modificada satisfactoriamente!");
								window.close();
							}
						}			
					});			
				}
			}	
			
			function addIcon(icon){ // Add icon attributes
				icon.shadow= icon_url + "mm_20_shadow.png";
				icon.iconSize = new GSize(12, 20);
				icon.shadowSize = new GSize(22, 20);
				icon.iconAnchor = new GPoint(6, 20);
				icon.infoWindowAnchor = new GPoint(5, 1);
			}
			
			function buildMap() { // Crear y posiciona el mapa
				if (GBrowserIsCompatible()) {
					map = new GMap2(mapdiv, {draggableCursor:"auto", draggingCursor:"move"});
					
					// Load initial map and a bunch of controls
					map.setCenter(new GLatLng(<? echo $datos[0]["latitud"].", ".$datos[0]["longitud"]; ?>), 13);
					map.addControl(new GLargeMapControl()); // Zoom control
					map.addControl(new GMapTypeControl());	
					map.enableScrollWheelZoom();
					map.disableDoubleClickZoom();
					
					// Add click listener
					GEvent.addListener(map, "click", leftClick);
				}
			}
			
			function leftClick(overlay, point) {
				if(point) {
					var icon = new GIcon();
					icon.image = icon_url +"mm_20_purple.png";
					addIcon(icon);
					
					// Make markers draggable
					var marker = new GMarker(point, {icon:icon, draggable:true, bouncy:false, dragCrossMove:true});
					map.addOverlay(marker);
					marker.content = count;
					markers.push(marker);
					
					// Drag listener
					GEvent.addListener(marker, "drag", function() {
						drawOverlay();
					});
					
					// Second click listener
					GEvent.addListener(marker, "click", function() {
						// Find out which marker to remove
						for(var n = 0; n < markers.length; n++) {
							if(markers[n] == marker) {
								map.removeOverlay(markers[n]);
								break;
							}
						}
					
						// Shorten array of markers and adjust counter
						markers.splice(n, 1);
						if(markers.length == 0) {
							count = 0;
						}else {
							count = markers[markers.length-1].content;
							drawOverlay();
						}
					});
					
					drawOverlay();
				}
			}
			
			function drawOverlay(){ // Dibuja el poligono en el mapa.
				if(poly){ 
					map.removeOverlay(poly); 
				}
				
				points.length = 0;
				
				for(i = 0; i < markers.length; i++) {
					points.push(markers[i].getLatLng());
				}
				
				points.push(markers[0].getLatLng());
				poly = new GPolygon(points, lineColor, lineWeight, lineOpacity, fillColor, fillOpacity);
				
				map.addOverlay(poly);
			}
			
			function inicializar(){
				<?php
				//imprimimos todos los puntos exepto el ultimo
				for ($a = 0; $a < count($datos)-1; $a++){
					echo 'leftClick(null, new GLatLng('.$datos[$a]['latitud'].', '.$datos[$a]['longitud'].'));';
				}
				?>
			}	
			//]]>
		</script>
		</head>
		<body>		
			<div class="formAddGeocercas">
				<label class="Flbl">Nombre</label>
				<input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="20" value="<?=$datos[0]['nombre']; ?>" />
				<label class="Flbl">Descripci&oacute;n</label>					
				<input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="50" value="<?=$datos[0]['descripcion']; ?>" />
				<input class="buttonAddGeo" type="button" value="Modificar Geo&aacute;rea" onClick="modGeoarea(); return false;" />
				<div>Presiona click izquierdo en el mapa para agregar un punto. Presiona click sobre un punto para eliminarlo. Cada punto puede ser arrastrado. La geo&aacute;rea debe tener m&iacute;nimo 3 puntos.</div>
			</div>					
			<div id="map" style="width:1035px; height:545px;"></div>
		</body>
		</html>
<?php
	}//end if (isset($_GET["ga"]) and strlen($_GET["ga"])>0)
}//end if ($u->isLogued)
	
ob_end_flush();
?>