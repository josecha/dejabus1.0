<?php
include("conexion.php");
include("funciones.php");
header("Content-type: text/html; charset=iso-8859-1");

if (isset($_POST["checando"]) and $_POST["checando"] == "1"){
	//checamos sesion del usuario
	$sesion = new Login();
	
	if ($sesion->isLogued()){
		//imprimimos nombre de user para mostrar bienvenida
		echo $sesion->nameUser;
	}else{
		//imprimimos bandera para mostrar form
		echo "showFormLogin";
	}
}elseif (isset($_POST["u"]) and strlen(trim($_POST["u"]))>0){
	//checamos sesion del usuario
	$sesion = new Login();
	if ($sesion->checarDatos(trim($_POST["u"]), trim($_POST["p"]))){
		//imprimimos nombre de user para mostrar bienvenida
		echo $sesion->nameUser;
	}else{
		//imprimimos bandera para mostrar error
		echo "showErrorLogin";
	}
}elseif (isset($_POST["d"]) and $_POST["d"] == "1"){
	//eliminamos sesion
	$sesion = new Login();
	$sesion->logOut();
}
?>