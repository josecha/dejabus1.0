<?php
ob_start();

include("conexion.php");
include("funciones.php");
$rs = New COM("ADODB.Recordset");

$u = new User();
if ($u->isLogued){
	if (isset($_GET["b"]) and strlen($_GET["b"])>0){
		include('head.php');	
		$idBase = (int)trim($_GET["b"]);
		
		//sacamos datos de esta base
		$rs->Open("SELECT * FROM geocercas WHERE id = $idBase", $conn); 
		$datos = fetch_assoc($rs); 
		$rs->Close();
?>			
		<title>Montecristo Data Mining - Tracking Tampering Technology - Modificar Geocerca</title>
		<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
		<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAi8mj96kyL8tl4LpmHvQWdBRf664DUNFDIWxt3GQfe0EgEFDFzxQQXv2MdIUYESSeDE37VxjSiN4nbQ" type="text/javascript"></script>
		<script type="text/javascript">  
			//<![CDATA[
			var map;		
			var marcadorNow;														
			var miIconoR = new GIcon();
			var miIconoC = new GIcon();
			var markerOptionsR;
			var markerOptionsC;		
			var markerCentro;
			var markerRadio;
			var handlerListenerClick;
			var handlerListenerDblClick;
			var latlngSingleClick;
			
			function inicializar(){
				//marker Centro
				if (!markerCentro){
					markerCentro = new GMarker(new GLatLng(<?=$datos[0]['latitud']; ?>, <?=$datos[0]['longitud']; ?>), markerOptionsC);					
					map.addOverlay(markerCentro);	
				}
				
				//marker Radio
				if (!markerRadio){
					markerRadio = new GMarker(new GLatLng(<?=$datos[0]['radiolat']; ?>, <?=$datos[0]['radiolon']; ?>), markerOptionsR);						 
					map.addOverlay(markerRadio);
				}
			}
			
			function load() {
				if (GBrowserIsCompatible()) {
					map = new GMap2(document.getElementById("map"));
					map.setCenter(new GLatLng(<?=$datos[0]['latitud']; ?>, <?=$datos[0]['longitud']; ?>), 15);	  						
					map.enableScrollWheelZoom();
					map.addControl(new GLargeMapControl());
					map.addControl(new GMapTypeControl());				
					
					// Personalizamos el icono para Radio		
					miIconoR.image = "http://www.montecristodm.com/viajes/t3/Imagenes/radioMarker.png";
					miIconoR.iconSize = new GSize(16, 16);
					miIconoR.iconAnchor = new GPoint(8, 8);
					miIconoR.infoWindowAnchor = new GPoint(8, 8);
					// Lo metemos a las configuraciones del marcador
					markerOptionsR = { icon:miIconoR };
					
					// Personalizamos el icono para Centro		
					miIconoC.image = "http://www.montecristodm.com/viajes/t3/Imagenes/centroMarker.png";
					miIconoC.iconSize = new GSize(16, 16);
					miIconoC.iconAnchor = new GPoint(8, 8);
					miIconoC.infoWindowAnchor = new GPoint(8, 8);
					// Lo metemos a las configuraciones del marcador
					markerOptionsC = { icon:miIconoC };
					
					handlerListenerClick = GEvent.addListener(map, "click", function(overlay, latlngClick){				
						//si ya habia un marcador de radio
						if (markerRadio){
							if (overlay != markerRadio){
								//si ya habia un marcador
								if (markerCentro){
									//si presion� otra cosa menos el marker del centro
									if (overlay != markerCentro){
										map.removeOverlay(markerCentro);
										
										markerCentro = new GMarker(new GLatLng(latlngClick.lat(), latlngClick.lng()), markerOptionsC);					
										$("#latCentro").val(latlngClick.lat());
										$("#lonCentro").val(latlngClick.lng());
												 
										map.addOverlay(markerCentro);	
									}
								}else{							
									markerCentro = new GMarker(new GLatLng(latlngClick.lat(), latlngClick.lng()), markerOptionsC);					
									$("#latCentro").val(latlngClick.lat());
									$("#lonCentro").val(latlngClick.lng());
											 
									map.addOverlay(markerCentro);	
								}		
							}
						}else{
							//si ya habia un marcador
							if (markerCentro){
								//si presion� otra cosa menos el marker del centro
								if (overlay != markerCentro){
									map.removeOverlay(markerCentro);
									
									markerCentro = new GMarker(new GLatLng(latlngClick.lat(), latlngClick.lng()), markerOptionsC);					
									$("#latCentro").val(latlngClick.lat());
									$("#lonCentro").val(latlngClick.lng());
											 
									map.addOverlay(markerCentro);	
								}
							}else{							
								markerCentro = new GMarker(new GLatLng(latlngClick.lat(), latlngClick.lng()), markerOptionsC);					
								$("#latCentro").val(latlngClick.lat());
								$("#lonCentro").val(latlngClick.lng());
										 
								map.addOverlay(markerCentro);	
							}		
						}														
					});									
					
					handlerListenerDblClick = GEvent.addListener(map, "singlerightclick", function(puntoXY){					
						if (markerRadio){
							map.removeOverlay(markerRadio);
						}
						
						latlngSingleClick = map.fromContainerPixelToLatLng(puntoXY);
						markerRadio = new GMarker(new GLatLng(latlngSingleClick.lat(), latlngSingleClick.lng()), markerOptionsR);
						$("#latRadio").val(latlngSingleClick.lat());
						$("#lonRadio").val(latlngSingleClick.lng());
								 
						map.addOverlay(markerRadio);						
					});		
					
					inicializar();			
				}
			}		
			
			function modGeocerca(){
				if (($("#latCentro").val()=="0") || ($("#lonCentro").val()=="0") || ($("#latRadio").val()=="0") || ($("#lonRadio").val()=="0") || isVacio($("#nombre").val())){
					alert("Selecciona el centro y perimetro de la base a agregar y escribe un nombre.");
				}else{
					$.ajax({
						type: "POST",
						url: "modGeocercasInDB.php",
						data: "idBase=<?=$idBase; ?>&nombre="+encodeURIComponent($("#nombre").val())+"&descripcion="+encodeURIComponent($("#descripcion").val())+"&latCentro="+$("#latCentro").val()+"&lonCentro="+$("#lonCentro").val()+"&latRadio="+$("#latRadio").val()+"&lonRadio="+$("#lonRadio").val()+"&tipo="+$("#icono").val(),
						dataType: "html",
						contentType: "application/x-www-form-urlencoded",
						success: function(datos){
							if (trim(datos)=='OK'){								
								alert("Geocerca modificada satisfactoriamente!");
								
								//cerramos p�gina
								window.close();
							}
						}			
					});			
				}
			}		
			//]]>
		</script>
		</head>
		<body onLoad="load()" onUnload="GUnload()">		
			<div class="formAddGeocercas">
				<label class="Flbl">Nombre</label>
				<input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="20" value="<?=$datos[0]['nombre']; ?>" />
				<label class="Flbl">Descripci&oacute;n</label>					
				<input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="50" value="<?=$datos[0]['descripcion']; ?>" />
				<label class="Flbl">Tipo</label>
				<select class="Ftxt" name="icono" id="icono"><?=str_replace('value="'.$datos[0]['tipo'].'"', 'value="'.$datos[0]['tipo'].'" selected="selected"', $u->printIconList()); ?></select>
				<input class="buttonAddGeo" type="button" value="Modificar Geocerca" onClick="modGeocerca(); return false;" />
				<div>Presiona click izquierdo en el mapa para poner el centro de la geocerca; despu&eacute;s presiona click derecho para poner el radio.</div>
				<input type="hidden" name="latCentro" id="latCentro" value="<?=$datos[0]['latitud']; ?>" />
				<input type="hidden" name="lonCentro" id="lonCentro" value="<?=$datos[0]['longitud']; ?>" />
				<input type="hidden" name="latRadio" id="latRadio" value="<?=$datos[0]['radiolat']+.01; ?>" />
				<input type="hidden" name="lonRadio" id="lonRadio" value="<?=$datos[0]['radiolon']+.01; ?>" />
			</div>					
			<div id="map" style="width:1035px; height:545px;"></div>
		</body>
		</html>
<?php
	}//end if (isset($_GET["b"]) and strlen($_GET["b"])>0)
}//end if ($u->isLogued)
	
ob_end_flush();
?>