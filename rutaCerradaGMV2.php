<?php
ob_start();
ini_set("memory_limit","32M");
set_time_limit(240);

if (isset($_GET["idViaje"]) and strlen($_GET["idViaje"])>0){ 
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	$u = new User();
	if ($u->isLogued){
		include('head.php');
		
		//recivimos datos	
		$idViaje = trim((int)$_GET["idViaje"]); 			

		//sacamos los demas datos
		$rs->Open("	SELECT 
						viajes.clave, 
						CONVERT(CHAR(19),viajes.finicial,120) AS fechaI,
						CONVERT(CHAR(19),viajes.ffinal,120) AS fechaF,						
						viajes.origen,
						viajes.destino,
						viajes.mercancia,
						viajes.vel,
						viajes.ruta,
						viajes.tiempomuerto,
						GPs.serie
					FROM viajes 
					INNER JOIN GPs
					ON (GPs.id = viajes.gps)
					WHERE viajes.idviaje = ".$idViaje, $conn);				
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		//si esque hay bases
		$bases = array();
		if (strlen(trim($datos[0]['ruta'])) > 0 and (int)$datos[0]['ruta'] != 0){
			//sacamos ruta de control
			$rs->Open("	SELECT 
							nombre, 
							descripcion
						FROM RutaControl 
						WHERE 
							id = ".(int)$datos[0]['ruta'], $conn);
			$rutaControl = fetch_assoc($rs);  
			$rs->Close();
		
			//sacamos todas las bases
			$rs->Open("	SELECT     
							geocercas.nombre,
							geocercas.descripcion,
							geocercastipo.tipo,
							geocercastipo.icono,
							geocercas.longitud,
							geocercas.latitud
						FROM geocercas 
						INNER JOIN geocercastipo
						ON (geocercastipo.id = geocercas.tipo)
						WHERE geocercas.id IN(
							SELECT idGeocerca
							FROM RutaGeocercas
							WHERE RutaGeocercas.idRuta = ".(int)$datos[0]['ruta']."
							GROUP BY idGeocerca
							HAVING (COUNT(RutaGeocercas.idGeocerca) > 0)
						)", $conn);
			$bases = fetch_assoc($rs);  
			$rs->Close();
		}//end if
		
		//velocidad maxima
		$vm = 100;
		if ((isset($_GET["vm"])) and (strlen($_GET["vm"])>0) and ((int)$_GET["vm"]>3)){
			$vm = (int)trim($_GET["vm"]);
		}elseif ((strlen(trim($datos[0]['vel']))>0) and ((int)$datos[0]['vel']>3)){
			$vm = (int)$datos[0]['vel'];
		}

		//tiempo muerto
		$tm = 60;
		if ((isset($_GET["tm"])) and (strlen($_GET["tm"])>0) and ((int)$_GET["tm"]>1)){
			$tm = (int)trim($_GET["tm"]);
		}elseif ((strlen(trim($datos[0]['tiempomuerto']))>0) and ((int)$datos[0]['tiempomuerto']>1)){
			$tm = (int)$datos[0]['tiempomuerto'];
		}
		
		//demas datos
		//fecha inicial [fechaInicial]
		$fechaI = date_parse(trim($datos[0]['fechaI']));
		$fechaI = date('Y-m-d\TH:i:s', 	mktime(
											$fechaI['hour'],
											$fechaI['minute'],
											$fechaI['second'],
											$fechaI['month'],
											$fechaI['day'],
											$fechaI['year']
										));		
		$fechaF = date_parse(trim($datos[0]['fechaF']));
		$fechaF = date('Y-m-d\TH:i:s', 	mktime(
											$fechaF['hour'],
											$fechaF['minute'],
											$fechaF['second'],
											$fechaF['month'],
											$fechaF['day'],
											$fechaF['year']
										));	
		$xt = $datos[0]['serie']; //gps										
		$mercancia = $datos[0]['mercancia']; //mercancia
		$destino = $datos[0]['destino']; //destino
		$origen = $datos[0]['origen']; //origen
		$clvViaje = $datos[0]['clave']; //clave

		//sacamos todos los puntos
		$rs->Open("	SELECT 				
						GPs.descripcion,
						datosghe.gps,						
						CONVERT(CHAR(19),datosghe.fechasend,120) AS fechasend,						
						CONVERT(CHAR(19),datosghe.fecharecv,120) AS fecharecv,	
						datosghe.lat,
						datosghe.lon,
						datosghe.alarma,
						datosghe.tambiental,
						datosghe.velocidad,
						datosghe.valido,
						datosghe.bateria,
						datosghe.aperturas,
						datosghe.rumbo						
					FROM datosghe 
					INNER JOIN GPs
					ON (GPs.serie = datosghe.gps)
					WHERE 
						datosghe.gps = $xt AND
						datosghe.fecharecv >= '$fechaI' AND 
						datosghe.fecharecv <= '$fechaF' AND 
						datosghe.lat <> 0 AND
						datosghe.lon <> 0 AND 
						datosghe.valido = 1
					ORDER BY datosghe.fechasend ASC", $conn);
		$datos = fetch_assoc($rs); 
		$rs->Close();	
?>			
			<title>Montecristo Data Mining - Tracking Tampering Technology - Viajes - Recorrido</title>
            
            <script src="http://maps.google.com/maps/api/js?libraries=geometry&sensor=false" type="text/javascript"></script>
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	  
			<script type="text/javascript">
				//<![CDATA[
				
				var map;
				var vi=0; 
				var band_vi=1;
				var band_rep=0;
				var band_now=0;
				var ruta_all=1;
				var marcadores=[];
				var PointsNow=[];
				var polylineNow;
				var band_polyNow=0;
				var marcadorNow;
				var polyline;
				var Finicial=[];
				var Ffinal=[];
				var Tiempo=[];
				var a;
				var miIconoBase;
				var markerOptionsBase;	
				var marcadorBase;	
				
<?php
				echo '
				//OBTENEMOS DATOS DE LA DB
				var datosdb = new Array([';
					for ($a=0; $a < count($datos)-1; $a++){ 
						echo $datos[$a]["gps"].','; 
					}  
					echo $datos[count($datos)-1]["gps"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo '"'.$datos[$a]["fecharecv"].'",'; 
					}  
					echo '"'.$datos[count($datos)-1]["fecharecv"];
					echo '"],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo '"'.$datos[$a]["fechasend"].'",'; 
					}  
					echo '"'.$datos[count($datos)-1]["fechasend"];
					echo '"],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						//19.2432N, 103.7011W (latitud,longitud) (Y,X) ->  19.2432, -103.7011
						$lat=$datos[$a]["lat"];	  
						switch(substr($lat,strlen($lat)-1,1)){
							case 'S': (double)$lat*=-1; break;
							case 'N': (double)$lat*=1; break;
						}
						//guardamos puntos en formato decimal 
						echo '"'.$lat.'",'; 	 
					}  //end for
					
					$lat=$datos[count($datos)-1]["lat"];	  
					switch(substr($lat,strlen($lat)-1,1)){
						case 'S': (double)$lat*=-1; break;
						case 'N': (double)$lat*=1; break;
					}
					//guardamos puntos en formato decimal 	 	 
					echo '"'.$lat;
					echo '"],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						//19.2432N, 103.7011W (latitud,longitud) (Y,X) ->  19.2432, -103.7011
						$lon=$datos[$a]["lon"];  
						switch(substr($lon,strlen($lon)-1,1)){
							case 'W': (double)$lon*=-1; break;
							case 'E': (double)$lon*=1; break;
						}
						//guardamos puntos en formato decimal
						echo '"'.$lon.'",'; 
					}  
					$lon=$datos[count($datos)-1]["lon"];  
					switch(substr($lon,strlen($lon)-1,1)){
						case 'W': (double)$lon*=-1; break;
						case 'E': (double)$lon*=1; break;
					} 
					echo '"'.$lon;  
					echo '"],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["alarma"].','; 
					}  
					echo $datos[count($datos)-1]["alarma"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["tambiental"].','; 
					}  
					echo $datos[count($datos)-1]["tambiental"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["velocidad"].','; 
					}  
					echo $datos[count($datos)-1]["velocidad"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["valido"].','; 
					}  
					echo $datos[count($datos)-1]["valido"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["bateria"].','; 
					}  
					echo $datos[count($datos)-1]["bateria"];
					echo '],
						[';					
					for ($a=0; $a < count($datos)-1; $a++){
						echo $datos[$a]["rumbo"].','; 
					}  
					echo $datos[count($datos)-1]["rumbo"];
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo '"'.$datos[$a]["descripcion"].'",'; 
					}  
					echo '"'.$datos[count($datos)-1]["descripcion"].'"';
					echo '],
						[';
					for ($a=0; $a < count($datos)-1; $a++){
						echo '"'.$datos[$a]["aperturas"].'",'; 
					}  
					echo '"'.$datos[count($datos)-1]["aperturas"].'"';
					echo ']
				);'; 
?>
				
				var largo=datosdb[0].length; //tamaño total de cada array dentro de "datosdb"
				
				function misdatos(micampo, minumero){  //funcion solo para tener mayor legibilidad
					var midato;
					switch(micampo){
						case "gps": midato=datosdb[0][minumero]; break;
						case "fecharecv": midato=datosdb[1][minumero]; break;
						case "fechasend": midato=datosdb[2][minumero]; break;
						case "lat": midato=datosdb[3][minumero]; break;
						case "lon": midato=datosdb[4][minumero]; break;
						case "alarma": midato=datosdb[5][minumero]; break;
						case "tambiental": midato=datosdb[6][minumero]; break;
						case "velocidad": midato=datosdb[7][minumero]; break;
						case "valido": midato=datosdb[8][minumero]; break;
						case "bateria": midato=datosdb[9][minumero]; break;
						case "rumbo": midato=datosdb[10][minumero]; break;
						case "descripcion": midato=datosdb[11][minumero]; break;
						case "aperturas": midato=datosdb[12][minumero]; break;
					}
					return midato;
				}	
								
				function GetRumbo(numeroR){
					var elrumbo;
					switch(misdatos("rumbo",numeroR)){
						case 0: elrumbo="Norte"; break;
						case 1: elrumbo="Noreste"; break;
						case 2: elrumbo="Este"; break;
						case 3: elrumbo="Sureste"; break;
						case 4: elrumbo="Sur"; break;
						case 5: elrumbo="Suroeste"; break;
						case 6: elrumbo="Oeste"; break;
						case 7: elrumbo="Noroeste"; break;
					}  
					
					return elrumbo;
				}
								
				//Obtenemos distancias			
				var Dtotal=0;	
				var distancias = new Array();
				var polylineD;
				distancias[0]=0;
				
<?php
				//obtenemos distancia entre este punto y el punto anterior
				for ($a=1; $a<count($datos); $a++){
					
					echo 'var point1=[];
                    point1.push(new google.maps.LatLng(misdatos("lat",'.($a-1).'), misdatos("lon",'.($a-1).')));
                    point1.push(new google.maps.LatLng(misdatos("lat",'.$a.'), misdatos("lon",'.$a.')));
                    Dtotal += google.maps.geometry.spherical.computeDistanceBetween(point1[0], point1[1]);
					distancias['.$a.'] = Dtotal;
                    
					'; 

            }        
				
?>				



				function crearMarcador(idDato, Opciones){
					var msjStop = '';
					var marker;
					if (Opciones == 1){ 
                         marker = new google.maps.Marker({
                            position: new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)),
                            map: map,
                            icon:markerOptionsStop,
                            tag:idDato
                            });
						//var marker = new GMarker(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), markerOptionsStop); 
						msjStop = '<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Duraci&oacute;n de inactividad: <b>'+ Tiempo[idDato]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Desde: <b>'+ Finicial[idDato]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Hasta: <b>'+ Ffinal[idDato]+'</b></div><br/>';
					}else if (Opciones == 2){
						marker = new google.maps.Marker({
                            position: new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)),
                            map: map,
                            icon:markerOptionsAlarma,
                            tag:idDato
                            });
						//var marker = new GMarker(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), markerOptionsAlarma); 
					}else if (Opciones == 3){
						marker = new google.maps.Marker({
                            position: new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)),
                            map: map,
                            icon:markerOptionsFast,
                            tag:idDato
                            });	

						//var marker = new GMarker(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), markerOptionsFast); 
					}else{ //Opciones==0
						
                        marker = new google.maps.Marker({
                        position: new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)),
                        map: map,
                        icon: markerOptions,
                        tag:idDato
                        });	

						//var marker = new GMarker(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), markerOptions); 
					}
					
                  

					//marker.value = idDato;
					//le agregamos el evento "click" para desplegar información
					google.maps.event.addListener(marker,"click", function() {
						var myHtml = '<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Modulo: <b>'+ misdatos('descripcion',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha: <b>'+ misdatos('fecharecv',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha UTC: <b>'+misdatos('fechasend',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fibra: <b>'+ ((misdatos('alarma',idDato)==1)?'Abierta':'No abierta')+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Aperturas: <b>'+ misdatos('aperturas',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Direcci&oacute;n: <b>'+ GetRumbo(idDato) +'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Velocidad: <b>'+misdatos('velocidad',idDato)+' km/h</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Temperatura: <b>'+ misdatos('tambiental',idDato)+'&deg; C</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Bater&iacute;a: <b>'+ misdatos('bateria',idDato)+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;"><b>'+ convertirM_KM(distancias[idDato])+'</b> recorridos</div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;"><b>'+ convertirM_KM(Dtotal)+'</b> totales</div><br/>'+trim(msjStop);

						
                                info.setContent(myHtml);
                                info.setPosition(new google.maps.LatLng(misdatos("lat",idDato), misdatos("lon",idDato)));
							    info.open(map);

						//map.openInfoWindowHtml(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), myHtml);
					});

					marcadores.push(marker);	  
				}
				 var info = new google.maps.InfoWindow();
				//funcion que guarda en variables globales el tiempo que transcurrio un camion en velocidad 0
				function crearMarcadorAux(idDato, Fini, Ffin, Tiem) { 
					Finicial[idDato]=Fini;
				   	Ffinal[idDato]=Ffin;
				   	Tiempo[idDato]=Tiem;
				   	crearMarcador(idDato, 1);
				}
										
				function load() {
					
              
                     var canvas=document.getElementById("map");
                     $("#map").css("height",$(window).height()-20);
      
                    var latlng;
                    
                    if(misdatos("lat",0)==""){
                    	latlng = new google.maps.LatLng(19.3,-103.3);
                    	alert("No hay datos");
                    }else{

                    	latlng = new google.maps.LatLng(misdatos("lat",0),misdatos("lon",0));             
                        
                    }

                     
                 
                     var settings = {
                       zoom: 10,
                       center: latlng,
                       mapTypeControl: true,
                       mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                       navigationControl: true,
                       navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                       mapTypeId: google.maps.MapTypeId.HYBRID
                     };
                     map = new google.maps.Map(canvas, settings);

                     $(window).resize(function() {
                      $("#map").css("height",$(window).height()-20);
                      });

                      <?php
                        //echo 'miIconoBase = new GIcon();';
						for ($a = 0; $a < count($bases); $a++){
							
                            
							echo '
							 
                            marcadorBase = new google.maps.Marker({
                            position: new google.maps.LatLng('.$bases[$a]['latitud'].', '.$bases[$a]['longitud'].'),
                            title:"'.$bases[$a]['nombre'].'",
                            icon:"'.$bases[$a]["icono"].'",
                            map: map,                           
                            tag:'.$a.'
                            });

                            
                            google.maps.event.addListener(marcadorBase,"click", function() {
						        var myHtml1 = \'<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Base: <b>'.$bases[$a]['nombre'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Descripci&oacute;n de base: <b>'.$bases[$a]['descripcion'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Tipo: <b>'.$bases[$a]['tipo'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Ruta de Control: <b>'.$rutaControl[0]['nombre'].'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Descripci&oacute;n de ruta de control: <b>'.$rutaControl[0]['descripcion'].'</b></div><br/>\';    
						
                                info.setContent(myHtml1);
                                info.setPosition(new google.maps.LatLng('.$bases[$a]['latitud'].', '.$bases[$a]['longitud'].'));
							    info.open(map);
                            
						      //map.openInfoWindowHtml(new GLatLng(misdatos("lat",idDato), misdatos("lon",idDato)), myHtml);
					        });
							
							';
						}		

                      ?>
                      
                      <?php             

                        echo "var point=[];"; 
                     
                        //echo 'var datoPoligono=[';
						for ($a=0; $a < count($datos)-1; $a++){
							//echo 'alert(misdatos("lat",'.$a.'));';
							echo 'point.push(new google.maps.LatLng(misdatos("lat",'.$a.'), misdatos("lon",'.$a.')));';
						}?>                     
						 
                       
                        polyline = new google.maps.Polyline({
                                 path: point
                                 , map: map
                                 , strokeColor: '#CC0000'
                                 , strokeWeight: 2
                                 , strokeOpacity: 0.6
                                 
                             });


											
						// Personalizamos el nuevo icono
					/*	var miIcono = new GIcon();
						miIcono.image = "http://www.montecristodm.com/viajes/t3/Imagenes/point.png";
						miIcono.iconSize = new GSize(16, 16);
						miIcono.iconAnchor = new GPoint(8, 8);
						miIcono.infoWindowAnchor = new GPoint(8, 8);
						
						// Personalizamos el icono de Stop
						var miIconoStop = new GIcon();
						miIconoStop.image = "http://www.montecristodm.com/viajes/t3/Imagenes/tm.png";
						miIconoStop.iconSize = new GSize(16, 16);
						miIconoStop.iconAnchor = new GPoint(8, 8);
						miIconoStop.infoWindowAnchor = new GPoint(8, 8);
						
						// Personalizamos el icono Alarma
						var miIconoAlarma = new GIcon();
						miIconoAlarma.image = "http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png";
						miIconoAlarma.iconSize = new GSize(16, 16);
						miIconoAlarma.iconAnchor = new GPoint(8, 8);
						miIconoAlarma.infoWindowAnchor = new GPoint(8, 8);
						
						// Personalizamos el icono Exeso de Velocidad
						var miIconoFast = new GIcon();
						miIconoFast.image = "http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png";
						miIconoFast.iconSize = new GSize(16, 16);
						miIconoFast.iconAnchor = new GPoint(8, 8);
						miIconoFast.infoWindowAnchor = new GPoint(8, 8);
				
						// Lo metemos a las configuraciones del marcador
						markerOptions = { icon:miIcono };
						markerOptionsStop = { icon:miIconoStop };
						markerOptionsAlarma = { icon:miIconoAlarma };
						markerOptionsFast = { icon:miIconoFast };
						
*/                      
                       var miIcono = "http://www.montecristodm.com/viajes/t3/Imagenes/point.png";
						var miIconoStop = "http://www.montecristodm.com/viajes/t3/Imagenes/tm.png";
						var miIconoAlarma = "http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png";
						var miIconoFast = "http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png";

						markerOptions =  miIcono;
						markerOptionsStop = miIconoStop;
						markerOptionsAlarma = miIconoAlarma;
						markerOptionsFast = miIconoFast;
                       


						<?php
						//creamos markadores
						for ($a=0; $a<count($datos); $a++){	
							//checamos si hubo cambio de aperturas
							$cambioAperturas = false;
							if ($a > 0){
								if ($datos[$a]["aperturas"] != $datos[$a-1]["aperturas"]){
									$cambioAperturas = true;
								}
							}
							
							//procesamos icono
							if ($datos[$a]["alarma"] == 1 or $cambioAperturas == true){								
								echo 'crearMarcador('.$a.',2);'; //fibra abierta
							}else{
								//fibra cerrada
								
								if ($datos[$a]["velocidad"] != 0){ 
									if ((int)$datos[$a]["velocidad"] > $vm){ //mostramos alarma de velocidad 
										echo 'crearMarcador('.$a.',3);';
									}else{							
										echo 'crearMarcador('.$a.',0);';
									}
								}else{
									//estuvo parado un tiempo... veremos si fue mas del limite
									if ($a < count($datos)-1){  //si no esta en el ultimo registro			      	
										if ($datos[$a+1]['velocidad']!=0){ //si solo era un cero unico (sin serie de ceros)
											echo 'crearMarcador('.$a.',0);';
										}else{
											//serie de ceros: recorremos hasta encontrar el fin de la serie e imprimimos marcador
											$b = $a;
											
											//sacamos fecha del primer cero	
											$fechaCero11 = date_parse($datos[$b]['fechasend']);
											$fechaCero11 = date('m/d/Y h:i:s A', mktime(
																					$fechaCero11['hour'],
																					$fechaCero11['minute'],
																					$fechaCero11['second'],
																					$fechaCero11['month'],
																					$fechaCero11['day'],
																					$fechaCero11['year']
																				) - 21600);
																				
											//convertimos fecha del primer cero a segundos para verificar si estuvo mas de 10 minutos
											$fechaCero1 = date_parse($datos[$b]['fechasend']);
											$fechaCero1 = mktime(
															$fechaCero1['hour'],
															$fechaCero1['minute'],
															$fechaCero1['second'],
															$fechaCero1['month'],
															$fechaCero1['day'],
															$fechaCero1['year']
														);
											
											$band_while = 0;		
											while (($datos[$b]['velocidad'] == 0) and ($band_while == 0)){  //posible error de desbordamiento
												++$b;
												if ($b == count($datos)){ //desbordado, entonces decrementamos y  encendemos bandera							
													--$b;
													$band_while = 1;
												} 
											} //$b contendra el id del siguiente registro con velocidad diferente de cero
											
											//sacamos fecha del siguiente registro despues del ultimo cero
											$fechaCero22 = date_parse($datos[$b]['fechasend']);
											$fechaCero22 = date('m/d/Y h:i:s A', mktime(
																					$fechaCero22['hour'],
																					$fechaCero22['minute'],
																					$fechaCero22['second'],
																					$fechaCero22['month'],
																					$fechaCero22['day'],
																					$fechaCero22['year']
																				) - 21600);
																				
											//convertimos fecha del siguiente registro despues del ultimo cero, a segundos para verificar si estuvo mas de 10 minutos
											$fechaCero2 = date_parse($datos[$b]['fechasend']);
											$fechaCero2 = mktime(
															$fechaCero2['hour'],
															$fechaCero2['minute'],
															$fechaCero2['second'],
															$fechaCero2['month'],
															$fechaCero2['day'],
															$fechaCero2['year']
														);
											
											if (($fechaCero2 - $fechaCero1) >= ($tm * 60)){ //ponemos icono de stop 
												echo 'crearMarcadorAux('.$a.', \''.$fechaCero11.'\', \''.$fechaCero22.'\', SegMinHrs('.($fechaCero2 - $fechaCero1).'));';						
											}else{ //ponemos icono normal 
												echo 'crearMarcador('.$a.',0);';
											}
											
											//movemos el puntero ($a) a la posicion del ultimo cero, para que a la siguiente vuelta se incremente solo
											$a = $b - 1;
										}
									}else{
										echo 'crearMarcador('.$a.',0);';
									}
								}	
							}							
						}
						?> 
						
						//imprimimos markadores
						/*for (a=0; a < marcadores.length; a++){ 
							map.addOverlay(marcadores[a]); 
						}*/
						google.maps.event.addListener(map, 'click', function(event) {
 	                       
 	                       getCalle1(event.latLng,1);
                        }); 
					}	

                var info1 = new google.maps.InfoWindow();
var maker1;
var primer=false;

function getCalle1(latlng,iden){
	//obtengo
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          info1.setMap(null);
          if(iden==1){
         

            if(!primer){          	
                         marker1 = new google.maps.Marker({
                         position: latlng,
                         draggable:false,
                         map: map
                         });

            primer=true;
           }
            else
             marker1.setPosition(latlng);
         
     
           
           info1.setContent(results[1].formatted_address);
           info1.setPosition(latlng);
           info1.open(map);
           
         }else if(iden==2) {

           info1.setContent(results[1].formatted_address);
           info1.setPosition(latlng);
           info1.open(map);
                      
         }

        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });

   
 }

				//]]>
			</script>
			
		</head>
		<body onLoad="load()" onUnload="GUnload()">
			<div id="map" style="width:100%; height:615px;"></div>
		</body>
		</html>				
<?php	
	}//end if ($u->isLogued)
} //end if (isset($_GET["idViaje"]) and strlen($_GET["idViaje"])>0)
ob_end_flush();
?>