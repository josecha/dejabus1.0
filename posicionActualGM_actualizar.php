<?php
ob_start();

if (isset($_GET["xt"]) and strlen($_GET["xt"])>0){
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");

	$u = new User();
	if ($u->isLogued){
		$xt = (int)trim($_GET["xt"]);
		$idViaje = (int)trim($_GET["idViaje"]);

		//verificamos que el viaje corresponda con el usuario
		$rs->Open("SELECT idviaje FROM viajes WHERE activo = 1 AND idviaje = ".$idViaje." AND usr = ".$u->id_user, $conn);
		$datos = fetch_assoc($rs);
		$rs->Close();

		if (count($datos)>0){ //el viaje si es del usuario y aun esta activo
			//SACAMOS EL PUNTO MAS ACTUAL
			$rs->Open("	SELECT
							GPs.descripcion,
							datosghe.id,
							datosghe.fecharecv,
							CONVERT(CHAR(19),dateadd(hour,DATEDIFF(Hour,getUTCDate(),getDate()),datosghe.fechasend),120) AS fechasend,
							datosghe.lat,
							datosghe.lon,
							datosghe.tambiental,
							datosghe.alarma,
							datosghe.velocidad,
							datosghe.bateria,
							datosghe.aperturas,
							datosghe.rumbo
						FROM datosghe
						INNER JOIN GPs
						ON (GPs.serie = datosghe.gps)
						WHERE
							datosghe.gps = ".$xt." AND
							datosghe.id = (
								SELECT MAX(datosghe.id)
								FROM datosghe
								WHERE
									datosghe.gps = ".$xt."
							)", $conn);
			$datos = fetch_assoc($rs);
			$rs->Close();

			$alarmaApertura = "0";
			if (isset($datos[0]["id"])){
				$maxId = $datos[0]["id"];

				//SACAMOS APERTURAS DEL PENULTIMO PUNTO MAS ACTUAL
				$rs->Open("		SELECT TOP 1
									datosghe.aperturas
								FROM datosghe
								INNER JOIN GPs
								ON (GPs.serie = datosghe.gps)
								WHERE
									datosghe.gps = ".$xt." AND
									datosghe.id < ".$maxId." ORDER BY datosghe.id DESC", $conn);
				$datosPen = fetch_assoc($rs);
				$rs->Close();

				$aperturasNew = $datos[0]["aperturas"];
				$aperturasOld = $datosPen[0]["aperturas"];

				if ($aperturasNew != $aperturasOld){
					$alarmaApertura = "1";
				}
			}

			//si la velocidad es 0 sacamos el tiempo total que lleva parado
			$tiempoParado = "0";
			if ($datos[0]["velocidad"] == "0"){
				$rs->Open("	SELECT
								CONVERT(CHAR(19),fechasend,120) AS fechasend
							FROM datosghe
							WHERE id = (
								SELECT
									MAX(id)
								FROM datosghe
								WHERE velocidad > 3 AND gps = ".$xt."
							)", $conn);
				$datosTiempoParado = fetch_assoc($rs);
				$rs->Close();

				//calculo los minutos transcurridos en velocidad 0
				$lastFechaMoving = date_parse(trim($datosTiempoParado[0]['fechasend']));
				$thisFechaStop = date_parse(trim($datos[0]['fechasend']));

				$tiempoParado =
						mktime(
							$thisFechaStop['hour'],
							$thisFechaStop['minute'],
							$thisFechaStop['second'],
							$thisFechaStop['month'],
							$thisFechaStop['day'],
							$thisFechaStop['year']
						) -
						mktime(
							$lastFechaMoving['hour'],
							$lastFechaMoving['minute'],
							$lastFechaMoving['second'],
							$lastFechaMoving['month'],
							$lastFechaMoving['day'],
							$lastFechaMoving['year']
						);

				if ((float)$tiempoParado<0){
					$tiempoParado = 0;
				}else{
					$tiempoParado = segMinHrs($tiempoParado);
				}
			}

			//modificamos el formato de las coordenadas
			$lat = $datos[0]["lat"];
			switch(substr($lat,strlen($lat)-1,1)){
				case 'S': (double)$lat*=-1; break;
				case 'N': (double)$lat*=1; break;
			}

			$lon = $datos[0]["lon"];
			switch(substr($lon,strlen($lon)-1,1)){
				case 'W': (double)$lon*=-1; break;
				case 'E': (double)$lon*=1; break;
			}

			//modificamos formato del rumbo
			switch($datos[0]["rumbo"]){
				case 0: $elrumbo="Norte"; break;
				case 1: $elrumbo="Noreste"; break;
				case 2: $elrumbo="Este"; break;
				case 3: $elrumbo="Sureste"; break;
				case 4: $elrumbo="Sur"; break;
				case 5: $elrumbo="Suroeste"; break;
				case 6: $elrumbo="Oeste"; break;
				case 7: $elrumbo="Noroeste"; break;
			}

			//imprimimos datos
  			echo trim($datos[0]["id"].", ".$datos[0]["fecharecv"].", ".$datos[0]["fechasend"].", ".$lat.", ".$lon.", ".$datos[0]["tambiental"]."&deg; C, ".$elrumbo.", ".$datos[0]["velocidad"]." km/h, ".$datos[0]["bateria"].", ".$datos[0]["alarma"].", ".str_replace(',','COMA',$datos[0]["descripcion"]).", ".trim($tiempoParado).", ".$datos[0]["aperturas"].", ".$alarmaApertura);
		}
	}
}

ob_end_flush();
?>