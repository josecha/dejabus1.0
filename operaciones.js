 $(document).ready(function(){
    
        var canvas=document.getElementById("map_canvas");
        $("#map_canvas").css("height",$(window).height()-40);
       
        var path = new google.maps.MVCArray;
        var markers = new google.maps.MVCArray;
        var latlng = new google.maps.LatLng(19.247946342006145, -103.72788206786447);
        var bandera=false;
        var settings = {
            zoom: 8,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
     
   map = new google.maps.Map(canvas, settings);
    
      
     // var kml = new google.maps.KmlLayer("ejem.kml",{map: map});
    

    poly = new google.maps.Polygon({
    strokeWeight: 3,
    fillColor: '#5555FF'
    });

    poly.setMap(map);
    poly.setPaths(new google.maps.MVCArray([path]));
   
    
    google.maps.event.addListener(map, 'click', function(event) {
     click(event.latLng);
     });

       
    focusMarker = new google.maps.Marker({
    map: map,
    title:"pol " + path.length,
    tag:path.length
      }); 

   function click(location) {   
     
      var companyMarker = new google.maps.Marker({
      position: location,
      map: map,
      title:location + "",
      draggable:true,
      tag:path.length
      }); 
      

      
      path.insertAt(path.length, location);
      markers.insertAt(markers.length, companyMarker);
        
      google.maps.event.addListener(companyMarker,'click', function() {
          clickMark(companyMarker);  
      });

      google.maps.event.addListener(companyMarker,'drag', function() {
         
          dragMark(companyMarker);
          
      });

       google.maps.event.addListener(companyMarker,'dragstart', function() {
          
      });

     google.maps.event.addListener(companyMarker,'dragend', function() {
          

      });

   
    }
    var band=false;

    function clickMark(marker){ 
      path.removeAt(marker.tag);
      markers.removeAt(marker.tag);
      marker.setMap(null);

      for (i=0; i < path.length; i++)
        markers.getAt(i).tag=i;
  
    }

     function dragMark(marker){ 
     
      path.removeAt(marker.tag);
      path.insertAt(marker.tag,marker.getPosition());

    }

  $(window).resize(function() {
     $("#map_canvas").css("height",$(window).height()-40);
  });


  $("#guardar").click(function(){
   
    if (path.length < 4 ){
        alert("Primero crea un área de mínimo 3 puntos y despues escribe un nombre.");
      }else{
         $("#respuesta").css("display","block");
    
        //creamos vector de coordenadas para enviarlas mediante ajax
        var puntos = "";
        for (i = 0; i < path.length; i++){
          puntos += path.getAt(i).lat() + "_" + path.getAt(i).lng();
          
          if (i != path.length - 1){
            puntos += "#";
          } 
        }
  
       
     $.ajax({
          type: "POST",
          url: "addGeoareasToDB.php",
          data: "nombre="+encodeURIComponent($("#nombre").val())+"&descripcion="+encodeURIComponent($("#des").val())+"&puntos="+puntos,
          dataType: "html",
          contentType: "application/x-www-form-urlencoded",
          success: function(datos){
            var respuesta=datos.charAt(datos.lastIndexOf('O')) + datos.charAt(datos.lastIndexOf('K'));
            if (respuesta=='OK'){
            //limpiamos variables
            
                                        
              //limpiamos cuadros de texto
              $("#nombre").val("");
              $("#des").val("");


               
              for (i=0; i < path.length; i++)
               markers.getAt(i).setMap(null);
              poly.setMap(null);
              path = new google.maps.MVCArray;
              markers = new google.maps.MVCArray;
              poly = new google.maps.Polygon({
              strokeWeight: 3,
              fillColor: '#5555FF'
              });
              
              poly.setMap(map);
              poly.setPaths(new google.maps.MVCArray([path]));

              $("#respuesta").css("display","none");                             
              alert("Geoárea agregada satisfactoriamente!");
              
              //damos foco al nombre
              document.getElementById("nombre").focus();
            }

          }     
        }        
        );     



    }

    return false;
    
    });

    

});






   
   