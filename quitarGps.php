<?php
ob_start();
header("Content-type: text/html; charset=iso-8859-1");
include("conexion.php");
include("funciones.php");
$rs = New COM("ADODB.Recordset");
$rc = New COM("ADODB.Command");
	
if (isset($_GET["idGPS"]) and strlen(trim($_GET["idGPS"]))>0 and isset($_GET["idContrato"]) and strlen(trim($_GET["idContrato"]))>0){ //regresamos datos
	//verificamos si el user ya inici� sesion
	$u = new User();
	if ($u->isLogued){
		//validamos que no deje un contrato sin gps
		$rs->Open("SELECT gps FROM gpscontratos WHERE activo <> 0 AND contrato = ".trim($_GET["idContrato"]), $conn); 					
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		if (count($datos)<2){
			echo "NO";
		}else{
			//cierro los viajes
			$rc->CommandText = "UPDATE viajes SET activo = 0, ffinal = '".fechaNow()."', obs = 'Cerrado por sistema' WHERE contrato = ".trim($_GET["idContrato"]); 
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
			
			//quitamos gps de contrato
			$rc->CommandText = "UPDATE gpscontratos SET activo = 0 WHERE contrato = ".trim($_GET["idContrato"])." AND gps = ".trim($_GET["idGPS"]); 
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
			
			//habilitamos gps para otros contratos
			$rc->CommandText = "UPDATE GPs SET activo = 1 WHERE id = ".trim($_GET["idGPS"]); 
			$rc->CommandType = 1;
			$rc->ActiveConnection = $conn;
			$rc->Execute;
			
			echo 1;
		}
	}else{
		echo "false";
	}
}else{
	echo "false";
}

ob_end_flush();
?>