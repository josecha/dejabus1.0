<?php 
ob_start();
header("Content-type: text/html; charset=iso-8859-1");

if (isset($_GET["d"]) and strlen($_GET["d"])>0){
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	$serieGps = (int)myDencr(urldecode(decodeSpecialChars(trim($_GET["d"]))));

	if ($serieGps>0){
		//recivimos velocidad				
		$vm = 100;
		if (isset($_GET["vel"]) and strlen(trim($_GET["vel"]))>0){
			$vm = (int)trim($_GET["vel"]);
		}
		
		//recivimos id del usuario
		$idUser = 0; 
		if (isset($_GET["iduser"]) and strlen($_GET["iduser"])>0){  //usuario
			$idUser = trim((int)$_GET["iduser"]); 		
		}
		
		//verificamos que el Gps corresponda con el usuario
		$rs->Open("	SELECT GPs.id 
					FROM (GPs 
					INNER JOIN gpscontratos
					ON (gpscontratos.gps = GPs.id))
						INNER JOIN contratos
						ON (contratos.id = gpscontratos.contrato)
						WHERE 
							contratos.activo = 1 AND
							contratos.usr = ".$idUser." AND
							GPs.serie = ".$serieGps, $conn);
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		//si es un usuario valido
		if (count($datos)>0){			
			//SACAMOS EL PUNTO MAS ACTUAL
			$rs->Open("	SELECT 
							GPs.descripcion,
							datosghe.id,
							datosghe.fecharecv,							
							CONVERT(CHAR(19),datosghe.fechasend,120) AS fechasend,
							datosghe.lat,
							datosghe.lon,
							datosghe.tambiental,
							datosghe.alarma,
							datosghe.velocidad,
							datosghe.bateria,
							datosghe.aperturas,
							datosghe.rumbo
						FROM datosghe 
						INNER JOIN GPs
						ON (GPs.serie = datosghe.gps)
						WHERE 
							datosghe.gps = ".$serieGps." AND
							datosghe.id = (
								SELECT MAX(datosghe.id) 
								FROM datosghe 
								WHERE 
									datosghe.gps = ".$serieGps."
							)", $conn);
			$datos = fetch_assoc($rs); 
			$rs->Close();
						
			//modificamos el formato de las coordenadas
			$lat = $datos[0]["lat"];	  
			switch(substr($lat,strlen($lat)-1,1)){
				case 'S': (double)$lat*=-1; break;
				case 'N': (double)$lat*=1; break;
			}
			
			$lon = $datos[0]["lon"];  
			switch(substr($lon,strlen($lon)-1,1)){
				case 'W': (double)$lon*=-1; break;
				case 'E': (double)$lon*=1; break;
			}
			
			header("Location: http://maps.google.com/?ie=UTF8&t=h&ll=$lat,$lon&spn=0.000704,0.001149&z=20");
		}//end	if (count($datos)>0)	
	}//end if ($serieGps>0)	
}//end if (isset($_GET["d"]) and strlen($_GET["d"])>0)
ob_end_flush();
?>