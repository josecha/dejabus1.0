//formulario
var formulario = '<form onsubmit="enviarFormLogin(); return false;"><div class="headLogin"></div><div class="bodyLogin"><div class="divUserLogin"><div><span>Usuario</span></div><div><input type="text" name="user" id="user" maxlength="10" /></div></div><div class="divPassLogin"><div><span>Contrase&ntilde;a</span></div><div><input type="password" name="pass" id="pass" maxlength="10" /></div></div><div class="divButtonLogin"><input type="submit" name="enviarLogin" value="Entrar" /><div id="imgWaitFormLogin"></div></div></div><div class="feetLogin"></div></form>';

var bienvenida = '<div class="headLogin"></div><div class="bodyLogin"><div><div class="divWelcome">Bienvenid@ <span id="userNameLogin"></span></div><div><span class="logoutLogin" onclick="logOut();">Cerrar sesi&oacute;n</span><div id="imgWaitLogin"></div></div></div></div><div class="feetLogin"></div>';

$(document).ready(function(){
	//imprimimos mensaje de espera
	$("#divLogin").html('<div class="waitLogin"><span>Cargando...</span><img src="../t3/Imagenes/ajax.gif" /></div>');
	
	//checamos sesion del usuario
	$.ajax({
		type: "POST",
		url: "checarLogin.php",
		data: "checando=1",
		dataType: "html",
		contentType: "application/x-www-form-urlencoded",
		success: function(datos){
			if (trim(datos) == "showFormLogin"){
				$("#divLogin").html(formulario);
			}else{
				$("#divLogin").html(bienvenida);
				$("#userNameLogin").html(trim(datos));
			}
		}			
	});
});

//ENVIA EL FORM DE LOGIN PARA QUE EL USER INICIE SESION
function enviarFormLogin(){
	if (isVacio($("#user").val()) || isVacio($("#pass").val())){
		msj("e", 6, "Introduce correctamente el nombre de usuario y contrase&ntilde;a");
		return false;
	}else{	
		$("#imgWaitFormLogin").html('<img src="../t3/Imagenes/ajax.gif" />');
		//enviamos datos
		$.ajax({
			type: "POST",
			url: "checarLogin.php",
			data: "checando=0&u=" + encodeURIComponent($("#user").val()) + "&p=" + MD5($("#pass").val()),
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			success: function(datos){
				if (trim(datos) == "showErrorLogin"){
					$("#imgWaitFormLogin").html('');
					msj("e", 6, "El nombre de usuario o la contrase&ntilde;a son incorrectos. Trata de nuevo.");
					return false;
				}else{
					document.location = "login.php";
				}
			}			
		});
	}
}

function logOut(){
	$("#imgWaitLogin").html('<img src="../t3/Imagenes/ajax.gif" />');
	//enviamos datos
	$.ajax({
		type: "POST",
		url: "checarLogin.php",
		data: "d=1",
		dataType: "html",
		contentType: "application/x-www-form-urlencoded",
		success: function(datos){
			document.location = "login.php";
		}			
	});
}
