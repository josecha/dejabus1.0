$(document).ready(function(){
	if (browser != 'IE'){ 		
		$('#barra2').Sortable({
			accept : 		'sortableitem',
			helperclass : 	'sorthelper',
			activeclass : 	'sortableactive',
			hoverclass : 	'sortablehover',
			opacity: 		0.8,
			fx:				200,
			axis:			'vertically',
			opacity:		0.4,
			revert:			true
		});
		
		//le asignamos estilo al cursor
		$("#barra2").css({cursor: "n-resize"}); 
		$("#barra2 div label").css({cursor: "n-resize"}); 			
	}//end if (browser != 'IE')
});

function checkBarra(checkID){
	//INTERCAMBIAMOS ARRAYS
	if (document.getElementById(checkID).checked){ //si esta checado es porque un elemento de barra1 fue precionado		
		barra2.push(barra1[checkID]);  
	}else{ //si no esta checado es porque un elemento de barra2 fue precionado
		barra2.splice(checkID,1);
	}		   
	
	//ACTUALIZAMOS BARRAS 1	
	for (a=0; a<=document.getElementById('formBarra1').elements.length-1; a++){ 
		if (document.getElementById('formBarra1').elements[a].type=="checkbox"){			
			document.getElementById('formBarra1').elements[a].checked = false;  
		}
	}  
	
	//ACTUALIZAMOS BARRAS 2
	document.getElementById('barra2').innerHTML='';
	//imprimimos checkboxes
	var aux = '';
	for (a=0; a<barra2.length; a++){ 
		aux += '<div class="sortableitem">' + barra2[a] + '</div>'; 
	}
	document.getElementById('barra2').innerHTML = aux;//'<ul>' + aux + '</ul>';
	
	//actualizamos id�s y onclick de los arrays de inputs 2
	for (a=0; a<=document.getElementById('formBarra2').elements.length-1; a++){ 
		if (document.getElementById('formBarra2').elements[a].type=="checkbox"){ 
			document.getElementById('formBarra2').elements[a].id = a;                                         
			document.getElementById('formBarra2').elements[a].checked = true;                                 
		}
	}  
	
	//asignamos capacidad de ordenamiento dinamico SOLO SI ESTAS EN FIREFOX
	if (browser != 'IE'){ 		
		$('#barra2').Sortable({
			accept : 		'sortableitem',
			helperclass : 	'sorthelper',
			activeclass : 	'sortableactive',
			hoverclass : 	'sortablehover',
			opacity: 		0.8,
			fx:				200,
			axis:			'vertically',
			opacity:		0.4,
			revert:			true
		});
		
		//le asignamos estilo al cursor
		$("#barra2").css({cursor: "n-resize"}); 
		$("#barra2 div label").css({cursor: "n-resize"}); 			
	}//end if (browser != 'IE')
}// end function	  


function enviar(){
	if (document.getElementById('formBarra2').elements.length>0){
		//actualizamos id�s de los arrays de inputs 2  (el value nos dir� su ordenamiento)
		for (a=0; a<=document.getElementById('formBarra2').elements.length-1; a++){ 
			if (document.getElementById('formBarra2').elements[a].type=="checkbox"){
				document.getElementById('formBarra2').elements[a].value = a; 
			}
		}  
		
		//creamos cadena de variables para ser enviadas
		var misVariables="Ruta=" + document.getElementById('ruta').value;
		var AllBases="";
		
		for (var i=0; i<=document.getElementById('formBarra2').elements.length-1; i++){ 
			if (document.getElementById('formBarra2').elements[i].type=="checkbox"){
				AllBases += document.getElementById('formBarra2').elements[i].name + "_" + document.getElementById('formBarra2').elements[i].value;
				if (i!=document.getElementById('formBarra2').elements.length-1){ 
					AllBases += ",";
				}
			}
		}
		
		//enviamos datos
		$.ajax({
			type: "POST",
			url: "userModRutasControlToDB.php",
			data: misVariables + "&idRutaOld=" + idRutaOld + "&AllBases=" + AllBases,
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			success: function(datos){
				datos = trim(datos);
				
				if (datos == 'OK'){
					location.href="userRutasControl.php?modok=1";				
				}else{
					msj('e', 10, 'Algo sucedi&oacute; mal. Intenta denuevo.');
				}
			}			
		});		
	}else{
		msj('e', 10, 'Es necesario seleccionar m&iacute;nimo una geocerca para esta ruta de control.');
		return false;
	}
}	   