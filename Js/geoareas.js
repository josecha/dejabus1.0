var map;															
var miIconoC = new GIcon();
var markerCentro;

function load() {
	if (GBrowserIsCompatible()) {
		map = new GMap2(document.getElementById("mapa"));
		map.setCenter(new GLatLng(23.644524, -101.99707), 6);	  						
		map.enableScrollWheelZoom();
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());				
			
		// Personalizamos el icono para Centro		
		miIconoC.image = "http://www.montecristodm.com/viajes/t3/Imagenes/centroMarker.png";
		miIconoC.iconSize = new GSize(16, 16);
		miIconoC.iconAnchor = new GPoint(8, 8);
		miIconoC.infoWindowAnchor = new GPoint(8, 8);
	}
}		

function verEnMapa(id){
	if (id > 0){	
		$.ajax({
			type: "POST",
			url: "getPuntosGA.php",
			data: "idGeoarea=" + id,
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			success: function(dat){
				dat = trim(dat);
				
				if (dat != "false"){
					map.clearOverlays();
					var allPuntos = dat.split("#");
					var punto;
					var points = [];
					
					for (var i = 0; i < allPuntos.length; i++){
						punto = allPuntos[i].split("_");
						points.push(new GLatLng(punto[0], punto[1]));
					}
					
					poly = new GPolygon(points, "#0000af", 3, .8, "#335599", .2);
					map.addOverlay(poly);
					map.setCenter(new GLatLng(punto[0], punto[1]), 13);	
				}				
			}			
		});		
	}  						
}