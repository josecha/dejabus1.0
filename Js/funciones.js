//CODIFICA UNA CADENA A MD5
var MD5 = function (string) {
    function RotateLeft(lValue, iShiftBits) {
        return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
    }

    function AddUnsigned(lX,lY) {
        var lX4,lY4,lX8,lY8,lResult;
        lX8 = (lX & 0x80000000);
        lY8 = (lY & 0x80000000);
        lX4 = (lX & 0x40000000);
        lY4 = (lY & 0x40000000);
        lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
        if (lX4 & lY4) {
            return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
        }
        if (lX4 | lY4) {
            if (lResult & 0x40000000) {
                return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
            } else {
                return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
            }
        } else {
            return (lResult ^ lX8 ^ lY8);
        }
     }

     function F(x,y,z) { return (x & y) | ((~x) & z); }
     function G(x,y,z) { return (x & z) | (y & (~z)); }
     function H(x,y,z) { return (x ^ y ^ z); }
    function I(x,y,z) { return (y ^ (x | (~z))); }

    function FF(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function GG(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function HH(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function II(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    function ConvertToWordArray(string) {
        var lWordCount;
        var lMessageLength = string.length;
        var lNumberOfWords_temp1=lMessageLength + 8;
        var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
        var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
        var lWordArray=Array(lNumberOfWords-1);
        var lBytePosition = 0;
        var lByteCount = 0;
        while ( lByteCount < lMessageLength ) {
            lWordCount = (lByteCount-(lByteCount % 4))/4;
            lBytePosition = (lByteCount % 4)*8;
            lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
            lByteCount++;
        }
        lWordCount = (lByteCount-(lByteCount % 4))/4;
        lBytePosition = (lByteCount % 4)*8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
        lWordArray[lNumberOfWords-2] = lMessageLength<<3;
        lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
        return lWordArray;
    };

    function WordToHex(lValue) {
        var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
        for (lCount = 0;lCount<=3;lCount++) {
            lByte = (lValue>>>(lCount*8)) & 255;
            WordToHexValue_temp = "0" + lByte.toString(16);
            WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
        }
        return WordToHexValue;
    };

    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    };

    var x=Array();
    var k,AA,BB,CC,DD,a,b,c,d;
    var S11=7, S12=12, S13=17, S14=22;
    var S21=5, S22=9 , S23=14, S24=20;
    var S31=4, S32=11, S33=16, S34=23;
    var S41=6, S42=10, S43=15, S44=21;

    string = Utf8Encode(string);

    x = ConvertToWordArray(string);

    a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

    for (k=0;k<x.length;k+=16) {
        AA=a; BB=b; CC=c; DD=d;
        a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
        d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
        c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
        b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
        a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
        d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
        c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
        b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
        a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
        d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
        c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
        b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
        a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
        d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
        c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
        b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
        a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
        d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
        c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
        b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
        a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
        d=GG(d,a,b,c,x[k+10],S22,0x2441453);
        c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
        b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
        a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
        d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
        c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
        b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
        a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
        d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
        c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
        b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
        a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
        d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
        c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
        b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
        a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
        d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
        c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
        b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
        a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
        d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
        c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
        b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
        a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
        d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
        c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
        b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
        a=II(a,b,c,d,x[k+0], S41,0xF4292244);
        d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
        c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
        b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
        a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
        d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
        c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
        b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
        a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
        d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
        c=II(c,d,a,b,x[k+6], S43,0xA3014314);
        b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
        a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
        d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
        c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
        b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
        a=AddUnsigned(a,AA);
        b=AddUnsigned(b,BB);
        c=AddUnsigned(c,CC);
        d=AddUnsigned(d,DD);
    }

    var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);

    return temp.toLowerCase();
}

//VERIFICA SI UNA CADENA ESTA VACIA: SI ESTA VACIA REGRESA TRUE, DE LO CONTRARIO FALSE
function isVacio (q){
	for (i = 0; i < q.length; i++){
		if (q.charAt(i) != " "){ 
			return false;
		}
	}
	return true;
}

//VERIFICA QUE UNA CADENA SEA UN CORREO CORRECTO
function isCorreo(cad){
	for (i = 0; i < cad.length; i++){
		if (cad.charAt(i) != " "){
			var filter=/^[A-Za-z][A-Za-z0-9_.]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
            if (filter.test(cad)){
				return true;
			}else{
				return false;
			}
	    }
    }
    return false
}

//VERIFICA SI UNA CADENA TIENE ESPACIOS EN BLANCO
function hayEspacios(cad){
	for (i = 0; i < cad.length; i++){
		if (cad.charAt(i) == " "){ 
			return true;
		}
	}
	return false;
}

//BORRA ESPACIOS EN BLANCO DEL INICIO Y FINAL DE UNA CADENA DADA
function trim (MiStr){
	return MiStr.replace(/^\s*|\s*$/g,"");
}

//REDONDEA UN NUMERO DE VARIOS DECIMALES A DOS DECIMALES
function round2(num){  
	var num1=parseFloat(num);
	return (Math.round(num1 * 100) / 100);
}

//REGRESA UNA CADENA DE VARIABLES DE UN FORMULARIO CON FORMATO: var1=valor1&var2=valor2
function getVarsFromForm(f){
	var i;
	var variables = "";
	var separador = "";
	
	for (i=0; i<f.elements.length; i++){
		if (f.elements[i].type != "checkbox"){
			variables += separador + f.elements[i].name + "=" + encodeURIComponent(f.elements[i].value);
			separador = "&";	
		}else if (f.elements[i].type == "checkbox" && f.elements[i].checked){ 
			variables += separador + f.elements[i].name + "=" + encodeURIComponent(f.elements[i].value);
			separador = "&";
		}
	}

	return variables;
}

//ESCONDE LOS MENSAJES DE AVISO
function tiempo(box){
	switch(box){
		case "w":
			$("#msjWarning").slideUp("slow");
			break;
		case "e":
			$("#msjError").slideUp("slow");
			break;
		case "i":
			$("#msjInfo").slideUp("slow");
			break;
	}
}

//MUESTRA MENSAJES DE AVISO
function msj(box, segundos, msj){	
	switch(box){
		case "w":
			$("#msjWarning").css({display: "none"});
			$("#msjWarning").html('<div class="msjWarning">Aviso: '+msj+'</div>');
			$("#msjWarning").slideDown("slow");
			if (stW != ""){ clearTimeout(stW); }
			stW = setTimeout('tiempo("' + box + '")', segundos * 1000);
			break;
		case "e":
			$("#msjError").css({display: "none"});
			$("#msjError").html('<div class="msjError">Error: '+msj+'</div>');			
			$("#msjError").slideDown("slow");
			if (stE != ""){ clearTimeout(stE); }
			stE = setTimeout('tiempo("' + box + '")', segundos * 1000);
			break;
		case "i":
			$("#msjInfo").css({display: "none"});
			$("#msjInfo").html('<div class="msjInfo"><div class="divForm">Info: '+msj+'</div></div>');	
			$("#msjInfo").slideDown("slow");
			if (stI != ""){ clearTimeout(stI); }
			stI = setTimeout('tiempo("' + box + '")', segundos * 1000);
			break;
	}//end switch
}

//VERIFICA QUE UN TEXTAREA NO SEA MAYOR A 1000 CARACTERES
function contador(id_text){
	if (document.getElementById(id_text).value.length>1000){
		document.getElementById(id_text).value = document.getElementById(id_text).value.substring(0,1000);
	}
}

//VERIFICA QUE UN TEXTAREA NO SEA MAYOR A N CARACTERES
function contadorN(id_text, n){
	if (document.getElementById(id_text).value.length>n){
		document.getElementById(id_text).value = document.getElementById(id_text).value.substring(0,n);
	}
}

//CONVERTIMOS A KM O METROS
function convertirM_KM(numeroV){ 
	var distanciaOk;
	if ( numeroV < 1000){ //lo ponemos en metros
		distanciaOk = round2(numeroV) + " m"; 
	}else{
		distanciaOk = round2(numeroV/1000) + " km"; 
	}
	
	return distanciaOk;
}

//CONVIERTE DE SEGUNDOS A MINUTOS Y SEGUNDOS
function SegMinTB(numeroAC){ 
	var numeroTB_ok;
	
	if (numeroAC < 60){ //solo segundos
		numeroTB_ok = numeroAC + " seg"; 
	}else{ 
		numeroTB_ok = round2(numeroAC / 60) + " min"; 
	}
	
	return numeroTB_ok;
}

//CONVIERTE DE EGUNDOS A HORAS MINUTOS Y SEGUNDOS
function SegMinHrs(numeroAC){ 
	var numeroTB_ok;
	if (numeroAC < 60){ //solo segundos
		numeroTB_ok = numeroAC + " seg"; 
	}else{ 
		if (numeroAC < 3600){ //solo minutos
		numeroTB_ok = round2(numeroAC / 60) + " min"; 
		}else{ 
			numeroTB_ok = round2(numeroAC / 3600); 
			if (numeroTB_ok==1){ 
				numeroTB_ok += " hr"; 
			}else{ 
				numeroTB_ok += " hrs"; 
			}
		}
	}
	
	return numeroTB_ok;
}

//REGRESA LOS BOTONES PARA AGREGAR
function formsAdd(tipo_usr){
	switch (tipo_usr){
		case '0': //root
			return '<div id="body_desplegable"><table cellpadding="2" cellspacing="0" border="0"><tr><td><div class="buttonOK" onClick="agregar(\'user\');"><span>Agregar Usuarios</span></div></td><td><div class="buttonOK" onClick="agregar(\'gps\');"><span>Agregar GPS</span></div></td></tr></table></div><div id="feet_desplegable"></div>';
			break;
		case '1': //admin
			return '<div id="body_desplegable"><table cellpadding="2" cellspacing="0" border="0"><tr><td><div class="buttonOK" onClick="agregar(\'userLow\');"><span>Agregar Usuarios</span></div></td><td><div class="buttonOK" onClick="agregar(\'contrato\');"><span>Agregar Contrato</span></div></td></tr></table></div><div id="feet_desplegable"></div>';
		case '2': //user
			return '<div id="body_desplegable"><table cellpadding="2" cellspacing="0" border="0"><tr><td><div class="buttonOK" onClick="agregar(\'viaje\');"><span>Agregar Viaje</span></div></td><td><div class="buttonOK" onClick="addGeocerca();"><span>Agregar Geocerca</span></div></td><td><div class="buttonOK" onClick="addGeoarea();"><span>Agregar Geo&aacute;rea</span></div></td><td><div class="buttonOK" onClick="agregar(\'areaControl\');"><span>Agregar &Aacute;rea de Ctrl.</span></div></td></tr><tr><td><div class="buttonOK" onClick="agregar(\'ruta\');"><span>Agregar Ruta</span></div></td><td><div class="buttonOK" onClick="location.href=\'userAddRutasControl.php\';"><span>Agregar Ruta de Ctrl.</span></div></td></tr></table></div><div id="feet_desplegable"></div>';
			break;
	}
}

//REGRESA LOS BOTONES PARA BUSCAR 
function formsFind(tipo_usr){
	switch (tipo_usr){
		case '0': //root
			return '<div id="body_desplegable"><table cellpadding="2" cellspacing="0" border="0"><tr><td><div class="buttonOK" onClick="buscar(\'user\');"><span>Buscar Usuarios</span></div></td><td><div class="buttonOK" onClick="buscar(\'gps\');"><span>Buscar GPS</span></div></td></tr></table></div><div id="feet_desplegable"></div>';
			break;
		case '1': //admin
			return '<div id="body_desplegable"><table cellpadding="2" cellspacing="0" border="0"><tr><td><div class="buttonOK" onClick="buscar(\'userLow\');"><span>Buscar Usuarios</span></div></td><td><div class="buttonOK" onClick="buscar(\'contrato\');"><span>Buscar Contrato</span></div></td><td><div class="buttonOK" onClick="buscar(\'contrCerrado\');"><span>Buscar Contr. Cerrado</span></div></td></tr></table></div><div id="feet_desplegable"></div>';
			break;
		case '2': //admin
			return '<div id="body_desplegable"><table cellpadding="2" cellspacing="0" border="0"><tr><td><div class="buttonOK" onClick="buscar(\'viaje\');"><span>Buscar Viajes</span></div></td><td><div class="buttonOK" onClick="buscar(\'viajeCerrado\');"><span>Buscar Viajes Cerrados</span></div></td><td><div class="buttonOK" onClick="buscar(\'geocerca\');"><span>Buscar Geocerca</span></div></td><td><div class="buttonOK" onClick="buscar(\'geoarea\');"><span>Buscar Geo&aacute;rea</span></div></td></tr><tr><td><div class="buttonOK" onClick="buscar(\'areaControl\');"><span>Buscar &Aacute;rea Ctrl.</span></div></td><td><div class="buttonOK" onClick="buscar(\'ruta\');"><span>Buscar Rutas</span></div></td><td><div class="buttonOK" onClick="buscar(\'rutaControl\');"><span>Buscar Rutas de Ctrl.</span></div></td><td><div class="buttonOK" onClick="buscar(\'reportes\');"><span>Buscar Reportes</span></div></td></tr></table></div><div id="feet_desplegable"></div>';
			break;
	}
}

//MUESTRA EL MENU DESPLEGABLE DE FORMULARIOS DE AGREGAR
function showAdd(){
	if (AddNow == 0){
		if (FindNow == 1){
			$("#desplegable").fadeOut('fast', function(){
				$("#desplegable").html(formsAdd(tipo_user)).fadeIn('fast');
				FindNow = 0;
				clearTimeout(stFind);
				stFind = "";
			});
		}else{
			$("#desplegable").css({display: "none"});
			$("#desplegable").html(formsAdd(tipo_user));
			$("#desplegable").slideDown("normal");
		}

		stAdd = setTimeout("hideMenu()", 10000);
		AddNow = 1;
	}
}

//MUESTRA EL MENU DESPLEGABLE DE FORMULARIOS DE BUSCAR
function showFind(){
	if (FindNow == 0){
		if (AddNow == 1){
			$("#desplegable").fadeOut('fast', function(){
				$("#desplegable").html(formsFind(tipo_user)).fadeIn('fast');
				AddNow = 0;
				clearTimeout(stAdd);
				stAdd = "";
			});
		}else{
			//$("#desplegable").hide("fast");
			$("#desplegable").css({display: "none"});
			$("#desplegable").html(formsFind(tipo_user));
			$("#desplegable").slideDown("normal");
		}
	
		stFind = setTimeout("hideMenu()", 10000);
		FindNow = 1;
	}
}

//HABILITA UN FORM
function habiliarForm(f){
	for (var i=0; i<=f.elements.length-1; i++){
		f.elements[i].disabled = false;
	}
}

//HABILITA EL FORMULARIO DE MODIFICAR
function habilitarMod(tabla){
	switch (tabla){
		case 'admin':
			habiliarForm(document.FormUserMod);
			
			//bloqueamos boton y damos foco
			$('#modButton').attr("disabled", true);
			document.FormUserMod.nombre.focus();
			break;
		case 'gps':
			habiliarForm(document.FormGpsMod);
			
			//bloqueamos boton y damos foco
			$('#modButton').attr("disabled", true);
			document.FormGpsMod.usr.focus();
			break;
		case 'userLow':
			habiliarForm(document.FormUserMod);
			
			//bloqueamos boton y damos foco
			$('#modButton').attr("disabled", true);
			document.FormUserMod.nombre.focus();
			break;
		case 'viajes':
			habiliarForm(document.FormViajeMod);
			
			//bloqueamos boton y damos foco
			$('#modButton').attr("disabled", true);
			document.FormViajeMod.origen.focus();
			break;
		case 'ruta':
			habiliarForm(document.FormRutaMod);
			
			//bloqueamos boton y damos foco
			$('#modButton').attr("disabled", true);
			document.FormRutaMod.nombre.focus();
			break;
	}
}

//VALIDA EL FORMULARIO DE MODIFICAR
function enviarFormMod(f, tipo){
	$("#imgWaitForm").html('<img src="../t3/Imagenes/ajax.gif" />');
	
	switch(tipo){
		case 'admin':
			if (isVacio(f.nombre.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente el nombre del usuario a modificar.");
				return false;
			}else if (isVacio(f.login.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente un login para el usuario a modificar.");
				return false;
			}else if (isVacio(f.pass.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente una contrase&ntilde;a para el usuario a modificar.");
				return false;
			}else{
				return true;
			}
			
			break;
		case 'userLow':
			if (isVacio(f.nombre.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente el nombre del usuario a modificar.");
				return false;
			}else if (isVacio(f.login.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente un login para el usuario a modificar.");
				return false;
			}else if (isVacio(f.pass.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente una contrase&ntilde;a para el usuario a modificar.");
				return false;
			}else{
				return true;
			}
			
			break;
		case 'viajes':			
			if (isVacio(f.tiempomuerto.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente un tiempo m&aacute;ximo permitido de inmovilidad.");
				return false;			
			}else{
				return true;
			}
			
			break;
		case 'areasControl':	
			if (!hayGpsSelectedInDivList()){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Selecciona m�nimo un modulo para el �rea de control.");
				return false;	
			}else if (isVacio(f.geocontrol.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Selecciona una Geo�rea para el �rea de control.");
				return false;	
			}else{
				$('#date1, #date2').attr("disabled", false);
				return true;
			}
			
			break;
		case 'ruta':			
			if (isVacio(f.nombre.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente un nombre de ruta.");
				return false;					
			}else{
				return true;
			}
			
			break;
	}
}

//MUESTRA EL FORMULARIO DE MODIFICACIONES
function modificar(tabla, id){
	var formMod='';
	
	//traemos los datos completos
	$.ajax({
		type: "POST",
		url: "modificarDatos.php",
		data: "id=" + id + "&tabla=" + tabla + "&thisUrlAll=" + encodeURIComponent(thisUrlAll),
		dataType: "html",
		contentType: "application/x-www-form-urlencoded",
		success: function(dat){
			dat = trim(dat);
			
			if (dat != "false"){
				//separamos datos
				var datos = [];
				datos = dat.split('@dt@');
				
				switch (tabla){
					case 'admin':
						formMod = '<div class="divButton"><input id="modButton" type="button" value="Modificar" onClick="habilitarMod(\''+tabla+'\');" /></div><form name="FormUserMod" method="post" action="modificarDatos.php?mod=1&tabla=' + tabla + '&thisUrlAll=' + thisUrlAll + '" onsubmit="return enviarFormMod(document.FormUserMod, \''+tabla+'\');"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Nombre*</label><br/><input class="Ftxt" type="text" name="nombre" id="nombre" disabled="disabled" value="'+trim(datos[0])+'" maxlength="30" /></div><div class="Fseleccion"><label class="Flbl">Login*</label><br/><input class="Ftxt" type="text" name="login" id="login" disabled="disabled" value="'+trim(datos[1])+'" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Contrase&ntilde;a*</label><br/><input class="Ftxt" type="password" name="pass" id="pass" disabled="disabled" value="'+trim(datos[2])+'" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Datos Adicionales</label><br/><input class="Ftxt" type="text" name="datos" id="datos" disabled="disabled" value="'+trim(datos[3])+'" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Telefono</label><br/><input class="Ftxt" type="text" name="tel" id="tel" disabled="disabled" value="'+trim(datos[4])+'" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Correo</label><br/><input class="Ftxt" type="text" name="email" id="email" disabled="disabled" value="'+trim(datos[5])+'" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 2</label><br/><input class="Ftxt" type="text" name="email2" id="email2" disabled="disabled" value="'+trim(datos[6])+'" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 3</label><br/><input class="Ftxt" type="text" name="email3" id="email3" disabled="disabled" value="'+trim(datos[7])+'" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Celular</label><br/><input type="text" class="Ftxt" name="cel" id="cel" disabled="disabled" value="'+trim(datos[8])+'" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Celular 2</label><br/><input type="text" class="Ftxt" name="cel2" id="cel2" disabled="disabled" value="'+trim(datos[9])+'" maxlength="12" /></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="modificar" disabled="disabled" value="Modificar Usuario" /><div id="imgWaitForm"></div></div></div></div><input type="hidden" value="'+trim(datos[10])+'" name="idMod" /></form>';
						break;
					case 'gps':
						formMod = '<div class="divButton"><input id="modButton" type="button" value="Modificar" onClick="habilitarMod(\''+tabla+'\');" /></div><form name="FormGpsMod" method="post" action="modificarDatos.php?mod=1&tabla=' + tabla + '&thisUrlAll=' + thisUrlAll + '"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Usuario</label><br/><select disabled="disabled" class="Ftxt" name="usr" id="usr">'+trim(datos[0])+'</select></div><div class="Fseleccion"><label class="Flbl">Descripci&oacute;n</label><br/><input class="Ftxt" type="text" name="descripcion" id="descripcion" disabled="disabled" value="'+trim(datos[2])+'" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Celular</label><br/><input class="Ftxt" type="text" name="celular" id="celular" disabled="disabled" value="'+trim(datos[3])+'" maxlength="50" /></div><div class="Fseleccion"><div class="divButton"><input type="submit" disabled="disabled" name="modificar" value="Modificar Gps" /><div id="imgWaitForm"></div></div></div></div><input type="hidden" value="'+trim(datos[1])+'" name="idMod" /></form>';						
						break;
					case 'userLow':
						formMod = '<div class="divButton"><input id="modButton" type="button" value="Modificar" onClick="habilitarMod(\''+tabla+'\');" /></div><form name="FormUserMod" method="post" action="modificarDatos.php?mod=1&tabla=' + tabla + '&thisUrlAll=' + thisUrlAll + '" onsubmit="return enviarFormMod(document.FormUserMod, \''+tabla+'\');"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Nombre*</label><br/><input class="Ftxt" type="text" name="nombre" id="nombre" disabled="disabled" value="'+trim(datos[0])+'" maxlength="30" /></div><div class="Fseleccion"><label class="Flbl">Login*</label><br/><input class="Ftxt" type="text" name="login" id="login" disabled="disabled" value="'+trim(datos[1])+'" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Contrase&ntilde;a*</label><br/><input class="Ftxt" type="password" name="pass" id="pass" disabled="disabled" value="'+trim(datos[2])+'" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Datos Adicionales</label><br/><input class="Ftxt" type="text" name="datos" id="datos" disabled="disabled" value="'+trim(datos[3])+'" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Telefono</label><br/><input class="Ftxt" type="text" name="tel" id="tel" disabled="disabled" value="'+trim(datos[4])+'" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Correo</label><br/><input class="Ftxt" type="text" name="email" id="email" disabled="disabled" value="'+trim(datos[5])+'" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 2</label><br/><input class="Ftxt" type="text" name="email2" id="email2" disabled="disabled" value="'+trim(datos[6])+'" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 3</label><br/><input class="Ftxt" type="text" name="email3" id="email3" disabled="disabled" value="'+trim(datos[7])+'" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Celular</label><br/><input type="text" class="Ftxt" name="cel" id="cel" disabled="disabled" value="'+trim(datos[8])+'" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Celular 2</label><br/><input type="text" class="Ftxt" name="cel2" id="cel2" disabled="disabled" value="'+trim(datos[9])+'" maxlength="12" /></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="modificar" disabled="disabled" value="Modificar Usuario" /><div id="imgWaitForm"></div></div></div></div><input type="hidden" value="'+trim(datos[10])+'" name="idMod" /></form>';
						break;
					case 'viajes':
						formMod = '<div class="divButton"><input id="modButton" type="button" value="Modificar" onClick="habilitarMod(\''+tabla+'\');" /></div><form name="FormViajeMod" method="post" action="modificarDatos.php?mod=1&tabla=' + tabla + '&thisUrlAll=' + thisUrlAll + '" onsubmit="return enviarFormMod(document.FormViajeMod, \''+tabla+'\');"><div class="divForm">	<div class="Fseleccion">			<label class="Flbl">Ruta de Control</label><br/>			<select class="Ftxt" name="ruta" id="ruta" disabled="disabled">'+trim(datos[6])+'</select>		</div>							<div class="Fseleccion">			<label class="Flbl">Origen</label><br/>			<input class="Ftxt" type="text" name="origen" id="origen" disabled="disabled" maxlength="30" value="'+trim(datos[0])+'" />		</div>		<div class="Fseleccion">			<label class="Flbl">Destino</label><br/>			<input class="Ftxt" disabled="disabled" type="text" name="destino" id="destino" maxlength="30" value="'+trim(datos[1])+'" />		</div>		<div class="Fseleccion">			<label class="Flbl">Mercanc&iacute;a</label><br/>		<input class="Ftxt" type="text" name="mercancia" id="mercancia" disabled="disabled" maxlength="30" value="'+trim(datos[2])+'" />		</div>		<div class="Fseleccion">			<label class="Flbl">Velocidad M&aacute;xima de Alarma</label><br/>			<select id="vel" name="vel" class="Ftxt" disabled="disabled">			' + ('<option value="30">30</option>				 <option value="35">35</option>				 <option value="40">40</option>				 <option value="45">45</option>				 <option value="50">50</option>				 <option value="55">55</option>				 <option value="60">60</option>			 <option value="65">65</option>				 <option value="70">70</option>				 <option value="75">75</option>				 <option value="80">80</option>				 <option value="85">85</option>				 <option value="90">90</option>	 				 <option value="95">95</option>				 <option value="100">100</option>				 <option value="105">105</option>				 <option value="110">110</option>				 <option value="115">115</option>				 <option value="120">120</option>				 <option value="125">125</option>				 <option value="130">130</option>				 <option value="135">135</option>				 <option value="140">140</option>				 <option value="145">145</option>				 <option value="150">150</option>				 <option value="155">155</option>				 <option value="160">160</option>').replace('<option value="'+trim(datos[3])+'">', '<option value="'+trim(datos[3])+'" selected="selected">') +'	</select><label class="Flbl">  Km/H</label>			</div>		<div class="Fseleccion">			<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>						<input class="Ftxt" disabled="disabled" type="text" style="width:60px;" value="'+trim(datos[4])+'" name="tiempomuerto" id="tiempomuerto" maxlength="4" />			<label class="Flbl">  Minutos</label>	</div>		<div class="Fseleccion">			<div class="divButton">				<input disabled="disabled" type="submit" name="enviar" value="Modificar Viaje" />		<div id="imgWaitForm"></div>			</div>		</div>	</div><input type="hidden" value="'+trim(datos[5])+'" name="idMod" /></form>';
						break;
					case 'ruta':
						formMod = '<div class="divButton"><input id="modButton" type="button" value="Modificar" onClick="habilitarMod(\''+tabla+'\');" /></div><form name="FormRutaMod" method="post" action="modificarDatos.php?mod=1&tabla=' + tabla + '&thisUrlAll=' + thisUrlAll + '" onsubmit="return enviarFormMod(document.FormRutaMod, \''+tabla+'\');"><div class="divForm">					<div class="Fseleccion">			<label class="Flbl">Nombre</label><br/>			<input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="30" value="'+trim(datos[0])+'" disabled="disabled" />		</div>		<div class="Fseleccion">			<label class="Flbl">Descripci&oacute;n</label><br/>			<input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="100" value="'+trim(datos[1])+'" disabled="disabled" />		</div>								<div class="Fseleccion"><div class="divButton"><input type="submit" disabled="disabled" name="modificar" value="Modificar Ruta" /><div id="imgWaitForm"></div></div></div></div><input type="hidden" value="'+trim(datos[2])+'" name="idMod" /></form>';						
						break;
					case 'areasControl':
						formMod = '<form name="FormAreaControlMod" method="post" action="modificarDatos.php?mod=1&tabla=' + tabla + '&thisUrlAll=' + thisUrlAll + '" onsubmit="return enviarFormMod(document.FormAreaControlMod, \''+tabla+'\');"><div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Geo&aacute;rea</label><br/>			<select class="Ftxt" name="geocontrol" id="geocontrol">'+trim(datos[6])+'</select>		</div>		<div class="Fseleccion">			<label class="Flbl">Modulo</label><br/>			<div id="gpsList" class="list">'+trim(datos[7])+'</div>		</div>		<div class="Fseleccion"><label class="Flbl">Tipo</label><br/>			<select class="Ftxt" name="tipo" id="tipo">'+('<option value="1">Entrada</option>				<option value="0">Salida</option>').replace('value="'+trim(datos[8])+'"','value="'+trim(datos[8])+'" selected="selected"')+'</select>		</div>		<div class="Fseleccion">			<label class="Flbl">Fecha Inicial</label><label class="Flbl lblH" style="padding-left:173px;">Hora</label><br/>			<input type="text" name="date1" id="date1" class="date-pick" maxlength="10" disabled="disabled" value="'+trim(datos[0])+'"/>		    <select id="selectHI" name="selectHI" class="Ftxt">'+('<option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>').replace('value="'+trim(datos[1])+'"','value="'+trim(datos[1])+'" selected="selected"')+'		    </select>		    <select id="selectMI" name="selectMI" class="Ftxt">'+('<option value="00">00</option>			 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59">59</option>').replace('value="'+trim(datos[2])+'"','value="'+trim(datos[2])+'" selected="selected"')+'</select>						<div class="corte"></div>		</div>		<div class="Fseleccion">			<label class="Flbl">Fecha Final</label><label class="Flbl lblH" style="padding-left:183px;">Hora</label><br/>			<input type="text" name="date2" id="date2" class="date-pick" maxlength="10" disabled="disabled" value="'+trim(datos[3])+'"/>		    <select id="selectHF" name="selectHF" class="Ftxt">'+('<option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>').replace('value="'+trim(datos[4])+'"','value="'+trim(datos[4])+'" selected="selected"')+'</select>		    <select id="selectMF" name="selectMF" class="Ftxt">'+('<option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59">59</option>').replace('value="'+trim(datos[5])+'"','value="'+trim(datos[5])+'" selected="selected"')+'</select>						<div class="corte"></div>		</div><div class="Fseleccion"><div class="divButton"><input type="submit" name="modificar" value="Modificar &Aacute;rea Ctrl." /><div id="imgWaitForm"></div></div></div></div><input type="hidden" value="'+trim(datos[9])+'" name="idMod" /></form>';
						break;
				}
				
				//le agregamos el resto de divs
				formMod = '<div id="head_desplegableDown"></div><div id="body_desplegableDown">' + formMod + '</div><div id="feet_desplegableDown"></div>';
				
				//mostramos form
				if (ModNow == 0){
					$("#desplegableDown").css({display: "none"}).html(formMod).fadeIn("normal");
					ModNow = 1;
					
					//si hay date picker
					if (tabla=='areasControl'){
						$('.date-pick').datePicker({startDate:'01/01/2000'});
					}
				}else{					
					$("#desplegableDown").fadeOut('fast', function(){
						$("#desplegableDown").html(formMod).fadeIn('fast');
						
						//si hay date picker
						if (tabla=='areasControl'){
							$('.date-pick').datePicker({startDate:'01/01/2000'});
						}
					});
				}	
			}
		}			
	});	
}

//ESCONDE EL MENU DE BOTONES
function hideMenu(){
	$("#desplegable").slideUp("normal");	
	FindNow = 0;
	AddNow = 0;
}

//CIERRA SESION
function logOutAll(){
	//enviamos datos
	$.ajax({
		type: "POST",
		url: "checarLogin.php",
		data: "d=1",
		dataType: "html",
		contentType: "application/x-www-form-urlencoded",
		success: function(datos){
			location.href="login.php";
		}			
	});	
}

//ACTUALIZA LAS LISTAS DE USUARIOS Y GPS�s UTILIZADAS EN LOS FORMULARIOS
function updateDatos(f){
	$.ajax({
		type: "GET",
		async: false,
		url: "updateListDatos.php",
		data: "tipo="+f,
		dataType: "html",
		contentType: "application/x-www-form-urlencoded",
		success: function(datos){
			datos = trim(datos);
			if (datos != "false"){	
				switch(f){
					case "user": 
						usersSelect = datos; 
						usersSelectChange = false;
						break;
					case "gps": 
						gpsList = datos; 
						gpsListChange = false;
						break;
					case "gpsAC": 
						gpsLibresAreasControl = datos; 
						gpsListAreaControlChange = false;
						break;	
				}
			}
		}
	});	
}

//MUESTRA EL FORMULARIO PARA AGREGAR
function agregar(f){
	var formulario;
	//asignamos formulario
	switch (f){
		case 'user':			
			formulario = '<form name="FormUser"  onsubmit="enviarFormAdd(document.FormUser, \'user\'); return false;"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Nombre*</label><br/><input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="30" /></div><div class="Fseleccion"><label class="Flbl">Login*</label><br/><input class="Ftxt" type="text" name="login" id="login" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Contrase&ntilde;a*</label><br/><input class="Ftxt" type="password" name="pass" id="pass" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Re-Contrase&ntilde;a*</label><br/><input class="Ftxt" type="password" name="repass" id="repass" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Datos Adicionales</label><br/><input class="Ftxt" type="text" name="datos" id="datos" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Telefono</label><br/><input class="Ftxt" type="text" name="tel" id="tel" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Correo</label><br/><input class="Ftxt" type="text" name="email" id="email" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 2</label><br/><input class="Ftxt" type="text" name="email2" id="email2" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 3</label><br/><input class="Ftxt" type="text" name="email3" id="email3" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Celular</label><br/><input type="text" class="Ftxt" name="cel" id="cel" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Celular 2</label><br/><input type="text" class="Ftxt" name="cel2" id="cel2" maxlength="12" /></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="enviar" value="Agregar Usuario" /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
		case 'gps':
			if (usersSelectChange){	updateDatos('user'); }
			formulario = '<form name="FormGps" onsubmit="enviarFormAdd(document.FormGps, \'gps\'); return false;">	<div class="divForm"><div class="Fseleccion">			<label class="Flbl">Serie</label><br/><input class="Ftxt" type="text" name="serie" id="serie" maxlength="11" /></div><div class="Fseleccion"><label class="Flbl">Descripci&oacute;n</label><br/><input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Usuario</label><br/><select class="Ftxt" name="usr" id="usr">' + usersSelect + '</select></div><div class="Fseleccion"><label class="Flbl">Tipo</label><br/><select class="Ftxt" name="tipo" id="tipo"><option value="1" selected="selected">Autonomo</option><option value="8">Regulador</option></select></div><div class="Fseleccion"><label class="Flbl">Celular</label><br/><input class="Ftxt" type="text" name="celular" id="celular" maxlength="10" /></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="enviar" value="Agregar Gps" /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
		case 'userLow':
			formulario = '<form name="FormUser"  onsubmit="enviarFormAdd(document.FormUser, \'userLow\'); return false;"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Nombre*</label><br/><input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="30" /></div><div class="Fseleccion"><label class="Flbl">Login*</label><br/><input class="Ftxt" type="text" name="login" id="login" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Contrase&ntilde;a*</label><br/><input class="Ftxt" type="password" name="pass" id="pass" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Re-Contrase&ntilde;a*</label><br/><input class="Ftxt" type="password" name="repass" id="repass" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Datos Adicionales</label><br/><input class="Ftxt" type="text" name="datos" id="datos" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Telefono</label><br/><input class="Ftxt" type="text" name="tel" id="tel" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Correo</label><br/><input class="Ftxt" type="text" name="email" id="email" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 2</label><br/><input class="Ftxt" type="text" name="email2" id="email2" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 3</label><br/><input class="Ftxt" type="text" name="email3" id="email3" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Celular</label><br/><input type="text" class="Ftxt" name="cel" id="cel" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Celular 2</label><br/><input type="text" class="Ftxt" name="cel2" id="cel2" maxlength="12" /></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="enviar" value="Agregar Usuario" /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
		case 'contrato':
			if (usersSelectChange){	updateDatos('user'); }
			if (gpsListChange){	updateDatos('gps'); }
			formulario = '<form name="FormContrato"  onsubmit="enviarFormAdd(document.FormContrato, \'contrato\'); return false;">	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Usuario</label><br/>			<select class="Ftxt" name="usr" id="usr">' + usersSelect + '</select>		</div>			<div class="Fseleccion">			<label class="Flbl">Numero de Contrato</label><br/>			<input class="Ftxt" type="text" name="numero" id="numero" maxlength="15" />		</div>				<div class="Fseleccion">								<label class="Flbl">Fecha de Inicio</label><label class="Flbl lblH">Hora</label><br/>					<input type="text" name="date1" id="date1" disabled="disabled" class="date-pick" maxlength="10"/>			    <select id="selectHI" name="selectHI" class="Ftxt">				 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05" selected="selected">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMI" name="selectMI" class="Ftxt">				 <option value="00" selected="selected">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>			 <option value="04">04</option>				 <option value="05">05</option>			 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>			 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>			 <option value="59">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion">			<label class="Flbl">Descripcion</label><br/>			<input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="30" />		</div><div class="Fseleccion">			<label class="Flbl">Modulos</label><br/>			<div class="gpsList">'+gpsList+'</div>		</div>	<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Agregar Contrato" />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;
		case 'viaje':
			formulario = '<form name="FormViajes"  onsubmit="enviarFormAdd(document.FormViajes, \'viaje\'); return false;">	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Modulo</label><br/>			<select class="Ftxt" name="gps" id="gps">'+gpsSelect+'</select>		</div>		<div class="Fseleccion">			<label class="Flbl">Ruta de Control</label><br/>			<select class="Ftxt" name="ruta" id="ruta">'+rutaSelectForViaje+'</select>		</div>	<div class="Fseleccion">			<label class="Flbl">Clave de Viaje</label><br/>			<input class="Ftxt" type="text" name="clave" id="clave" maxlength="30" />		</div>				<div class="Fseleccion">			<label class="Flbl">Fecha de Inicio</label><label class="Flbl" style="padding-left:158px;">Hora</label><br/>			<input type="text" name="date1" id="date1" class="date-pick" maxlength="10" disabled="disabled"/>		    <select id="selectHI" name="selectHI" class="Ftxt">				 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05" selected="selected">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMI" name="selectMI" class="Ftxt">				 <option value="00" selected="selected">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>			 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion">			<label class="Flbl">Origen</label><br/>			<input class="Ftxt" type="text" name="origen" id="origen" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Destino</label><br/>			<input class="Ftxt" type="text" name="destino" id="destino" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Mercanc&iacute;a</label><br/>		<input class="Ftxt" type="text" name="mercancia" id="mercancia" maxlength="30" /><br /></div><div class="Fseleccion"><label class="Flbl">Correo o Correos separados por comas</label><br /><input type="text" name="cCorreo" class="Ftxt" size="50" /><br /><input type="checkbox" name="envCorreo" value="si" /><label class="Flb1" >Deseas enviar Correo?</label>		</div>		<div class="Fseleccion">			<label class="Flbl">Velocidad M&aacute;xima de Alarma</label><br/>			<select id="vel" name="vel" class="Ftxt">				 <option value="30">30</option>				 <option value="35">35</option>				 <option value="40">40</option>				 <option value="45">45</option>				 <option value="50">50</option>				 <option value="55">55</option>				 <option value="60">60</option>			 <option value="65">65</option>				 <option value="70">70</option>				 <option value="75">75</option>				 <option value="80">80</option>				 <option value="85">85</option>				 <option value="90">90</option>	 				 <option value="95">95</option>				 <option value="100" selected="selected">100</option>				 <option value="105">105</option>				 <option value="110">110</option>				 <option value="115">115</option>				 <option value="120">120</option>				 <option value="125">125</option>				 <option value="130">130</option>				 <option value="135">135</option>				 <option value="140">140</option>				 <option value="145">145</option>				 <option value="150">150</option>				 <option value="155">155</option>				 <option value="160">160</option>		    </select><label class="Flbl">  Km/H</label>			</div>		<div class="Fseleccion">			<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>						<input class="Ftxt" type="text" style="width:60px;" value="10" name="tiempomuerto" id="tiempomuerto" maxlength="4" />			<label class="Flbl">  Minutos</label>	</div>		<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Agregar Viaje" />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;
		case 'ruta':			
			formulario = '<form name="FormUser"  onsubmit="enviarFormAdd(document.FormUser, \'ruta\'); return false;">	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Nombre</label><br/>			<input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Descripci&oacute;n</label><br/>			<input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="100" />		</div>				<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Agregar Ruta" />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;
		case 'areaControl':
			if (gpsListAreaControlChange){ updateDatos('gpsAC'); }
			formulario = '<form name="FormAreasControl"  onsubmit="enviarFormAdd(document.FormAreasControl, \'areasControl\'); return false;">			<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Geo&aacute;rea</label><br/>			<select class="Ftxt" name="geocontrol" id="geocontrol">'+geoareaSelect+'</select>		</div>		<div class="Fseleccion">			<label class="Flbl">Modulo</label><br/>			<div id="gpsList" class="list">'+gpsLibresAreasControl+'</div>		</div>		<div class="Fseleccion"><label class="Flbl">Tipo</label><br/>			<select class="Ftxt" name="tipo" id="tipo">				<option value="1">Entrada</option>				<option value="0">Salida</option>			</select>		</div>		<div class="Fseleccion">			<label class="Flbl">Fecha Inicial</label><label class="Flbl lblH" style="padding-left:173px;">Hora</label><br/>			<input type="text" name="date1" id="date1" class="date-pick" maxlength="10" disabled="disabled"/>		    <select id="selectHI" name="selectHI" class="Ftxt">				 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05" selected="selected">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMI" name="selectMI" class="Ftxt">				 <option value="00" selected="selected">00</option>			 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion">			<label class="Flbl">Fecha Final</label><label class="Flbl lblH" style="padding-left:183px;">Hora</label><br/>			<input type="text" name="date2" id="date2" class="date-pick" maxlength="10" disabled="disabled"/>		    <select id="selectHF" name="selectHF" class="Ftxt">				 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23" selected="selected">23</option>		    </select>		    <select id="selectMF" name="selectMF" class="Ftxt">				 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59" selected="selected">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Agregar &Aacute;rea Ctrl." />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;
	}
	
	//metemos el form en el div para desplegarlo
	formulario = '<div id="body_desplegable">' + formulario + '</div><div id="feet_desplegable"></div>';
	
	//mostramos formulario
	if (AddNow == 1){
		$("#desplegable").fadeOut('fast', function(){
			$("#desplegable").html(formulario).fadeIn('normal');
			AddNow = 0;
			clearTimeout(stAdd);
			stAdd = "";
			
			//si hay date picker
			if (f=='contrato' || f=='viaje' || f=='areaControl'){
				$('.date-pick').datePicker().val(new Date().asString()).trigger('change');
				$('.date-pick').datePicker({startDate:'01/01/2000'});
			}			
		});
	}
}

//MUESTRA EL FORMULARIO PARA BUSCAR
function buscar(f){
	var formulario;
	//asignamos formulario
	switch (f){
		case 'user':
			formulario = '<form name="FormUser" method="get" action="rootAdmin.php"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Nombre</label><br/><input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="30" /></div><div class="Fseleccion"><label class="Flbl">Login</label><br/><input class="Ftxt" type="text" name="login" id="login" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Datos Adicionales</label><br/><input class="Ftxt" type="text" name="datos" id="datos" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Telefono</label><br/><input class="Ftxt" type="text" name="tel" id="tel" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Correo</label><br/><input class="Ftxt" type="text" name="email" id="email" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 2</label><br/><input class="Ftxt" type="text" name="email2" id="email2" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 3</label><br/><input class="Ftxt" type="text" name="email3" id="email3" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Celular</label><br/><input type="text" class="Ftxt" name="cel" id="cel" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Celular 2</label><br/><input type="text" class="Ftxt" name="cel2" id="cel2" maxlength="12" /></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="enviar" value="Buscar Usuarios" /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
		case 'gps':
			formulario = '<form name="FormGps" action="rootGps.php" method="get"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Serie</label><br/><input class="Ftxt" type="text" name="serie" id="serie" maxlength="11" /></div><div class="Fseleccion"><label class="Flbl">Descripci&oacute;n</label><br/><input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Usuario</label><br/><select class="Ftxt" name="usr" id="usr"><option value="*" selected="selected">Cualquiera</option>' + usersSelect.split('selected="selected"').join('') + '</select></div><div class="Fseleccion"><label class="Flbl">Activo</label><br/><select class="Ftxt" name="activo" id="activo"><option value="*" selected="selected">Cualquiera</option><option value="1">Activo</option><option value="0">Inactivo</option></select></div><div class="Fseleccion"><label class="Flbl">Tipo</label><br/><select class="Ftxt" name="tipo" id="tipo"><option value="*" selected="selected">Cualquiera</option><option value="1">Autonomo</option><option value="8">Regulador</option></select></div><div class="Fseleccion"><label class="Flbl">Celular</label><br/><input class="Ftxt" type="text" name="celular" id="celular" maxlength="10" /></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="enviar" value="Buscar Gps" /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
		case 'userLow':
			formulario = '<form name="FormUser" method="get" action="adminUsers.php"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Nombre</label><br/><input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="30" /></div><div class="Fseleccion"><label class="Flbl">Login</label><br/><input class="Ftxt" type="text" name="login" id="login" maxlength="10" /></div><div class="Fseleccion"><label class="Flbl">Datos Adicionales</label><br/><input class="Ftxt" type="text" name="datos" id="datos" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Telefono</label><br/><input class="Ftxt" type="text" name="tel" id="tel" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Correo</label><br/><input class="Ftxt" type="text" name="email" id="email" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 2</label><br/><input class="Ftxt" type="text" name="email2" id="email2" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Correo 3</label><br/><input class="Ftxt" type="text" name="email3" id="email3" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Celular</label><br/><input type="text" class="Ftxt" name="cel" id="cel" maxlength="12" /></div><div class="Fseleccion"><label class="Flbl">Celular 2</label><br/><input type="text" class="Ftxt" name="cel2" id="cel2" maxlength="12" /></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="enviar" value="Buscar Usuarios" /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
		case 'contrato':
			formulario = '<form name="FormContrato" method="get" action="adminContratos.php" onsubmit="javascript:this.date1.value=this.date1N.value;"><div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Usuario</label><br/>			<select class="Ftxt" name="usr" id="usr"><option value="*" selected="selected">Cualquiera</option>' + usersSelect.split('selected="selected"').join('') + '</select>		</div>			<div class="Fseleccion">			<label class="Flbl">Numero de Contrato</label><br/>			<input class="Ftxt" type="text" name="numero" id="numero" maxlength="15" />		</div>				<div class="Fseleccion">								<label class="Flbl">Fecha Inicial</label><label class="Flbl lblHI">Hora</label><br/>					<input type="text" name="date1N" id="date1N" disabled="disabled" class="date-pick" maxlength="10"/>	<input type="hidden" name="date1" id="date1" />		    <select id="selectHI" name="selectHI" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMI" name="selectMI" class="Ftxt">		<option value="*" selected="selected">--</option>		 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>			 <option value="04">04</option>				 <option value="05">05</option>			 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>			 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>			 <option value="59">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion">			<label class="Flbl">Descripcion</label><br/>			<input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="30" />		</div><div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Buscar Contrato" />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;
		case 'contrCerrado':
			formulario = '<form name="FormContrato" method="get" action="adminContratosCerrados.php" onsubmit="javascript:this.date1.value=this.date1N.value;this.date2.value=this.date2N.value;"><div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Usuario</label><br/>			<select class="Ftxt" name="usr" id="usr"><option value="*" selected="selected">Cualquiera</option>' + usersSelect.split('selected="selected"').join('') + '</select>		</div>			<div class="Fseleccion">			<label class="Flbl">Numero de Contrato</label><br/>			<input class="Ftxt" type="text" name="numero" id="numero" maxlength="15" />		</div>				<div class="Fseleccion">								<label class="Flbl">Fecha Inicial</label><label class="Flbl lblHI">Hora</label><br/>					<input type="text" name="date1N" id="date1N" disabled="disabled" class="date-pick" maxlength="10"/>	<input type="hidden" name="date1" id="date1" />		    <select id="selectHI" name="selectHI" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMI" name="selectMI" class="Ftxt">		<option value="*" selected="selected">--</option>		 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>			 <option value="04">04</option>				 <option value="05">05</option>			 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>			 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>			 <option value="59">59</option>		    </select>						<div class="corte"></div></div>		<div class="Fseleccion">											<label class="Flbl">Fecha Final</label><label class="Flbl lblHF">Hora</label><br/>					<input type="text" name="date2N" id="date2N" disabled="disabled" class="date-pick" maxlength="10"/>	<input type="hidden" name="date2" id="date2" />		    <select id="selectHF" name="selectHF" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMF" name="selectMF" class="Ftxt">		<option value="*" selected="selected">--</option>		 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>			 <option value="04">04</option>				 <option value="05">05</option>			 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>			 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>			 <option value="59">59</option>		    </select>										<div class="corte"></div>					</div>					<div class="Fseleccion">			<label class="Flbl">Descripcion</label><br/>			<input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="30" />		</div><div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Buscar Contr. Cerr." />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;	
		case 'viaje':
			formulario = '<form name="FormViajes" method="get" action="userViajes.php" onsubmit="javascript:this.date1.value=this.date1N.value;">	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Modulo</label><br/>			<select class="Ftxt" name="gps" id="gps"><option value="*" selected="selected">Cualquiera</option>'+gpsSelectInUse+'</select>		</div>		<div class="Fseleccion">			<label class="Flbl">Ruta de Control</label><br/>			<select class="Ftxt" name="ruta" id="ruta">'+rutaSelectForFindViaje+'</select>		</div>	<div class="Fseleccion">			<label class="Flbl">Clave de Viaje</label><br/>			<input class="Ftxt" type="text" name="clave" id="clave" maxlength="30" />		</div>				<div class="Fseleccion">			<label class="Flbl">Fecha de Inicio</label><label class="Flbl" style="padding-left:158px;">Hora</label><br/>			<input type="text" name="date1N" id="date1N" class="date-pick" maxlength="10" disabled="disabled"/>	<input type="hidden" name="date1" id="date1" />		    <select id="selectHI" name="selectHI" class="Ftxt">				 <option value="*" selected="selected">--</option><option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMI" name="selectMI" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>			 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion">			<label class="Flbl">Origen</label><br/>			<input class="Ftxt" type="text" name="origen" id="origen" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Destino</label><br/>			<input class="Ftxt" type="text" name="destino" id="destino" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Mercanc&iacute;a</label><br/>		<input class="Ftxt" type="text" name="mercancia" id="mercancia" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Velocidad M&aacute;xima de Alarma</label><br/>			<select id="vel" name="vel" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="30">30</option>				 <option value="35">35</option>				 <option value="40">40</option>				 <option value="45">45</option>				 <option value="50">50</option>				 <option value="55">55</option>				 <option value="60">60</option>			 <option value="65">65</option>				 <option value="70">70</option>				 <option value="75">75</option>				 <option value="80">80</option>				 <option value="85">85</option>				 <option value="90">90</option>	 				 <option value="95">95</option>				 <option value="100">100</option>				 <option value="105">105</option>				 <option value="110">110</option>				 <option value="115">115</option>				 <option value="120">120</option>				 <option value="125">125</option>				 <option value="130">130</option>				 <option value="135">135</option>				 <option value="140">140</option>				 <option value="145">145</option>				 <option value="150">150</option>				 <option value="155">155</option>				 <option value="160">160</option>		    </select><label class="Flbl">  Km/H</label>			</div>		<div class="Fseleccion">			<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>						<input class="Ftxt" type="text" style="width:60px;" name="tiempomuerto" id="tiempomuerto" maxlength="4" />			<label class="Flbl">  Minutos</label>	</div>		<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Buscar Viaje" />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;
		case 'viajeCerrado':
			formulario = '<form name="FormViajes" method="get" action="userHistorial.php" onsubmit="javascript:this.date1.value=this.date1N.value; this.date2.value=this.date2N.value;">	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Modulo</label><br/>			<select class="Ftxt" name="gps" id="gps"><option value="*" selected="selected">Cualquiera</option>'+gpsSelectClosed+'</select>		</div>		<div class="Fseleccion">			<label class="Flbl">Ruta de Control</label><br/>			<select class="Ftxt" name="ruta" id="ruta">'+rutaSelectForFindHistorial+'</select>		</div>	<div class="Fseleccion">			<label class="Flbl">Clave de Viaje</label><br/>			<input class="Ftxt" type="text" name="clave" id="clave" maxlength="30" />		</div>				<div class="Fseleccion">			<label class="Flbl">Fecha Inicial</label><label class="Flbl" style="padding-left:158px;">Hora</label><br/>			<input type="text" name="date1N" id="date1N" class="date-pick" maxlength="10" disabled="disabled"/>	<input type="hidden" name="date1" id="date1" />		    <select id="selectHI" name="selectHI" class="Ftxt">				 <option value="*" selected="selected">--</option><option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMI" name="selectMI" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>			 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion">			<label class="Flbl">Fecha Final</label><label class="Flbl" style="padding-left:158px;">Hora</label><br/>			<input type="text" name="date2N" id="date2N" class="date-pick" maxlength="10" disabled="disabled"/>	<input type="hidden" name="date2" id="date2" />		    <select id="selectHF" name="selectHF" class="Ftxt">				 <option value="*" selected="selected">--</option><option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMF" name="selectMF" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>			 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion"><label class="Flbl">Origen</label><br/>			<input class="Ftxt" type="text" name="origen" id="origen" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Destino</label><br/>			<input class="Ftxt" type="text" name="destino" id="destino" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Mercanc&iacute;a</label><br/>		<input class="Ftxt" type="text" name="mercancia" id="mercancia" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Velocidad M&aacute;xima de Alarma</label><br/>			<select id="vel" name="vel" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="30">30</option>				 <option value="35">35</option>				 <option value="40">40</option>				 <option value="45">45</option>				 <option value="50">50</option>				 <option value="55">55</option>				 <option value="60">60</option>			 <option value="65">65</option>				 <option value="70">70</option>				 <option value="75">75</option>				 <option value="80">80</option>				 <option value="85">85</option>				 <option value="90">90</option>	 				 <option value="95">95</option>				 <option value="100">100</option>				 <option value="105">105</option>				 <option value="110">110</option>				 <option value="115">115</option>				 <option value="120">120</option>				 <option value="125">125</option>				 <option value="130">130</option>				 <option value="135">135</option>				 <option value="140">140</option>				 <option value="145">145</option>				 <option value="150">150</option>				 <option value="155">155</option>				 <option value="160">160</option>		    </select><label class="Flbl">  Km/H</label>			</div>		<div class="Fseleccion">			<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>						<input class="Ftxt" type="text" style="width:60px;" name="tiempomuerto" id="tiempomuerto" maxlength="4" />			<label class="Flbl">  Minutos</label>	</div>		<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Buscar Viaje" />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;
		case 'geocerca':
			formulario = '<form name="FormUser" method="get" action="userGeocercas.php"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Nombre</label><br/><input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="20" /></div><div class="Fseleccion"><label class="Flbl">Descripci&oacute;n</label><br/><input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="50" /></div><div class="Fseleccion"><label class="Flbl">Tipo</label><br/><select class="Ftxt" name="tipo" id="tipo"><option selected="selected" value="*">Cualquiera</option>'+listGeocercas+'</select></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="enviar" value="Buscar Geocerca" /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
		case 'geoarea':
			formulario = '<form name="FormUser" method="get" action="userGeoareas.php"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Nombre</label><br/><input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="20" /></div><div class="Fseleccion"><label class="Flbl">Descripci&oacute;n</label><br/><input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="50" /></div><div class="divButton"><input type="submit" name="enviar" value="Buscar Geo&aacute;rea" /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
		case 'areaControl':
			formulario = '<form name="FormUser" method="get" action="userAreasControl.php" onsubmit = "javascript:this.date1.value=this.date1N.value; this.date2.value=this.date2N.value;"><div class="divForm"><div class="Fseleccion">			<label class="Flbl">Geo&aacute;rea</label><br/>			<select class="Ftxt" name="geocontrol" id="geocontrol"><option value="*" selected="selected">Cualquiera</option>'+geoareaSelect+'</select>		</div>		<div class="Fseleccion">			<label class="Flbl">Modulo</label><br/>			<select class="Ftxt" name="gps" id="gps"><option value="*" selected="selected">Cualquiera</option>'+gpsAreasControl+'</select>		</div>		<div class="Fseleccion"><label class="Flbl">Tipo</label><br/>			<select class="Ftxt" name="tipo" id="tipo">	<option value="*" selected="selected">Cualquiera</option>			<option value="1">Entrada</option>				<option value="0">Salida</option>			</select>		</div>		<div class="Fseleccion">			<label class="Flbl">Fecha Inicial</label><label class="Flbl lblH" style="padding-left:173px;">Hora</label><br/>			<input type="text" name="date1N" id="date1N" class="date-pick" maxlength="10" disabled="disabled"/>		<input type="hidden" name="date1" id="date1" />    <select id="selectHI" name="selectHI" class="Ftxt">		<option value="*" selected="selected">--</option>		 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMI" name="selectMI" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="00">00</option>			 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion">			<label class="Flbl">Fecha Final</label><label class="Flbl lblH" style="padding-left:183px;">Hora</label><br/>			<input type="text" name="date2N" id="date2N" class="date-pick" maxlength="10" disabled="disabled"/>	<input type="hidden" name="date2" id="date2" />	    <select id="selectHF" name="selectHF" class="Ftxt">		<option value="*" selected="selected">--</option>		 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option>				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>		    </select>		    <select id="selectMF" name="selectMF" class="Ftxt">	<option value="*" selected="selected">--</option>			 <option value="00">00</option>				 <option value="01">01</option>				 <option value="02">02</option>				 <option value="03">03</option>				 <option value="04">04</option>				 <option value="05">05</option>				 <option value="06">06</option>				 <option value="07">07</option>				 <option value="08">08</option>				 <option value="09">09</option>				 <option value="10">10</option>				 <option value="11">11</option>				 <option value="12">12</option>	 				 <option value="13">13</option>				 <option value="14">14</option>				 <option value="15">15</option>				 <option value="16">16</option				 <option value="17">17</option>				 <option value="18">18</option>				 <option value="19">19</option>				 <option value="20">20</option>				 <option value="21">21</option>				 <option value="22">22</option>				 <option value="23">23</option>				 <option value="24">24</option>				 <option value="25">25</option>				 <option value="26">26</option>				 <option value="27">27</option>				 <option value="28">28</option>				 <option value="29">29</option>				 <option value="30">30</option>				 <option value="31">31</option>				 <option value="32">32</option>				 <option value="33">33</option>				 <option value="34">34</option>				 <option value="35">35</option>				 <option value="36">36</option>				 <option value="37">37</option>				 <option value="38">38</option>				 <option value="39">39</option>				 <option value="40">40</option>				 <option value="41">41</option>				 <option value="42">42</option>				 <option value="43">43</option>				 <option value="44">44</option>				 <option value="45">45</option>				 <option value="46">46</option>				 <option value="47">47</option>				 <option value="48">48</option>				 <option value="49">49</option>				 <option value="50">50</option>				 <option value="51">51</option>				 <option value="52">52</option>				 <option value="53">53</option>				 <option value="54">54</option>				 <option value="55">55</option>				 <option value="56">56</option>				 <option value="57">57</option>				 <option value="58">58</option>				 <option value="59">59</option>		    </select>						<div class="corte"></div>		</div>		<div class="Fseleccion"><div class="divButton"><input type="submit" name="enviar" value="Buscar &Aacute;rea Ctrl." /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
		case 'ruta':
			formulario = '<form name="FormContrato" method="get" action="userRutas.php"><div class="divForm"><div class="Fseleccion">			<label class="Flbl">Nombre</label><br/>			<input class="Ftxt" type="text" name="nombre" id="nombre" maxlength="30" />		</div>		<div class="Fseleccion">			<label class="Flbl">Descripci&oacute;n</label><br/>			<input class="Ftxt" type="text" name="descripcion" id="descripcion" maxlength="100" />		</div>	<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Buscar Ruta" />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;
		case 'rutaControl':
			formulario = '<form name="FormContrato" method="get" action="userRutasControl.php"><div class="divForm"><div class="Fseleccion">			<label class="Flbl">Ruta</label><br/>	<select class="Ftxt" name="idRuta" id="idRuta">' + rutasSelect + '</select>	</div>	<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Buscar Ruta Ctrl." />				<input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" />				<div id="imgWaitForm"></div>			</div>		</div>	</div></form>';
			break;
		case 'reportes':
			formulario = '<form name="FormReporte" onSubmit="verReporteStatusFiltro(document.FormReporte.serie.value); return false;"><div class="divForm"><div class="Fseleccion"><label class="Flbl">Modulo</label><br/><select class="Ftxt" name="serie" id="serie"><option value="*" selected="selected">Cualquiera</option>' + gpsSelectForReporte + '</select></div><div class="Fseleccion"><div class="divButton"><input type="submit" name="enviar" value="Buscar Reporte" /><input type="button" name="cancelar" value="Cancelar" onClick="hideMenu(); return false;" /><div id="imgWaitForm"></div></div></div></div></form>';
			break;
	}
	
	//metemos el form en el div para desplegarlo
	formulario = '<div id="body_desplegable">' + formulario + '</div><div id="feet_desplegable"></div>';
	
	//mostramos formulario
	if (FindNow == 1){
		$("#desplegable").fadeOut('fast', function(){
			$("#desplegable").html(formulario).fadeIn('normal');
			FindNow = 0;
			clearTimeout(stFind);
			stFind = "";
			
			//si hay date picker
			if (f=='contrato' || f=='contrCerrado' || f=='viaje' || f=='viajeCerrado' || f=='areaControl'){
				$('.date-pick').datePicker({startDate:'01/01/2000'});
			}	
		});
	}
}

//VERIFICA SI YA HAY SELECCIONADO UN GPS DE LA LISTA (regresa true si hay un gps seleccionado, de lo contrario regresa false)
function gpsSelected(f){
	for (var i=0; i<f.elements.length; i++){
		if (f.elements[i].type == "checkbox" && f.elements[i].checked){ 
			return true;
		}
	}
	
	return false;
}

//VERIFICA SI SE SELECCION� MINIMO UN GPS DE LA DivList 
function hayGpsSelectedInDivList(){
	var resultado = false;
	
	$(".gpsDivList").each(function(){
		if ($(this).attr("checked")){
			resultado = true;
		}
	});
	
	return resultado;
}

//REGRESA UNA CADENA DE VARIABLES DEL FORMULARIO PARA AGREGAR AREAS DE CONTROL CON FORMATO: var1=valor1&var2=valor2
function getVarsFromFormAddAreaControl(f){
	var i;
	var variables = "";
	var separador = "";
	var gpsSeparador = "";
	var gpsCheckList = "";
	
	for (i=0; i<f.elements.length; i++){
		if (f.elements[i].type != "checkbox"){
			variables += separador + f.elements[i].name + "=" + encodeURIComponent(f.elements[i].value);
			separador = "&";	
		}else if (f.elements[i].type == "checkbox" && f.elements[i].checked){ 
			gpsCheckList += gpsSeparador + encodeURIComponent(f.elements[i].value);
			gpsSeparador = "_";
		}
	}

	return variables + "&gpsCheckList=" + gpsCheckList;
}

//VALIDA Y ENVIA LOS FORMULARIOS PARA AGREGAR
function enviarFormAdd(f, formulario){
	var valido = 0;
	var variables = "";
	var pagina = "addDatos.php";
	$("#imgWaitForm").html('<img src="../t3/Imagenes/ajax.gif" />');
	//validamos formulario
	switch (formulario){
		case 'user':
			if (isVacio(f.nombre.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente el nombre del nuevo usuario.");
				return false;
			}else if (isVacio(f.login.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente un login para el nuevo usuario.");
				return false;
			}else if (isVacio(f.pass.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente una contrase&ntilde;a para el nuevo usuario.");
				return false;
			}else if (f.repass.value != f.pass.value){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "La contrase&ntilde;a y la re-contrase&ntilde;a deben ser iguales. Verificalas");
				return false;
			}else{
				variables = "llego=si&tipoForm=user&" + getVarsFromForm(f);
				valido = 1;
			}
			
			break;
		case 'gps':
			if (isVacio(f.serie.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente una serie para el Gps.");
				return false;
			}else if (isVacio(f.descripcion.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente una breve descripci&oacute;n para el Gps.");
				return false;
			}else{
				variables = "llego=si&tipoForm=gps&" + getVarsFromForm(f);
				valido = 1;
			}
			
			break;
		case 'userLow':
			if (isVacio(f.nombre.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente el nombre del nuevo usuario.");
				return false;
			}else if (isVacio(f.login.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente un login para el nuevo usuario.");
				return false;
			}else if (isVacio(f.pass.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente una contrase&ntilde;a para el nuevo usuario.");
				return false;
			}else if (f.repass.value != f.pass.value){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "La contrase&ntilde;a y la re-contrase&ntilde;a deben ser iguales. Verificalas");
				return false;
			}else{
				variables = "llego=si&tipoForm=userLow&" + getVarsFromForm(f);
				valido = 1;
			}
			
			break;
		case 'contrato':
			if (isVacio(f.numero.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente un numero de contrato.");
				return false;
			}else if (!gpsSelected(f)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Selecciona m&iacute;nimo un modulo de la lista.");
				return false;			
			}else{
				variables = "llego=si&tipoForm=contrato&" + getVarsFromForm(f);
				valido = 1;
			}
			
			break;
		case 'viaje':
			if (isVacio(f.clave.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente una clave de viaje.");
				return false;				
			}else if (isVacio(f.tiempomuerto.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente un tiempo m&aacute;ximo permitido de inmovilidad.");
				return false;			
			}else{
				variables = "llego=si&tipoForm=viaje&" + getVarsFromForm(f);
				valido = 1;
			}
			
			break;
		case 'areasControl':
			if (!hayGpsSelectedInDivList()){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Selecciona m�nimo un modulo para el �rea de control.");
				return false;	
			}else if (isVacio(f.geocontrol.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Selecciona una Geo�rea para el �rea de control.");
				return false;	
			}else{
				variables = "llego=si&tipoForm=areasControl&" + getVarsFromFormAddAreaControl(f);
				valido = 1;
			}
			
			break;
		case 'ruta':
			if (isVacio(f.nombre.value)){
				$("#imgWaitForm").html('');	
				document.location = "#top";
				msj("e", 7, "Introduce correctamente un nombre de ruta.");
				return false;
			}else{
				variables = "llego=si&tipoForm=ruta&" + getVarsFromForm(f);
				valido = 1;
			}
			
			break;
	}
	
	if (valido == 1){
		//enviamos formulario
		$.ajax({
   			type: "POST",
   			url: pagina,
  			data: variables,
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
   			success: function(datos){
				datos = trim(datos);
				$("#imgWaitForm").html('');	
				
				switch (formulario){
					case 'user':
						if (datos != "false"){
							if (datos == "true"){
								//activamos bandera de modificaci�n
								usersSelectChange = true;
								
								document.location = "#top";
								msj("i", 7, "Usuario agregado satisfactoriamente!");
								$("#desplegable").slideUp("fast");
							}else{
								document.location = "#top";
								msj("e", 7, "El login del nuevo usuario ya existe. Trata con otro");
							}
						}
						break;
					case 'gps':
						if (datos != "false"){
							if (datos == "true"){
								document.location = "#top";
								msj("i", 7, "Gps agregado satisfactoriamente!");
								$("#desplegable").slideUp("fast");
							}else{
								document.location = "#top";
								msj("e", 7, "El Gps ya existe. Trata con otro");
							}
						}
						break;
					case 'userLow':
						if (datos != "false"){
							if (datos == "true"){
								//activamos bandera de modificaci�n
								usersSelectChange = true;
								
								document.location = "#top";
								msj("i", 7, "Usuario agregado satisfactoriamente!");
								$("#desplegable").slideUp("fast");
							}else{
								document.location = "#top";
								msj("e", 7, "El login del nuevo usuario ya existe. Trata con otro");
							}
						}
						break;
					case 'contrato':
						if (datos != "false"){
							if (datos == "true"){
								//activamos bandera de modificaci�n
								gpsListChange = true;
								
								document.location = "#top";
								msj("i", 7, "Contrato agregado satisfactoriamente!");
								$("#desplegable").slideUp("fast");
							}else{
								document.location = "#top";
								msj("e", 7, "Este contrato ya existe. Trata con otro");
							}
						}
						break;
					case 'viaje':
						if (datos != "false"){
							if (datos == "true"){
								document.location = "#top";
								msj("i", 7, "Viaje agregado satisfactoriamente!");
								$("#desplegable").slideUp("fast");
							}else{
								document.location = "#top";
								msj("e", 7, "Este viaje ya existe. Trata con otro");
							}
						}
						break;
					case 'ruta':
						if (datos != "false"){
							if (datos == "true"){
								document.location = "#top";
								msj("i", 7, "Ruta agregada satisfactoriamente!");
								$("#desplegable").slideUp("fast");
							}else{
								document.location = "#top";
								msj("e", 7, "Esta ruta ya existe. Trata con otro nombre");
							}
						}
						break;
					case 'areasControl':
						if (datos != "false"){
							if (datos == "true"){
								//activamos bandera de modificaci�n
								gpsListAreaControlChange = true;
								
								document.location = "#top";
								msj("i", 7, "&Aacute;rea de control agregada satisfactoriamente!");
								$("#desplegable").slideUp("fast");
							}
						}
						break;
				}
  			}			
		});	
	}else{
		$("#imgWaitForm").html('');	
	}
}

//DESPLIEGA EL HISTORIAL DE GPS
function verHistorial(id){
	//nos traemos la info del historial
	$.ajax({
		type: "GET",
		url: "historialGps.php",
		data: "id=" + id,
		dataType: "html",
		contentType: "application/x-www-form-urlencoded",
		success: function(dat){
			dat = trim(dat);
			
			if (dat != "false" && dat.length>0){
				//parseamos datos
				var filas = dat.split('@fi@');
				var a,b;
				var trs = '';
				var tds = '';
				
				for (a=0; a<filas.length; a++){
					trs += '<tr class="filHist filNew'+id+'"><td class="'+((a==filas.length-1 || filas.length==1)?'imgPoints':'imgPointsLong')+'"></td>';//inicio de fila
					
					//sacamos las columnas de esta fila
					filas[a] = trim(filas[a]);
					tds = filas[a].split('@co@');
					
					//metemos columnas a esta fila
					for (b=0; b<tds.length; b++){
						trs += '<td '+((b==tds.length-1)?'colspan="5"':'')+' >'+trim(tds[b])+'</td>';
					}
					
					trs += '</tr>'; //fin de fila
				}
				
				$(".rowGps_"+id).after(trs);
				$(".tdImgMasMin_"+id).html('<img class="btnHistorial" id="btnHistorial_'+id+'" src="Imagenes/minus.gif" title="Ocultar Historial" onclick="hideHistorial('+id+');" />');
			}
		}			
	});		
}

//ELIMINA LAS FILAS DEL HISTORIAL DE UN GPS
function hideHistorial(id){
	$(".filNew"+id).remove();	
	$(".tdImgMasMin_"+id).html('<img class="btnHistorial" id="btnHistorial_'+id+'" src="Imagenes/plus.gif" title="Ver Historial" onclick="verHistorial('+id+');" />');
}

//QUITA UN GPS DEL CONTRATO
function quitarGps(idContrato, idGPS){
	if (confirm("Esta seguro que desea quitar un GPS de este contrato?")){
		//quitamos gps		
		$.ajax({
			type: "GET",
			url: "quitarGps.php",
			data: "idGPS=" + idGPS + "&idContrato=" + idContrato,
			dataType: "html",
			contentType: "application/x-www-form-urlencoded",
			success: function(dat){
				dat = trim(dat);
				if (dat != "false"){
					if (dat != "NO"){
						//verificamos si este gps es el ultimo en la lista de este contrato
						var classes = $("#"+idGPS).attr("class");
						var isLast = true;
						
						if (classes.indexOf("isLast") == -1){
							isLast = false;
						}
						
						//si este gps era el ultimo, entonces le asignamos al gps anterior de la lista la imagen de imgPoints
						if (isLast){
							$("#"+idGPS).prev().children(".imgPointsLong").removeClass("imgPointsLong").addClass("imgPoints");
						}
						
						//quitamos este gps
						$("#"+idGPS).remove(); 	
						
						//activamos bandera de modificaci�n
						gpsListChange = true;
					}else{
						document.location = "#top";
						msj("e", 7, "No puedes tener un contrato sin GPS alguno.");
					}
				}				
			}			
		});		
	}
}

function obtCorreos(idViaje){
	$.ajax({
		type: "POST",
		dataType: "html",
		url: "obtCorreos.php",
		data: "idviaje="+idViaje,
		success: function(dat){
				document.getElementById("comentEnd").value=":"+dat;
			}					
		});
	}

//MUESTRA UN CUADRO DE TEXTO PARA AGREGAR COMENTARIOS AL CONTRATO A CERRAR
function verTxtContratoEnd(idContrato){
	document.location = "#top";
	msj("i", 300, 'Escribe alg&uacute;n comentario sobre el contrato (Opcional).<br/><form method="post" name="cerrarContrato" action="cerrarContrato.php" onsubmit="javascript: return confirm(\'Estas seguro que deseas cerrar el contrato?\');"><textarea rows="3" name="comentEnd" id="comentEnd" class="Ftxt"></textarea><input type="submit" name="cerrar" class="boton" id="cerrar" value="Cerrar Contrato '+idContrato+'" /><input type="hidden" name="idContrato" id="idContrato" value="'+idContrato+'" /><input type="hidden" name="urlRedir" id="urlRedir" value="'+thisUrlAll+'" /></form>');
}

//MUESTRA UN CUADRO DE TEXTO PARA AGREGAR COMENTARIOS AL VIAJE A CERRAR
function verTxtViajeEnd(idViaje){
	obtCorreos(idViaje);
	document.location = "#top";
	msj("i", 300, '<style type="text/css">a.dp-choose-date {float: left;width: 28px;height: 28px;padding: 0;margin: 5px 3px 0;display: block;text-indent: -2000px;overflow: hidden;	background: url(Imagenes/calendario.png) no-repeat; }a.dp-choose-date.dp-disabled {background-position: 0 -20px;cursor: default;}input.dp-applied {width: 140px;float: left;}</style><script type="text/javascript">$(function(){$(".date-pick1").datePicker().val(new Date().asString()).trigger("change");});</script><form method="post" name="cerrarViaje" action="cerrarViaje.php" onsubmit="javascript: return confirm(\'Estas seguro que deseas cerrar el viaje?\');">Escribe alg&uacute;n comentario sobre el viaje (Opcional).<br/><textarea rows="3" name="comentEnd" id="comentEnd" class="Ftxt"></textarea><label>Fecha de termino</label><br /><br /><input type="text" name="date1" id="date1" class="date-pick1" maxlength="10" disabled="disabled"/><br /><br /><br /><input type="submit" name="cerrar" class="boton" id="cerrar" value="Cerrar Viaje '+idViaje+'" /><input type="hidden" name="idViaje" id="idViaje" value="'+idViaje+'" /><input type="hidden" name="urlRedir" id="urlRedir" value="'+thisUrlAll+'" /></form>');
}

//VALIDA EL FORM DE DATOS DEL USER
function validarFormUser(f){
	if (isVacio(f.nombre.value)){
		document.location = "#top";
		msj("e", 7, "Introduce correctamente un nombre.");
		return false;
	}else if (isVacio(f.pass.value)){
		document.location = "#top";
		msj("e", 7, "Introduce correctamente una contrase&ntilde;a.");
		return false;
	}else if (f.repass.value != f.pass.value){
		document.location = "#top";
		msj("e", 7, "La contrase&ntilde;a y la re-contrase&ntilde;a deben ser iguales. Verificalas");
		return false;
	}else if (isVacio(f.tel.value) && isVacio(f.cel.value) && isVacio(f.cel2.value)){
		document.location = "#top";
		msj("e", 7, "Introduce m&iacute;nimo un telefono o n&uacute;mero de celular.");
		return false;
	}else if (!isCorreo(f.email.value) && !isCorreo(f.email2.value) && !isCorreo(f.email3.value)){
		document.location = "#top";
		msj("e", 7, "Introduce m&iacute;nimo un correo electronico.");
		return false;
	}else{
		return true;
	}			
}

//MUESTRA UN CUADRO DE TEXTO PARA AGREGAR VELOCIDAD Y TIEMPO MUERTO AL DESCARGAR LA RUTA ACTUAL
function verFormVelTm(idviaje){
	formVmTm = '<div id="head_desplegableDown"></div><div id="body_desplegableDown"><form name="FormViajeVmTm" action="XMLRutaActualViaje.php" method="get">	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Velocidad M&aacute;xima de Alarma (km/h)</label><br/>			<input class="Ftxt" type="text" name="vm" id="vm" maxlength="3" style="width:60px;" />	<label class="Flbl">  Km/H</label>		</div>		<div class="Fseleccion">			<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>			<input class="Ftxt" style="width:60px;" type="text" name="tm" id="tm" maxlength="4" />	<label class="Flbl">  Minutos</label>	</div>		<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Descargar Ruta" />	</div>		</div>	</div><input type="hidden" name="idviaje" id="idviaje" value="'+idviaje+'" /></form></div><div id="feet_desplegableDown"></div>';
		
	//mostramos form
	if (ModNow == 0){
		$("#desplegableDown").css({display: "none"}).html(formVmTm).fadeIn("normal");
		ModNow = 1;
	}else{					
		$("#desplegableDown").fadeOut('fast', function(){
			$("#desplegableDown").html(formVmTm).fadeIn('fast');
		});
	}
}

//MUESTRA UN CUADRO DE TEXTO PARA AGREGAR VELOCIDAD Y TIEMPO MUERTO AL DESCARGAR LA RUTA
function verFormVelTmForClosed(idviaje){
	formVmTm = '<div id="head_desplegableDown"></div><div id="body_desplegableDown"><form name="FormViajeVmTm" action="XMLRutaCerradaViaje.php" method="get">	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Velocidad M&aacute;xima de Alarma (km/h)</label><br/>			<input class="Ftxt" type="text" name="vm" id="vm" maxlength="3" style="width:60px;" />	<label class="Flbl">  Km/H</label>		</div>		<div class="Fseleccion">			<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>			<input class="Ftxt" style="width:60px;" type="text" name="tm" id="tm" maxlength="4" />	<label class="Flbl">  Minutos</label>	</div>		<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Descargar Ruta" />	</div>		</div>	</div><input type="hidden" name="idviaje" id="idviaje" value="'+idviaje+'" /></form></div><div id="feet_desplegableDown"></div>';
		
	//mostramos form
	if (ModNow == 0){
		$("#desplegableDown").css({display: "none"}).html(formVmTm).fadeIn("normal");
		ModNow = 1;
	}else{					
		$("#desplegableDown").fadeOut('fast', function(){
			$("#desplegableDown").html(formVmTm).fadeIn('fast');
		});
	}
}

//ABRE UNA VENTANA PARA MOSTRAR EL MAPA Y EN �L, EL PUNTO ACTUAL
function verPosActInGM(idViaje){
	window.open('posicionActualGMV2.php?idViaje='+idViaje,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=no, resizable=no, width=950, height=630');
}

//MUESTRA UN CUADRO DE TEXTO PARA AGREGAR VELOCIDAD Y TIEMPO MUERTO AL DESCARGAR LA RUTA ACTUAL
function verFormVelTm(idviaje){
	formVmTm = '<div id="head_desplegableDown"></div><div id="body_desplegableDown"><form action="XMLRutaActualViaje.php" method="get">	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Velocidad M&aacute;xima de Alarma (km/h)</label><br/>			<input class="Ftxt" type="text" name="vm" id="vm" maxlength="3" style="width:60px;" />	<label class="Flbl">  Km/H</label>		</div>		<div class="Fseleccion">			<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>			<input class="Ftxt" style="width:60px;" type="text" name="tm" id="tm" maxlength="4" />	<label class="Flbl">  Minutos</label>	</div>		<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Descargar Ruta" />	</div>		</div>	</div><input type="hidden" name="idviaje" id="idviaje" value="'+idviaje+'" /></form></div><div id="feet_desplegableDown"></div>';
		
	//mostramos form
	if (ModNow == 0){
		$("#desplegableDown").css({display: "none"}).html(formVmTm).fadeIn("normal");
		ModNow = 1;
	}else{					
		$("#desplegableDown").fadeOut('fast', function(){
			$("#desplegableDown").html(formVmTm).fadeIn('fast');
		});
	}
}

//ABRE UNA VENTANA PARA MOSTRAR EL MAPA Y EN �L, LA RUTA ACTUAL
function verRutaActInGM(idViaje, vm, tm){
	window.open('rutaActualGMV2.php?idViaje='+idViaje+'&vm='+vm+'&tm='+tm,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=no, resizable=no, width=950, height=630');
}

//MUESTRA UN CUADRO DE TEXTO PARA AGREGAR VELOCIDAD Y TIEMPO MUERTO AL VER LA RUTA ACTUAL
function verFormVelTmToGM(idviaje){
	formVmTm = '<div id="head_desplegableDown"></div><div id="body_desplegableDown"><form name="formRaInGm" onsubmit="verRutaActInGM('+idviaje+', document.formRaInGm.vm.value, document.formRaInGm.tm.value); return false;" >	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Velocidad M&aacute;xima de Alarma (km/h)</label><br/>			<input class="Ftxt" type="text" name="vm" id="vm" maxlength="3" style="width:60px;" />	<label class="Flbl">  Km/H</label>		</div>		<div class="Fseleccion">			<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>			<input class="Ftxt" style="width:60px;" type="text" name="tm" id="tm" maxlength="4" />	<label class="Flbl">  Minutos</label>	</div>		<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Ver Ruta" />	</div>		</div>	</div><input type="hidden" name="idviaje" id="idviaje" value="'+idviaje+'" /></form></div><div id="feet_desplegableDown"></div>';
		
	//mostramos form
	if (ModNow == 0){
		$("#desplegableDown").css({display: "none"}).html(formVmTm).fadeIn("normal");
		ModNow = 1;
	}else{					
		$("#desplegableDown").fadeOut('fast', function(){
			$("#desplegableDown").html(formVmTm).fadeIn('fast');
		});
	}
}

//ABRE UNA VENTANA PARA MOSTRAR EL MAPA Y EN �L, LA RUTA
function verRutaActInGMForClosed(idViaje, vm, tm){
	window.open('rutaCerradaGMV2.php?idViaje='+idViaje+'&vm='+vm+'&tm='+tm,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=no, resizable=no, width=950, height=630');
}

//MUESTRA UN CUADRO DE TEXTO PARA AGREGAR VELOCIDAD Y TIEMPO MUERTO AL VER LA RUTA CERRADA
function verFormVelTmToGMForClosed(idviaje){
	formVmTm = '<div id="head_desplegableDown"></div><div id="body_desplegableDown"><form name="formRaInGm" onsubmit="verRutaActInGMForClosed('+idviaje+', document.formRaInGm.vm.value, document.formRaInGm.tm.value); return false;" >	<div class="divForm">		<div class="Fseleccion">			<label class="Flbl">Velocidad M&aacute;xima de Alarma (km/h)</label><br/>			<input class="Ftxt" type="text" name="vm" id="vm" maxlength="3" style="width:60px;" />	<label class="Flbl">  Km/H</label>		</div>		<div class="Fseleccion">			<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>			<input class="Ftxt" style="width:60px;" type="text" name="tm" id="tm" maxlength="4" />	<label class="Flbl">  Minutos</label>	</div>		<div class="Fseleccion">			<div class="divButton">				<input type="submit" name="enviar" value="Ver Ruta" />	</div>		</div>	</div><input type="hidden" name="idviaje" id="idviaje" value="'+idviaje+'" /></form></div><div id="feet_desplegableDown"></div>';
		
	//mostramos form
	if (ModNow == 0){
		$("#desplegableDown").css({display: "none"}).html(formVmTm).fadeIn("normal");
		ModNow = 1;
	}else{					
		$("#desplegableDown").fadeOut('fast', function(){
			$("#desplegableDown").html(formVmTm).fadeIn('fast');
		});
	}
}

//MUESTRA EL PUNTO ACTUAL/RUTA EN GOOGLE MAPS - DESCARGA EL PUNTO ACTUAL/RUTA EN GOOGLE EARTH --> PARA EL USER TIPO 2 EN LOGIN.PHP
function verPuntos(tipo){
	var datos = 'd='+$("#gps").val()+'&fi='+encodeURIComponent(trim($("#date1").val()))+'&hi='+encodeURIComponent(trim($("#selectHI").val()))+'&mi='+encodeURIComponent(trim($("#selectMI").val()))+'&ff='+encodeURIComponent(trim($("#date2").val()))+'&hf='+encodeURIComponent(trim($("#selectHF").val()))+'&mf='+encodeURIComponent(trim($("#selectMF").val()))+'&vm='+encodeURIComponent(trim($("#vm").val()))+'&tm='+encodeURIComponent(trim($("#tm").val()));
	
	switch(tipo){
		case 'pointInGM':
			window.open('posicionActualGMforUserV2.php?'+datos,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=950, height=680');
			break;
		case 'rutaInGM':
			window.open('rutaActualGMForUserV2.php?'+datos,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=no, resizable=no, width=950, height=630');
			break;
		case 'pointInGE':
			document.location = 'XMLPosActualViajeForUser.php?'+datos;
			break;
		case 'rutaInGE':
			document.location = 'XMLRutashowAddActualViajeForUser.php?'+datos;
			break;
		case '*':
			window.open('posicionActualAllGMforV2.php?vm='+encodeURIComponent(trim($("#vm").val())),'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=950, height=680');
			break;
		case '*GE':
			document.location = 'XMLPosActualAllForUser.php?vm='+encodeURIComponent(trim($("#vm").val()));
			break;
		case 'turn':
			window.open('turnOnOff.php?'+datos,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=950, height=680');
			break;
		
	}
}

//MUESTRA UNA GRAFICA DE TEMPERATURA/VELOCIDAD PARA EL USER TIPO 2 EN LOGIN.PHP
function verGrafica(tipo){
	var datos = 'd='+$("#gps").val()+'&fi='+encodeURIComponent(trim($("#date1").val()))+'&hi='+encodeURIComponent(trim($("#selectHI").val()))+'&mi='+encodeURIComponent(trim($("#selectMI").val()))+'&ff='+encodeURIComponent(trim($("#date2").val()))+'&hf='+encodeURIComponent(trim($("#selectHF").val()))+'&mf='+encodeURIComponent(trim($("#selectMF").val()))+'&vm='+encodeURIComponent(trim($("#vm").val()))+'&tm='+encodeURIComponent(trim($("#tm").val()))+'&it='+encodeURIComponent(trim($("#selectIT").val()));
	
	switch(tipo){
		case 'temperatura':
			window.open('graficaTemperatura2.php?'+datos,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=950, height=680');
			break;
		case 'velocidad':
			window.open('graficaVelocidad2.php?'+datos,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=no, resizable=no, width=950, height=630');
			break;
		case 'inactividad':
			window.open('phpToAsp.php?'+datos,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=1200, height=400');
		break;
case 'recorrido':
			window.open('reporteRecorrido.php?'+datos,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=1200, height=400');
			break;
	}
}

//INSERTA EL NOMBRE DE LA CALLE DE UN PUNTO EN UN INPUT HIDDEN
function getCalle1(latlng, con1){
	//obtengo direcciones de este marcador				
	
   
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          
          if ($("#calle_"+con1).length == 1){
                $("#calle_"+con1).val(results[1].formatted_address);
                
            }else{
                $("#calles").append('<input type="hidden" id="calle_'+con1+'" value="'+results[1].formatted_address+'" />');
                      
              }

        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });

   /* geocoder.getLocations(new GLatLng(latitud, longitud), function(response) { 		     								
		if (!(!response || response.Status.code != 200)) {
			if ($("#calle_"+con1).length == 1){
				$("#calle_"+con1).val(response.Placemark[0].address);
			}else{
				$("#calles").append('<input type="hidden" id="calle_'+con1+'" value="'+response.Placemark[0].address+'" />');
			}
		}
	});*/
}


function getCalle(latitud, longitud, con1){
    //obtengo direcciones de este marcador              
    

    geocoder.getLocations(new GLatLng(latitud, longitud), function(response) {                                            
        if (!(!response || response.Status.code != 200)) {
            if ($("#calle_"+con1).length == 1){
                $("#calle_"+con1).val(response.Placemark[0].address);
                
            }else{
                $("#calles").append('<input type="hidden" id="calle_'+con1+'" value="'+response.Placemark[0].address+'" />');
               
            }
        }
    });
}


//ABRE UNA VENTANA PARA AGREGAR GEOCERCAS
function addGeocerca(){
	window.open('AgregarGeocerca.php','_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=no, resizable=no, width=1050, height=630');
}

//ABRE UNA VENTANA PARA AGREGAR GEOAREAS
function addGeoarea(){
	
    window.open('viewAddGeoArea.php','_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=no, resizable=no, width=1050, height=630');
}

//ABRE UNA VENTANA PARA MODIFICAR GEOCERCAS
function modBaseInGm(idBase){
	window.open('modGeocercas.php?b='+idBase,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=no, resizable=no, width=1050, height=630');
}

//ABRE UNA VENTANA PARA MODIFICAR GEOAREAS
function modGAInGm(idGA){
	window.open('modGeoareas.php?ga='+idGA,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=no, resizable=no, width=1050, height=630');
}

//ABRE UNA VENTANA PARA MOSTRAR EL REPORTE DE STATUS
function verReporteStatus(){
	window.open('userReportes.php','_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=1200, height=550');
}

//ABRE UNA VENTANA PARA MOSTRAR EL REPORTE DE STATUS CON FILTRO
function verReporteStatusFiltro(serie){
	window.open('userReportes.php?serie='+serie,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=1200, height=550');
}

//ABRE UNA VENTANA PARA MOSTRAR EL REPORTE DE VIAJES CERRADOS
function verReporteViajesCerrados(idViaje){
	window.open('userReportesViajesCerrados.php?i='+idViaje,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=1000, height=550');
}

//ABRE UNA VENTANA PARA MOSTRAR EL REPORTE DE AREAS DE CONTROL
function verReporteAreaControl(idAC){
	window.open('userReportesAreaControl.php?i='+idAC,'_blank','top=100, left=50, toolbar=no, location=no, status=no, directories=no, menubar=no, scrollbars=yes, resizable=no, width=800, height=550');
}


function comandosOnOff(option)
{
var modulo=$("#gps").val()

$.ajax({
	type: "POST",
	url: "comandos.php",
	data: "comando="+option+"&modulo="+modulo,
	dataType: "html",
	contentType: "application/x-www-form-urlencoded",
	success: alert("Comando enviado")
});
}
