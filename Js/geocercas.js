var map;															
var miIconoC = new GIcon();
var markerCentro;

function load() {
	if (GBrowserIsCompatible()) {
		map = new GMap2(document.getElementById("mapa"));
		map.setCenter(new GLatLng(23.644524, -101.99707), 6);	  						
		map.enableScrollWheelZoom();
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());				
			
		// Personalizamos el icono para Centro		
		miIconoC.image = "http://www.montecristodm.com/viajes/t3/Imagenes/centroMarker.png";
		miIconoC.iconSize = new GSize(16, 16);
		miIconoC.iconAnchor = new GPoint(8, 8);
		miIconoC.infoWindowAnchor = new GPoint(8, 8);
	}
}		

function verEnMapa(lat, lng, nombre){
	if (markerCentro){
		map.removeOverlay(markerCentro);
	}
	
	markerCentro = new GMarker(new GLatLng(lat, lng), { icon:miIconoC, title:nombre });					
	map.addOverlay(markerCentro);	
	map.setCenter(new GLatLng(lat, lng), 15);	  						
}