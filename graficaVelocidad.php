<?php
  require '../phplot/phplot.php';
  include 'conexion.php';
  
  function mes($mes)
   {
	switch($mes)
	 {
	  case 01: $mes='Enero'; break;
	  case 02: $mes='Febrero'; break;
	  case 03: $mes='Marzo'; break;
	  case 04: $mes='Abril'; break;
	  case 05: $mes='Mayo'; break;
	  case 06: $mes='Junio'; break;
	  case 07: $mes='Julio'; break;
	  case 08: $mes='Agosto'; break;
	  case 09: $mes='Septiembre'; break;
	  case 10: $mes='Octubre'; break;
	  case 11: $mes='Noviembre'; break;
	  case 12: $mes='Diciembre'; break; 
	 }
	 return $mes;
   }//end function
   
   function twodig($num) //convierte esto ---- 1 a esto ----01
    {
	 if (strlen($num)==1)
	  { $num='0'.$num; }
	 return $num; 
	}//end function
  
  //creamos recordset
  $rs = New COM("ADODB.Recordset");
  if (isset($_GET["fi"])) $fi=$_GET["fi"]; else $fi=0;
  if (isset($_GET["ff"])) $ff=$_GET["ff"]; else $ff=0;
  if (isset($_GET["xt"])) $xt=$_GET["xt"]; else $xt=755; // HRG**:Cual equipo
  if (isset($_GET["vm"])) $vm=$_GET["vm"]; else $vm=100; // HRG**:Cual velocidad

  //recojemos fecha inicial y final
  if (($fi==0) || ($ff==0)) //si algunas de las fechas para rango no llego entonces hacemos consulta libre e imprimimos una grafika
    { 
	 $rs->Open("SELECT * FROM vistadatos WHERE gps=$xt ORDER BY fecharecv ASC", $conn); 
	 //recuperamos datos
	 $datos=fetch_assoc($rs); 
 
     //obtenemos titulo
	 $f=date_parse($datos[0]['fecharecv']);
	 $f2=date_parse($datos[count($datos)-1]['fecharecv']);	 
	 $titulo='Velocidades de la ruta '.$datos[0]['gps'].' entre las '.twodig($f['hour']).':'.twodig($f['minute']).':'.twodig($f['second']).' horas del '.$f['day'].' de '.mes($f['month']).' de '.$f['year']."\ny las ".twodig($f2['hour']).':'.twodig($f2['minute']).':'.twodig($f2['second']).' horas del '.$f2['day'].' de '.mes($f2['month']).' de '.$f2['year'];
	 
     /*echo "<pre>";
     print_r($datos);
     echo "</pre>";*/

     //IMPRIMIMOS GRAFICA 
     $grafica = new PHPlot(1000,430);
     $grafica->SetPlotType('lines');
     $grafica->SetDataType('data-data');
     $grafica->SetTitle($titulo);
     $grafica->SetXLabel('Horas');
     $grafica->SetDataColors(array('DarkGreen','red')); // HRG**:Prueba de color

	 $grafica->SetYLabel('Velocidad Km/h');
	 //limpiamos vector de datos a graficar
	 $data=array();
     //sacamos velocidades de cada registro
     for ($a=0; $a<=count($datos)-1; $a++) { $data[$a] = array('', $a, $datos[$a]['velocidad'],(int) $vm); }// HRG**:Prueba limite vel...
     $grafica->SetDataValues($data);
     $grafica->DrawGraph();
	}
  else  //(si envio un rango de dias)  separamos por dias e imprimimos grafika por dia
    { 
	 $rs->Open("SELECT * FROM vistadatos WHERE gps=$xt AND (fecharecv BETWEEN '$fi' AND '$ff') ORDER BY fecharecv ASC", $conn);  
	 //recuperamos datos
	 $datos=fetch_assoc($rs); 
	 
	 //dividimos fecha en h,m,s,m,d,a
	 $f=date_parse($datos[0]['fecharecv']);
	 
	 //obtenemos titulo
	 $f2=date_parse($datos[count($datos)-1]['fecharecv']);
	 $titulo='Velocidades de la ruta '.$datos[0]['gps'].' entre las '.twodig($f['hour']).':'.twodig($f['minute']).':'.twodig($f['second']).' horas del '.$f['day'].' de '.mes($f['month']).' de '.$f['year']."\ny las ".twodig($f2['hour']).':'.twodig($f2['minute']).':'.twodig($f2['second']).' horas del '.$f2['day'].' de '.mes($f2['month']).' de '.$f2['year'];

	 //DIVIDIMOS EL ARRAY EN DIAS
	 $b=0;  //contador de cada dia
	 $c=0;  //contador de registros por dia

	 $f=date_parse($datos[0]['fecharecv']);
	 $d=$f["day"]; //$d contiene el primer dia
  
	 for ($a=0; $a<=count($datos)-1; $a++)
   	  {
       $f=date_parse($datos[$a]['fecharecv']);  //obtenemos array con la fecha
       if ($d==$f["day"]) //si este nuevo dia es igual al viejo 
	    { 
	     //guardamos en el array de un mismo dia
	     $dias[$b][$c]=$datos[$a];
	     ++$c;
	    }
	   else 
	    { 
	     $d=$f["day"]; 
	     $c=0;
	     ++$b;
		 //guardamos en el array de un mismo dia
	     $dias[$b][$c]=$datos[$a];
		 ++$c;
	    }
   	  }
   
      /*echo "<pre>";
      print_r($dias);
      echo "</pre>";*/

      //IMPRIMIMOS GRAFICA POR DIA 
      $y=40; //contador del eje Y inferior derecho
	  #$nom_file=md5("grafica".rand(0,1000)).'.png'; //creamos nombre temporal
	  //$nom_file="grafika.png";
      $grafica = new PHPlot(1000,(count($dias)*430));//,$nom_file); //'Y' se tiene ke modifikar
      //$grafica->SetIsInline(true); //desactivamos el envio de las cabezeras http
	  $grafica->SetPrintImage(0);  //deshabilitamos que se imprima por default una sola grafica
      $grafica->SetDataColors(array('DarkGreen','red')); // HRG**:Prueba de color
      $grafica->SetPlotType('lines');
	  $grafica->SetDataType('data-data');
      $grafica->SetTitle($titulo);
      $grafica->SetXLabel('Minutos');
	  
	  #$grafica->SetOutputFile($nom_file); //asignamos nombre de archivo de salida
      
      for ($b=0; $b<=count($dias)-1; $b++)  //ciclo ke pasa por cada dia
       {   
	    //limpiamos vector de datos a graficar
        //sacamos velocidades de cada registro
		$data=array();
        for ($a=0; $a<=count($dias[$b])-1; $a++) 
		 { 
		  //dividimos fecha en h,m,s,m,d,a
		  $f=date_parse($dias[$b][$a]['fecharecv']);

		  //convertimos la fecha en minutos
		  $Fs = (mktime($f['hour'],$f['minute'],$f['second'],$f['month'],$f['day'],$f['year']) - mktime(00,00,00,$f['month'],$f['day'],$f['year']));
		  $Fs = round($Fs/60, 2);
		  
		  //guardamos vector de datos de un dia
		  $data[$a] = array('', $Fs, $dias[$b][$a]['velocidad'],(int) $vm); 
		 } 
		
		//obtenemos el Xmin y Xmax para eskalar la grafica.. RECORDAR: los valores de X son los minutos trancurridos desde los valores GET fi y ff
		//dividimos fecha en h,m,s,m,d,a
		$fXmin=date_parse($dias[$b][0]['fecharecv']);
		$fXmax=date_parse($dias[$b][count($dias[$b])-1]['fecharecv']);
		
		//convertimos la fecha en minutos
		//minimo
		$Xmin = (mktime($fXmin['hour'],$fXmin['minute'],$fXmin['second'],$fXmin['month'],$fXmin['day'],$fXmin['year']) - mktime(00,00,00,$fXmin['month'],$fXmin['day'],$fXmin['year']));
		$Xmin = round($Xmin/60, 2);
		//maximo
		$Xmax = (mktime($fXmax['hour'],$fXmax['minute'],$fXmax['second'],$fXmax['month'],$fXmax['day'],$fXmax['year']) - mktime(00,00,00,$fXmax['month'],$fXmax['day'],$fXmax['year']));
		$Xmax = round($Xmax/60, 2);

	    //graficamos
	    $grafica->SetPlotAreaPixels(90, $y, 950, $y+350);	
	    $y+=390;
        $grafica->SetDataValues($data);
		$grafica->SetPlotAreaWorld($Xmin, 0, $Xmax, NULL);  //recalculamos eskalas
	    $grafica->SetYTitle("Dia ".($b+1)."\nVelocidad Km/h");
        $grafica->DrawGraph();
       } 
      $grafica->PrintImage(); //imprimimos imagen
	  
	}//end if
?>