<?php
ob_start();
header("Content-type: text/html; charset=iso-8859-1");

if (isset($_GET["d"]) and strlen($_GET["d"])>0){
	include("conexion.php");
	include("funciones.php");

	//el archivo solo tiene vigencia de 5 dias. Si pasan los 5 dias, el archivo deja de mostrar informaci�n
	//obtenemos tiempo de descarga del archivo
	$archivoHabilitado = false;
	$fechaInicialArchivo = 0;
	$tiempoVigencia = 5 * 24 * 60 * 60;
	if (isset($_GET["n"]) and strlen($_GET["n"])>0){
		$fechaInicialArchivo = desencrNum(trim($_GET["n"]));

		if ((time() - $fechaInicialArchivo) < $tiempoVigencia){
			$archivoHabilitado = true;
		}
	}

	if ($archivoHabilitado){
		$rs = New COM("ADODB.Recordset");
		$serieGps = (int)myDencr(urldecode(decodeSpecialChars(trim($_GET["d"]))));

		if ($serieGps>0){
			//recivimos velocidad
			$vm = 100;
			if (isset($_GET["vel"]) and strlen(trim($_GET["vel"]))>0){
				$vm = (int)trim($_GET["vel"]);
			}

			//recivimos id del usuario
			$idUser = 0;
			if (isset($_GET["iduser"]) and strlen($_GET["iduser"])>0){  //usuario
				$idUser = trim((int)$_GET["iduser"]);
			}

			//verificamos que el Gps corresponda con el usuario
			$rs->Open("	SELECT GPs.id
						FROM (GPs
						INNER JOIN gpscontratos
						ON (gpscontratos.gps = GPs.id))
							INNER JOIN contratos
							ON (contratos.id = gpscontratos.contrato)
							WHERE
								contratos.activo = 1 AND
								contratos.usr = ".$idUser." AND
								GPs.serie = ".$serieGps, $conn);
			$datos = fetch_assoc($rs);
			$rs->Close();

			//si es un usuario valido
			if (count($datos)>0){
				//SACAMOS EL PUNTO MAS ACTUAL
				$rs->Open("	SELECT
								GPs.descripcion,
								datosghe.id,
								datosghe.fecharecv,
						CONVERT(CHAR(19),dateadd(hour,DATEDIFF(Hour,getUTCDate(),getDate()),datosghe.fechasend),120) AS fechasend,
								datosghe.lat,
								datosghe.lon,
								datosghe.tambiental,
								datosghe.alarma,
								datosghe.velocidad,
								datosghe.bateria,
								datosghe.aperturas,
								datosghe.rumbo,
								datosghe.valido
							FROM datosghe
							INNER JOIN GPs
							ON (GPs.serie = datosghe.gps)
							WHERE
								datosghe.gps = ".$serieGps." AND
								datosghe.id = (
									SELECT MAX(datosghe.id)
									FROM datosghe
									WHERE
										datosghe.gps = ".$serieGps."
								)", $conn);
				$datos = fetch_assoc($rs);
				$rs->Close();
				$alarmaApertura = "0";
				$aperturasNew = 0;

				if (isset($datos[0]["id"])){
					$maxId = $datos[0]["id"];

					//SACAMOS APERTURAS DEL PENULTIMO PUNTO MAS ACTUAL
					$rs->Open("	SELECT TOP 1
									datosghe.aperturas
								FROM datosghe
								INNER JOIN GPs
								ON (GPs.serie = datosghe.gps)
								WHERE
									datosghe.gps = ".$serieGps." AND
									datosghe.id < ".$maxId." ORDER BY datosghe.id DESC", $conn);
					$datosPen = fetch_assoc($rs);
					$rs->Close();

					$aperturasNew = $datos[0]["aperturas"];
					$aperturasOld = $datosPen[0]["aperturas"];

					if ($aperturasNew != $aperturasOld){
						$alarmaApertura = "1";
					}
				}

				//si la velocidad es 0 sacamos el tiempo total que lleva parado
				$tiempoParado = 0;
				if ($datos[0]["velocidad"] == "0"){
					$rs->Open("	SELECT
									CONVERT(CHAR(19),fechasend,120) AS fechasend
								FROM datosghe
								WHERE id = (
									SELECT
										MAX(id)
									FROM datosghe
									WHERE velocidad > 3 AND gps = ".$serieGps."
								)", $conn);
					$datosTiempoParado = fetch_assoc($rs);
					$rs->Close();

					//calculo los minutos transcurridos en velocidad 0
					$lastFechaMoving = date_parse(trim($datosTiempoParado[0]['fechasend']));
					$thisFechaStop = date_parse(trim($datos[0]['fechasend']));

					$tiempoParado =
							mktime(
								$thisFechaStop['hour'],
								$thisFechaStop['minute'],
								$thisFechaStop['second'],
								$thisFechaStop['month'],
								$thisFechaStop['day'],
								$thisFechaStop['year']
							) -
							mktime(
								$lastFechaMoving['hour'],
								$lastFechaMoving['minute'],
								$lastFechaMoving['second'],
								$lastFechaMoving['month'],
								$lastFechaMoving['day'],
								$lastFechaMoving['year']
							);

					if ((float)$tiempoParado<0){
						$tiempoParado = 0;
					}else{
						$tiempoParado = segMinHrs($tiempoParado);
					}
				}

				//modificamos el formato de las coordenadas
				$lat = $datos[0]["lat"];
				switch(substr($lat,strlen($lat)-1,1)){
					case 'S': (double)$lat*=-1; break;
					case 'N': (double)$lat*=1; break;
				}

				$lon = $datos[0]["lon"];
				switch(substr($lon,strlen($lon)-1,1)){
					case 'W': (double)$lon*=-1; break;
					case 'E': (double)$lon*=1; break;
				}

				//modificamos formato del rumbo
				switch($datos[0]["rumbo"]){
					case 0: $elrumbo="Norte"; break;
					case 1: $elrumbo="Noreste"; break;
					case 2: $elrumbo="Este"; break;
					case 3: $elrumbo="Sureste"; break;
					case 4: $elrumbo="Sur"; break;
					case 5: $elrumbo="Suroeste"; break;
					case 6: $elrumbo="Oeste"; break;
					case 7: $elrumbo="Noroeste"; break;
					case 8: $elrumbo="Norte"; break;
				}

				//activo
				$activo = ($datos[0]["valido"] == "0")?"X":"Ok";

				//procesamos icono
				if ($datos[0]["alarma"] == 1 or $alarmaApertura == "1" or $datos[0]["alarma"] == 21 or $alarmaApertura == "21" or $datos[0]["alarma"] == 50 or $alarmaApertura == "50"){
					//fibra abierta
					$icono = 'http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png';
					$msj = 'Abierta'; //fibra abierta
				}else{
					//fibra cerrada
					$msj = 'No abierta'; //fibra abierta

					if ($datos[0]["velocidad"] > $vm){ //mostramos alarma de velocidad
						$icono = 'http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png';
					}else{
						if ($datos[0]["velocidad"] == 0){ //si esta parado
							$OffOn="Off";
						}else{
							$OffOn="On";
						}

						$icono = "http://www.montecristodm.com/viajes/t3/Imagenes/".$elrumbo.$OffOn.".png";
					}
				}

				echo trim('<?xml version="1.0" encoding="iso-8859-1"?>
						<kml xmlns="http://earth.google.com/kml/2.0">
							<Folder>
								<Placemark>
									<description>
										<![CDATA[
											<div><b>Fecha: </b>'.$datos[0]["fecharecv"].'</div>
											<div><b>Fecha UTC: </b>'.$datos[0]["fechasend"].'</div>
											<div><b>GPS: </b>'.$activo.'</div>
											<div><b>Fibra: </b>'.$msj.'</div>
											<div><b>Aperturas: </b>'.$aperturasNew.'</div>
											<div><b>Direcci&oacute;n: </b>'.$elrumbo.'</div>
											<div><b>Velocidad: </b>'.$datos[0]["velocidad"].' Km/h - '.round(($datos[0]["velocidad"] / 1.60935), 2).' Mi/h</div>
											<div><b>Temperatura: </b>'.$datos[0]["tambiental"].' &deg;C - '.round((($datos[0]["tambiental"] * 1.8) + 32), 2).' &deg;F</div>
											<div><b>Bater&iacute;a: </b>'.$datos[0]["bateria"].'</div>
											'.(($tiempoParado!=0)?('<div><b>Tiempo parado: </b>'.$tiempoParado.'</div>'):'').'
										]]>
									</description>
									<name>'.$datos[0]["descripcion"].'</name>
									<Style>
										<IconStyle>
											<scale>0.5</scale>
											<Icon>
												<href>'.$icono.'</href>
											</Icon>
										</IconStyle>
										<LabelStyle>
											<color>ff0055ff</color>
											<scale>0.7</scale>
										</LabelStyle>
									</Style>
									<Point>
										<coordinates>
											'.$lon.','.$lat.',0
										</coordinates>
									</Point>
								</Placemark>
							</Folder>
						</kml>');

			}else{
				//el viaje esta cerrado o el user no corresponde al viaje
				echo trim('<?xml version="1.0" encoding="iso-8859-1"?>
						<kml xmlns="http://earth.google.com/kml/2.0">
							<Folder>
								<Placemark>
									<name>Viaje Cerrado</name>
									<Style>
										<IconStyle>
											<scale>0.5</scale>
											<Icon>
												<href>http://www.montecristodm.com/viajes/t3/Imagenes/IconStop.png</href>
											</Icon>
										</IconStyle>
										<LabelStyle>
											<color>ff0055ff</color>
											<scale>0.7</scale>
										</LabelStyle>
									</Style>
									<Point>
										<coordinates>
											-99.146118,19.466592,0
										</coordinates>
									</Point>
								</Placemark>
							</Folder>
						</kml>');
			}//end	if (count($datos)>0)
		}//end if ($serieGps>0)
	}else{
		//el archivo solo tenia vigencia de 5 dias
		echo trim('<?xml version="1.0" encoding="iso-8859-1"?>
				<kml xmlns="http://earth.google.com/kml/2.0">
					<Folder>
						<Placemark>
							<name>Este Archivo est� vencido. Descarga uno nuevo</name>
							<Style>
								<IconStyle>
									<scale>0.5</scale>
									<Icon>
										<href>http://www.montecristodm.com/viajes/t3/Imagenes/IconStop.png</href>
									</Icon>
								</IconStyle>
								<LabelStyle>
									<color>ff0055ff</color>
									<scale>0.7</scale>
								</LabelStyle>
							</Style>
							<Point>
								<coordinates>
									-99.146118,19.466592,0
								</coordinates>
							</Point>
						</Placemark>
					</Folder>
				</kml>');
	}
}//end if (isset($_GET["d"]) and strlen($_GET["d"])>0)
ob_end_flush();
?>
