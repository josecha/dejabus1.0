<?php
if (isset($_GET["lat"]{0})){
	//recibimos lat y lon
	$lat = (double)$_GET["lat"];
	$lon = (double)$_GET["lon"];
	$num = (int)$_GET["num"];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAi8mj96kyL8tl4LpmHvQWdBRf664DUNFDIWxt3GQfe0EgEFDFzxQQXv2MdIUYESSeDE37VxjSiN4nbQ" type="text/javascript"></script>	
	<script type="text/javascript" src="Js/jquery.js"></script>	
	<script type="text/javascript" language="javascript" charset="iso-8859-1">
		var geocoder;	
		var map;
		var lat = <?=$lat; ?>;
		var lon = <?=$lon; ?>;
		var num = <?=$num; ?>;
		
		function getThisCalle(){
			geocoder.getLocations(new GLatLng(lat, lon), function(response) { 		   				
				if (response.Placemark){	
					$("#calle").html(response.Placemark[0].address);
				}			
			});
		}
		
		function startCalle(){
			//obtenemos calle
			getThisCalle()
			
			//si aun no se ha cargado la calle, intentamos nuevamente cargarla
			if ($.trim($("#calle").html()).length == 0){
				setTimeout("startCalle()", 5000);	
			}else{
				$.ajax({
					type: "POST",
					url: "saveCalle.php",
					data: "num="+num+"&calle="+encodeURIComponent($("#calle").html()),
					dataType: "html",
					contentType: "application/x-www-form-urlencoded"
				});	
			}
		}
		
		function load() {
			if (GBrowserIsCompatible()) {  	 	
				map = new GMap2(document.getElementById("map"));
				map.setCenter(new GLatLng(lat, lon), 6);	  										
				
				geocoder = new GClientGeocoder();							
				startCalle();
			}
		}
	</script>		
  	</head>
  	<body onLoad="load()">
		<div id="map" style="width:0px; height:0px; visibility:hidden; display:none;"></div>
		<div id="calle"></div>	  	
	</body>
</html>
<?php
}
?>