<?
ob_start();
include("conexion.php");
$rs = New COM("ADODB.Recordset");
$rc = New COM("ADODB.Command");

include("funciones.php");
include("head.php");
$u = new User();
if ($u->isLogued){
?> 
	<title>Montecristo Data Mining - Tracking Tampering Technology - Reporte de Status</title>
   	<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
	<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAi8mj96kyL8tl4LpmHvQWdBRf664DUNFDIWxt3GQfe0EgEFDFzxQQXv2MdIUYESSeDE37VxjSiN4nbQ" type="text/javascript"></script>	
	<script type="text/javascript" language="javascript" charset="iso-8859-1">		
		var geocoder;	
		var map;
		var totFilas;
		var band = 0;
		
		function printCalles(){
			var str;
			for (var a = 0; a < totFilas; a++){		
				str = "";
				
				if ($("#calle_" + a).length == 1){
					str += $("#calle_" + a).val();
					str = trim(str);
					
					if (str.length > 0){
						$(".calle"+a).html(str);
					}else{
						$(".calle"+a).html("Cargando...");
					}
				}
			}
		}
		
		function getThisCalle(lat, lon, a){
			geocoder.getLocations(new GLatLng(lat, lon), function(response) { 		   				
				if (response.Placemark){	
					if ($("#calle_" + a).length == 1){
						$("#calle_" + a).val(response.Placemark[0].address);
					}else{
						$("#calles").append('<input type="hidden" id="calle_'+a+'" value="'+response.Placemark[0].address+'" />');
					}
				}else{
					if ($("#calle_" + a).length == 0){
						$("#calles").append('<input type="hidden" id="calle_'+a+'" value="" />');
					}
				}				
			});
		}
		
		function startCalle(){
			totFilas = parseInt($("#totFilas").val());

			for (var a = 0; a < totFilas; a++){					
				getThisCalle(parseFloat($(".lat"+a).html()), parseFloat($(".lon"+a).html()), a);
			}
			
			if (band < 3){
				printCalles();
				setTimeout("startCalle()", 5000);				
				band++;
			}else{
				printCalles();
			}
		}
		
		function load() {
			if (GBrowserIsCompatible()) {  	 	
				map = new GMap2(document.getElementById("map"));
				map.setCenter(new GLatLng(31.1, -110.9), 6);	  										
				
				geocoder = new GClientGeocoder();							
				startCalle();
			}
		}
	</script>		
  </head>
  <body onLoad="load()">
  	<div id="map" style="width:0px; height:0px; visibility:hidden; display:none;"></div>
  	<div id="calles"></div>	  	
	<? $u->printReporte(); ?>	
  </body>
</html>
<?php
}//endif ($u->isLogued)
ob_end_flush();
?>