<?php 
ob_start();
ini_set("memory_limit","32M");
set_time_limit(240);

if (isset($_GET["idviaje"]) and strlen($_GET["idviaje"])>0){ 
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	$u = new User();
	if ($u->isLogued){
		//recivimos datos	
		$idViaje = trim((int)$_GET["idviaje"]); 			

		//sacamos los demas datos
		$rs->Open("	SELECT 
						viajes.clave, 
						CONVERT(CHAR(19),viajes.finicial,120) AS fechaI,
						CONVERT(CHAR(19),viajes.ffinal,120) AS fechaF,
						viajes.origen,
						viajes.destino,
						viajes.mercancia,
						viajes.vel,
						viajes.ruta,
						viajes.tiempomuerto,
						GPs.serie
					FROM viajes 
					INNER JOIN GPs
					ON (GPs.id = viajes.gps)
					WHERE viajes.idviaje = ".$idViaje, $conn);				
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		//si esque hay bases
		$bases = array();
		if (strlen(trim($datos[0]['ruta'])) > 0 and (int)$datos[0]['ruta'] != 0){
			//sacamos ruta de control
			$rs->Open("	SELECT 
							nombre, 
							descripcion
						FROM RutaControl 
						WHERE 
							id = ".(int)$datos[0]['ruta'], $conn);
			$rutaControl = fetch_assoc($rs);  
			$rs->Close();
		
			//sacamos todas las bases
			$rs->Open("	SELECT     
							geocercas.nombre,
							geocercas.descripcion,
							geocercastipo.tipo,
							geocercastipo.icono,
							geocercas.longitud,
							geocercas.latitud
						FROM geocercas 
						INNER JOIN geocercastipo
						ON (geocercastipo.id = geocercas.tipo)
						WHERE geocercas.id IN(
							SELECT idGeocerca
							FROM RutaGeocercas
							WHERE RutaGeocercas.idRuta = ".(int)$datos[0]['ruta']."
							GROUP BY idGeocerca
							HAVING (COUNT(RutaGeocercas.idGeocerca) > 0)
						)", $conn);
			$bases = fetch_assoc($rs);  
			$rs->Close();
		}//end if
		
		//velocidad maxima
		$vm = 100;
		if ((isset($_GET["vm"])) and (strlen($_GET["vm"])>0) and ((int)$_GET["vm"]>3)){
			$vm = (int)trim($_GET["vm"]);
		}elseif ((strlen(trim($datos[0]['vel']))>0) and ((int)$datos[0]['vel']>3)){
			$vm = (int)$datos[0]['vel'];
		}
		
		//tiempo muerto
		$tm = 60;
		if ((isset($_GET["tm"])) and (strlen($_GET["tm"])>0) and ((int)$_GET["tm"]>1)){
			$tm = (int)trim($_GET["tm"]);
		}elseif ((strlen(trim($datos[0]['tiempomuerto']))>0) and ((int)$datos[0]['tiempomuerto']>1)){
			$tm = (int)$datos[0]['tiempomuerto'];
		}
		
		//demas datos
		//fecha inicial [fechaInicial]
		$fechaI = date_parse(trim($datos[0]['fechaI']));
		$fechaI = date('Y-m-d\TH:i:s', mktime(
											$fechaI['hour'],
											$fechaI['minute'],
											$fechaI['second'],
											$fechaI['month'],
											$fechaI['day'],
											$fechaI['year']
										));	
		//fecha final								
		$fechaF = date_parse(trim($datos[0]['fechaF']));
		$fechaF = date('Y-m-d\TH:i:s', mktime(
											$fechaF['hour'],
											$fechaF['minute'],
											$fechaF['second'],
											$fechaF['month'],
											$fechaF['day'],
											$fechaF['year']
										));			
		$xt = $datos[0]['serie']; //gps										
		$mercancia = $datos[0]['mercancia']; //mercancia
		$destino = $datos[0]['destino']; //destino
		$origen = $datos[0]['origen']; //origen
		$clvViaje = $datos[0]['clave']; //clave

		//sacamos todos los puntos
		$rs->Open("	SELECT 				
						GPs.descripcion,
						datosghe.gps,						
						CONVERT(CHAR(19),datosghe.fechasend,120) AS fechasend,						
						CONVERT(CHAR(19),datosghe.fecharecv,120) AS fecharecv,	
						datosghe.lat,
						datosghe.lon,
						datosghe.alarma,
						datosghe.tambiental,
						datosghe.velocidad,
						datosghe.valido,
						datosghe.bateria,
						datosghe.aperturas,
						datosghe.rumbo						
					FROM datosghe 
					INNER JOIN GPs
					ON (GPs.serie = datosghe.gps)
					WHERE 
						datosghe.gps = $xt AND
						datosghe.fecharecv >= '$fechaI' AND 
						datosghe.fecharecv <= '$fechaF' AND 
						datosghe.lat <> 0 AND
						datosghe.lon <> 0 AND 
						datosghe.valido = 1
					ORDER BY datosghe.fechasend ASC", $conn);
		$datos = fetch_assoc($rs); 
		$rs->Close();
			
		//CREAMOS INICIO DE KML
		$kml = '<?xml version="1.0" encoding="ISO-8859-1"?>
					<kml xmlns="http://earth.google.com/kml/2.1">
						<Folder>
							<name>Ruta Cerrada - '.$datos[0]["descripcion"].'</name>
							<Folder>
								<name>Puntos</name>'; 	
		
		function imgIcono($tipo){
			$img = '';
			switch ($tipo){
				case 'Ok': $img = 'http://www.montecristodm.com/viajes/t3/Imagenes/point.png'; break;
				case 'Fast': $img = 'http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png'; break;
				case 'Stop': $img = 'http://www.montecristodm.com/viajes/t3/Imagenes/tm.png'; break;
				case 'Alarma': $img = 'http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png'; break;
			}
			
			return $img;
		}
		
		$coord = ''; //contendra todas las coordenadas para dibujar la linea		
		for ($a=0; $a<count($datos); $a++){
			$tmMsj = '';	
			
			//modificamos el formato de las coordenadas
			$lat = $datos[$a]["lat"];	  
			switch(substr($lat,strlen($lat)-1,1)){
				case 'S': (double)$lat*=-1; break;
				case 'N': (double)$lat*=1; break;
			}
			
			$lon = $datos[$a]["lon"];  
			switch(substr($lon,strlen($lon)-1,1)){
				case 'W': (double)$lon*=-1; break;
				case 'E': (double)$lon*=1; break;
			}
			
			//modificamos formato del rumbo
			switch($datos[$a]["rumbo"]){
				case 0: $elrumbo="Norte"; break;
				case 1: $elrumbo="Noreste"; break;
				case 2: $elrumbo="Este"; break;
				case 3: $elrumbo="Sureste"; break;
				case 4: $elrumbo="Sur"; break;
				case 5: $elrumbo="Suroeste"; break;
				case 6: $elrumbo="Oeste"; break;
				case 7: $elrumbo="Noroeste"; break;
			}  
			
			//checamos si hubo cambio de aperturas
			$cambioAperturas = false;
			if ($a > 0){
				if ($datos[$a]["aperturas"] != $datos[$a-1]["aperturas"]){
					$cambioAperturas = true;
				}
			}
						
			//procesamos icono
			if ($datos[$a]["alarma"] == 1 or $cambioAperturas == true){
				//fibra abierta
				$icono = imgIcono('Alarma');
				$msj = 'Abierta'; //fibra abierta
			}else{
				//fibra cerrada
				$msj = 'No abierta'; //fibra abierta
				
				if ($datos[$a]["velocidad"] != 0){ 
					if ((int)$datos[$a]["velocidad"] > $vm){ //mostramos alarma de velocidad 
						$icono = imgIcono('Fast');
					}else{							
						$icono = imgIcono('Ok');
					}
				}else{
					//estuvo parado un tiempo... veremos si fue mas del limite
					if ($a < count($datos)-1){  //si no esta en el ultimo registro			      	
						if ($datos[$a+1]['velocidad']!=0){ //si solo era un cero unico (sin serie de ceros)
							$icono = imgIcono('Ok');
					   	}else{
							//serie de ceros: recorremos hasta encontrar el fin de la serie e imprimimos marcador
							$b = $a;
							
							//sacamos fecha del primer cero	
							$fechaCero11 = date_parse($datos[$b]['fechasend']);
							$fechaCero11 = date('m/d/Y h:i:s A', mktime(
																	$fechaCero11['hour'],
																	$fechaCero11['minute'],
																	$fechaCero11['second'],
																	$fechaCero11['month'],
																	$fechaCero11['day'],
																	$fechaCero11['year']
																) - 21600);
																
							//convertimos fecha del primer cero a segundos para verificar si estuvo mas de 10 minutos
							$fechaCero1 = date_parse($datos[$b]['fechasend']);
							$fechaCero1 = mktime(
											$fechaCero1['hour'],
											$fechaCero1['minute'],
											$fechaCero1['second'],
											$fechaCero1['month'],
											$fechaCero1['day'],
											$fechaCero1['year']
										);
							
							$band_while = 0;		
							while (($datos[$b]['velocidad'] == 0) and ($band_while == 0)){  //posible error de desbordamiento
								++$b;
								if ($b == count($datos)){ //desbordado, entonces decrementamos y  encendemos bandera							
							  		--$b;
							  		$band_while = 1;
								} 
							} //$b contendra el id del siguiente registro con velocidad diferente de cero
							
							//sacamos fecha del siguiente registro despues del ultimo cero
							$fechaCero22 = date_parse($datos[$b]['fechasend']);
							$fechaCero22 = date('m/d/Y h:i:s A', mktime(
																	$fechaCero22['hour'],
																	$fechaCero22['minute'],
																	$fechaCero22['second'],
																	$fechaCero22['month'],
																	$fechaCero22['day'],
																	$fechaCero22['year']
																) - 21600);
																
							//convertimos fecha del siguiente registro despues del ultimo cero, a segundos para verificar si estuvo mas de 10 minutos
							$fechaCero2 = date_parse($datos[$b]['fechasend']);
							$fechaCero2 = mktime(
											$fechaCero2['hour'],
											$fechaCero2['minute'],
											$fechaCero2['second'],
											$fechaCero2['month'],
											$fechaCero2['day'],
											$fechaCero2['year']
										);
							
							if (($fechaCero2 - $fechaCero1) >= ($tm * 60)){ //ponemos icono de stop 
								$icono = imgIcono('Stop');
								$tmMsj = '	<br/>
												<div><b>Duraci&oacute;n de inactividad: </b>'.segMinHrs($fechaCero2 - $fechaCero1).'</div>
												<div><b>Desde: </b>'.$fechaCero11.'</div>
												<div><b>Hasta: </b>'.$fechaCero22.'</div>
											<br/>';								
							}else{ //ponemos icono normal 
								$icono = imgIcono('Ok');
							}
							
							//movemos el puntero ($a) a la posicion del ultimo cero, para que a la siguiente vuelta se incremente solo
							$a = $b - 1;
						}
			      	}else{
						$icono = imgIcono('Ok');
					}
				}	
			}
			
			//fecha para el TimeStamp
			$fechaTS = date_parse($datos[$a]["fechasend"]);
			$fechaTS = date('Y-m-d\TH:i:s', mktime(
												$fechaTS['hour'],
												$fechaTS['minute'],
												$fechaTS['second'],
												$fechaTS['month'],
												$fechaTS['day'],
												$fechaTS['year']
											));	
				   		   			
			$kml .= '			<Placemark>
									<description>
										<![CDATA[ 								
											<div><b>Modulo: </b>'.$datos[$a]["descripcion"].'</div>
											<div><b>Fecha: </b>'.$datos[$a]["fecharecv"].'</div>
											<div><b>Fecha UTC: </b>'.$datos[$a]["fechasend"].'</div>
											<div><b>Fibra: </b>'.$msj.'</div>'.trim($tmMsj).'
											<div><b>Aperturas: </b>'.$datos[$a]["aperturas"].'</div>
											<div><b>Origen: </b>'.$origen.'</div>
											<div><b>Destino: </b>'.$destino.'</div>
											<div><b>Direcci&oacute;n: </b>'.$elrumbo.'</div>
											<div><b>Mercancia: </b>'.$mercancia.'</div>
											<div><b>Velocidad: </b>'.$datos[$a]["velocidad"].' Km/h - '.round(($datos[$a]["velocidad"] / 1.60935), 2).' Mi/h</div>
											<div><b>Temperatura: </b>'.$datos[$a]["tambiental"].' &deg;C - '.round((($datos[$a]["tambiental"] * 1.8) + 32), 2).' &deg;F</div>									
											<div><b>Bater&iacute;a: </b>'.$datos[$a]["bateria"].'</div>
										]]>
									</description>								
									<Style>
										<IconStyle>
											<scale>0.5</scale>
											<Icon>
												<href>'.$icono.'</href>
											</Icon>
										</IconStyle>								
									</Style>
									<TimeStamp id="'.$a.'">
										<when>'.$fechaTS.'Z</when>
									</TimeStamp>
									<Point>
										<coordinates>'.$lon.','.$lat.',0</coordinates>
									</Point>
								</Placemark>';
			$coord .= $lon.','.$lat.',0 ';
		}//end for
		
		//CREAMOS FINAL DE KML
		$kml .= '			
							</Folder>
							<Folder>
								<name>Linea</name>
								<Placemark>
									<Style>
										<LineStyle>
											<color>E0552AFF</color>
											<width>2</width>
										</LineStyle>
									</Style>
									<LineString>
										<tessellate>1</tessellate>
										<coordinates>'.$coord."\n".'</coordinates>
									</LineString>
								</Placemark>	
							</Folder>'; 
							
		//si hay bases, las imprimimos		
		if (count($bases) > 0){
			$kml .= ' 
							<Folder>
								<name>Bases</name>';
			
			for ($a = 0; $a < count($bases); $a++){
				$kml .= '
								<Placemark>
									<description>
										<![CDATA[ 								
											<div><b>Base: </b>'.$bases[$a]["nombre"].'</div>
											<div><b>Descripci&oacute;n de base: </b>'.$bases[$a]["descripcion"].'</div>
											<div><b>Tipo: </b>'.$bases[$a]["tipo"].'</div>
											<div><b>Ruta de Control: </b>'.$rutaControl[0]['nombre'].'</div>
											<div><b>Descripci&oacute;n de ruta de control: </b>'.$rutaControl[0]['descripcion'].'</div>
										]]>
									</description>
									<name>'.$bases[$a]["nombre"].'</name>
									<Style>
										<IconStyle>
											<scale>0.5</scale>
											<Icon>
												<href>'.$bases[$a]["icono"].'</href>
											</Icon>
										</IconStyle>
										<LabelStyle>
											<color>ff0055ff</color>
											<scale>0.7</scale>
										</LabelStyle>
									</Style>
									<Point>
										<coordinates>
											'.$bases[$a]["longitud"].','.$bases[$a]["latitud"].',0
										</coordinates>
									</Point>
								</Placemark>';
			}//end for						
					
			$kml .= '		</Folder>';
		}//end if
			
		$kml .= '		</Folder>
					</kml>'; 					

		header("Pragma: public"); 
		header("Expires: 0");       
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		header("Content-Type: application/vnd.google-earth.kml+xml kml; charset=ISO-8859-1");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".strlen($kml));
		header("Pragma: no-cache");
		header("Content-disposition: attachment; filename=Ruta_Actual_Viaje_".$clvViaje.".kml"); 
		
		echo $kml;
	}//end if ($u->isLogued)
} //end if (isset($_GET["idviaje"]) and strlen($_GET["idviaje"])>0)
ob_end_flush();
?>