<?php
ob_start();

include("conexion.php");
include("funciones.php");
$rs = New COM("ADODB.Recordset");

$u = new User();	
if ($u->isLogued){
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />	
	<meta name="description" content="T3 es la forma sencilla y moderna de rastrear un contenedor en tiempo real y detectar su apertura en el momento en que esta ocurre, con un bajo costo de operaci&oacute;n." />
	<meta name="keywords" content="Rastreo,Sistema,Colima,GPS,Geoposicionamiento,Mexico,Contenedores,Rastreo Satelital" />
	<meta http-equiv="content-language" content="es" />
	<meta name="Language" content="Spanish" />
	<meta name="Distribution" content="Global" />
	<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
	<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/jquery.js"></script>
	<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/funciones.js"></script>	
<?	
	//sacamos todos los gps de este user
	$rs->Open("		
		SELECT 
			GPs.serie, 
			GPs.descripcion			
		FROM GPs 
		INNER JOIN gpscontratos 
		ON (GPs.id = gpscontratos.gps) 
		WHERE (
			gpscontratos.contrato IN (
				SELECT id 
				FROM contratos 
				WHERE 
					activo = 1 AND 
					usr = ".$u->id_user."))
		ORDER BY GPs.descripcion ASC", $conn); 	
	$datos = fetch_assoc($rs); 
	$rs->Close();
	
	if (count($datos)>0){
		//procesamos datos					
		$vm = 100;
		if (isset($_GET["vm"]) and strlen(trim($_GET["vm"]))>0){
			$vm = (int)trim($_GET["vm"]);
		}
								
		//SACAMOS EL PUNTO MAS ACTUAL
		$rs->Open("	SELECT lat, lon
					FROM datosghe 
					WHERE 
						gps = ".$datos[0]['serie']." AND
						id = (
							SELECT MAX(id) 
							FROM datosghe 
							WHERE 
								gps = ".$datos[0]['serie']."
						)", $conn);
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		//modificamos el formato de las coordenadas
		$lat = $datos[0]["lat"];	  
		switch(substr($lat,strlen($lat)-1,1)){
			case 'S': (double)$lat*=-1; break;
			case 'N': (double)$lat*=1; break;
		}
		
		$lon = $datos[0]["lon"];  
		switch(substr($lon,strlen($lon)-1,1)){
			case 'W': (double)$lon*=-1; break;
			case 'E': (double)$lon*=1; break;
		}			
?>			
		<title>Montecristo Data Mining - Tracking Tampering Technology - Puntos Actuales</title>
		<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAi8mj96kyL8tl4LpmHvQWdBRf664DUNFDIWxt3GQfe0EgEFDFzxQQXv2MdIUYESSeDE37VxjSiN4nbQ" type="text/javascript"></script>
		<script type="text/javascript">  
			//<![CDATA[
			var map;		
			var marcadorNow = [];		
			var con=0;		
			var msj;
			var miIcono1;					
			var OffOn;		
			var GPS = [];
			var datosNow;	
			var myHtml1;
			var datos;
			var geocoder;
			
			function load() {
				if (GBrowserIsCompatible()) {  	  
					map = new GMap2(document.getElementById("map"));
					map.setCenter(new GLatLng(19.3,-103.3, 6));	  						
					map.addControl(new GSmallMapControl());
					map.addControl(new GMapTypeControl());
					map.enableScrollWheelZoom();
					geocoder = new GClientGeocoder();
					miIcono1 = new GIcon();				
					
					mostrarNow(); //iniciamos actualizaciones
				}
			}			
				
			function mostrarNow(){ 
				$.ajax({
					type: "GET",
					url: "posicionActualAllGMforUser_actualizar.php",
					data: "ver=1",
					dataType: "html",
					contentType: "application/x-www-form-urlencoded",
					success: function(datos1){
						//sacamos cada gps de la lista de GPSs
						datos = trim(datos1);
						datos = datos.split("@_gps_@");		
						
						//inicializamos tabla									
						$("#tablaMaps").html('<table cellpadding="0" cellspacing="0"><tr class="headTabla"><td>#</td><td>Modulo</td><td>Fecha</td><td>Fecha UTC</td>						<td>Fibra</td><td>Aperturas</td><td>Velocidad</td><td>Temperatura</td><td>Tiempo parado</td><td>Direcci&oacute;n</td></tr></table>');
						
						for (con=0; con<datos.length; con++){
							GPS = trim(datos[con]);
							datosNow = GPS.split(", ");

							//si ya estaba este marcador, lo quitamos
							if (marcadorNow[con]){ 
								map.removeOverlay(marcadorNow[con]);			
							}
																			
							//CREAMOS MARKADOR
							// Personalizamos el nuevo icono							
							msj = 'No abierta';
							
							//verificamos que la alarma este apagada
							if (datosNow[9]==1 || datosNow[13]==1  || datosNow[9]==50  || datosNow[9]==21 ){ //fibra abierta, ponemos icono de alerta
								miIcono1.image = "http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png"; 
								msj = 'Abierta';
							}else if (datosNow[7].substring(0, datosNow[7].length-5)><?=$vm; ?>){ //va a exeso de velocidad
								miIcono1.image = "http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png"; 
							}else{
								//ponemos iconos normales
								if (datosNow[7].substring(0, datosNow[7].length-5)==0){ //si esta parado 
									OffOn="Off"; 
								}else{ 
									OffOn="On"; 
								}
								
								miIcono1.image = "http://www.montecristodm.com/viajes/t3/Imagenes/" + datosNow[6] + OffOn + ".png";
							}
							
							miIcono1.iconSize = new GSize(16, 16);
							miIcono1.iconAnchor = new GPoint(8, 8);
							miIcono1.infoWindowAnchor = new GPoint(8, 8);
							
							getCalle(datosNow[3], datosNow[4], con);
							marcadorNow[con] = new GMarker(new GLatLng(datosNow[3], datosNow[4]), {icon:miIcono1, title:(datosNow[10].split('COMA').join(','))});
							marcadorNow[con].value = datosNow[0];						
							marcadorNow[con].bindInfoWindowHtml('<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Modulo: <b>'+ (datosNow[10].split('COMA').join(','))+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha: <b>'+ datosNow[1]+'</b></div><br/><div><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fecha UTC: <b>'+ datosNow[2]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">GPS: <b>'+ datosNow[14]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Fibra: <b>'+ msj+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Aperturas: <b>'+ datosNow[12]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Orientaci&oacute;n: <b>'+ datosNow[6] +'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Velocidad: <b>'+ datosNow[7]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Temperatura: <b>'+ datosNow[5]+'</b></div><br/><div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Bater&iacute;a: <b>'+ datosNow[8]+'</b></div><br/>'+ ((datosNow[11]!='0')?('<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Tiempo parado: <b>'+ datosNow[11]+'</b></div><br/>'):'')+'<div style="height:3px; font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000;">Direcci&oacute;n: <b>'+(($("#calle_"+con).val())?$("#calle_"+con).val():'Cargando...')+'</b></div><br/>');
							
							//actualizamos tabla
							$(".headTabla").after('<tr class="bodyTable'+(con%2)+'"><td>'+(datos.length-con)+'</td><td>'+(datosNow[10].split('COMA').join(','))+'</td><td>'+datosNow[1]+'</td><td>'+datosNow[2]+'</td><td>'+msj+'</td><td>'+datosNow[12]+'</td><td>'+datosNow[7]+'</td><td>'+datosNow[5]+'</td><td>'+((datosNow[11]!='0')?datosNow[11]:'0 min')+'</td><td>'+(($("#calle_"+con).val())?$("#calle_"+con).val():'Cargando...')+'</td></tr>');
													
							//agregamos el marcador
							map.addOverlay(marcadorNow[con]);
						}//end for
																							
						//creamos recursividad
						setTimeout ("mostrarNow()", 30000);  //en 30 segundos se vuelve mandar llamar la funcion 													
					}
				});
			}
			//]]>
		</script>
		</head>
		<body onLoad="load()" onUnload="GUnload()">
			<div id="calles"></div>			
			<div id="map" style="width:918px; height:616px;"></div>			
			<div id="tablaMaps">
				<table cellpadding="0" cellspacing="0">
					<tr class="headTabla">
						<td>#</td>
						<td>Modulo</td>
						<td>Fecha</td>
						<td>Fecha UTC</td>
						<td>Fibra</td>
						<td>Aperturas</td>
						<td>Velocidad</td>
						<td>Temperatura</td>
						<td>Tiempo parado</td>
						<td>Direcci&oacute;n</td>
					</tr>
				</table>
			</div>
		</body>
		</html>
<?php		
	}//end if ($serieGps>0)
}//end if ($u->isLogued)
ob_end_flush();
?>
