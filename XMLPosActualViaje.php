<?php
ob_start();

if (isset($_GET["idviaje"]) and strlen($_GET["idviaje"])>0){
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	$u = new User();
	if ($u->isLogued){
		$idViaje = trim((int)$_GET["idviaje"]);
		//RECUPERAMOS DATOS DE VIAJE
		$rs->Open("	SELECT 
						viajes.clave, 
						viajes.origen,
						viajes.destino,
						viajes.mercancia,
						viajes.vel,
						GPs.serie
					FROM viajes 
					INNER JOIN GPs
					ON (GPs.id = viajes.gps)
					WHERE viajes.idviaje = ".$idViaje, $conn);
		$datos = fetch_assoc($rs);  
		$rs->Close();
			
		$Kml = '<?xml version="1.0" encoding="ISO-8859-1"?>
		<kml xmlns="http://earth.google.com/kml/2.0">
		<NetworkLink>
		  <name>Viaje '.$datos[0]["clave"].'</name>
		  <Url>
			<href>http://www.montecristodm.com/viajes/t3/PosActualViaje.php?idviaje='.$idViaje.'&amp;clvViaje='.$datos[0]["clave"].'&amp;iduser='.$u->id_user.'&amp;vel='.$datos[0]["vel"].'&amp;xt='.$datos[0]["serie"].'&amp;or='.$datos[0]["origen"].'&amp;de='.$datos[0]["destino"].'&amp;me='.$datos[0]["mercancia"].'</href>
			<refreshMode>onInterval</refreshMode>
			<refreshInterval>30</refreshInterval>
		  </Url>
		</NetworkLink>
		</kml>';
		
		header("Pragma: public"); 
		header("Expires: 0");       
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		header("Content-Type: application/vnd.google-earth.kml+xml kml; charset=ISO-8859-1");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".strlen($Kml));
		header("Pragma: no-cache");
		header("Content-disposition: attachment; filename=Posicion_Actual_Viaje_".$datos[0]["clave"].".kml"); 
		
		echo $Kml;
	}//endif ($u->isLogued)
}//endif (isset($_GET["idviaje"]) and strlen($_GET["idviaje"])>0)
ob_end_flush();
?>