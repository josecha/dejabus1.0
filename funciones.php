<?php
header("Content-type: text/html; charset=iso-8859-1");

//VARIABLE GLOBAL QUE INDICA EL NAVEGADOR EN EL QUE SE ENCUENTRA EL USER
$browser = "";

//INCLUYE LOS ARCHIVOS DE LAS CLASES
function __autoload ($class_name) {
    include('class_'.$class_name.'.php');
}

//REGRESA LA URL DE LA PAGINA DESDE DONDE SE LLAMA A ESTA FUNCION
function thisUrlAll(){
	$uri = $_SERVER['SCRIPT_NAME'];
	
	if ($_SERVER['QUERY_STRING'] != ''){
		$uri .= '?'.$_SERVER['QUERY_STRING'];
	}
	
	return basename($uri);
}

//SELECCIONA EL ESTILO CORRECTO DEPENDIENDO DEL NAVEGADOR
function browserStyle (){
	global $browser;
	
    if(ereg("MSIE", $_SERVER["HTTP_USER_AGENT"])){ 
		$browser='IE'; 
		$css = 'Estilos/estiloIE.css'; 
	}elseif(ereg("Opera", $_SERVER["HTTP_USER_AGENT"])){ 
		$browser='OP'; 
		$css = 'Estilos/estiloOP.css'; 
	}else{ 
		$browser='FF'; 
		$css = 'Estilos/estilo.css'; 
	}
  	return $css;
}

//REGRESA EL NAVEGADOR EN EL QUE ESTÁ EL USUARIO
function browser(){
	global $browser;
	return $browser;
}

//CONVIERTE NUMEROS CON UN DIGITO A 2 DIGITOS
function twoDigit($n){
    if (strlen((string)$n)==1){
		return ('0'.$n);
	}else{ 
		return (string)$n;
	}
}	

//CONVIERTE DE SEGUNDOS A HORAS MINUTOS Y SEGUNDOS
function segMinHrs($numeroAC){  	
	if ($numeroAC < 60){ //solo segundos
		$numeroTB_ok = $numeroAC." seg"; 
	}elseif ($numeroAC < 3600){ //solo minutos
		$numeroTB_ok = round($numeroAC / 60, 2)." min"; 
	}else{ 
		$numeroTB_ok = round($numeroAC / 3600, 2); 
		if ($numeroTB_ok == 1){ 
			$numeroTB_ok.=" hr"; 
		}else{ 
			$numeroTB_ok.=" hrs"; 
		}
	}
	
	return $numeroTB_ok;
}

//MUESTTRA MENSAJES DE AVISO
function msj($box, $seg, $msj){
	echo "\n".'<script language="javascript">'."\n".'msj("'.$box.'", '.$seg.', "'.addslashes($msj).'");'."\n".'</script>'."\n";    
}

//REGRESA UNA LA FECHA ACTUAL CON FORMATO YYYY-MM-DDTHH:MM:SS
function fechaNow(){
	return date("Y")."-".date("m")."-".date("d")."T".date("H").":".date("i").":".date("s");
}

//REGRESA LAS HORAS MINUTOS Y SEGUNDOS EN FORMA DE NUMERO ENTERO (USADO PARA NOMBRES DE FOTOS Y EVITAR REPETICIONES)
function timeNow(){
	return date("G").date("i").date("s");
}

//REGRESA LA FECHA EN ESTE INSTANTE PERO EN SEGUNDOS
function fechaNowSeg(){
	return mktime(date("G"),date("i"),date("s"),date("n"),date("j"),date("Y"));
}

//LA MISMA FUNCION QUE date_parse PERO FUNCIONA EN PHP VER 4
function date_parseOK($datetime){
	$datetime = strtotime($datetime);
	
	$fecha['year'] = date('Y',$datetime);
    $fecha['month'] = date('m',$datetime);
    $fecha['day'] = date('d',$datetime);
    $fecha['hour'] = date('G',$datetime);
    $fecha['minute'] = date('i',$datetime);
    $fecha['second'] = date('s',$datetime);
	$fecha['ampm'] = date('a',$datetime);
	
	return $fecha;
}


/**
 * invierte el numero y le suma 1 a todos los numeros distintos de 9
 * @param String. $num
 * 47293   ->   39274   -> 40385
**/
function encrNum($num){
	$num .= "";
	
	$n = "";
	for ($a = 0; $a < strlen($num); $a++){
		if ($num{$a} === '9'){
			$n = '0'.$n;
		}else{
			$n = ($num{$a}+1).$n;
		} 
	}

	return (int)$n;
}


/**
 * desencripta un numero encripado con encrNum()
 * @param String. $num
 * 40385   ->  58304   ->  47293
**/
function desencrNum($num){
	$num .= "";
	
	$n = "";
	for ($a = 0; $a < strlen($num); $a++){
		if ($num{$a} === '0'){
			$n = '9'.$n;
		}else{
			$n = ($num{$a}-1).$n;
		} 
	}
	
	return $n;
}

//DESENCRIPTA UN NUMERO CON CODIFICACIÓN MIA
function myDencr($t){
	$txt = trim($t);
	$txt = split('__', $txt);	
	$aux = split('_', trim($txt[0]));
	$txt = stripslashes(trim($txt[2]));		
			
	$asc = split('_', $txt);		
	$r = "";

	if (count($asc)%2==0){
		for ($a=0; $a<count($asc); $a++){
			$r .= chr(($a%2==0)?ord($asc[$a])-15:ord($asc[$a])+15);
		}
	}else{
		for ($a=count($asc)-1; $a>-1; $a--){
			$r .= chr(($a%2==0)?ord($asc[$a])-15:ord($asc[$a])+15);
		}
	}
	
	$r = trim($r);
	if (trim($aux[0])!=md5($r)){ //los datos no llegaron bien
		$r = 0;
	}
		
	return $r;		
}

//ENCRIPTA UN NUMERO CON CODIFICACIÓN MIA
function myEncr($t){
	$txt = trim($t);
	$asc = array();		
	
	for ($a=0; $a<strlen($txt); $a++){
		$asc[$a] = chr((($a%2==0)?ord($txt{$a})+15:ord($txt{$a})-15));
	}
		
	$r="";
	if (count($asc)%2==0){
		for ($a=0; $a<count($asc); $a++){
			$r .= $asc[$a].(($a==count($asc)-1)?"":"_");
		}
	}else{
		for ($a=count($asc)-1; $a>-1; $a--){
			$r .= $asc[$a].(($a==0)?"":"_");
		}
	}
	
	return md5($txt).'_'.(chr(rand(33,72)).'A'.chr(rand(33,72)).'__'.chr(rand(33,72)).'_'.chr(rand(33,72)).'__'.$r.'__'.chr(rand(33,72)).'_'.chr(rand(33,72)).'_'.chr(rand(33,72)));
}

//REGRESA UNA CADENA SUSTITUYENDO TODOS LOS CARACTERES ESPECIALES POR UNA REPRESENTACION ALFANUMERICA
function encondeSpecialChars($txt){
	$newTxt = "";
	
	for ($a=0; $a<strlen($txt); $a++){
		switch($txt{$a}){
			case '!': $newTxt .= 'CE01CE'; break;
			case '"': $newTxt .= 'CE02CE'; break;
			case '#': $newTxt .= 'CE03CE'; break;
			case '$': $newTxt .= 'CE04CE'; break;
			case '%': $newTxt .= 'CE05CE'; break;
			case '&': $newTxt .= 'CE06CE'; break;
			case "'": $newTxt .= 'CE07CE'; break;
			case '(': $newTxt .= 'CE08CE'; break;
			case ')': $newTxt .= 'CE09CE'; break;
			case '*': $newTxt .= 'CE10CE'; break;
			case '+': $newTxt .= 'CE11CE'; break;
			case ',': $newTxt .= 'CE12CE'; break;
			case '-': $newTxt .= 'CE13CE'; break;
			case '.': $newTxt .= 'CE14CE'; break;
			case '/': $newTxt .= 'CE15CE'; break;
			case ':': $newTxt .= 'CE16CE'; break;
			case ';': $newTxt .= 'CE17CE'; break;
			case '<': $newTxt .= 'CE18CE'; break;
			case '=': $newTxt .= 'CE19CE'; break;
			case '>': $newTxt .= 'CE20CE'; break;
			case '?': $newTxt .= 'CE21CE'; break;
			case '@': $newTxt .= 'CE22CE'; break;
			case '[': $newTxt .= 'CE23CE'; break;
			case ']': $newTxt .= 'CE24CE'; break;
			case '^': $newTxt .= 'CE25CE'; break;
			case '_': $newTxt .= 'CE26CE'; break;
			default: $newTxt .= $txt{$a};
		}		
	}
		
	return trim($newTxt);
}

//REGRESA UNA CADENA SUSTITUYENDO TODOS LAS REPRESENTACIONES ALFANUMERICAS POR CARACTERES ESPECIALES 
function decodeSpecialChars($txt){
	$newTxt = "";

	for ($a=0; $a<=strlen($txt)-6; $a++){
		$thisCad = substr($txt, $a, 6);
		
		if (substr($thisCad,0,2)=='CE'){
			switch($thisCad){
				case 'CE01CE': $newTxt .= '!'; $a += 5; break;
				case 'CE02CE': $newTxt .= '"'; $a += 5; break;
				case 'CE03CE': $newTxt .= '#'; $a += 5; break;
				case 'CE04CE': $newTxt .= '$'; $a += 5; break;
				case 'CE05CE': $newTxt .= '%'; $a += 5; break;
				case 'CE06CE': $newTxt .= '&'; $a += 5; break;
				case 'CE07CE': $newTxt .= "'"; $a += 5; break;
				case 'CE08CE': $newTxt .= '('; $a += 5; break;
				case 'CE09CE': $newTxt .= ')'; $a += 5; break;
				case 'CE10CE': $newTxt .= '*'; $a += 5; break;
				case 'CE11CE': $newTxt .= '+'; $a += 5; break;
				case 'CE12CE': $newTxt .= ','; $a += 5; break;
				case 'CE13CE': $newTxt .= '-'; $a += 5; break;
				case 'CE14CE': $newTxt .= '.'; $a += 5; break;
				case 'CE15CE': $newTxt .= '/'; $a += 5; break;
				case 'CE16CE': $newTxt .= ':'; $a += 5; break;
				case 'CE17CE': $newTxt .= ';'; $a += 5; break;
				case 'CE18CE': $newTxt .= '<'; $a += 5; break;
				case 'CE19CE': $newTxt .= '='; $a += 5; break;
				case 'CE20CE': $newTxt .= '>'; $a += 5; break;
				case 'CE21CE': $newTxt .= '?'; $a += 5; break;
				case 'CE22CE': $newTxt .= '@'; $a += 5; break;
				case 'CE23CE': $newTxt .= '['; $a += 5; break;
				case 'CE24CE': $newTxt .= ']'; $a += 5; break;
				case 'CE25CE': $newTxt .= '^'; $a += 5; break;	
				case 'CE26CE': $newTxt .= '_'; $a += 5; break;			
				default: $newTxt .= $txt{$a};
			}		
		}else{
			$newTxt .= $txt{$a};
		}
		
	}

	return trim($newTxt);
}

//REGRESA 1 SI UN PUNTO (X, Y) ESTA DENTRO DE UN CIRCULO CON RADIO "R", DE LO CONTRARIO REGRESA 0
function inCircle($x, $y, $h, $k, $r){  
	if (sqrt(pow((double)$x-(double)$h, 2) + pow((double)$y-(double)$k, 2))  <= $r){
		return 1;
	}else{
		return 0;	
	}
}

//CONVIERTE UNA FECHA CON FORMATO YYYY-MM-DDTHH:MM:SS A SEGUNDOS
function fechaToSeg($fecha){
	$fecha = date_parse($fecha);
	return mktime(
		$fecha['hour'], 
		$fecha['minute'], 
		$fecha['second'], 
		$fecha['month'], 
		$fecha['day'], 
		$fecha['year']
	);
}
?>