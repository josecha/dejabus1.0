<?php 
ob_start();
header("Content-type: text/html; charset=iso-8859-1");

if (isset($_GET["idviaje"]) and strlen($_GET["idviaje"])>0){ 
	//si envio un xt, idViaje y user    
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	//recivimos datos
	$xt = 0; 
	if (isset($_GET["xt"]) and strlen($_GET["xt"])>0){  //gps
		$xt = trim((int)$_GET["xt"]); 		
	}
	
	$idUser = 0; 
	if (isset($_GET["iduser"]) and strlen($_GET["iduser"])>0){  //usuario
		$idUser = trim((int)$_GET["iduser"]); 		
	}
	
	$idViaje = 0; 
	if (isset($_GET["idviaje"]) and strlen($_GET["idviaje"])>0){  //viaje
		$idViaje = trim((int)$_GET["idviaje"]); 		
	}

	$vm = 110; 
	if (isset($_GET["vel"]) and strlen($_GET["vel"])>0 and $_GET["vel"]>3){  //velocidad
		$vm = trim((int)$_GET["vel"]); 		
	}
		
	$clvViaje = ""; 	
	if (isset($_GET["clvViaje"]) and strlen($_GET["clvViaje"])>0){  //clave del viaje
		$clvViaje = trim($_GET["clvViaje"]); 
	}
	
	$origen = "Sin especificar"; 	
	if (isset($_GET["or"]) and strlen($_GET["or"])>0){  //origen
		$origen = trim($_GET["or"]); 
	}
	
	$destino = "Sin especificar"; 	
	if (isset($_GET["de"]) and strlen($_GET["de"])>0){  //destino
		$destino = trim($_GET["de"]); 
	}
	
	$mercancia = "Sin especificar"; 	
	if (isset($_GET["me"]) and strlen($_GET["me"])>0){  //mercancia
		$mercancia = trim($_GET["me"]); 
	}
	
	//verificamos que el viaje corresponda con el usuario
    $rs->Open("SELECT idviaje FROM viajes WHERE activo = 1 AND idviaje = ".$idViaje." AND usr = ".$idUser, $conn);
    $datos = fetch_assoc($rs); 
	$rs->Close();
	
	if (count($datos)>0){ //el viaje si es del usuario y aun esta activo		
		//RECUPERAMOS RUTA DE VIAJE
		$rs->Open("	SELECT 
						ruta
					FROM viajes 
					WHERE idviaje = ".$idViaje, $conn);
		$datosViaje = fetch_assoc($rs);  
		$rs->Close();
		
		//si esque hay bases
		$bases = array();
		if (strlen(trim($datosViaje[0]['ruta'])) > 0 and (int)$datosViaje[0]['ruta'] != 0){
			//sacamos ruta de control
			$rs->Open("	SELECT 
							nombre, 
							descripcion
						FROM RutaControl 
						WHERE 
							id = ".(int)$datosViaje[0]['ruta'], $conn);
			$rutaControl = fetch_assoc($rs);  
			$rs->Close();
						
			//sacamos todas las bases
			$rs->Open("	SELECT     
							geocercas.nombre,
							geocercas.descripcion,
							geocercastipo.tipo,
							geocercastipo.icono,
							geocercas.longitud,
							geocercas.latitud
						FROM geocercas 
						INNER JOIN geocercastipo
						ON (geocercastipo.id = geocercas.tipo)
						WHERE geocercas.id IN(
							SELECT idGeocerca
							FROM RutaGeocercas
							WHERE RutaGeocercas.idRuta = ".(int)$datos[0]['ruta']."
							GROUP BY idGeocerca
							HAVING (COUNT(RutaGeocercas.idGeocerca) > 0)
						)", $conn);
			$bases = fetch_assoc($rs);  
			$rs->Close();
		}//end if
			
		//sacamos el punto más actual
		$rs->Open("	SELECT 
						GPs.descripcion,
						datosghe.id,
						datosghe.fecharecv,							
						CONVERT(CHAR(19),datosghe.fechasend,120) AS fechasend,
						datosghe.lat,
						datosghe.lon,
						datosghe.tambiental,
						datosghe.alarma,
						datosghe.velocidad,
						datosghe.bateria,
						datosghe.aperturas,
						datosghe.rumbo
					FROM datosghe 
					INNER JOIN GPs
					ON (GPs.serie = datosghe.gps)
					WHERE 
						datosghe.gps = ".$xt." AND
						datosghe.id = (
							SELECT MAX(datosghe.id) 
							FROM datosghe 
							WHERE 
								datosghe.gps = ".$xt."
						)", $conn);
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		$alarmaApertura = "0";
		if (isset($datos[0]["id"])){
			$maxId = $datos[0]["id"];
			
			//SACAMOS APERTURAS DEL PENULTIMO PUNTO MAS ACTUAL
			$rs->Open("		SELECT TOP 1
								datosghe.aperturas
							FROM datosghe 
							INNER JOIN GPs
							ON (GPs.serie = datosghe.gps)
							WHERE 
								datosghe.gps = ".$xt." AND
								datosghe.id < ".$maxId." ORDER BY datosghe.id DESC", $conn);
			$datosPen = fetch_assoc($rs); 
			$rs->Close();	
			
			$aperturasNew = $datos[0]["aperturas"];
			$aperturasOld = $datosPen[0]["aperturas"];
			
			if ($aperturasNew != $aperturasOld){
				$alarmaApertura = "1";
			}
		}
		
		//si la velocidad es 0 sacamos el tiempo total que lleva parado			
		$tiempoParado = 0;
		if ($datos[0]["velocidad"] == "0"){
			$rs->Open("	SELECT  
							CONVERT(CHAR(19),fechasend,120) AS fechasend
						FROM datosghe 						
						WHERE id = (
							SELECT 
								MAX(id)
							FROM datosghe
							WHERE velocidad > 3 AND gps = ".$xt." 
						)", $conn);
			$datosTiempoParado = fetch_assoc($rs); 
			$rs->Close();
			
			//calculo los minutos transcurridos en velocidad 0				
			$lastFechaMoving = date_parse(trim($datosTiempoParado[0]['fechasend']));
			$thisFechaStop = date_parse(trim($datos[0]['fechasend']));
			
			$tiempoParado = 				
					mktime(
						$thisFechaStop['hour'],
						$thisFechaStop['minute'],
						$thisFechaStop['second'],
						$thisFechaStop['month'],
						$thisFechaStop['day'],
						$thisFechaStop['year']
					) -				
					mktime(
						$lastFechaMoving['hour'],
						$lastFechaMoving['minute'],
						$lastFechaMoving['second'],
						$lastFechaMoving['month'],
						$lastFechaMoving['day'],
						$lastFechaMoving['year']
					);		
			
			if ((float)$tiempoParado<0){
				$tiempoParado = 0;
			}else{
				$tiempoParado = segMinHrs($tiempoParado);
			}
		}
		
		//modificamos el formato de las coordenadas
		$lat = $datos[0]["lat"];	  
		switch(substr($lat,strlen($lat)-1,1)){
			case 'S': (double)$lat*=-1; break;
			case 'N': (double)$lat*=1; break;
		}
		
		$lon = $datos[0]["lon"];  
		switch(substr($lon,strlen($lon)-1,1)){
			case 'W': (double)$lon*=-1; break;
			case 'E': (double)$lon*=1; break;
		}
		
		//modificamos formato del rumbo
		switch($datos[0]["rumbo"]){
			case 0: $elrumbo="Norte"; break;
			case 1: $elrumbo="Noreste"; break;
			case 2: $elrumbo="Este"; break;
			case 3: $elrumbo="Sureste"; break;
			case 4: $elrumbo="Sur"; break;
			case 5: $elrumbo="Suroeste"; break;
			case 6: $elrumbo="Oeste"; break;
			case 7: $elrumbo="Noroeste"; break;
		}  
	   
		//procesamos icono
		if ($datos[0]["alarma"] == 1 or $alarmaApertura == "1"){
			//fibra abierta
			$icono = 'http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png';
			$msj = 'Abierta'; //fibra abierta
		}else{
			//fibra cerrada
			$msj = 'No abierta'; //fibra abierta
			
			if ($datos[0]["velocidad"] > $vm){ //mostramos alarma de velocidad 
				$icono = 'http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png';
			}else{
				if ($datos[0]["velocidad"] == 0){ //si esta parado 
					$OffOn="Off"; 
				}else{ 
					$OffOn="On"; 
				}
						
				$icono = "http://www.montecristodm.com/viajes/t3/Imagenes/".$elrumbo.$OffOn.".png";
			}
		}
		
		echo '<?xml version="1.0" encoding="iso-8859-1"?>
				<kml xmlns="http://earth.google.com/kml/2.0">
					<Folder>	
						<Folder>	
							<name>Punto</name>					
							<Placemark>
								<description>
									<![CDATA[ 								
										<div><b>Fecha: </b>'.$datos[0]["fecharecv"].'</div>
										<div><b>Fecha UTC: </b>'.$datos[0]["fechasend"].'</div>
										<div><b>Fibra: </b>'.$msj.'</div>
										<div><b>Aperturas: </b>'.$datos[0]["aperturas"].'</div>
										<div><b>Origen: </b>'.$origen.'</div>
										<div><b>Destino: </b>'.$destino.'</div>
										<div><b>Direcci&oacute;n: </b>'.$elrumbo.'</div>
										<div><b>Mercancia: </b>'.$mercancia.'</div>
										<div><b>Velocidad: </b>'.$datos[0]["velocidad"].' Km/h - '.round(($datos[0]["velocidad"] / 1.60935), 2).' Mi/h</div>
										<div><b>Temperatura: </b>'.$datos[0]["tambiental"].' &deg;C - '.round((($datos[0]["tambiental"] * 1.8) + 32), 2).' &deg;F</div>									
										<div><b>Bater&iacute;a: </b>'.$datos[0]["bateria"].'</div>
										'.(($tiempoParado!=0)?('<div><b>Tiempo parado: </b>'.$tiempoParado.'</div>'):'').'
									]]>
								</description>
								<name>'.$datos[0]["descripcion"].'</name>
								<Style>
									<IconStyle>
										<scale>0.5</scale>
										<Icon>
											<href>'.$icono.'</href>
										</Icon>
									</IconStyle>
									<LabelStyle>
										<color>ff0055ff</color>
										<scale>0.7</scale>
									</LabelStyle>
								</Style>
								<Point>
									<coordinates>
										'.$lon.','.$lat.',0
									</coordinates>
								</Point>
							</Placemark>
						</Folder>';
		
		//si hay bases, las imprimimos		
		if (count($bases) > 0){
			echo ' 
					<Folder>
						<name>Bases</name>';
			
			for ($a = 0; $a < count($bases); $a++){
				echo '
							<Placemark>
								<description>
									<![CDATA[ 								
										<div><b>Base: </b>'.$bases[$a]["nombre"].'</div>
										<div><b>Descripci&oacute;n de base: </b>'.$bases[$a]["descripcion"].'</div>
										<div><b>Tipo: </b>'.$bases[$a]["tipo"].'</div>
										<div><b>Ruta de Control: </b>'.$rutaControl[0]['nombre'].'</div>
										<div><b>Descripci&oacute;n de ruta de control: </b>'.$rutaControl[0]['descripcion'].'</div>
									]]>
								</description>
								<name>'.$bases[$a]["nombre"].'</name>
								<Style>
									<IconStyle>
										<scale>0.5</scale>
										<Icon>
											<href>'.$bases[$a]["icono"].'</href>
										</Icon>
									</IconStyle>
									<LabelStyle>
										<color>ff0055ff</color>
										<scale>0.7</scale>
									</LabelStyle>
								</Style>
								<Point>
									<coordinates>
										'.$bases[$a]["longitud"].','.$bases[$a]["latitud"].',0
									</coordinates>
								</Point>
							</Placemark>';
			}//end for						
					
			echo '		</Folder>';
		}//end if
				
		echo '		</Folder>
				</kml>';
	}else{
		//el viaje esta cerrado o el user no corresponde al viaje
		echo '<?xml version="1.0" encoding="iso-8859-1"?>
				<kml xmlns="http://earth.google.com/kml/2.0">
					<Folder>						
						<Placemark>							
							<name>Viaje Cerrado</name>
							<Style>
								<IconStyle>
									<scale>0.5</scale>
									<Icon>
										<href>http://www.montecristodm.com/viajes/t3/Imagenes/IconStop.png</href>
									</Icon>
								</IconStyle>
								<LabelStyle>
									<color>ff0055ff</color>
									<scale>0.7</scale>
								</LabelStyle>
							</Style>
							<Point>
								<coordinates>
									-99.146118,19.466592,0
								</coordinates>
							</Point>
						</Placemark>
					</Folder>
				</kml>';
	}//end if (count($datos)>0)
} //end if (isset($_GET["xt"]))
ob_end_flush();
?>