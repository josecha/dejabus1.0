<?php 
ob_start();

if (isset($_GET["d"]) and strlen($_GET["d"])>0){
	ini_set("memory_limit","32M");
	set_time_limit(240);
	
	include("conexion.php");
	include("funciones.php");
	$rs = New COM("ADODB.Recordset");
	
	$u = new User();
	if ($u->isLogued){
		//recivimos datos	
		$serieGps = (int)myDencr(trim($_GET["d"]));
		
		//obtenemos velocidad				
		$vm = 100;
		if (isset($_GET["vm"]) and strlen(trim($_GET["vm"]))>0){
			$vm = (int)trim($_GET["vm"]);
		}
		
		//obtenemos tiempo muerto				
		$tm = 60;
		if (isset($_GET["tm"]) and strlen(trim($_GET["tm"]))>0){
			$tm = (int)trim($_GET["tm"]);
		}
		
		//sacamos fecha inicial de contrato
		$rs->Open("	SELECT 
					CONVERT(CHAR(19),contratos.finicio,120) AS fechaI 
					FROM (contratos 
					INNER JOIN gpscontratos
					ON (gpscontratos.contrato = contratos.id))
						INNER JOIN GPs
						ON (gpscontratos.gps = GPs.id)
						WHERE 
							contratos.usr = ".$u->id_user." AND 
							contratos.activo = 1 AND
							GPs.serie = ".$serieGps, $conn);				
		$datos = fetch_assoc($rs); 
		$rs->Close();
		
		//obtenemos fecha inicial
		$fechaInicial = date('Y-m-d\TH:i:s');	
		
		//si este usuario si pertenece a este contrato con gps			
		if (count($datos)>0){
			//fecha inicial del contrato
			$fechaI = date_parse(trim($datos[0]['fechaI']));
			$fechaI = mktime(
						$fechaI['hour'],
						$fechaI['minute'],
						$fechaI['second'],
						$fechaI['month'],
						$fechaI['day'],
						$fechaI['year']
					);	
				
			if (isset($_GET["fi"]) and strlen($_GET["fi"])>0){
				//fecha inicial de selecci�n			
				$fechaISeleccion = mktime(
							((isset($_GET["hi"]) and strlen($_GET["hi"])>0)?$_GET["hi"]:00),
							((isset($_GET["mi"]) and strlen($_GET["mi"])>0)?$_GET["mi"]:00),
							00,
							substr($_GET["fi"],3,2),
							substr($_GET["fi"],0,2),
							substr($_GET["fi"],6,4)
						);		
				
				//comparo fechas para valdiar que no se pase de la fecha de inicio de contrato
				if ($fechaISeleccion >= $fechaI){
					$fechaInicial = $fechaISeleccion; //fecha dentro del contrato, todo ok
				}else{
					$fechaInicial = $fechaI; //fecha fuera del contrato, ponemos por default, la fecha del inicio del contrato
				}
			}else{
				$fechaInicial = $fechaI; //no hay con que comparar, asi que ponemos fecha del inicio de contrato
			}
			
			$fechaInicial = date('Y-m-d\TH:i:s', $fechaInicial);	//le damos formato dal datestamp
		}
		
		//obtenemos fecha final
		if (isset($_GET["ff"]) and strlen($_GET["ff"])>0){
			$fechaFinal = trim(substr($_GET["ff"],6,4).'-'.substr($_GET["ff"],3,2).'-'.substr($_GET["ff"],0,2).'T'.((isset($_GET["hf"]) and strlen($_GET["hf"])>0)?$_GET["hf"]:00).':'.((isset($_GET["mf"]) and strlen($_GET["mf"])>0)?$_GET["mf"]:00).':00');			
		}else{
			$fechaFinal = date('Y-m-d\TH:i:s');	
		}
				
		//sacamos todos los puntos
		$rs->Open("	SELECT 				
						GPs.descripcion,
						datosghe.gps,						
						CONVERT(CHAR(19),dateadd(hour,DATEDIFF(Hour,getUTCDate(),getDate()),datosghe.fechasend),120) AS fechasend,						
						CONVERT(CHAR(19),datosghe.fecharecv,120) AS fecharecv,	
						datosghe.lat,
						datosghe.lon,
						datosghe.alarma,
						datosghe.tambiental,
						datosghe.velocidad,
						datosghe.valido,
						datosghe.bateria,
						datosghe.aperturas,
						datosghe.rumbo						
					FROM datosghe 
					INNER JOIN GPs
					ON (GPs.serie = datosghe.gps)
					WHERE 
						datosghe.gps = $serieGps AND
						datosghe.fecharecv >= '$fechaInicial' AND 
						datosghe.fecharecv <= '$fechaFinal' AND
						datosghe.lat <> 0 AND
						datosghe.lon <> 0 AND 
						datosghe.valido = 1
					ORDER BY datosghe.fechasend ASC", $conn);
		$datos = fetch_assoc($rs); 
		$rs->Close();
			
		//CREAMOS INICIO DE KML
		$kml = '<?xml version="1.0" encoding="ISO-8859-1"?>
					<kml xmlns="http://earth.google.com/kml/2.1">
						<Folder>
							<name>'.$datos[0]['descripcion'].'</name>
							<Folder>
								<name>Puntos</name>'; 	
		
		function imgIcono($tipo){
			$img = '';
			switch ($tipo){
				case 'Ok': $img = 'http://www.montecristodm.com/viajes/t3/Imagenes/point.png'; break;
				case 'Fast': $img = 'http://www.montecristodm.com/viajes/t3/Imagenes/exclamation.png'; break;
				case 'Stop': $img = 'http://www.montecristodm.com/viajes/t3/Imagenes/tm.png'; break;
				case 'Alarma': $img = 'http://www.montecristodm.com/viajes/t3/Imagenes/alerta.png'; break;
			}
			
			return $img;
		}
		
		$coord = ''; //contendra todas las coordenadas para dibujar la linea		
		for ($a=0; $a<count($datos); $a++){
			$tmMsj = '';	
			
			//modificamos el formato de las coordenadas
			$lat = $datos[$a]["lat"];	  
			switch(substr($lat,strlen($lat)-1,1)){
				case 'S': (double)$lat*=-1; break;
				case 'N': (double)$lat*=1; break;
			}
			
			$lon = $datos[$a]["lon"];  
			switch(substr($lon,strlen($lon)-1,1)){
				case 'W': (double)$lon*=-1; break;
				case 'E': (double)$lon*=1; break;
			}
			
			//modificamos formato del rumbo
			switch($datos[$a]["rumbo"]){
				case 0: $elrumbo="Norte"; break;
				case 1: $elrumbo="Noreste"; break;
				case 2: $elrumbo="Este"; break;
				case 3: $elrumbo="Sureste"; break;
				case 4: $elrumbo="Sur"; break;
				case 5: $elrumbo="Suroeste"; break;
				case 6: $elrumbo="Oeste"; break;
				case 7: $elrumbo="Noroeste"; break;
			}  
					
			//checamos si hubo cambio de aperturas
			$cambioAperturas = false;
			if ($a > 0){
				if ($datos[$a]["aperturas"] != $datos[$a-1]["aperturas"]){
					$cambioAperturas = true;
				}
			}		
						
			//procesamos icono
			if ($datos[$a]["alarma"] == 1 or $cambioAperturas == true){
				//fibra abierta
				$icono = imgIcono('Alarma');
				$msj = 'Abierta'; //fibra abierta
			}else{
				//fibra cerrada
				$msj = 'No abierta'; //fibra abierta
				
				if ($datos[$a]["velocidad"] != 0){ 
					if ((int)$datos[$a]["velocidad"] > $vm){ //mostramos alarma de velocidad 
						$icono = imgIcono('Fast');
					}else{							
						$icono = imgIcono('Ok');
					}
				}else{
					//estuvo parado un tiempo... veremos si fue mas del limite
					if ($a < count($datos)-1){  //si no esta en el ultimo registro			      	
						if ($datos[$a+1]['velocidad']!=0){ //si solo era un cero unico (sin serie de ceros)
							$icono = imgIcono('Ok');
					   	}else{
							//serie de ceros: recorremos hasta encontrar el fin de la serie e imprimimos marcador
							$b = $a;
							
							//sacamos fecha del primer cero	
							$fechaCero11 = date_parse($datos[$b]['fechasend']);
							$fechaCero11 = date('m/d/Y h:i:s A', mktime(
																	$fechaCero11['hour'],
																	$fechaCero11['minute'],
																	$fechaCero11['second'],
																	$fechaCero11['month'],
																	$fechaCero11['day'],
																	$fechaCero11['year']
																));
																
							//convertimos fecha del primer cero a segundos para verificar si estuvo mas de 10 minutos
							$fechaCero1 = date_parse($datos[$b]['fechasend']);
							$fechaCero1 = mktime(
											$fechaCero1['hour'],
											$fechaCero1['minute'],
											$fechaCero1['second'],
											$fechaCero1['month'],
											$fechaCero1['day'],
											$fechaCero1['year']
										);
							
							$band_while = 0;		
							while (($datos[$b]['velocidad'] == 0) and ($band_while == 0)){  //posible error de desbordamiento
								++$b;
								if ($b == count($datos)){ //desbordado, entonces decrementamos y  encendemos bandera							
							  		--$b;
							  		$band_while = 1;
								} 
							} //$b contendra el id del siguiente registro con velocidad diferente de cero
							
							//sacamos fecha del siguiente registro despues del ultimo cero
							$fechaCero22 = date_parse($datos[$b]['fechasend']);
							$fechaCero22 = date('m/d/Y h:i:s A', mktime(
																	$fechaCero22['hour'],
																	$fechaCero22['minute'],
																	$fechaCero22['second'],
																	$fechaCero22['month'],
																	$fechaCero22['day'],
																	$fechaCero22['year']
																));
																
							//convertimos fecha del siguiente registro despues del ultimo cero, a segundos para verificar si estuvo mas de 10 minutos
							$fechaCero2 = date_parse($datos[$b]['fechasend']);
							$fechaCero2 = mktime(
											$fechaCero2['hour'],
											$fechaCero2['minute'],
											$fechaCero2['second'],
											$fechaCero2['month'],
											$fechaCero2['day'],
											$fechaCero2['year']
										);
							
							if (($fechaCero2 - $fechaCero1) >= ($tm * 60)){ //ponemos icono de stop 
								$icono = imgIcono('Stop');
								$tmMsj = '	<br/>
												<div><b>Duraci&oacute;n de inactividad: </b>'.segMinHrs($fechaCero2 - $fechaCero1).'</div>
												<div><b>Desde: </b>'.$fechaCero11.'</div>
												<div><b>Hasta: </b>'.$fechaCero22.'</div>
											<br/>';								
							}else{ //ponemos icono normal 
								$icono = imgIcono('Ok');
							}
							
							//movemos el puntero ($a) a la posicion del ultimo cero, para que a la siguiente vuelta se incremente solo
							$a = $b - 1;
						}
			      	}else{
						$icono = imgIcono('Ok');
					}
				}	
			}
			
			//fecha para el TimeStamp
			$fechaTS = date_parse($datos[$a]["fechasend"]);
			$fechaTS = date('Y-m-d\TH:i:s', mktime(
												$fechaTS['hour'],
												$fechaTS['minute'],
												$fechaTS['second'],
												$fechaTS['month'],
												$fechaTS['day'],
												$fechaTS['year']
											));	
				   		   			
			$kml .= '			<Placemark>
									<description>
										<![CDATA[ 								
											<div><b>Modulo: </b>'.$datos[$a]["descripcion"].'</div>
											<div><b>Fecha: </b>'.$datos[$a]["fecharecv"].'</div>
											<div><b>Fecha UTC: </b>'.$datos[$a]["fechasend"].'</div>
											<div><b>Fibra: </b>'.$msj.'</div>'.trim($tmMsj).'
											<div><b>Aperturas: </b>'.$datos[$a]["aperturas"].'</div>
											<div><b>Direcci&oacute;n: </b>'.$elrumbo.'</div>
											<div><b>Velocidad: </b>'.$datos[$a]["velocidad"].' Km/h - '.round(($datos[$a]["velocidad"] / 1.60935), 2).' Mi/h</div>
											<div><b>Temperatura: </b>'.$datos[$a]["tambiental"].' &deg;C - '.round((($datos[$a]["tambiental"] * 1.8) + 32), 2).' &deg;F</div>									
											<div><b>Bater&iacute;a: </b>'.$datos[$a]["bateria"].'</div>
										]]>
									</description>								
									<Style>
										<IconStyle>
											<scale>0.5</scale>
											<Icon>
												<href>'.$icono.'</href>
											</Icon>
										</IconStyle>								
									</Style>
									<TimeStamp id="'.$a.'">
										<when>'.$fechaTS.'Z</when>
									</TimeStamp>
									<Point>
										<coordinates>'.$lon.','.$lat.',0</coordinates>
									</Point>
								</Placemark>';
			$coord .= $lon.','.$lat.',0 ';
		}//end for
		
		//CREAMOS FINAL DE KML
		$kml .= '			
							</Folder>
							<Folder>
								<name>Linea</name>
								<Placemark>
									<Style>
										<LineStyle>
											<color>E0552AFF</color>
											<width>2</width>
										</LineStyle>
									</Style>
									<LineString>
										<tessellate>1</tessellate>
										<coordinates>'.$coord."\n".'</coordinates>
									</LineString>
								</Placemark>	
							</Folder>						
						</Folder>
					</kml>'; 

		header("Pragma: public"); 
		header("Expires: 0");       
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		header("Content-Type: application/vnd.google-earth.kml+xml kml; charset=ISO-8859-1");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".strlen($kml));
		header("Pragma: no-cache");
		header("Content-disposition: attachment; filename=Ruta_Actual_Viaje_".$serieGps.".kml"); 
		
		echo trim($kml);
	}//end if ($u->isLogued)
} //end if (isset($_GET["d"]) and strlen($_GET["d"])>0)
ob_end_flush();
?>