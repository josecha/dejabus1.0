<?
ob_start();
include("conexion.php");
$rs = New COM("ADODB.Recordset");
$rc = New COM("ADODB.Command");

include("funciones.php");
include("head.php");
$u = new User();
?> 
	<title>Montecristo Data Mining - Control de Transmigrantes - Sesiones</title>
   	<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
	<script type="text/javascript" language="javascript" charset="iso-8859-1">
		var browser = "<?=browser(); ?>";
		var thisUrlAll = encodeURIComponent('<?=thisUrlAll();?>');
		<? if ($u->isLogued) { ?>
			var usersSelect = '<?=$u->usersSelect(); ?>';
			var gpsList = '<?=$u->gpsList(); ?>';
			var tipo_user = '<?=$u->tipo_user; ?>';
			var gpsSelect = '<?=$u->gpsSelect(); ?>';		
			var gpsSelectInUse = '<?=$u->gpsSelectInUse(); ?>';					
			var gpsSelectClosed = '<?=$u->gpsSelectClosed(); ?>';	
			var rutasSelect = '<?=$u->rutaSelectForFindForm(); ?>';
			var rutaSelectForViaje = '<?=$u->rutaSelectForViajeForm(); ?>';		
			var rutaSelectForFindViaje = '<?=$u->rutaSelectForFindViaje(); ?>';		
			var rutaSelectForFindHistorial = '<?=$u->rutaSelectForFindHistorial(); ?>';	
			var gpsSelectForReporte = '<?=$u->selectGpsForReporte(); ?>';	
		<? } ?>
	</script>
	<? if ($u->tipo_user != 2){ ?>
		<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/class_Login.js"></script>
	<? } ?>
  </head>
  <body class="bodyAdmin">
	<div id="fondoAdminAll">
      <div id="fondoAdmin">
    	<div id="headAdmin">
        	<div id="headAdminLeft"></div>
            <div id="headAdmincenter"></div>
            <div id="headAdminright">
				<? if ($u->isLogued) { ?><div id="opt_buscar" onClick="showFind();"></div>
				<div id="opt_agregar" onClick="showAdd();"></div><? } ?>
			</div>
            <div class="corte"></div>
        </div>
        <div id="bodyAdmin">
        	<div id="bodyAdminLeft">
				<div class="MenuHover"><div id="M_inicio" onClick="location.href = 'index.php';"></div></div>
                <div class="MenuSelected"><div id="M_sesion" onClick="location.href = 'login.php';"></div></div>
				<? if ($u->isLogued) { echo $u->menu_user(); } ?>
                <div id="M_line_bottom"></div>
            </div>
            <div id="bodyAdminRight">
            	<div id="bodyAdminRightContentHead"></div>
                <div id="bodyAdminRightContentBody">
					<div id="desplegable" style="display:none;"></div>
                	<div id="msjError"></div>
                    <div id="msjWarning"></div>
                    <div id="msjInfo"></div>
					<a name="top"></a>
					<? if ($u->tipo_user != 2){ ?>
						<div id="divLogin">
					</div>  
					<? }else{ ?>                  
						<form name="FormViajesLibres">
							<div class="divForm">
								<div class="Fseleccion">
									<label class="Flbl">Modulo</label><br/>
									<select class="Ftxt" name="gps" id="gps">
										<?=$u->gpsSelectForUser(); ?>
									</select>
								</div>										
								<div class="Fseleccion">
									<label class="Flbl">Fecha Inicial</label><label class="Flbl" style="margin-left:175px;">Hora</label><label class="Flbl" style="margin-left:85px;">Intervalo Recorrido</label><br/>
									<input type="text" name="date1" id="date1" class="date-pick" maxlength="10" disabled="disabled"/>
									<select id="selectHI" name="selectHI" class="Ftxt">
										 <option value="00">00</option>
										 <option value="01">01</option>
										 <option value="02">02</option>
										 <option value="03">03</option>
										 <option value="04">04</option>
										 <option value="05" selected="selected">05</option>
										 <option value="06">06</option>
										 <option value="07">07</option>
										 <option value="08">08</option>
										 <option value="09">09</option>
										 <option value="10">10</option>
										 <option value="11">11</option>
										 <option value="12">12</option>	 
										 <option value="13">13</option>
										 <option value="14">14</option>
										 <option value="15">15</option>
										 <option value="16">16</option>
										 <option value="17">17</option>
										 <option value="18">18</option>
										 <option value="19">19</option>
										 <option value="20">20</option>
										 <option value="21">21</option>
										 <option value="22">22</option>
										 <option value="23">23</option>
									</select>
									<select id="selectMI" name="selectMI" class="Ftxt">
										 <option value="00" selected="selected">00</option>
										 <option value="01">01</option>
										 <option value="02">02</option>
										 <option value="03">03</option>
										 <option value="04">04</option>
										 <option value="05">05</option>
										 <option value="06">06</option>
										 <option value="07">07</option>
										 <option value="08">08</option>
										 <option value="09">09</option>
										 <option value="10">10</option>
										 <option value="11">11</option>
										 <option value="12">12</option>	 
										 <option value="13">13</option>
										 <option value="14">14</option>
										 <option value="15">15</option>
										 <option value="16">16</option>
										 <option value="17">17</option>
										 <option value="18">18</option>
										 <option value="19">19</option>
										 <option value="20">20</option>
										 <option value="21">21</option>
										 <option value="22">22</option>
										 <option value="23">23</option>
										 <option value="24">24</option>
										 <option value="25">25</option>
										 <option value="26">26</option>
										 <option value="27">27</option>
										 <option value="28">28</option>
										 <option value="29">29</option>
										 <option value="30">30</option>
										 <option value="31">31</option>
										 <option value="32">32</option>
										 <option value="33">33</option>
										 <option value="34">34</option>
										 <option value="35">35</option>
										 <option value="36">36</option>
										 <option value="37">37</option>
										 <option value="38">38</option>
										 <option value="39">39</option>
										 <option value="40">40</option>
										 <option value="41">41</option>
										 <option value="42">42</option>
										 <option value="43">43</option>
										 <option value="44">44</option>
										 <option value="45">45</option>
										 <option value="46">46</option>
										 <option value="47">47</option>
										 <option value="48">48</option>
										 <option value="49">49</option>
										 <option value="50">50</option>
										 <option value="51">51</option>
										 <option value="52">52</option>
										 <option value="53">53</option>
										 <option value="54">54</option>
										 <option value="55">55</option>
										 <option value="56">56</option>
										 <option value="57">57</option>
										 <option value="58">58</option>
										 <option value="59">59</option>
									</select>
									

<select id="selectInterval" name="selectInterval" class="Ftxt">
<option value="15">15 Mins</option>
<option value="30">30 Mins</option>
<option value="60">1 Hr</option>
<option value="120">2 Hrs</option>

</select>
									<div class="corte"></div>
								</div>
								<div class="Fseleccion">		
									<label class="Flbl">Fecha Final</label><label class="Flbl" style="margin-left:175px;">Hora</label><br/>
									<input type="text" name="date2" id="date2" class="date-pick" maxlength="10" disabled="disabled"/>
									<select id="selectHF" name="selectHF" class="Ftxt">
										 <option value="00">00</option>
										 <option value="01">01</option>
										 <option value="02">02</option>
										 <option value="03">03</option>
										 <option value="04">04</option>
										 <option value="05">05</option>
										 <option value="06">06</option>
										 <option value="07">07</option>
										 <option value="08">08</option>
										 <option value="09">09</option>
										 <option value="10">10</option>
										 <option value="11">11</option>
										 <option value="12">12</option>	 
										 <option value="13">13</option>
										 <option value="14">14</option>
										 <option value="15">15</option>
										 <option value="16">16</option>
										 <option value="17">17</option>
										 <option value="18">18</option>
										 <option value="19">19</option>
										 <option value="20">20</option>
										 <option value="21" selected="selected">21</option>
										 <option value="22">22</option>
										 <option value="23">23</option>
									</select>
									<select id="selectMF" name="selectMF" class="Ftxt">
										 <option value="00">00</option>
										 <option value="01">01</option>
										 <option value="02">02</option>
										 <option value="03">03</option>
										 <option value="04">04</option>
										 <option value="05">05</option>
										 <option value="06">06</option>
										 <option value="07">07</option>
										 <option value="08">08</option>
										 <option value="09">09</option>
										 <option value="10">10</option>
										 <option value="11">11</option>
										 <option value="12">12</option>	 
										 <option value="13">13</option>
										 <option value="14">14</option>
										 <option value="15">15</option>
										 <option value="16">16</option>
										 <option value="17">17</option>
										 <option value="18">18</option>
										 <option value="19">19</option>
										 <option value="20">20</option>
										 <option value="21">21</option>
										 <option value="22">22</option>
										 <option value="23">23</option>
										 <option value="24">24</option>
										 <option value="25">25</option>
										 <option value="26">26</option>
										 <option value="27">27</option>
										 <option value="28">28</option>
										 <option value="29">29</option>
										 <option value="30">30</option>
										 <option value="31">31</option>
										 <option value="32">32</option>
										 <option value="33">33</option>
										 <option value="34">34</option>
										 <option value="35">35</option>
										 <option value="36">36</option>
										 <option value="37">37</option>
										 <option value="38">38</option>
										 <option value="39">39</option>
										 <option value="40">40</option>
										 <option value="41">41</option>
										 <option value="42">42</option>
										 <option value="43">43</option>
										 <option value="44">44</option>
										 <option value="45">45</option>
										 <option value="46">46</option>
										 <option value="47">47</option>
										 <option value="48">48</option>
										 <option value="49">49</option>
										 <option value="50">50</option>
										 <option value="51">51</option>
										 <option value="52">52</option>
										 <option value="53">53</option>
										 <option value="54">54</option>
										 <option value="55">55</option>
										 <option value="56">56</option>
										 <option value="57">57</option>
										 <option value="58">58</option>
										 <option value="59" selected="selected">59</option>
									</select>			
									<div class="corte"></div>
								</div>								
								<div class="Fseleccion">
									<label class="Flbl">Velocidad M&aacute;xima de Alarma (km/h)</label><br/>
									<input class="Ftxt" type="text" name="vm" id="vm" value="100" maxlength="3" style="width:60px;" />	
									<label class="Flbl">  Km/H</label>	
								</div>
								<div class="Fseleccion">
									<label class="Flbl">Tiempo M&aacute;ximo Permitido de Inmovilidad</label><br/>
									<input class="Ftxt" style="width:60px;" type="text" value="60" name="tm" id="tm" maxlength="4" />	
									<label class="Flbl">  Minutos</label>	
								</div>
								<div class="Fseleccion">
									<div class="divButton2">									
										<input type="button" class="botonLoginUser" style="width:105px;" onClick="verPuntos('pointInGM');" value="Punto GMaps" />
										<input type="button" class="botonLoginUser" style="width:105px;" onClick="verPuntos('rutaInGM');" value="Ruta GMaps" />
										<input type="button" class="botonLoginUser" style="width:105px;" onClick="verPuntos('pointInGE');" value="Punto GEarth" />
										<input type="button" class="botonLoginUser" style="width:105px;" onClick="verPuntos('rutaInGE');" value="Ruta GEarth" />
										<input type="button" class="botonLoginUser" style="width:110px; margin-top:8px;" onClick="verPuntos('*');" value="Puntos GMaps" />
										<input type="button" class="botonLoginUser" style="width:110px; margin-top:8px;" onClick="verPuntos('*GE');" value="Puntos GEarth" /><br/>
										<input type="button" class="botonLoginUser" style="width:170px;" onClick="verGrafica('temperatura');" value="Gr&aacute;fica de Temperatura" />
										<input type="button" class="botonLoginUser" style="width:170px;" onClick="verGrafica('velocidad');" value="Gr&aacute;fica de Velocidad" />
										<input type="button" class="botonLoginUser" style="width:170px;" onClick="verGrafica('inactividad');" value="Reporte de Inactividad" />
										<input type="button" class="botonLoginUser" style="width:170px;" onClick="verGrafica('inactividad');" value="Reporte Recorrido" />
										<?php
										if($u->id_user==123)
										{?>
										<br/>
										<input type="button" class="botonLoginUser" style="width:110px;" onClick="comandosOnOff('off');" value="Apagar" />
										<input type="button" class="botonLoginUser" style="width:110px;" onClick="comandosOnOff('on');" value="Encender" />
										
										<?php } ?>

									</div>
								</div>
							</div>
						</form>
						<script type="text/javascript">
							<!--
								$('.date-pick').datePicker().val(new Date().asString()).trigger('change');
								$('.date-pick').datePicker({startDate:'01/01/2000'});
							-->
						</script>
					<? } ?>    
                </div>
                <div id="bodyAdminRightContentFeet"></div>
            </div>
            <div class="corte"></div>
        </div>
        <div id="feetAdmin">
        	<div id="feetAdminLeft"></div>
            <div id="feetAdmincenter">
            	<div id="creditos"></div>
            </div>
            <div id="feetAdminright"></div>
            <div class="corte"></div>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
ob_end_flush();
?>
