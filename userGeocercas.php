<?
ob_start();
include("conexion.php");
$rs = New COM("ADODB.Recordset");
$rc = New COM("ADODB.Command");

include("funciones.php");
include("head.php");
$u = new User();
if ($u->isLogued){
?> 
	<title>Montecristo Data Mining - Tracking Tampering Technology - Geocercas</title>
   	<link href="<?=browserStyle(); ?>" type="text/css" rel="stylesheet" />
	<script type="text/javascript" language="javascript" charset="iso-8859-1">
		var browser = "<?=browser(); ?>";
		var tipo_user = '<?=$u->tipo_user; ?>';
		var thisUrlAll = encodeURIComponent('<?=thisUrlAll();?>');
		var gpsSelect = '<?=$u->gpsSelect(); ?>';		
		var gpsSelectInUse = '<?=$u->gpsSelectInUse(); ?>';		
		var gpsSelectClosed = '<?=$u->gpsSelectClosed(); ?>';		
		var listGeocercas = '<?=$u->printIconList(); ?>';
		var rutasSelect = '<?=$u->rutaSelectForFindForm(); ?>';
		var rutaSelectForViaje = '<?=$u->rutaSelectForViajeForm(); ?>';		
		var rutaSelectForFindViaje = '<?=$u->rutaSelectForFindViaje(); ?>';		
		var rutaSelectForFindHistorial = '<?=$u->rutaSelectForFindHistorial(); ?>';	
		var gpsSelectForReporte = '<?=$u->selectGpsForReporte(); ?>';		
		var gpsLibresAreasControl = '<?=$u->gpsLibresAreasControl(); ?>';	
		var gpsAreasControl = '<?=$u->gpsAreasControl(); ?>';
		var geoareaSelect = '<?=$u->geoareaSelect(); ?>';
	</script>
	<script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAAS8qcIPtV0x0MIRmvjcEpwxQyZyC6eC3MnCpFWkAtXr1SV4-GRBTWQBPqeRYxIRE7Di0Eumx97i0eSQ" type="text/javascript"></script>
	<script type="text/javascript" language="javascript" charset="iso-8859-1" src="Js/geocercas.js"></script>
  </head>
  <body class="bodyAdmin" onLoad="load()" onUnload="GUnload()">
	<div id="fondoAdminAll">
      <div id="fondoAdmin">
    	<div id="headAdmin">
        	<div id="headAdminLeft"></div>
            <div id="headAdmincenter"></div>
            <div id="headAdminright">
				<div id="opt_buscar" onClick="showFind();"></div>
				<div id="opt_agregar" onClick="showAdd();"></div>
			</div>
            <div class="corte"></div>
        </div>
        <div id="bodyAdmin">
        	<div id="bodyAdminLeft">
				<div class="MenuHover"><div id="M_inicio" onClick="location.href = 'index.php';"></div></div>
                <div class="MenuHover"><div id="M_sesion" onClick="location.href = 'login.php';"></div></div>
				<?=str_replace('MenuHover"><div id="M_geocercas', 'MenuSelected"><div id="M_geocercas', $u->menu_user()); ?>
                <div id="M_line_bottom"></div>
            </div>
            <div id="bodyAdminRight">
            	<div id="bodyAdminRightContentHead"></div>
                <div id="bodyAdminRightContentBody">
					<div id="desplegable" style="display:none;"></div>
                	<div id="msjError"></div>
                    <div id="msjWarning"></div>
                    <div id="msjInfo"></div>
					<a name="top"></a>
					<div id="mapa" style="width:745px; height:180px;" class="mapaViewGeocerca"></div>
					<div id="tabla">
						<? $u->datosTabla('geocerca'); ?>
					</div>
					<div id="desplegableDown" style="display:none;"></div>
                </div>
                <div id="bodyAdminRightContentFeet"></div>
            </div>
            <div class="corte"></div>
        </div>
        <div id="feetAdmin">
        	<div id="feetAdminLeft"></div>
            <div id="feetAdmincenter">
            	<div id="creditos"></div>
            </div>
            <div id="feetAdminright"></div>
            <div class="corte"></div>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
}else{
	header("Location: login.php");
}//endif ($u->isLogued)
ob_end_flush();
?>